
shader_type canvas_item;

uniform float value: hint_range(0, 100); // %
uniform float thickness;
uniform float slope;
uniform bool side;
uniform vec3 fgColor;

void fragment() {
	if (!side) {
	if (
		(UV.y > 0.5 - thickness - UV.x * slope) &&
		(UV.y < 0.5 + thickness) && (value / 100.0 > UV.x)
		) {
		COLOR.rgba = vec4(fgColor, 1.0);
	} else {
		COLOR.a = 0.0;
	}
	} else {
	if (
		(UV.y > 0.5 - thickness) &&
		(UV.y < 0.5 + thickness + UV.x * slope) && (value / 100.0 > UV.x)
		) {
		COLOR.rgba = vec4(fgColor, 1.0);
	} else {
		COLOR.a = 0.0;
	}
	}
}