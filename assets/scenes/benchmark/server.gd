extends Net

onready var tw = $Tween
onready var shit = $World/Moving_Shit

var max_players: int = DEFAULT_MAX_PLAYERS

var connection = {}

class pl:
	var id = -1
	var moving_shit_vec = Vector2(0,0)
	var latency = 0
	var interpolate = false


func toggle_int(id: int, val: bool) -> void:
	if !connection.has(id):
		G.debug_print("Some bullshit happened when " + str(id) + " tried to toggle interpolation!")
		return
	
	connection[id].interpolate = val


func move_shit(id: int, pos: Vector2) -> void:
	if !connection.has(id):
		G.debug_print("Some bullshit happened when " + str(id) + " tried to show how his shit moves!")
		return
	
	if connection[id].interpolate:
		tw.remove(connection[id], "moving_shit_vec")
		tw.interpolate_property(connection[id], "moving_shit_vec", null, pos,
				float(OS.get_ticks_msec() - connection[id].latency) / 1000,
				Tween.TRANS_LINEAR, Tween.EASE_OUT)
		tw.start()
	else:
		connection[id].moving_shit_vec = pos
	
	send_set("/root/Main/Client", "serv_latency", OS.get_ticks_msec() - connection[id].latency, id)
	connection[id].latency = OS.get_ticks_msec()

func _process(delta: float) -> void:
	for i in connection.keys():
		send_call("/root/Main/Client", "server_moving", [shit.position], i)
		send_call("/root/Main/Client", "server_showing", [connection[i].moving_shit_vec], i)


func create_server(s_port=port, s_max_players=max_players) -> void:
	G.debug_print("START SERVER ON " + str(host) + ":" + str(port))
	peer.create_server(s_port, s_max_players)
	get_tree().set_network_peer(peer)
	G.Peer = self
	OS.set_window_title("SERVER")
	
	peer.connect("peer_connected", self, "add_player")
	peer.connect("peer_disconnected", self, "del_player")

func add_player(id: int) -> void:
	if connection.has(id):
		G.debug_print("Some bullshit happened when " + str(id) + " connected(")
		return
	G.debug_print(str(id) + " CONNECTED")
	connection[id] = pl.new()
	connection[id].id = id

func del_player(id: int) -> void:
	if !connection.has(id):
		G.debug_print("Some bullshit happened when " + str(id) + " disconnected(")
		return
	G.debug_print(str(id) + " DISCONNECTED")
	connection.erase(id)

func _ready() -> void:
	G.Server = self
