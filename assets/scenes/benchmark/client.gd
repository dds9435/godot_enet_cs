extends Net

onready var shit_client = $World/Moving_Shit_Client
onready var shit_server = $World/Moving_Shit_Server
onready var shit_server_show = $World/Moving_Shit_Server_SHOWING
onready var shit_server_show_raw = $World/Moving_Shit_Server_SHOWING_RAW


onready var info = $UI/INFO
onready var mult = $UI/MULT
onready var type = $UI/TWEEN_TYPE
onready var eas3 = $UI/TWEEN_EASE

onready var show_loc = $UI/Client_LOC
onready var show_raw = $UI/Server_RAW
onready var show_int = $UI/Server_INT
onready var show_red = $UI/Server_RED

onready var toggle_int = $UI/Interpolate


onready var tw = $Tween

var id = -1
var latency = 0

var mult_val = 1
var type_val = 0
var ease_val = 0

var my_latency = 0
var my_latency_storage = []
var my_average_latency = 0
var serv_latency = 0

func create_client(host: String, port: int) -> void:
	set_process(false)
	G.debug_print("START CLIENT")
	peer.connect("server_disconnected", self, "_on_server_disconnected")
	peer.connect("connection_succeeded", self, "_on_connection_succeeded")
	peer.connect("connection_failed", self, "_on_connection_failed")
	peer.create_client(host, port)
	get_tree().set_network_peer(peer)
	G.Peer = self
	id = get_tree().get_network_unique_id()
	OS.set_window_title("CLIENT:" + str(id))

func _process(delta: float) -> void:
	send_call("/root/Main/Server", "move_shit", [id, shit_client.position], SERVER_ID)
	info.text = "MY LATENCY: " + str(my_latency) + ", MY AV LATENCY: " + str(my_average_latency) + ", SERV LATENCY: " + str(serv_latency)

func server_moving(pos: Vector2) -> void:
	shit_server.position = pos - Vector2(256, 0)

func server_showing(pos: Vector2) -> void:
	
	shit_server_show_raw.position = pos
	
	if my_latency_storage.size() > 0:
		my_average_latency = 0
		for i in my_latency_storage:
			my_average_latency += i
		my_average_latency /= my_latency_storage.size()
	else:
		my_average_latency = 16 # one frame
	
	tw.remove(shit_server_show, "position")
	tw.interpolate_property(shit_server_show, "position",
	null, pos, (my_average_latency / 1000.0) * mult_val,
	type_val, ease_val)
	tw.start()
	
	my_latency = OS.get_ticks_msec() - latency
	my_latency_storage.append(my_latency)
	if my_latency_storage.size() > 10:
		my_latency_storage.pop_front()
	
	latency = OS.get_ticks_msec()

func _on_connection_succeeded() -> void:
	G.debug_print("CONNECTION SUCCESS!")
	set_process(true)

func _on_connection_failed() -> void:
	G.debug_print("CONNECTION FAILED")
	get_tree().quit()

func _on_server_disconnected() -> void:
	G.debug_print("SERVER DISCONNECTED")
	get_tree().quit()

func _mult_val(val: int) -> void:
	mult_val = val

func _tw_idx_pressed(idx: int) -> void:
	type_val = idx
	type.text = type.get_popup().get_item_text(idx)

func _ease_idx_pressed(idx: int) -> void:
	ease_val = idx
	eas3.text = eas3.get_popup().get_item_text(idx)

func _show_loc_toggle(val: bool) -> void:
	shit_client.visible = val

func _show_raw_toggle(val: bool) -> void:
	shit_server_show_raw.visible = val

func _show_int_toggle(val: bool) -> void:
	shit_server_show.visible = val

func _show_red_toggle(val: bool) -> void:
	shit_server.visible = val

func _int_toggle(val: bool) -> void:
	send_call("/root/Main/Server", "toggle_int", [id, val], SERVER_ID)

func _ready() -> void:
	mult.connect("value_changed", self, "_mult_val")
	type.get_popup().connect("index_pressed", self, "_tw_idx_pressed")
	eas3.get_popup().connect("index_pressed", self, "_ease_idx_pressed")
	show_loc.connect("toggled", self, "_show_loc_toggle")
	show_raw.connect("toggled", self, "_show_raw_toggle")
	show_int.connect("toggled", self, "_show_int_toggle")
	show_red.connect("toggled", self, "_show_red_toggle")
	toggle_int.connect("toggled", self, "_int_toggle")
	G.Client = self
