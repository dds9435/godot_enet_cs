#include <gdnative_api_struct.gen.h>
#include <open-simplex-noise.c>
#include <stdlib.h>
#include <stdio.h>

#define TNONE -1
#define TDEFA 0
#define TBACK 1
#define THARD 2
#define TFENC 3
#define TLABO 4
#define TPINT 5
#define TMEGU 6
#define TALTR 7

#define THARD_LABO 0
#define THARD_DEFA (rand() % 2 + 1)
#define TBACK_RARA 3
#define TFENC_DEFA 4
#define TFENC_VERT 5
#define TBACK_RARB 6
#define TBACK_DEFA (rand() % 3 + 7)
#define TDEFA_DEFA (rand() % 3 + 10)
#define TDEFA_RARA 13
#define TDEFA_RARB 14

const godot_gdnative_core_api_struct *api = NULL;
const godot_gdnative_ext_nativescript_api_struct *nativescript_api = NULL;

GDCALLINGCONV void *gen_constructor(godot_object *p_instance, void *p_method_data);
GDCALLINGCONV void gen_destructor(godot_object *p_instance, void *p_method_data, void *p_user_data);
godot_variant outer_generate(godot_object *p_instance, void *p_method_data, void *p_user_data, int p_num_args, godot_variant **p_args);
godot_variant outer_circle(godot_object *p_instance, void *p_method_data, void *p_user_data, int p_num_args, godot_variant **p_args);
godot_variant outer_place(godot_object *p_instance, void *p_method_data, void *p_user_data, int p_num_args, godot_variant **p_args);

void set_cell(godot_object *_tilemap, int x, int y, int id);
int get_cell(godot_object *_tilemap, int x, int y);
void line(godot_object *_tilemap, int x0, int y0, int x1, int y1, int id);
void circle(godot_object *_tilemap, int x, int y, int id, int radius);

int *island_rect(godot_object *_tilemap, int x, int y);
int build_bridge(godot_object *_tilemap, int *r, int y, int side);
void build_pillar(godot_object *_tilemap, int *r, int x);
void dig_labo(godot_object *_tilemap, int x_rng, int y_rng);
bool is_corner(godot_object *_tilemap, int x, int y);

godot_method_bind *meth_call;

int BRG_MIN;
int BRG_MAX;
int LAB_BMI;
int LAB_BMA;
int LAB_TRS;

int LINE_SIZE;

void GDN_EXPORT godot_gdnative_init(godot_gdnative_init_options *p_options) {
	api = p_options->api_struct;

	// now find our extensions
	for (int i = 0; i < api->num_extensions; i++) {
		switch (api->extensions[i]->type) {
			case GDNATIVE_EXT_NATIVESCRIPT: {
				nativescript_api = (godot_gdnative_ext_nativescript_api_struct *)api->extensions[i];
			}; break;
			default: break;
		};
	};	
}

void GDN_EXPORT godot_gdnative_terminate(godot_gdnative_terminate_options *p_options) {
	api = NULL;
	nativescript_api = NULL;
}

void GDN_EXPORT godot_nativescript_init(void *p_handle) {
	godot_instance_create_func create = { NULL, NULL, NULL };
	create.create_func = &gen_constructor;

	godot_instance_destroy_func destroy = { NULL, NULL, NULL };
	destroy.destroy_func = &gen_destructor;

	nativescript_api->godot_nativescript_register_class(p_handle, "generator", "Reference", create, destroy);

	godot_instance_method generate = { NULL, NULL, NULL };
	godot_instance_method circle = { NULL, NULL, NULL };
	godot_instance_method place = { NULL, NULL, NULL };

	generate.method = &outer_generate;
	circle.method = &outer_circle;
	place.method = &outer_place;
	
	godot_method_attributes attributes = { GODOT_METHOD_RPC_MODE_DISABLED };

	nativescript_api->godot_nativescript_register_method(p_handle, "generator", "generate", attributes, generate);
	nativescript_api->godot_nativescript_register_method(p_handle, "generator", "circle", attributes, circle);
	nativescript_api->godot_nativescript_register_method(p_handle, "generator", "place", attributes, place);

	meth_call = api->godot_method_bind_get_method("Object", "call");
}

GDCALLINGCONV void *gen_constructor(godot_object *p_instance, void *p_method_data) {
	return NULL;
}

GDCALLINGCONV void gen_destructor(godot_object *p_instance, void *p_method_data, void *p_user_data) {
}

int rand_tile(int type) {
	return (type * LINE_SIZE) + rand () % LINE_SIZE;
}

godot_variant outer_place(godot_object *p_instance, void *p_method_data, void *p_user_data, int p_num_args, godot_variant **p_args) {
	if (p_num_args == 8) {
		godot_object *tilemap	= api->godot_variant_as_object(p_args[0]);
		LINE_SIZE			= api->godot_variant_as_int(p_args[1]);

		int C_RANGE		= api->godot_variant_as_int(p_args[2]);
		int X_RANGE		= api->godot_variant_as_int(p_args[3]);
		int Y_RANGE		= api->godot_variant_as_int(p_args[4]);

		int C_RANGE_2	= api->godot_variant_as_int(p_args[5]);
		int X_RANGE_2	= api->godot_variant_as_int(p_args[6]);
		int Y_RANGE_2	= api->godot_variant_as_int(p_args[7]);

		int mx_range = 0;
		int my_range = 0;
		int mc_range = 0;

		if (X_RANGE > X_RANGE_2)
			mx_range = X_RANGE;
		else mx_range = X_RANGE_2;

		if (Y_RANGE > Y_RANGE_2)
			my_range = Y_RANGE;
		else my_range = Y_RANGE_2;

		if (C_RANGE > C_RANGE_2)
			mc_range = C_RANGE;
		else mc_range = C_RANGE_2;

		int trand;
		int ccell;
		for (int i = -mx_range / 2 - mc_range; i <= mx_range / 2 + mc_range; i++)
		for (int j = -my_range / 2 - mc_range; j <= my_range/ 2 + mc_range; j++) {
			ccell = get_cell(tilemap, i, j);

			if (ccell == TMEGU || ccell == TPINT) {
				set_cell(tilemap, i, j, TBACK);
				ccell = TBACK;
			}

			if (ccell == TDEFA) {
				trand = rand () % 5;
				if (trand == 0)
					set_cell(tilemap, i, j, rand_tile(TDEFA_RARA));
				else if (trand == 1)
					set_cell(tilemap, i, j, rand_tile(TDEFA_RARB));
				else
					set_cell(tilemap, i, j, rand_tile(TDEFA_DEFA));
			}

			if (ccell == THARD) {
				if ((get_cell(tilemap, i + 1, j) == TLABO ||
					get_cell(tilemap, i, j + 1) == TLABO ||
					get_cell(tilemap, i + 1, j + 1) == TLABO) && rand() % 2 == 0)
					set_cell(tilemap, i, j, rand_tile(THARD_LABO));
				else
					set_cell(tilemap, i, j, rand_tile(THARD_DEFA));
			}

			if (ccell == TFENC) {
				if (get_cell(tilemap, i + 1, j) != TFENC)
					set_cell(tilemap, i, j, rand_tile(TFENC_VERT));
				else
					set_cell(tilemap, i, j, rand_tile(TFENC_DEFA));
			}

			if (ccell == TLABO) {
				trand = rand() % 3;
				if (trand == 0)
					set_cell(tilemap, i, j, rand_tile(TBACK_RARA));
				else if (trand == 1)
					set_cell(tilemap, i, j, rand_tile(TBACK_RARB));
				else
					set_cell(tilemap, i, j, rand_tile(TBACK_DEFA));
			}

			if (ccell == TBACK) {
				trand = rand() % 10;
				if (trand == 0)
					set_cell(tilemap, i, j, rand_tile(TBACK_RARA));
				else if (trand == 1)
					set_cell(tilemap, i, j, rand_tile(TBACK_RARB));
				else
					set_cell(tilemap, i, j, rand_tile(TBACK_DEFA));
			}

		}
	}
	godot_variant ret;
	api->godot_variant_new_nil(&ret);
	return ret;
}

godot_variant outer_generate(godot_object *p_instance, void *p_method_data, void *p_user_data, int p_num_args, godot_variant **p_args) {
	if (p_num_args == 27) {
		godot_object *tilemap = api->godot_variant_as_object(p_args[0]);
		int seed = api->godot_variant_as_int(p_args[1]);
		
		int C_COUNT		= api->godot_variant_as_int(p_args[2]);
		int C_RANGE		= api->godot_variant_as_int(p_args[3]);
		int C_MIN		= api->godot_variant_as_int(p_args[4]);
		int X_RANGE		= api->godot_variant_as_int(p_args[5]);
		int Y_RANGE		= api->godot_variant_as_int(p_args[6]);
		int BOX_CHC		= api->godot_variant_as_int(p_args[7]);

		int C_COUNT_2	= api->godot_variant_as_int(p_args[8]);
		int C_RANGE_2	= api->godot_variant_as_int(p_args[9]);
		int C_MIN_2		= api->godot_variant_as_int(p_args[10]);
		int X_RANGE_2	= api->godot_variant_as_int(p_args[11]);
		int Y_RANGE_2	= api->godot_variant_as_int(p_args[12]);
		int BOX_CHC_2	= api->godot_variant_as_int(p_args[13]);

		float N_BIGM	= api->godot_variant_as_real(p_args[14]);
		float N_BIGT	= api->godot_variant_as_real(p_args[15]);
		float N_SMLM	= api->godot_variant_as_real(p_args[16]);
		float N_SMLT	= api->godot_variant_as_real(p_args[17]);

		int BRG_TRS		= api->godot_variant_as_int(p_args[18]);
		BRG_MIN			= api->godot_variant_as_int(p_args[19]); 
		BRG_MAX			= api->godot_variant_as_int(p_args[20]);

		LAB_BMI			= api->godot_variant_as_int(p_args[21]);
		LAB_BMA			= api->godot_variant_as_int(p_args[22]);
		LAB_TRS			= api->godot_variant_as_int(p_args[23]);

		int MEGU_CHC	= api->godot_variant_as_int(p_args[24]);
		int PINT_CHC	= api->godot_variant_as_int(p_args[25]);

		bool SIMPLE		= api->godot_variant_as_bool(p_args[26]);

		struct osn_context *ctx;
		open_simplex_noise(seed, &ctx);
		srand(seed);

		int rect_range = 0;
		int rect_x = 0;
		int rect_y = 0;

		int mx_range = 0;
		int my_range = 0;
		int mc_range = 0;

		if (X_RANGE > X_RANGE_2)
			mx_range = X_RANGE;
		else mx_range = X_RANGE_2;

		if (Y_RANGE > Y_RANGE_2)
			my_range = Y_RANGE;
		else my_range = Y_RANGE_2;

		if (C_RANGE > C_RANGE_2)
			mc_range = C_RANGE;
		else mc_range = C_RANGE_2;

		for (int i = 0; i < C_COUNT; i++) {
			if (rand()%BOX_CHC != 0)
				circle(
						tilemap, rand() % X_RANGE - X_RANGE / 2,
						rand() % Y_RANGE - Y_RANGE / 2,
						TDEFA, rand() % (C_RANGE - C_MIN) + C_MIN
					);
			else {
				rect_range = rand() % (C_RANGE - C_MIN) + C_MIN;
				rect_x = rand() % X_RANGE - X_RANGE / 2;
				rect_y = rand() % Y_RANGE - Y_RANGE / 2;
				for (int j = -rect_range; j <= rect_range; j++)
				for (int k = -rect_range; k <= rect_range; k++)
					set_cell(tilemap, rect_x + j, rect_y + k, TDEFA); 
			}
		}

		if (C_COUNT_2 > 0) {
		for (int i = 0; i < C_COUNT_2; i++) {
				if (rand()%BOX_CHC_2 != 0)
				circle(
						tilemap, rand() % X_RANGE_2 - X_RANGE_2 / 2,
						rand() % Y_RANGE_2 - Y_RANGE_2 / 2,
						TDEFA, rand() % (C_RANGE_2 - C_MIN_2) + C_MIN_2
					);
			else {
				rect_range = rand() % (C_RANGE_2 - C_MIN_2) + C_MIN_2;
				rect_x = rand() % X_RANGE_2 - X_RANGE_2 / 2;
				rect_y = rand() % Y_RANGE_2 - Y_RANGE_2 / 2;
				for (int j = -rect_range; j <= rect_range; j++)
				for (int k = -rect_range; k <= rect_range; k++)
					set_cell(tilemap, rect_x + j, rect_y + k, TDEFA); 
			}
		}
		}

		for (int i = -mx_range / 2 - mc_range; i <= mx_range / 2 + mc_range; i++)
		for (int j = -my_range / 2 - mc_range; j <= my_range / 2 + mc_range; j++) {
			if (get_cell(tilemap, i, j) == TDEFA) {
				if (open_simplex_noise2(ctx, i * N_BIGM, j * N_BIGM) > N_BIGT)
					set_cell(tilemap, i, j, TNONE);
			}
		}

		if (!SIMPLE)
		for (int i = -mx_range / 2 - mc_range; i <= mx_range / 2 + mc_range; i++)
		for (int j = -my_range / 2 - mc_range; j <= my_range / 2 + mc_range; j++) {
			if (get_cell(tilemap, i, j) == TDEFA) {
				if (open_simplex_noise2(ctx, i * N_SMLM, j * N_SMLM) > N_SMLT)
					set_cell(tilemap, i, j, TBACK);
			}
		}

		if (!SIMPLE)
		for (int q = 0; q < (int) N_SMLM / 2 + 1; q++)
		for (int i = -mx_range / 2 - mc_range; i <= mx_range / 2 + mc_range; i++)
		for (int j = -my_range / 2 - mc_range; j <= my_range / 2 + mc_range; j++) {
			if (get_cell(tilemap, i, j) == TDEFA) {
				int s = 0;
				for (int x = -1; x < 2; x++)
				for (int y = -1; y < 2; y++) {
					if (get_cell(tilemap, i + x, j + y) == TDEFA)
						s++;
				}
				if (s < 3)
					set_cell(tilemap, i, j, TBACK);
			}
		}

		if (!SIMPLE)
		for (int q = 0; q < (int) N_SMLM / 2 + 1; q++)
		for (int i = -mx_range / 2 - mc_range; i <= mx_range / 2 + mc_range; i++)
		for (int j = -my_range / 2 - mc_range; j <= my_range / 2 + mc_range; j++) {
			if (get_cell(tilemap, i, j) == TDEFA) {
				for (int x = -1; x < 2; x++)
				for (int y = -1; y < 2; y++) {
					if (get_cell(tilemap, i + x, j + y) == TBACK ||
						get_cell(tilemap, i + x, j + y) == TNONE) {
						int s = 0;
						for (int xx = -1; xx < 2; xx++)
						for (int yy = -1; yy < 2; yy++) {
							if (get_cell(tilemap, i + x + xx, j + y + yy) == TDEFA)
								s++;
						if (s > 4)
							set_cell(tilemap, i + x, j + y, TDEFA);
						}
					}
				}
			}
		}

		if (!SIMPLE)
		for (int i = 0; i < LAB_TRS; i++)
			dig_labo(tilemap, mx_range + mc_range * 2, my_range + mc_range * 2);
	
		int x = 0;
		int y = 0;
		int side = 0;

		if (!SIMPLE)
		for (int i = 0; i < BRG_TRS; i++) {
			x = rand() % (mx_range + mc_range * 2) - mx_range / 2 - mc_range;
			y = rand() % (my_range + mc_range * 2) - my_range / 2 - mc_range;
			
			if (get_cell(tilemap, x, y) == TDEFA) {
				int *r = island_rect(tilemap, x, y);
				int step = rand() % 3 + 1;
				for (int j = r[0] + 1; j < r[2]; j+=step) {
					build_pillar(tilemap, r, j);
				}
				for (int j = r[1] + 2; j < r[3]; j++) {
					side = rand() % 2;
					if (build_bridge(tilemap, r, j, side) > 0)
						break;
					else if (build_bridge(tilemap, r, j, 1 - side) > 0)
						break;
				}
				free(r);
			}
		}

		if (!SIMPLE)
		for (int i = -mx_range / 2 - mc_range; i <= mx_range / 2 + mc_range; i++)
		for (int j = -my_range / 2 - mc_range; j <= my_range / 2 + mc_range; j++) {
			if (get_cell(tilemap, i, j) == TALTR)
				set_cell(tilemap, i, j, TDEFA);
		}

		if (!SIMPLE)
		for (int i = -mx_range / 2 - mc_range; i <= mx_range / 2 + mc_range; i++)
		for (int j = -my_range / 2 - mc_range; j <= my_range / 2 + mc_range; j++) {
			if (get_cell(tilemap, i, j) == TBACK) {
				int s = 0;
				for (int x = -1; x < 2; x++)
				for (int y = -1; y < 2; y++) {
					if (get_cell(tilemap, i + x, j + y) == TBACK)
						s++;
				}
				if (s < 3)
					set_cell(tilemap, i, j, TNONE);
			}
		}

		if (!SIMPLE)
		for (int i = -mx_range / 2 - mc_range; i <= mx_range / 2 + mc_range; i++)
		for (int j = -my_range / 2 - mc_range; j <= my_range / 2 + mc_range; j++) {
			if (get_cell(tilemap, i, j) == TLABO) {
				if (get_cell(tilemap, i, j - 1) == TDEFA) {
					set_cell(tilemap, i, j, THARD);
				}
				else if (get_cell(tilemap, i, j + 1) == TDEFA) {
					set_cell(tilemap, i, j, THARD);
				}
			}
		}

		if (!SIMPLE)
		for (int i = -mx_range / 2 - mc_range; i <= mx_range / 2 + mc_range; i++)
		for (int j = -my_range / 2 - mc_range; j <= my_range / 2 + mc_range; j++) {
			if (get_cell(tilemap, i, j) == TLABO) {
				if (get_cell(tilemap, i, j - 1) == THARD) {
					if (rand() % 8 == 0) {
						set_cell(tilemap, i, j - 1, TLABO);
						for (int k = 1; k < rand() % LAB_BMA + 1; k++) {
							if (get_cell(tilemap, i, j - 1 - k) == TDEFA)
								set_cell(tilemap, i, j - 1 - k, TLABO);
							if (get_cell(tilemap, i, j - 1 - k) == THARD)
								break;
						}
					}
				}
				else if (get_cell(tilemap, i, j + 1) == THARD) {
					if (rand() % 8 == 0) {
						set_cell(tilemap, i, j + 1, TLABO);
						for (int k = 1; k < rand() % LAB_BMA + 1; k++) {
							if (get_cell(tilemap, i, j + 1 + k) == TDEFA)
								set_cell(tilemap, i, j + 1 + k, TLABO);
							if (get_cell(tilemap, i, j + 1 + k) == THARD)
								break;
						}
					}
				}
			}
		}

		int h;
		int end;
		int ccell;
		bool rare = false;

		if (!SIMPLE)
		for (int i = -mx_range / 2 - mc_range; i <= mx_range / 2 + mc_range; i++)
		for (int j = -my_range / 2 - mc_range; j <= my_range / 2 + mc_range; j++) {
			if (rand() % PINT_CHC == 0)
			if (is_corner(tilemap, i, j)) {
				if (rand() % MEGU_CHC == 0)
					set_cell(tilemap, i, j, TMEGU);
				else {
					end = 0;
					ccell = get_cell(tilemap, i, j + 1);

					if (ccell == TDEFA || ccell == THARD) {
						rare = false;
						while (true) {
							if (!(is_corner(tilemap, i + end, j)))
								break;
							ccell = get_cell(tilemap, i + end, j + 1);
							if (!(ccell == TDEFA || ccell == THARD))
								break;
							for (h = 1; h < 4; h++) {
								ccell = get_cell(tilemap, i + end, j - h);
								if (ccell == TDEFA || ccell == THARD || ccell == TPINT)
									break;
							}
							if (h < 4)
								break;
							ccell = get_cell(tilemap, i + end, j + 2);
							if (!(ccell == TDEFA || ccell == THARD) ||
								get_cell(tilemap, i + end, j) == TLABO )
								rare = true;
							end++;
							if (end > 3)
								break;
						}
						if (end > 2 && ((rare && rand() % 32 == 0) || !rare)) {
							line(tilemap, i + 1, j, i + end - 1, j, TPINT);
						}
					} else {
						ccell = get_cell(tilemap, i + 1, j);
						if (ccell == TDEFA || ccell == THARD) {
							rare = false;
							while (true) {
								if (!(is_corner(tilemap, i, j + end)))
									break;
								ccell = get_cell(tilemap, i + 1, j + end);
								if (!(ccell == TDEFA || ccell == THARD))
									break;
								for (h = 1; h < 4; h++) {
									ccell = get_cell(tilemap, i - h, j + end);
									if (ccell == TDEFA || ccell == THARD || ccell == TPINT)
										break;
								}
								if (h < 4)
									break;
								ccell = get_cell(tilemap, i + 2, j + end);
								if (!(ccell == TDEFA || ccell == THARD) ||
									get_cell(tilemap, i, j + end) == TLABO)
									rare = true;
								end++;
								if (end > 3)
									break;
							}
							if (end > 2 && ((rare && rand() % 32 == 0) || !rare)) {
								for (int q = j + 1; q < j + end - 1; q++) {
									set_cell(tilemap, i, q, TPINT);
									}
								}
						} else {
							ccell = get_cell(tilemap, i - 1, j);
							if (ccell == TDEFA || ccell == THARD) {
								rare = false;
								while (true) {
									if (!(is_corner(tilemap, i, j + end)))
										break;
									ccell = get_cell(tilemap, i - 1, j + end);
									if (!(ccell == TDEFA || ccell == THARD))
										break;
									for (h = 1; h < 4; h++) {
										ccell = get_cell(tilemap, i + h, j + end);
										if (ccell == TDEFA || ccell == THARD || ccell == TPINT)
											break;
									}
									if (h < 4)
										break;
									ccell = get_cell(tilemap, i - 2, j + end);
									if (!(ccell == TDEFA || ccell == THARD) ||
										get_cell(tilemap, i, j + end) == TLABO)
										rare = true;
									end++;
									if (end > 3)
										break;
								}
								if (end > 2 && ((rare && rand() % 32 == 0) || !rare)) {
									for (int q = j + 1; q < j + end - 1; q++) {
										set_cell(tilemap, i, q, TPINT);
										}
									}
								}
							}
						}
					}
				}
			}
		}
	godot_variant ret;
	api->godot_variant_new_nil(&ret);
	return ret;
}

bool is_corner(godot_object *_tilemap, int x, int y) {
	if (get_cell(_tilemap, x, y) == TNONE ||
		get_cell(_tilemap, x, y) == TBACK ||
		get_cell(_tilemap, x, y) == TLABO) {
		int l = get_cell(_tilemap, x - 1, y);
		int r = get_cell(_tilemap, x + 1, y);
		int u = get_cell(_tilemap, x, y - 1);
		int d = get_cell(_tilemap, x, y + 1);

		if (l == TPINT || r == TPINT ||
			u == TPINT || d == TPINT)
			return false;

		if (l == TDEFA || l == THARD ||
			r == TDEFA || r == THARD ||
			u == TDEFA || u == THARD ||
			d == TDEFA || d == THARD)
			return true;
	}
	return false;
}

void dig_labo(godot_object *_tilemap, int x_rng, int y_rng) {
	int bsize = rand() % (LAB_BMA - LAB_BMI) + LAB_BMI;
	int dir = rand() % 2;

	int x = 0;
	int y = 0;

	switch(dir) {
		case 0:
			x = - x_rng / 2;
			y = rand() % y_rng - y_rng / 2;
			break;
		case 1:
			x = x_rng / 2;
			y = rand() % y_rng - y_rng / 2;
			break;
	}

	while (true) {
		for (int j = x - bsize; j <= x; j++)
		for (int k = y - bsize; k <= y; k++) {
			if (get_cell(_tilemap, j, k) == TDEFA)
				set_cell(_tilemap, j, k, TLABO);
			if (get_cell(_tilemap, j, k) == THARD)
				return;
		}
		switch(dir) {
			case 0:
				if (x > x_rng / 2)
					return;
				x++;
				break;
			case 1:
				if (x < - x_rng / 2)
					return;
				x--;
				break;
		}
	}
}

void build_pillar(godot_object *_tilemap, int *r, int x) {
	if (get_cell(_tilemap, x - 1, r[1]) == TNONE &&
		get_cell(_tilemap, x + 1, r[1]) == TNONE) {
		int start = 0;
		int end = 0;
		for (int i = 0; i < BRG_MAX; i++) {
			if (get_cell(_tilemap, x, r[1] + i) == TDEFA ||
				get_cell(_tilemap, x, r[1] + i) == TALTR) {
				start = i;
				break;
			}
		}
		if (start > 0) {
			for (int i = 1; i < BRG_MAX; i++) {
				if (get_cell(_tilemap, x, r[1] + start - i) == TDEFA ||
					get_cell(_tilemap, x, r[1] + start - i) == TALTR) {
					end = i;
					break;
				}
			}
			if (end > 0) {
				for (int i = r[1] + start; i > r[1] + start - end; i--)
					if (get_cell(_tilemap, x, i) == TNONE ||
						get_cell(_tilemap, x, i) == TBACK)
						set_cell(_tilemap, x, i, TFENC);
			}
		}
	}
}

int bridge_start(godot_object *_tilemap, int x, int y, int side) {
	int loc_x;
	for (int i = 0; i < BRG_MAX; i++) {
		loc_x = x + i * (1 - side * 2);
		if (get_cell(_tilemap, loc_x, y) == TALTR ||
			get_cell(_tilemap, loc_x, y) == TDEFA) {
			return i;
		}
	}
	return 0;
}

int bridge_end(godot_object *_tilemap, int x, int y, int side, int start) {
	int loc_x;
	bool cpad = true;
	bool pad = false;
	for (int i = 1; i < BRG_MAX; i++) {
		loc_x = x + (start - i) * (1 - side * 2);
		for (int j = 1; j < 16; j++) {
			cpad = get_cell(_tilemap, loc_x, y - j) == TNONE &&
					get_cell(_tilemap, loc_x, y + j) == TNONE;
			if (!cpad) break;
		}
		if (cpad)
			pad = true;
		if (get_cell(_tilemap, loc_x, y) == TDEFA ||
			get_cell(_tilemap, loc_x, y) == TALTR) {
			if (!pad)
				return 0;
			return i;
		}
	}
	return 0;
}

int build_bridge(godot_object *_tilemap, int *r, int y, int side) {
	int x = 0;
	int start = bridge_start(_tilemap, x, y, side);
	int end = bridge_end(_tilemap, x, y, side, start);
	if (end > BRG_MIN && start > 0) {
		int start_s = 0;
		int end_s = 0;
		int step = -2;
		if (end > BRG_MAX / 2) {
			start_s = bridge_start(_tilemap, x, y + step, side);
			end_s = bridge_end(_tilemap, x, y + step, side, start_s);
			if (end_s > BRG_MIN && start_s > 0) {
				line(_tilemap, x + start_s * (1 - side * 2), y + step,
					x + (start_s - end_s) * (1 - side * 2), y + step, THARD);
			}
			else {
				step = 2;
				start_s = bridge_start(_tilemap, x, y + step, side);
				end_s = bridge_end(_tilemap, x, y + step, side, start_s);
				if (end_s > BRG_MIN && start_s > 0) {
					line(_tilemap, x + start_s * (1 - side * 2), y + step,
						x + (start_s - end_s) * (1 - side * 2), y + step, THARD);
				} else {
					start_s = 0;
					end_s = 0;
				}
			}
		}
		line(_tilemap, x + start * (1 - side * 2), y,
			x + (start - end) * (1 - side * 2), y, THARD);
		if (start_s > 0 && end_s > 0) {
			if (start_s < start)
				start_s = start;
			if (end > end_s)
				end_s = end;
			int ftype = rand () % 2;
			for (int i = x + start_s * (1 - side * 2);
				i < x + (start_s - end_s) * (1 - side * 2); i += 1 + ftype)
				if (get_cell(_tilemap, i, y + step / 2) != TDEFA &&
					get_cell(_tilemap, i, y + step / 2) != TALTR)
					set_cell(_tilemap, i, y + step / 2, TFENC);
		}
		return end;
	} else
		return 0;
}

int *island_rect(godot_object *_tilemap, int x, int y) {
	
	if (get_cell(_tilemap, x, y) == TDEFA) {
		set_cell(_tilemap, x, y, TALTR);
		int *p = malloc(sizeof(int) * 4);
		p[0] = x;
		p[1] = y;
		p[2] = x;
		p[3] = y;
		int *pl = island_rect(_tilemap, x - 1, y);
		int *pr = island_rect(_tilemap, x + 1, y);
		int *pu = island_rect(_tilemap, x, y - 1);
		int *pd = island_rect(_tilemap, x, y + 1);
		
		if (pl != NULL) {
			if (pl[0] < p[0])
				p[0] = pl[0];
			if (pl[1] < p[1])
				p[1] = pl[1];
			if (pl[2] > p[2])
				p[2] = pl[2];
			if (pl[3] > p[3])
				p[3] = pl[3];
		}
		if (pr != NULL) {
			if (pr[0] < p[0])
				p[0] = pr[0];
			if (pr[1] < p[1])
				p[1] = pr[1];
			if (pr[2] > p[2])
				p[2] = pr[2];
			if (pr[3] > p[3])
				p[3] = pr[3];
	}
		if (pu != NULL) {
			if (pu[0] < p[0])
				p[0] = pu[0];
			if (pu[1] < p[1])
				p[1] = pu[1];
			if (pu[2] > p[2])
				p[2] = pu[2];
			if (pu[3] > p[3])
				p[3] = pu[3];
	}
		if (pd != NULL) {
			if (pd[0] < p[0])
				p[0] = pd[0];
			if (pd[1] < p[1])
				p[1] = pd[1];
			if (pd[2] > p[2])
				p[2] = pd[2];
			if (pd[3] > p[3])
				p[3] = pd[3];
	}
		return p;
	} else
		return NULL;
}

godot_variant outer_circle(godot_object *p_instance, void *p_method_data, void *p_user_data, int p_num_args, godot_variant **p_args) {
	
	if (p_num_args == 5) {
		int x		= api->godot_variant_as_int(p_args[1]);
		int y		= api->godot_variant_as_int(p_args[2]);
		int id		= api->godot_variant_as_int(p_args[3]);
		int radius	= api->godot_variant_as_int(p_args[4]);
		godot_object *_tilemap = api->godot_variant_as_object(p_args[0]);
	
		int N = 2 * radius +1; 
		int _x,_y;
	
		for (int i = 0; i < N; i++)
		for (int j = 0; j < N; j++) { 
			_x = i-radius; 
			_y = j-radius; 
			if ( _x * _x + _y * _y <= radius * radius + 1 )
				set_cell(_tilemap, _x + x, _y + y, id);
	    }

	}
	godot_variant ret;
	api->godot_variant_new_nil(&ret);
	return ret;
}

void circle(godot_object *_tilemap, int x, int y, int id, int radius) {
	int N = 2 * radius +1; 
	int _x,_y;

	for (int i = 0; i < N; i++)
	for (int j = 0; j < N; j++) { 
		_x = i-radius; 
		_y = j-radius; 
		if (_x * _x + _y * _y <= radius * radius + 1 ) 
			set_cell(_tilemap, _x + x, _y + y, id);
    } 
}

void line(godot_object *_tilemap, int x0, int y0, int x1, int y1, int id) {
	int deltax = abs(x1 - x0);
	int deltay = abs(y1 - y0);
	int error = 0;
	int deltaerr = (deltay + 1);
	int y = y0;
	int diry = y1 - y0;
	if (diry > 0) 
		diry = 1;
	if (diry < 0)
		diry = -1;
	for (int x = x0; x < x1; x++) {
		set_cell(_tilemap, x, y, id);
		error += deltaerr;
		if (error >= (deltax + 1)) {
			y += diry;
			error -= deltax + 1;
		}
	}

}

void set_cell(godot_object *_tilemap, int x, int y, int id) {
	godot_variant _x;
	godot_variant _y;
	godot_variant _id;
	godot_variant _vname;
	godot_string  _sname;
	
	api->godot_string_new(&_sname);
	api->godot_string_parse_utf8(&_sname, "set_cell");
	api->godot_variant_new_string(&_vname, &_sname);
	api->godot_variant_new_int(&_x, x);
	api->godot_variant_new_int(&_y, y);
	api->godot_variant_new_int(&_id, id);
	const godot_variant *args[4] = {&_vname, &_x, &_y, &_id};
	api->godot_method_bind_call(meth_call, _tilemap, args, 4, NULL);
	api->godot_variant_destroy(&_x);
	api->godot_variant_destroy(&_y);
	api->godot_variant_destroy(&_id);
	api->godot_variant_destroy(&_vname);
	api->godot_string_destroy(&_sname);
}

int get_cell(godot_object *_tilemap, int x, int y) {
	godot_variant _x;
	godot_variant _y;
	godot_variant _vname;
	godot_string  _sname;
	
	api->godot_string_new(&_sname);
	api->godot_string_parse_utf8(&_sname, "get_cell");
	api->godot_variant_new_string(&_vname, &_sname);
	api->godot_variant_new_int(&_x, x);
	api->godot_variant_new_int(&_y, y);
	const godot_variant *args[3] = {&_vname, &_x, &_y};
	godot_variant _ret = api->godot_method_bind_call(meth_call, _tilemap, args, 3, NULL);
	int ret = api->godot_variant_as_int(&_ret);
	api->godot_variant_destroy(&_x);
	api->godot_variant_destroy(&_y);
	api->godot_variant_destroy(&_vname);
	api->godot_string_destroy(&_sname);
	return ret;
}
