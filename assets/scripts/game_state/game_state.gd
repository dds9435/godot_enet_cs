extends Node
class_name GameState
# Состояние игры (клиентская часть)


signal changed_timer(val)
signal changed_state(val)
signal changed_stage(val)
signal changed_score_max(val)
signal changed_score(val, type_score)
signal exit_win(val)

signal feed_ev(feed_id, feed_data)

# feed
enum {
	FEED_RESP, FEED_KILL,
	FEED_HIT, FEED_CAP,
	FEED_PKILL, FEED_WIN
}

# stages
enum {
	STATE_NONE, STATE_WAIT,
	STATE_GAME, STATE_FREP
}

# stats
enum {
	STAT_KILL, STAT_DEATH,
	STAT_CAP, STAT_PKILL,
	STAT_KARMA
}

const DEFAULT_KARMA		= 3

# score
enum {
	SCORE_1, SCORE_2,
	SCORE_1_BONUS,
	SCORE_2_BONUS
}

# modules
enum {
	OPERATION_BUY,
	OPERATION_CREDIT,
	OPERATION_SELL
}

var timer: float		= 0 setget set_timer
var state: int			= 0 setget set_state
var stage: int			= 0 setget set_stage
var score_max			= 0 setget set_score_max

var score: PoolIntArray = [0, 0, 0, 0] # only read, for write use set_score

var players_count: int = 0
var last_timer: float = 0
var last_state: int = -1
var last_stage: int = -1
var last_score_team: int = -1 # last score team incremented
var last_score_max: int = -1

var win_team = null setget set_win_team


func _init():
	pass


func _ready():
	G.GameState = self


func set_win_team(val):
	win_team = val
	G.debug_print("Exitting after win of " + str(val))
	# TODO GOTO AFTERMATCH
	G.Main.go_to_menu()


func set_score(val, type_score = null):
	if type_score != null:
		score[type_score] = val
	else:
		score = val
	emit_signal("changed_score", val, type_score)


func set_timer(val):
	timer = val
	if round(timer) != last_timer:
		emit_signal("changed_timer", timer)
		last_timer = round(timer)


func set_state(val):
	state = val
	if state != last_state:
		emit_signal("changed_state", val)
		last_state = val


func set_stage(val):
	stage = val
	if stage != last_stage:
		emit_signal("changed_stage", val)
		last_stage = val


func set_score_max(val):
	score_max = val
	if score_max != last_score_max:
		emit_signal("changed_score_max", val)
		last_score_max = val


func feed_event(feed_id: int, feed_data=[]) -> void:
	emit_signal("feed_ev", feed_id, feed_data)
