extends GameState
# Состояние игры (серверная часть)

signal approved_module_op(player, m_name, operation_type, data)


func approve_module_ev(player_id, m_name, operation_type) -> void:
	if state == STATE_NONE:
		G.debug_print("[MODULE] player " + str(player_id) +
				" tried to buy module " + m_name +
				" before game start!")
		return
	var player = G.World.players.get_node_or_null(str(player_id))
	if player == null:
		G.debug_print(["[MODULE] Can't find player with id " + str(player_id)])
		return
	var price = G.ModulesServer.get_module_price(m_name, player, operation_type)
	if price == -1:
		G.debug_print(["[MODULE] " + player.name + " requested noexistent module " + m_name])
		return
	var module = player.get_node_or_null("Modules/" + m_name)
	var level = -1
	var temp_level = 0
	if module != null:
		level = module.level
		temp_level = module.temp_level
	
	if price == 0:
		G.debug_print("[MODULE] Module " + m_name + " price 0!")
		return
	
	
	match operation_type:
		OPERATION_BUY:
			if player.money >= price or G.Server.rules.TEST_MODE:
				player.money = clamp(player.money - price, 0, Rules.MAX_MONEY * clamp(stage, 1, INF))
				emit_signal("approved_module_op", player, m_name, operation_type, [level + 1, temp_level])
			else:
				G.debug_print("[MODULE] " + player.name + " didn't have enough money for " + m_name + "!")
		OPERATION_CREDIT:
			if player.credit >= price or G.Server.rules.TEST_MODE:
				player.credit = clamp(player.credit - price, 0, Rules.MAX_MONEY * clamp(stage, 1, INF))
				emit_signal("approved_module_op", player, m_name, operation_type, [level + 1, temp_level + 1])
			else:
				G.debug_print("[MODULE] " + player.name + " didn't have enough credits for " + m_name + "!")
		OPERATION_SELL:
			player.money = clamp(player.money + price, 0, Rules.MAX_MONEY * clamp(stage, 1, INF))
			emit_signal("approved_module_op", player, m_name, operation_type, [])


# если на клиенте не должно обрабатываться, ничего кроме сообщения об ивенте, но иеет смысл разделить файл на серверную и клиентскую часть
# необходимо подключить все ивенты к соответствуующим событиям
func feed_event(feed_id: int, feed_data=[]) -> void:
	var message = ""
	var process_score = true


	match feed_id:
		FEED_RESP: # feed_data = [player_id]
			if feed_data.size() != 1: return
			var player_id: int = feed_data[0]
			
			process_score = false


		FEED_KILL: # feed_data = [player_id, killed_id]
			if feed_data.size() != 2: return
			var player_id: int = feed_data[0]
			var killed_id: int = feed_data[1]

			var reward = Rules.REWARD_KILL * clamp(stage, 1, INF)

			var player = G.World.players.get_node_or_null(str(player_id))
			var killed = G.World.players.get_node_or_null(str(killed_id))
			if player and killed: # null === false
				var team = killed.team
				# player reward and stats
				if team == player.team:
					set_score(score[1 - (team - 1)] + (reward / 2), 1 - (team - 1))
					recalc_score()
					last_score_team = 1 - (team - 1)
				else:
					set_score(score[team - 1] + reward, team - 1)
					recalc_score()
					last_score_team = team - 1
					# player reward
					killed.money += reward
					killed.money = clamp(killed.money, 0, Rules.MAX_MONEY * clamp(stage, 1, INF))
					killed.stats[STAT_KILL] += 1
					killed.stats[STAT_KARMA] += reward * Rules.KARMA_POINT
				process_score = false
				player.stats[STAT_DEATH] += 1
				player.stats[STAT_KARMA] -= 1
				player.credit = clamp(score[(player.team - 1) + 2], 0, INF)
				
				
				if !(null in [killed.caption, player.caption]):
					message = "%s killed %s!"
					message = message % [killed.caption, player.caption]


		FEED_HIT: # feed_data = [player_id, hitted_id, dmg, part]
			if feed_data.size() != 4: return
			var player_id: int = feed_data[0]
			var hitted_id: int = feed_data[1]
			var dmg: float = feed_data[2]
			var part: int = feed_data[3]

			var reward = int(dmg * Rules.REWARD_DMG_MULT)

			var player = G.World.players.get_node_or_null(str(player_id))
			var hitted = G.World.players.get_node_or_null(str(hitted_id))
			if player and hitted:
				var team = hitted.team
				# team reward
				if team == player.team:
					set_score(score[1 - (team - 1)] + (reward / 2), 1 - (team - 1))
					last_score_team = 1 - (team - 1)
				else:
					set_score(score[team - 1] + reward, team - 1)
					last_score_team = team - 1
					# player reward
					hitted.money += reward
					hitted.money = clamp(hitted.money, 0, Rules.MAX_MONEY * clamp(stage, 1, INF))



		FEED_CAP: # feed_data = [point_id, capturers, team]
			if feed_data.size() != 3: return
			var point_id: int = feed_data[0]
			var capturers: PoolIntArray = feed_data[1]
			var team: int = feed_data[2]

			var reward = Rules.REWARD_CAP

			# team reward
			set_score(score[team - 1] + reward, team - 1)
			last_score_team = team - 1
			for player_id in capturers:
				var player = G.World.players.get_node_or_null(str(player_id))
				if player:
					# player reward and stats
					player.money += reward
					player.money = clamp(player.money, 0, Rules.MAX_MONEY * clamp(stage, 1, INF))
					player.stats[STAT_CAP] += 1
					player.stats[STAT_KARMA] += reward * Rules.KARMA_POINT
					
					message += player.name + ", " # <<< 
			
			message = message.left(message.length() - 2)
			message += " captured point %s"
			message = message % str(point_id)


		FEED_PKILL: # feed_data = [point_id, player_id]
			if feed_data.size() != 2: return
			var point_id: int = feed_data[0]
			var player_id: int = feed_data[1]
			
			var reward = Rules.REWARD_PKILL
			
			var player = G.World.players.get_node_or_null(str(player_id))
			if player:
				var team = player.team
				# team reward
				set_score(score[team - 1] + reward, team - 1)
				last_score_team = team - 1
				# player reward and stats
				player.money += reward
				player.money = clamp(player.money, 0, Rules.MAX_MONEY * clamp(stage, 1, INF))
				player.stats[STAT_PKILL] += 1
				player.stats[STAT_KARMA] += reward * Rules.KARMA_POINT
				
				message = "%s destroyed point %s of team %s"
				message = message % [player.caption, str(point_id), str(team)]


		FEED_WIN: # feed_data = [team]
			if feed_data.size() != 1: return
			var team: int = feed_data[0]
			process_score = false
			
			if team > -1:
				message = "Team %s wins!"
				message = message % str(team + 1)
			else:
				message = "Nobody wins :c"
			
			
			emit_signal("exit_win", team)


	if message != "":
		G.debug_print("[FEED] " + message)

	if process_score: recalc_score()

	emit_signal("feed_ev", feed_id, feed_data)


func recalc_score():
	var sum = score[SCORE_1] + score[SCORE_2]
	
	# balance score
	if sum > score_max and last_score_team > -1:
		set_score(
				self.score[1 - last_score_team] - (sum - score_max), 
				1 - last_score_team
		)
		last_score_team = -1

	for team in range(2):
		# clamp from 0 to max
		set_score(clamp(score[team], 0, score_max), team)
		# count bonus
		var score_diff = score[1 - team] - score[team]
		set_score(clamp(score_diff, 0, INF), team + 2)
