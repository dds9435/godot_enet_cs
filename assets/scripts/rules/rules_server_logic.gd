extends Rules
# Реакция на события по правилам (только серверная часть)

var GS

func _init() -> void:
	GS = G.GameState
	GS.connect("changed_timer", self, "_on_gs_changed_timer")
	set_physics_process(false)


func _on_gs_changed_timer(timer: float) -> void:
	if GS.state == GS.STATE_GAME:
		var stage = count_stage(int(timer / STAGE_DURATION))
		if stage != GS.last_stage:
			GS.stage = stage
			GS.score_max = Rules.MAX_SCORE * clamp(stage, 1, INF)
			GS.recalc_score()


func _physics_process(delta: float) -> void:
	if GS.timer > 0:
		GS.timer -= delta
		
	elif GS.state > GS.STATE_NONE:
		match GS.state:
			GS.STATE_WAIT:
				GS.state = GS.STATE_GAME
				GS.timer = MATCH_TIME
			GS.STATE_GAME:
				GS.state = GS.STATE_FREP
				GS.timer = FREP_TIME
			GS.STATE_FREP:
				var winning_team = -1
				if GS.score[GS.SCORE_1] > GS.score[GS.SCORE_2]:
					winning_team = G.TEAM_1
				elif GS.score[GS.SCORE_2] > GS.score[GS.SCORE_1]:
					winning_team = G.TEAM_2
				GS.feed_event(GS.FEED_WIN, [winning_team])
				set_physics_process(false)


func add_player(id: int) -> void:
	GS.players_count += 1
	if (GS.players_count == MIN_PLAYERS and !TEST_MODE) or (TEST_MODE and GS.players_count == 1):
		GS.state = GS.STATE_WAIT
		if TEST_MODE:
			GS.timer = 1.0
		else:
			GS.timer = WAIT_TIME
		set_physics_process(true)


func del_player(id: int) -> void:
	GS.players_count -= 1
	if TEST_MODE:
		set_physics_process(false)
		return
	if GS.players_count < MIN_PLAYERS:
		if GS.state > GS.STATE_WAIT:
			GS.feed_event(GS.FEED_WIN, [-1])
		GS.state = GS.STATE_NONE
		GS.timer = 0.0
		set_physics_process(false)


# util funcs
func count_stage(timer_stage: int) -> int:
	var temp_stage = STAGE_COUNT - timer_stage
	var p_weight_s = clamp(GS.players_count / STAGE_P_WEIGHT + 1, 0, STAGE_COUNT)
	
	if temp_stage > p_weight_s:
		return temp_stage
	else:
		return p_weight_s
