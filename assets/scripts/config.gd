extends Node
class_name Config

const CFG_PATH = "user://dno.cfg"

var CFG_GFX_POSTPROCESSING : bool	= true    # world enviroment post processing shaders
var CFG_GFX_LIGHTMAP_QUALITY : int	= 2       # lightmap quality preset
var CFG_GFX_LIGHTMAP_NOSHADE : bool = false   # lightmap disable shading
var CFG_GFX_SMOKE : bool			= true    # smoke
var CFG_GFX_FULLSCREEN : bool		= false   # fullscreen
var CFG_GFX_VSYNC : bool			= true    # vsync
var CFG_GFX_VSYNC_WINDOWS : bool	= false   # windows vsync
var CFG_GFX_VIDEO_DRIVER : String	= "GLES3" # video driver (need restart)

const boolVariants = ["true", "false"]
const videoDrivers = ["GLES2", "GLES3"]
onready var cfgs = cfg_list()


func save_cfg() -> void:
	var f = ConfigFile.new()
	f.load(CFG_PATH)
	for i in cfgs.keys():
		f.set_value("GFX", i, get(i))
	f.save(CFG_PATH)


func load_cfg() -> void:
	var f = ConfigFile.new()
	var err = f.load(CFG_PATH)
	if err == OK:
		for i in f.get_section_keys("GFX"):
			tweak_setting(i, f.get_value("GFX", i))
		if G.Main != null:
			G.Main.update_config()
	else:
		save_cfg()


func _ready() -> void:
	G.Config = self
	if G.Console != null:
		G.Console.register_command(
			"config", {
				desc   = "Change config",
				args   = [2, "<key:string> <val:string>"],
				target = self,
				completer_target = self,
				completer_method = "a_config"
			}
		)
	load_cfg()

func cfg_list() -> Dictionary:
	var props = get_property_list()
	var cooked = {}
	for i in props:
		if i["name"].begins_with("CFG"):
			cooked[i["name"]] = i["type"]
	return cooked

var _last_cmd_idx = 0


func a_config(idx: int) -> Array:
	if idx == 0:
		_last_cmd_idx = G.Console.a_variant_idx
		return cfgs.keys()
	elif idx == 1:
		var key = cfgs.keys()[_last_cmd_idx]
		match cfgs[key]:
			1:
				return boolVariants
			2:
				return []
			4:
				return videoDrivers
			_:
				return []
	else:
		return []


func tweak_setting(key, val) -> void:
	if !(key in cfgs):
		G.debug_print("No such key: " + str(key) + "!")
		save_cfg()
		return
	set(key, val)


func config(key = "", val = "") -> void:
	
	if key == "":
		var cooked_msg = ""
		var cval = ""
		for i in cfgs.keys():
			cval = str(get(i))
			if cval.to_lower() in boolVariants:
				cval = cval.to_lower()
			cooked_msg += "[color=#ffff44]" + i + "[/color] = [color=#22ffff]" + cval + "[/color]\n"
		cooked_msg = cooked_msg.left(cooked_msg.length() - 1)
		G.Console.message(cooked_msg, false)
		return
	
	if val.to_lower() in boolVariants:
		if val.to_lower() == "true": val = true
		elif val.to_lower() == "false": val = false
	
	elif val.to_upper() in videoDrivers:
		val = val.to_upper()
	
	else:
		val = int(val)
	
	tweak_setting(key, val)
	
	G.Main.update_config()
	save_cfg()
