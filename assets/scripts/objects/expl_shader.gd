extends Node2D

const BLINK_TIME = 0.025

var shader_end = false

func _ready() -> void:
	if get_tree().current_scene.name == "expl":
		hide()
		yield(get_tree().create_timer(1), "timeout")
		show()
	while true:
		if G.Server != null:
			break
		material.set_shader_param("abX", randf() * 0.1)
		material.set_shader_param("abY", randf() * 0.1)
		material.set_shader_param("disp_size", randf() * 0.4 + 0.6)
		material.set_shader_param("dispAmt", randf() * 0.02)
		yield(get_tree().create_timer(BLINK_TIME), "timeout")
		if $"../visual".vis_end:
			shader_end = true
			break
