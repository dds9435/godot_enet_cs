extends StaticBody2D

const RADIUS = Balance.MEGU_RADIUS
const DAMAGE = Balance.MEGU_DAMAGE


func hit(point, damage, owner_id) -> void:
	if G.Server != null:
		set_collision_layer_bit(0, false)
		set_collision_layer_bit(1, false)
		set_collision_mask_bit(0, false)
		set_collision_mask_bit(1, false)
		G.World.map.explosion(position, RADIUS, DAMAGE, owner_id)
		queue_free()


func get_obj_hp() -> int:
	if G.Server != null:
		return 1
	return 0
