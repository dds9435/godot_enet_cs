extends Area2D

const TIMEOUT = Balance.EXPL_LISTENER_TIMEOUT

var receiver = null
var damage = 0
var center = Vector2(0,0)
var radius = 0
var owner_id = -1

var bodies = []

func _ready() -> void:
	if G.Server != null and receiver != null:
		position = center
		connect("area_entered", self, "_on_area_entered")
		connect("body_entered", self, "_on_body_entered")
	yield(get_tree().create_timer(TIMEOUT), "timeout")
	if receiver != null:
		receiver.explosion_bodies(bodies, damage, center, radius, owner_id)
		get_parent().queue_free()


func _on_area_entered(area: Area2D) -> void:
	if !(area in bodies): bodies.append(area)


func _on_body_entered(body: Node) -> void:
	if !(body in bodies): bodies.append(body)
