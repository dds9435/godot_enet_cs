extends Node2D

const BLINKS		= 3
const BLINK_TIME	= 0.075

var c = true
var radius = 0
var pos = Vector2(0,0)
var vis_end = false

func _ready() -> void:
	if get_tree().current_scene.name == "expl":
		radius = 64
		pos = Vector2(256,256)
		hide()
		yield(get_tree().create_timer(1), "timeout")
		show()
	if G.Server == null:
		position = pos
		$"../effect".position = pos
		$"../glow".position = pos
		$"../particles".position = pos
		$"../particles".process_material.emission_sphere_radius = radius / 1.5
		$"../particles".amount = radius
		scale = Vector2(radius / 256.0, radius / 256.0)
		$"../Tween2".remove(self, "scale")
		$"../Tween2".interpolate_property(self, "scale", Vector2(radius / 256.0, radius / 256.0), Vector2(radius / 512.0, radius / 512.0), (BLINKS - 1) * BLINK_TIME, Tween.TRANS_BOUNCE, Tween.EASE_IN, BLINK_TIME)
		$"../Tween2".start()
		$"../effect".scale = Vector2( radius / 256.0, radius / 256.0)
		$"../glow".scale = Vector2(radius / 170.0, radius / 170.0)
		$"../hazeGlow".scale = $"../glow".scale
		$"../hazeGlow".position = $"../glow".position
		$"../particles".emitting = true
		for i in range(BLINKS):
			if c: c = false
			else: c = true
			_update()
			yield(get_tree().create_timer(BLINK_TIME), "timeout")
			radius -= radius * 0.1
			$"../glow".scale = Vector2(radius / 256.0, radius / 256.0)
		vis_end = true
		while true:
			if $"../effect".shader_end:
				break
			yield(get_tree(), "idle_frame")
		$"../effect".hide()
		$"../glow".hide()
		$"../hazeGlow".show()
		$"../Tween".remove($"../hazeGlow", "modulate")
		$"../Tween".interpolate_property($"../hazeGlow", "modulate", Color(1, 1, 1, 1), Color(1, 1, 1, 0), 3.0, Tween.TRANS_CIRC, Tween.EASE_OUT)
		$"../Tween".start()
		hide()
		yield(get_tree().create_timer(10), "timeout")
		get_parent().queue_free()


func _update() -> void:
	if c:
		modulate = Color(0,0,0, 0.25)
	else:
		modulate = Color(1,1,1)
