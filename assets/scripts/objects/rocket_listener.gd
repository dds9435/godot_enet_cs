extends RigidBody2D

var timer = 60.0

var integrate = 0

var RADIUS = 0
var DAMAGE = 0
var owner_id = 0

var is_shutdown = false 

func _ready() -> void:
	if get_tree().current_scene.name == "rocket":
		set_physics_process(false)
		yield(get_tree().create_timer(1.0), "timeout")
		apply_impulse(Vector2(0,0), Vector2(Balance.ROCKET_DEF_IMPULSE, 0).rotated(deg2rad(45)))
		while true:
			yield(get_tree().create_timer(3.0), "timeout")
			position = Vector2(0,0)


func server_shutdown() -> void:
	G.Server.shutdown_rocket(get_path())
	G.World.map.explosion(global_position, RADIUS, DAMAGE, owner_id)
	queue_free()


func client_shutdown() -> void:
	if is_shutdown: return
	is_shutdown = true
	set_deferred("mode", RigidBody2D.MODE_KINEMATIC)
	$col.queue_free()
	$tex.hide()
	if has_node("glow"):
		$glow.hide()
	if has_node("Smoke"):
		$Smoke.emitting = false
	yield(get_tree().create_timer(1.0), "timeout")
	queue_free()


func _integrate_forces(state) -> void:
	if integrate != 0:
		var t = state.transform
		t.origin = position + linear_velocity * integrate + (state.total_gravity * pow(integrate, 2)) / 2
		state.transform = t
		state.linear_velocity += state.total_gravity * integrate
		integrate = 0


func _on_rocket_body_entered(body) -> void:
	if G.Server != null:
		server_shutdown()
	else:
		client_shutdown()


func hit(point, damage, _owner_id) -> void:
	if G.Server != null:
		server_shutdown()
	else:
		client_shutdown()


func _physics_process(delta: float) -> void:
	timer -= delta
	if timer < 0:
		if G.Server != null:
			server_shutdown()
		else:
			client_shutdown()
		set_physics_process(false)
