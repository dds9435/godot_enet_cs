extends Panel

const CELL_COLOR			= Color(0.1, 0.1, 0.1)
const HARD_CELL_COLOR		= Color(0.4, 0.4, 0.4)
const BG_CELL_COLOR			= Color(0.0, 0.0, 0.0)
const SEAT_COLOR			= Color(0.1, 0.1, 0.1)
const TEAM_1_COLOR			= Color(1.0, 0.0, 0.0)
const TEAM_2_COLOR			= Color(0.0, 0.0, 1.0)

const POINT_ACTIVE_COLOR	= Color(1.0, 1.0, 1.0)
const POINT_CAPPING_1_COLOR	= Color(1.0, 0.5, 0.5)
const POINT_CAPPING_2_COLOR	= Color(0.5, 0.5, 1.0)
const POINT_CAPPED_1_COLOR	= Color(1.0, 0.2, 0.2)
const POINT_CAPPED_2_COLOR	= Color(0.2, 0.2, 1.0)

const GRID_SIZE				= 2
const TILE_SIZE				= 32
const OFFSET				= Vector2(0, -48)

onready var map_drawer			= $Container/Map
onready var map_bg_drawer		= $Container/MapBg
onready var indicators_drawer	= $Container/Indicators

func _ready() -> void:
	set_process(false)
	if G.Main == null: return
	map_drawer.GRID_SIZE = GRID_SIZE
	map_bg_drawer.GRID_SIZE = GRID_SIZE
	indicators_drawer.GRID_SIZE = GRID_SIZE
	indicators_drawer.TILE_SIZE = TILE_SIZE
	indicators_drawer.SEAT_COLOR = SEAT_COLOR
	indicators_drawer.TEAM_1_COLOR = TEAM_1_COLOR
	indicators_drawer.TEAM_2_COLOR = TEAM_2_COLOR
	indicators_drawer.POINT_ACTIVE_COLOR = POINT_ACTIVE_COLOR
	indicators_drawer.POINT_CAPPING_1_COLOR = POINT_CAPPING_1_COLOR
	indicators_drawer.POINT_CAPPING_2_COLOR = POINT_CAPPING_2_COLOR
	indicators_drawer.POINT_CAPPED_1_COLOR = POINT_CAPPED_1_COLOR
	indicators_drawer.POINT_CAPPED_2_COLOR = POINT_CAPPED_2_COLOR
	yield(G.Main, "start")
	if G.Client != null:
		G.World.map.connect("received_cells", self, "_on_received_cells")
		G.World.map.connect("received_bg_cells", self, "_on_received_bg_cells")
		G.World.map.connect("cell_destroy", self, "_on_cell_destroy")
		G.World.map.connect("seat_status", self, "_on_seat_status")
		set_process(true)


const DRAW_TIMEOUT = 0.1
const INDICATORS_TIMEOUT = 0.02
var timer = 0.0
var indicators_timer = INDICATORS_TIMEOUT
var pos = Vector2(0,0)
var players = {}

var _old_block = false

func _process(delta: float) -> void:
	if timer > 0:
		timer -= delta
		if timer < 0:
			map_drawer.draw_removed()
			timer = 0.0
	
	if G.my_player != null:
		if G.my_player.block_camera:
#			if _old_block == true:
#				rect_size = Vector2(576, 384)
#				margin_top = -384
#				margin_bottom = 0
#				_old_block = false
			pos = pos.move_toward(rect_size / 2 - G.my_player.get_node("Camera2D").global_position / TILE_SIZE * GRID_SIZE + OFFSET, 4)
		else:
#			if _old_block == false:
#				rect_size = Vector2(384, 256)
#				margin_top = -256
#				margin_bottom = 0
#				_old_block = true
			pos = rect_size / 2 - G.my_player.position / TILE_SIZE * GRID_SIZE + OFFSET
		map_drawer.position = pos
		map_bg_drawer.position = pos
		indicators_drawer.position = pos
	
	if indicators_timer > 0:
		indicators_timer -= delta
		if indicators_timer < 0:
			players = {}
			for pl in G.World.players.get_children():
				if is_instance_valid(pl):
					players[pl.name] = [pl.position, pl.team, pl.hide_from_map]
					indicators_drawer.process_players(players)
			indicators_timer = INDICATORS_TIMEOUT


func _on_seat_status(_pos, status) -> void:
	indicators_drawer.process_seat(_pos, status)


func _on_cell_destroy(cell) -> void:
	map_drawer.remove_cell(cell)
	timer = DRAW_TIMEOUT


func _on_received_cells(cells, hard_cells, struct_bg_cells, rect) -> void:
	for i in struct_bg_cells:
		cells.erase(i)
	map_drawer.draw_cells(cells, CELL_COLOR, rect)
	map_drawer.draw_hard_cells(hard_cells, HARD_CELL_COLOR)


func _on_received_bg_cells(cells, rect) -> void:
	map_bg_drawer.draw_cells(cells, BG_CELL_COLOR, rect)
