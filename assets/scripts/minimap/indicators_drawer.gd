extends Node2D

const POINT_POS				= 0
const POINT_IDX				= 1
const POINT_STATE			= 2
const POINT_TIME			= 3
const POINT_HP				= 4
const POINT_CAPTURERS		= 5


const STATE_DEACTIVE		= 0
const STATE_ACTIVE			= 1
const STATE_CAPPING_1		= 2
const STATE_CAPPING_2		= 3
const STATE_CAPPED_1		= 4
const STATE_CAPPED_2		= 5 


var GRID_SIZE				= 0
var TILE_SIZE				= 0
var SEAT_COLOR				= Color(1,1,1)
var TEAM_1_COLOR			= Color(0,0,0)
var TEAM_2_COLOR			= Color(0,0,0)

var POINT_ACTIVE_COLOR		= Color(0, 0, 0)
var POINT_CAPPING_1_COLOR	= Color(0, 0, 0)
var POINT_CAPPING_2_COLOR	= Color(0, 0, 0)
var POINT_CAPPED_1_COLOR	= Color(0, 0, 0)
var POINT_CAPPED_2_COLOR	= Color(0, 0, 0)

var seats = []
var last_players = {}
var last_player_color = Color(0, 0, 0)
var last_point_color = Color(0, 0, 0)


func process_players(players):
	last_players = players
	update()


func process_seat(pos, status):
	if status and !seats.has(pos):
		seats.append(pos)
		update()
	if !status and seats.has(pos):
		seats.erase(pos)
		update()


func _draw() -> void:
	for i in seats:
		draw_rect(Rect2(i * GRID_SIZE / TILE_SIZE - Vector2(2, -4) * GRID_SIZE, Vector2(4, 1) * GRID_SIZE), SEAT_COLOR)
	for i in last_players:
		if last_players[i][2] and i != G.my_player.name: continue
		if last_players[i][1] == G.TEAM_1:
			last_player_color = TEAM_1_COLOR
		elif last_players[i][1] == G.TEAM_2:
			last_player_color = TEAM_2_COLOR
		draw_rect(Rect2(last_players[i][0] * GRID_SIZE / TILE_SIZE - Vector2(1, -1) * GRID_SIZE, Vector2(2, 2) * GRID_SIZE), last_player_color)
#	if G.Client != null:
#		for i in G.World.points.points:
#			if i != null:
#				match i[POINT_STATE]:
#					STATE_ACTIVE:
#						last_point_color = POINT_ACTIVE_COLOR
#					STATE_CAPPING_1:
#						last_point_color = POINT_CAPPING_1_COLOR
#					STATE_CAPPING_2:
#						last_point_color = POINT_CAPPING_2_COLOR
#					STATE_CAPPED_1:
#						last_point_color = POINT_CAPPED_1_COLOR
#					STATE_CAPPED_2:
#						last_point_color = POINT_CAPPED_2_COLOR
#					_:
#						last_point_color = Color(0, 0, 0, 0)
#				if last_point_color != Color(0, 0, 0):
#					draw_circle(i[POINT_POS] * GRID_SIZE, 2 * GRID_SIZE, last_point_color)
