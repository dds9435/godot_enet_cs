extends Node2D

var GRID_SIZE = 0

var last_cells = []
var last_color = Color(0,0,0)
var last_rect = Rect2()

var last_image_texture = null
var removed_cells = []


func draw_hard_cells(cells, color) -> void:
	if last_rect.size.length() == 0 and last_image_texture == null:
		return
	var img = (last_image_texture as ImageTexture).get_data()
	img.lock()
	for i in cells:
		for j in range(GRID_SIZE):
			for k in range(GRID_SIZE):
				img.set_pixelv((i - last_rect.position) * GRID_SIZE + Vector2(j, k), color)
	img.unlock()
	(last_image_texture as ImageTexture).set_data(img)
	update()


func draw_removed() -> void:
	if last_rect.size.length() == 0 and last_image_texture == null:
		return
	var img = (last_image_texture as ImageTexture).get_data()
	img.lock()
	for i in removed_cells:
		for j in range(GRID_SIZE):
			for k in range(GRID_SIZE):
				img.set_pixelv((i - last_rect.position) * GRID_SIZE + Vector2(j, k), Color(0,0,0,0))
	img.unlock()
	(last_image_texture as ImageTexture).set_data(img)
	update()


func remove_cell(cell) -> void:
	removed_cells.append(cell)


func draw_cells(cells, color, rect) -> void:
	last_cells = cells
	last_color = color
	last_rect = rect
	if last_rect.size.length() == 0:
		return
	last_image_texture = ImageTexture.new()
	var img = Image.new()
	img.create(last_rect.size.x * GRID_SIZE, last_rect.size.y * GRID_SIZE, false, Image.FORMAT_RGBA8)
	img.lock()
	for i in last_cells:
		for j in range(GRID_SIZE):
			for k in range(GRID_SIZE):
				img.set_pixelv((i - last_rect.position) * GRID_SIZE + Vector2(j, k), last_color)
	img.unlock()
	last_image_texture.create_from_image(img)
	update()


func _draw() -> void:
	if last_image_texture != null:
		draw_texture(last_image_texture, last_rect.position * GRID_SIZE )
