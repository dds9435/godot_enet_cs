extends Node

signal _added()
signal client_added()
signal world_added()
signal ui_added()

var Main
var World
var UI
var Server							# пир сервера, если клиент, то равен null
var Client							# пир клиента, если сервер, то равен null
var Peer:	Net						# пир текущего типа работы приложения (клиент или сервер), для работы независимой от типа пира (клиет или сервер)
var GameState
var ModulesServer
var Console
var Config:	Config
var genlib_instance = null

const TEAM_1 = 1
const TEAM_2 = 2

var my_player

var max_size_log = 100
var msg_log = []
var _cur_log_size = 0

func _ready() -> void:
	var genlib = preload("res://assets/lib/gen.gdns")
	if genlib.library.get_current_library_path() == "":
		G.debug_print("[ERROR] Can't find a lib for current platform!")
		get_tree().quit()
		return
	genlib_instance = genlib.new()
	G.debug_print("[GENLIB] LIB LOADED!")

func debug_print(msgs, logs=false):
	if msgs is Array:
		msgs = PoolStringArray(msgs).join(" ")
	var dt = OS.get_datetime()
	var msg = ("[%d-%02d-%02d %02d:%02d:%02d] " % [dt.year, dt.month, dt.day, dt.hour, dt.minute, dt.second]) + msgs
	print(msg)
	if logs:
		if max_size_log == _cur_log_size:
			msg_log.pop_back()
			msg_log.append(msg)
		else:
			_cur_log_size += 1
