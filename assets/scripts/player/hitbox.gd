extends Area2D

var player = null
var visual_part = null
var hitbox_id = -1
var strong_spirit = false


func get_obj_hp():
	if G.Server != null:
		if player.dead:
			return INF
		else:
			return clamp(player.hitbox[hitbox_id], 0, INF)
	else:
		return


func hit(point, damage, owner_id) -> void:
	if G.Server != null and !player.dead:
		var owner_node = G.World.players.get_node_or_null(str(owner_id))
		if owner_node != null:
			if owner_node.team == player.team and owner_id != int(player.name):
				return
		var clamped_damage = clamp(damage, 0, player.hitbox[hitbox_id])
		if strong_spirit and player.hitbox[hitbox_id] > 1:
			player.hitbox[hitbox_id] = 1
		else:
			player.hitbox[hitbox_id] -= damage
		player.process_hitbox(hitbox_id, visual_part, self, owner_id, clamped_damage)
