extends Player

signal me_changed_position(pos)
signal me_changed_motion(motion)
signal me_changed_crouch(crouch)
signal me_changed_jet_fuel(jet_fuel)
signal me_changed_jet(jet)
signal serv_hitbox(me, id, hp)
signal serv_changed_position(mt, pos)
signal me_hitted()
signal ready()
signal pl_changed_position(id, pos)
signal pl_hide_from_map(id, val)
signal pl_changed_crouch(id, val)

signal completely_added()


const SEND_TIMER			= 0.75

const MOTION_BIAS			= Balance.PL_MOTION_BIAS
const GRAVITY				= Balance.PL_GRAVITY
const STOPFORCE				= Balance.PL_STOPFORCE
const MIN_SPEED				= Balance.PL_MIN_SPEED
const MIN_JUMP				= Balance.PL_MIN_JUMP

var MAX_SPEED				= Balance.PL_MAX_SPEED
var MAX_JUMP				= Balance.PL_MAX_JUMP
var MAX_JUMPS				= Balance.PL_MAX_JUMPS
var MAX_JET_FUEL			= Balance.PL_MAX_JET_FUEL

var ACCEL					= Balance.PL_ACCEL
var JUMP_ACCEL				= Balance.PL_JUMP_ACCEL
var JET						= Balance.PL_JET

var JET_ACTIVATION			= Balance.PL_JET_ACTIVATION
var JET_REFUEL_FACT			= Balance.PL_JET_REFUEL_FACT

var DEATH_EXPLODE_RADIUS	= Balance.PL_DEATH_EXPLODE_RADIUS
var DEATH_EXPLODE_DAMAGE	= Balance.PL_DEATH_EXPLODE_DAMAGE

# LINKS

onready var weapon = $weapon
onready var drill = $drill
onready var visual = $visual

# PROCESS

var crouch = false

var accum = 0
var jump_accum = 0
var jumps = 0
var jet_timer = 0.0
var jet_mode = false
var jet_fuel = MAX_JET_FUEL

var side = 1
var motion = Vector2()

var pos_update_timer = 0.0
var last_pos = Vector2()
var last_motionv = Vector2()
var last_jet_fuel = 0.0
var last_jet_fuel_ui = 0.0
var last_pos_timer = 0.0
var last_motion_timer = 0.0
var last_jet_fuel_timer = 0.0
var last_crouch = false
var last_jet = false

var fuel_indicator_def = 0.0

# HITBOX

const BLOW_IMPULSE	= Balance.PL_HITBOX_BLOW_IMPULSE
const BLOW_TORQUE 	= Balance.PL_HITBOX_BLOW_TORQUE

enum {
	HT_LT, HT_RT,
	HT_RB, HT_LB,
	HT_CE
}

var hitbox_default = [
	Balance.PL_HITBOX_DEFAULT_HP,
	Balance.PL_HITBOX_DEFAULT_HP,
	Balance.PL_HITBOX_DEFAULT_HP,
	Balance.PL_HITBOX_DEFAULT_HP,
	Balance.PL_HITBOX_DEFAULT_CE_HP
]
onready var hitbox = hitbox_default.duplicate()
onready var hitbox_max = hitbox_default.duplicate()

# INIT

var in_init = false
var completely_added = false

# MINIMAP

var hide_from_map = false setget _set_hide_from_map
var last_minimap_pos = Vector2(0,0)

var block_camera = false

# BICHARS

var put_out_arr = []

# ANIM

enum {
	IDLE, WALK,
	JUMP, LAND
}

var last_state  = 0
var last_motion = 0
var last_orient = 0

var anim_str = "idle"
var anim_angle = 0.0

# REWIND

var buf_rewind_pos = Vector2(0,0)
var position_log = {}


# INIT

func _ready():
	position = Vector2(0, -16384)
	set_physics_process(true)
	$hitbox/lt.player = self
	$hitbox/lt.visual_part = $visual/body_lt
	$hitbox/lt.hitbox_id = HT_LT
	$hitbox/rt.player = self
	$hitbox/rt.visual_part = $visual/body_rt
	$hitbox/rt.hitbox_id = HT_RT
	$hitbox/rb.player = self
	$hitbox/rb.visual_part = $visual/body_rb
	$hitbox/rb.hitbox_id = HT_RB
	$hitbox/lb.player = self
	$hitbox/lb.visual_part = $visual/body_lb
	$hitbox/lb.hitbox_id = HT_LB
	$hitbox/ce.player = self
	$hitbox/ce.visual_part = $visual/heart
	$hitbox/ce.hitbox_id = HT_CE
	$hitbox/ce.strong_spirit = true
	$visual/body_lt.material = $visual/body_lt.material.duplicate()
	$visual/body_rt.material = $visual/body_rt.material.duplicate()
	$visual/body_rb.material = $visual/body_rb.material.duplicate()
	$visual/body_lb.material = $visual/body_lb.material.duplicate()
	$visual/heart.material = $visual/heart.material.duplicate()
	connect("dead", self, "init")
	connect("resurrect", self, "post_init")
	connect("changed_team", self, "team_color")
	fuel_indicator_def = $visual/legs/left/team.scale.x
	if G.Server != null:
		$pos_tween.connect("tween_step", self, "_on_tween_step")
		connect("about_to_be_killed", self, "_on_prekilled")
	emit_signal("ready")

func team_color(player, team_id: int) -> void:
	var color = (Color(1,1,0))
	
	match team_id:
		G.TEAM_1:
			color = Color.red
		G.TEAM_2:
			color = Color.slateblue
	
	var half_color = color.lightened(0.5)
	var eye_color = color.lightened(0.5)
	var heart_color = color.lightened(0.3)
	var seat_color = color.lightened(0.6)
	var legs_color = color.lightened(0.3)
	
	
	$visual/eye/team.modulate.r = eye_color.r
	$visual/eye/team.modulate.g = eye_color.g
	$visual/eye/team.modulate.b = eye_color.b
	$visual/eye/team/glow.modulate = half_color
	$visual/heart/team.modulate.r = heart_color.r
	$visual/heart/team.modulate.g = heart_color.g
	$visual/heart/team.modulate.b = heart_color.b
	$visual/heart/team/glow.modulate = half_color
	$visual/legs/left/team.modulate.r = legs_color.r
	$visual/legs/left/team.modulate.g = legs_color.g
	$visual/legs/left/team.modulate.b = legs_color.b
	$visual/legs/left/team/glow.modulate = half_color
	$visual/legs/right/team.modulate.r = legs_color.r
	$visual/legs/right/team.modulate.g = legs_color.g
	$visual/legs/right/team.modulate.b = legs_color.b
	$visual/legs/right/team/glow.modulate = half_color
	while true:
		if has_node(mon_seatoni):
			get_node(mon_seatoni).get_node("effect").modulate.r = seat_color.r
			get_node(mon_seatoni).get_node("effect").modulate.g = seat_color.g
			get_node(mon_seatoni).get_node("effect").modulate.b = seat_color.b
			break
		else:
			yield(get_tree().create_timer(0.1), "timeout")

func init(player, first = false) -> void:
	
	if !dead: return
	in_init = true
	
	if !completely_added:
		yield(self, "completely_added")
	
	$preview_anim.play("idle")
	get_node(mon_seatoni).get_node("effect").emitting = true
	jet_fuel = MAX_JET_FUEL
	$weapon.reset()
	
	if team == G.TEAM_1:
		$visual.scale.x = 1
	else:
		$visual.scale.x = -1
	
	hitbox = hitbox_default.duplicate()
	jet_fuel = MAX_JET_FUEL
	
	var hb
	var vis
	
	for i in $hitbox.get_children():
		hb = i
		vis = i.visual_part
		vis.show()
		vis.material.set_shader_param("force", 0.0)
		hb.set_collision_layer_bit(0, true)
		hb.set_collision_layer_bit(1, true)
		hb.set_collision_mask_bit(0, true)
		hb.set_collision_mask_bit(1, true)
	$hitbox/ce.strong_spirit = true
	
	$appear_anim.play("appear")
	$legs_anim.play("dead")
	
	if G.Client != null:
		if control:
			motion = Vector2(0,0)
			
			$tw.remove(camera, "global_position")
			$tw.interpolate_property(camera, "global_position",
					null, last_death_position, 1.0,
					Tween.TRANS_CUBIC, Tween.EASE_OUT)
			$tw.start()
			
			yield(get_tree().create_timer(1.0), "timeout")
			
			$tw.remove(camera, "position")
			$tw.interpolate_property(camera, "position",
					null, Vector2(0, 24), 0.5,
					Tween.TRANS_CUBIC, Tween.EASE_OUT)
			$tw.remove(camera, "zoom")
			$tw.interpolate_property(camera, "zoom",
					null, Vector2(0.4, 0.4), 1,
					Tween.TRANS_CUBIC, Tween.EASE_OUT)
			$tw.start()


func post_init(player) -> void:
	
	if dead: return
	
	if G.Client != null and control:
		$tw.remove(camera, "zoom")
		$tw.interpolate_property(camera, "zoom",
				null, Vector2(1, 1),
				1, Tween.TRANS_CUBIC, Tween.EASE_OUT)
		$tw.start()
	$preview_anim.play("still")
	
	if has_node(mon_seatoni):
		get_node(mon_seatoni).get_node("effect").emitting = false
	
	in_init = false

###

# HITBOX

func process_hitbox(id: int, vis, hb, damage_owner = -1, damage = 0) -> void:
	if in_init: return
	if G.Server != null:
		emit_signal("serv_hitbox", self, id, hitbox[id])
	
	var force = 0
	var mhp = 0
	mhp = hitbox_max[id]
	force = float((mhp - hitbox[id])) / mhp
	
	emit_signal("me_hitted")
	vis.material.set_shader_param("force", force)
	vis.get_node("hit").play("hit")
	
	if hitbox[id] < 1:
		vis.hide()
		hb.set_collision_layer_bit(0, false)
		hb.set_collision_layer_bit(1, false)
		hb.set_collision_mask_bit(0, false)
		hb.set_collision_mask_bit(1, false)
		
		if G.Client != null:
			hitbox_part(vis, hb)
		
		if G.Server != null and id == HT_CE:
			for i in range(4):
				if hitbox[i] > 0:
					emit_signal("serv_hitbox", self, i, -1)
			G.World.map.explosion(position +
					motion * G.Server.connections_rtt.get(int(name), 0.016) * 1.5,
					DEATH_EXPLODE_RADIUS, DEATH_EXPLODE_DAMAGE, int(name))
			if !prekill:
				kill(str(damage_owner))
	if G.Server != null:
		G.GameState.feed_event(GameState.FEED_HIT,
				[int(name), damage_owner, damage, id])


func process_hitbox_from_server(id: int, hp) -> void:
	if in_init: return
	var vis
	var hb
	
	for i in $hitbox.get_children():
		if i.hitbox_id == id:
			hb = i
			vis = i.visual_part
	
	hitbox[id] = hp
	process_hitbox(id, vis, hb)


func hitbox_part(vis, hb) -> void:
	var part = RigidBody2D.new()
	var tex = Sprite.new()
	var col = CollisionPolygon2D.new()
	
	part.set_script(load("res://assets/scripts/player/part.gd"))
	tex.name = "tex"
	tex.texture = vis.texture
	tex.material = vis.material.duplicate()
	tex.material.set_shader_param("force", 0.9)
	tex.modulate = Color(0.3,0.3,0.3)
	col.name = "col"
	col.polygon = hb.get_node("col").polygon
	part.add_child(tex)
	part.add_child(col)
	
	part.position = vis.global_position
	part.set_collision_layer_bit(0, false)
	part.set_collision_mask_bit(0, false)
	part.set_collision_layer_bit(4, true)
	part.apply_central_impulse(
			vis.position.normalized() * BLOW_IMPULSE * randf())
	part.apply_torque_impulse((randf() - 0.5) * BLOW_TORQUE)
	part.gravity_scale = 6.0
	
	G.World.add_child(part)

func _on_prekilled() -> void:
	$hitbox/ce.strong_spirit = false
	$hitbox/ce.hit(Vector2(0, 0), 1000, int(name))

###

# PROCESS

func _set_hide_from_map(val: bool) -> void:
	hide_from_map = val
	emit_signal("pl_hide_from_map", int(name), val)

func _physics_process(delta: float) -> void:
	
	if position.snapped(Vector2(1,1)) != last_minimap_pos:
		emit_signal("pl_changed_position", int(name), position)
		last_minimap_pos = position.snapped(Vector2(1,1))
	
	if !dead and G.Server != null:
		if position.y > G.World.map.BOTTOM:
			kill(name)
	
	if !dead and control:
		
		if is_instance_valid(G.UI) and !G.UI.block_input:
			
			if Input.is_action_pressed("move_right"):
				if side != 1:
					side = 1
					accum = 0
				motion.x = MIN_SPEED + accum
				accum = clamp(accum + ACCEL, 0, MAX_SPEED - MIN_SPEED)
			elif Input.is_action_pressed("move_left"):
				if side != -1:
					side = -1
					accum = 0
				motion.x = - (MIN_SPEED + accum)
				accum = clamp(accum + ACCEL, 0, MAX_SPEED - MIN_SPEED)
			else:
				if abs(motion.x) > 0:
					if motion.x > 0:
						motion.x -= STOPFORCE
						if motion.x < 0:
							motion.x = 0
					elif motion.x < 0:
						motion.x += STOPFORCE
						if motion.x > 0:
							motion.x = 0
				accum = 0
			
			
			if Input.is_action_just_pressed("crouch"):
				if !crouch:
					$crouch_anim.play("process")
					crouch = true
			
			
			if Input.is_action_pressed("move_up"):
				if (jump_accum < MAX_JUMP - MIN_JUMP and
					jumps != MAX_JUMPS and
					!jet_mode):
					motion.y = - (MIN_JUMP + jump_accum)
					motion.y = - (MIN_JUMP + jump_accum)
					jump_accum = clamp(
							jump_accum + JUMP_ACCEL,
							0, MAX_JUMP - MIN_JUMP)
				elif jet_mode and jet_fuel > 0:
					if motion.y > 0:
						jet_fuel -= delta * 2
						motion.y = - JET * 2
					else:
						jet_fuel -= delta
						motion.y = - JET
				else:
					jet_mode = false
					jet_timer += delta
					if (jet_timer > JET_ACTIVATION
							and jet_fuel > 0):
						jet_mode = true
						jump_accum = 0
			
			
			elif Input.is_action_just_released("move_up"):
				if !jet_mode:
					jet_timer = 0.0
				jet_mode = false
				
				if motion.y < 0 and !jet_mode:
					if jumps != MAX_JUMPS:
						jumps += 1
						jump_accum = 0
					motion.y *= 0.5
			
		for bichara in put_out_arr:
			if bichara.get_parent() is KinematicBody2D:
				motion = motion.linear_interpolate(\
					- (bichara.get_parent().position - position).normalized() *\
					1000, 0.3)
		
		motion.y += GRAVITY * delta
		motion = move_and_slide(motion, Vector2.UP)
		motion = motion.clamped(MAX_SPEED * 10)
	
	if !block_camera and control:
		if !dead:
			camera.position = Vector2(0,0).linear_interpolate(
						get_local_mouse_position(), 0.5).snapped(Vector2(1,1))
			camera.smoothing_speed = clamp(motion.length() / 100, 10, INF)
	
	if is_on_floor() or dead:
		jumps = 0
		jump_accum = 0
		jet_timer = 0.0
		jet_mode = false
		
		if jet_fuel < MAX_JET_FUEL:
			jet_fuel += delta * JET_REFUEL_FACT
			if jet_fuel > MAX_JET_FUEL:
				jet_fuel = MAX_JET_FUEL
	
	if crouch != last_crouch:
		if crouch:
			$crouch_anim.play("process")
		else:
			$crouch_anim.play_backwards("process")
		emit_signal("pl_changed_crouch", int(name), crouch)
		if control:
			emit_signal("me_changed_crouch", crouch)
		last_crouch = crouch
	
	$visual/legs/right/Smoke.emitting = jet_mode
	$visual/legs/left/Smoke.emitting = jet_mode
	
	if jet_mode != last_jet:
		if jet_mode:
			$visual/legs/left/fire.play("open")
			$visual/legs/right/fire.play("open")
			$visual/legs/left/fire/glow.show()
			$visual/legs/right/fire/glow.show()
		else:
			$visual/legs/left/fire.play("close")
			$visual/legs/right/fire.play("close")
		if control:
			emit_signal("me_changed_jet", jet_mode)
		last_jet = jet_mode
	
	if (jet_mode or test_move(transform,
			Vector2(0, 4)) or dead) and jet_fuel <= MAX_JET_FUEL:
		$visual/legs/left/team.scale.x = (
				jet_fuel / MAX_JET_FUEL) * fuel_indicator_def
		$visual/legs/right/team.scale.x = $visual/legs/left/team.scale.x
	
	if !dead:
		$visual.scale.x = Vector2.RIGHT.rotated($weapon.rotation).sign().x
		$hitbox.scale.x = $visual.scale.x
		$glitch_mask.scale.x = $visual.scale.x
		
		if abs(motion.y) < MOTION_BIAS and test_move(transform, Vector2(0, 4)):
			if abs(motion.x) > MOTION_BIAS:
				animate(WALK, motion, $visual.scale.x)
			else:
				animate(IDLE, motion, $visual.scale.x)
		else:
			if motion.y < 0:
				animate(JUMP, motion, $visual.scale.x)
			else:
				animate(LAND, motion, $visual.scale.x)
	
	
	if control:
		
		if !Input.is_action_pressed("crouch") or dead:
			if crouch and !test_move(transform, Vector2.UP * 8):
				$crouch_anim.play_backwards("process")
				crouch = false
		
		if last_pos.snapped(Vector2(1,1)) != position.snapped(Vector2(1,1)):
			if last_pos_timer > 0:
				emit_signal("me_changed_position", position)
				last_pos = position
			last_pos_timer = SEND_TIMER
		else:
			if last_pos_timer > 0:
				last_pos_timer -= delta
				emit_signal("me_changed_position", position)
				if last_pos_timer < 0:
					last_pos_timer = 0.0
		if last_motionv.snapped(Vector2(1,1)) != motion.snapped(Vector2(1,1)):
			if last_motion_timer > 0:
				emit_signal("me_changed_motion", motion)
				last_motionv = motion
			last_motion_timer = SEND_TIMER
		else:
			if last_motion_timer > 0:
				last_motion_timer -= delta
				emit_signal("me_changed_motion", motion)
				if last_motion_timer < 0:
					last_motion_timer = 0.0
		if round(last_jet_fuel) != round(jet_fuel):
			if last_jet_fuel_timer > 0:
				emit_signal("me_changed_jet_fuel", jet_fuel)
				last_jet_fuel = jet_fuel
			last_jet_fuel_timer = SEND_TIMER
		else:
			if last_jet_fuel_timer > 0:
				last_jet_fuel_timer -= delta
				emit_signal("me_changed_jet_fuel", jet_fuel)
				if last_jet_fuel_timer < 0:
					last_jet_fuel_timer = 0.0
		
		if jet_fuel != last_jet_fuel_ui:
			G.UI.emit_signal("jet_value", jet_fuel / MAX_JET_FUEL)
			last_jet_fuel_ui = jet_fuel

# REWIND

func log_position() -> void:
	position_log[G.Server.pack_timer] = position
	if  G.Server.pack_timer - position_log.keys().front() > 1.0:
		position_log.erase(position_log.keys().front())

func rewind(time: float) -> void:
	buf_rewind_pos = position
	if position_log.keys().size() < 1: return
	if time > position_log.keys().back():
		return
	var diff = INF
	var n = position_log.keys().size()
	var cur = 0.0
	for i in range(n):
		cur = abs(position_log.keys()[i - 1] - time)
		if cur > diff:
			diff = cur
		else:
			position = position_log[position_log.keys()[i - 1]]
			break

func unrewind() -> void:
	position = buf_rewind_pos

###

# INTEGRATE

func integrate_position(pos: Vector2) -> void:
	var timer = 0.0
	if G.Client != null:
		if control: return
		timer = clamp(G.Client.RTT, 0.016, 1.0)
	else:
		timer = clamp(G.Server.connections_rtt.get(int(name), 1.0), 0.016, 1.0)
	
	$pos_tween.remove(self, "position")
	$pos_tween.interpolate_property(self, "position",
			null, pos, timer,
			Tween.TRANS_CUBIC, Tween.EASE_OUT)
	$pos_tween.start()


func _on_tween_step(
		object: Object, key: NodePath, elapsed: float, value: Object
	):
	emit_signal("serv_changed_position", self, position)
	log_position()

###

# ANIMATION

func animate(state, _motion, orient):
	if state != last_state:
		last_state = state
		match state:
			IDLE: anim_str = "idle"
			WALK: anim_str = "walk"
			JUMP: anim_str = "jump"
			LAND: anim_str = "land"
		
		$legs_anim.playback_speed = 1
		
		if state != WALK:
			$legs_anim.play(anim_str)
		else:
			last_orient = 0
			last_motion = 0
	
	if state == WALK:
		
		$legs_anim.playback_speed = abs(_motion.x) / 120
		
		if (_motion.sign().x != last_motion or
				orient != last_orient):
			last_motion = _motion.sign().x
			last_orient = orient
			
			if last_motion != last_orient:
				$legs_anim.play_backwards(anim_str)
			else:
				$legs_anim.play(anim_str)
	
	if state in [JUMP, LAND]:
		if state == JUMP:
			anim_angle = (_motion.angle() + PI / 2) * orient + deg2rad(10)
		else:
			anim_angle = (_motion.angle() + PI / 2) * orient * (-orient) * 0.15
		$visual/legs/left.rotation = lerp(
				$visual/legs/left.rotation,
				clamp(anim_angle,
				deg2rad(-35), deg2rad(35)),
				0.2)
		$visual/legs/right.rotation = $visual/legs/left.rotation


func _on_fire_animation_finished():
	if $visual/legs/left/fire.animation == "open":
		$visual/legs/left/fire.play("process")
		$visual/legs/right/fire.play("process")
	if $visual/legs/left/fire.animation == "close":
		$visual/legs/left/fire/glow.hide()
		$visual/legs/right/fire/glow.hide()
		$visual/legs/left/fire/glow.position = Vector2(0, -7)
		$visual/legs/right/fire/glow.position = Vector2(0, -7)

func _on_fire_frame_changed() -> void:
	if jet_mode:
		$visual/legs/left/fire/glow.position = Vector2(
				0 + randi()%4 - 2, -7 + randi()%4 - 2)
		$visual/legs/right/fire/glow.position = Vector2(
				0 + randi()%4 - 2, -7 + randi()%4 - 2)

###

# PUTTIN OUT BICHARS

func _on_put_out_area_entered(area: Area2D) -> void:
	if !put_out_arr.has(area):
		put_out_arr.append(area)


func _on_put_out_area_exited(area: Area2D) -> void:
	if put_out_arr.has(area):
		put_out_arr.erase(area)
