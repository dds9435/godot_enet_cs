extends Node2D

func _ready() -> void:
	set_physics_process(false)

var timer = 0.01
var TIMEOUT = 0.01

func _physics_process(delta: float) -> void:
	if timer > 0:
		timer -= delta
		if timer < 0:
			timer = TIMEOUT
			material.set_shader_param("dispAmt", randf() * 0.05 + 0.04)
			material.set_shader_param("abX", randf() * 0.01)
			material.set_shader_param("abY", randf() * 0.01)
			material.set_shader_param("disp_size", randf() * 0.5 + 0.4)
	$foot_l.global_position = $"../visual/legs/left".global_position
	$foot_r.global_position = $"../visual/legs/right".global_position
	$foot_l.global_rotation = $"../visual/legs/left".global_rotation
	$foot_r.global_rotation = $"../visual/legs/right".global_rotation
