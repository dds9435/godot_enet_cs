extends RigidBody2D

func _ready() -> void:
	
	yield(get_tree().create_timer(3), "timeout")
	while true:
		if linear_velocity.length() < 10:
			break
		yield(get_tree().create_timer(randi()%4 + 1), "timeout")
	mode = MODE_STATIC
	get_node("col").queue_free()

	var tw = Tween.new()
	add_child(tw)

	var target_pos : Vector2 = global_position - Vector2(0, 384)
	var target_rot : float = 0.5 - randf()
	
	tw.interpolate_property(
			get_node("tex"),
			"global_position",
			null,
			target_pos,
			10,
			Tween.TRANS_CUBIC,
			Tween.EASE_IN,
			0)
	tw.interpolate_property(
			get_node("tex"),
			"modulate",
			Color(0.3, 0.3, 0.3, 1.0),
			Color(0.0, 0.0, 0.0, 0.0),
			10,
			Tween.TRANS_CUBIC,
			Tween.EASE_IN,
			0)
	tw.interpolate_property(
			get_node("tex"),
			"rotation",
			0,
			target_rot,
			4,
			Tween.TRANS_CUBIC,
			Tween.EASE_IN,
			0)
	tw.start()
	
	yield(tw, "tween_all_completed")
	queue_free()
