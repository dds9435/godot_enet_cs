extends KinematicBody2D
class_name Player
#TEST PLAYER
# годо не позволяет объявить переменные ссылки без onready , а с ним они НЕ работают в вызовах до _ready
signal me_added_module(m_name, level, temp_level, send_to)
signal me_deleted_module(m_name, send_to)
signal me_module_state(m_name, state, send_to)
signal resurrect(player)
signal dead(player)
signal changed_team(player, team)
signal changed_seat(player, team, index)
signal changed_flip(player, tex, flip)
signal changed_money(player, val)
signal changed_credit(player, val)
signal changed_stats(player, val)
signal changed_cap_weight(player, val)

signal about_to_be_killed()

signal map_seat(pos, active)


var caption							setget set_caption
var dead		= true				setget _set_dead
var team							setget _set_team
var seat							setget _set_seat
var money		= 0					setget _set_money
var credit		= 0					setget _set_credit
var cap_weight	= 1					setget _set_cap_weight
var stats		= [0, 0, 0, 0, 0.0]	setget _set_stats # KILLS, DEATHS, CAPS, PKILLS, KARMA


var mon_seatoni: String = ""


var hit_colliders = [
	".",
	"hitbox/lt",
	"hitbox/rt",
	"hitbox/rb",
	"hitbox/lb",
	"hitbox/ce"
	]
var col_shapes = [
	"body_col",
	"legs_col"
	]

var last_death_position = Vector2(0,0)
var camera


func send_modules(send_to: int) -> void:
	for i in get_node("Modules").get_children():
		emit_signal("me_added_module", i.name, i.level, i.temp_level, send_to)


func delete_credit_modules() -> void:
	for i in get_node("Modules").get_children():
		if i.temp_level > 0:
			i.level -= i.temp_level
			i.temp_level = 0
			emit_signal("me_added_module", i.name, i.level, i.temp_level, Net.BROADCAST)
			if i.level < 0:
				i.queue_free()


func get_module_state(m_name: String):
	if get_node("Modules").has_node(m_name):
		var module = get_node("Modules/" + m_name)
		return module.state
	else:
		return null


func module_state(m_name: String, not_slave: bool, state: bool):
	if get_node("Modules").has_node(m_name):
		var module = get_node("Modules/" + m_name)
		if module.state == state:
			G.debug_print("Module " + m_name + " already have status " + str(state))
			return
		module.state = state
		module.update_module()
		if not_slave:
			emit_signal("me_module_state", m_name, state, Net.BROADCAST)
	else:
		G.debug_print("No such module " + m_name)


func add_module(m_name: String, not_slave: bool, level: int, temp_level: int) -> void:
	if get_node("Modules").has_node(m_name):
		var module = get_node("Modules/" + m_name)
		if module.level == level: return
		module.level = level
		module.temp_level = temp_level
		if module.level < 0:
			module.queue_free()
			return
		module.state = true
		module.update_module()
	else:
		var module = G.ModulesServer.get_module(m_name, not_slave)
		module.player = self
		module.level = level
		module.temp_level = temp_level
		get_node("Modules").add_child(module)
	if not_slave:
		emit_signal("me_added_module", m_name, level, temp_level, Net.BROADCAST)


func del_module(m_name: String, not_slave: bool) -> void:
	if !get_node("Modules").has_node(m_name): return
	var module = get_node("Modules/" + m_name)
	module.level -= 1
	if module.level == -1:
		get_node("Modules").get_node(m_name).queue_free()
	else:
		module.update_module()
	if not_slave:
		emit_signal("me_deleted_module", m_name, Net.BROADCAST)


func _set_cap_weight(val: int) -> void:
	cap_weight = val
	emit_signal("changed_cap_weight", self, val)


func _set_money(val: int) -> void:
	money = val
	emit_signal("changed_money", self, val)


func _set_credit(val: int) -> void:
	credit = val
	emit_signal("changed_credit", self, val)


func _set_stats(val) -> void:
	stats = val
	emit_signal("changed_stats", self, val)


func _set_seat(val: int) -> void:
	if mon_seatoni != "":
		var old_seat = get_node(mon_seatoni)
		old_seat.visible = false
		old_seat.set_collision_layer_bit(0, false)
		old_seat.set_collision_mask_bit(0, false)
		old_seat.set_collision_layer_bit(1, false)
		old_seat.set_collision_mask_bit(1, false)
		old_seat.set_collision_layer_bit(4, false)
		old_seat.set_collision_mask_bit(4, false)
	seat = val
	var t
	if team == G.TEAM_1:
		t = "1"
	else:
		t = "2"
	var my_seat = G.World.seats.get_node("Team_" + t + "/Seat_" + str(val))
	position = my_seat.position
	my_seat.visible = true
	my_seat.set_collision_layer_bit(0, true)
	my_seat.set_collision_mask_bit(0, true)
	my_seat.set_collision_layer_bit(1, true)
	my_seat.set_collision_mask_bit(1, true)
	my_seat.set_collision_layer_bit(4, true)
	my_seat.set_collision_mask_bit(4, true)
	mon_seatoni = my_seat.get_path()
	emit_signal("changed_seat", self, team, val)
	if G.Client != null:
		G.World.map.emit_signal("seat_status", my_seat.position, true)


func _on_Player_tree_exited():
	var my_seat = G.World.seats.get_node_or_null("Team_" + str(team) + "/Seat_" + str(seat))
	if my_seat == null:
		return
	my_seat.visible = false
	my_seat.set_collision_layer_bit(0, false)
	my_seat.set_collision_mask_bit(0, false)
	my_seat.set_collision_layer_bit(1, false)
	my_seat.set_collision_mask_bit(1, false)
	my_seat.set_collision_layer_bit(4, false)
	my_seat.set_collision_mask_bit(4, false)
	if G.Client != null:
		G.World.map.emit_signal("seat_status", my_seat.position, false)


func set_caption(val: String) -> void:
	$"Label".text = val


func _set_team(val: int) -> void:
	team = val
	emit_signal("changed_team", self, team)


func _ready() -> void:
	set_physics_process(false)
	caption = name


func _set_dead(val: bool) -> void:
	dead = val
	if dead:
		if control:
			last_death_position = position
		emit_signal("dead", self)
		var my_seat = G.World.seats.get_node_or_null(
					"Team_" + str(team) + "/Seat_" + str(seat))
		position = Vector2(0, -16384)
		if my_seat == null:
			yield(self, "changed_seat")
			my_seat = G.World.seats.get_node_or_null(
					"Team_" + str(team) + "/Seat_" + str(seat))
		position = my_seat.position
	else:
		emit_signal("resurrect", self)
	for i in col_shapes:
		get_node(i).call_deferred('set_disabled', val)


func resurrect() -> void:
	if G.GameState.state < 1: return
	self.dead = false


var prekill = false

func kill(by_who : String) -> void:
	if G.GameState.state < 1: return
	if self.dead == true: return
	
	prekill = true
	emit_signal("about_to_be_killed")
	
	self.dead = true
	prekill = false
	if G.Server != null:
		G.GameState.feed_event(GameState.FEED_KILL, [int(name), int(by_who)])


var control = false setget _set_control

func _set_control(val) -> void:
	control = val
	set_physics_process(val)
