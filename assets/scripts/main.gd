extends Node

signal start()

var target_fps = 60

func _ready() -> void:
	
	G.Main = self
	G.debug_print('MAIN')
	var args := OS.get_cmdline_args()
	G.debug_print(['CMD ARGS:', args])
	var cfg_scn_inst = load("res://assets/scenes/config.tscn").instance()
	call_deferred("add_child", cfg_scn_inst)
	yield(cfg_scn_inst, "ready")
	console_init()
	for arg in args:
		if arg.begins_with("--fps=") and int(arg.replace("--fps=", "")) > 0:
			target_fps = int(arg.replace("--fps=", ""))
		if arg.begins_with("--server-bench"):
			if arg.begins_with("--server-bench=") and int(arg.replace("--server-bench=", "")) > 0:
				create_server_bench(int(arg.replace("--server-bench=", "")))
			else:
				create_server_bench(Net.DEFAULT_SERVER_PORT)
		elif arg.begins_with("--server"): # --server=[port]
			if arg.begins_with("--server=") and int(arg.replace("--server=", "")) > 0:
				create_server(int(arg.replace("--server=", "")))
			else:
				create_server(Net.DEFAULT_SERVER_PORT)
	Engine.target_fps = target_fps


func update_config() -> void:
	# POSTPROCESSING
	if G.Config.CFG_GFX_POSTPROCESSING:
		$WorldEnvironment.environment = load("res://assets/post.tres")
	else:
		$WorldEnvironment.environment = null
	
	# LIGHTMAP
	G.World.get_node("LightMap").material.set_shader_param(
			"force", G.Config.CFG_GFX_LIGHTMAP_QUALITY)
	G.World.get_node("LightMap").material.set_shader_param(
			"res", 1.0 / G.Config.CFG_GFX_LIGHTMAP_QUALITY)
	G.World.get_node("LightMap").material.set_shader_param(
			"disableShade", G.Config.CFG_GFX_LIGHTMAP_NOSHADE)
	
	# FULLSCREEN
	OS.window_fullscreen = G.Config.CFG_GFX_FULLSCREEN
	
	# VSYNC
	OS.vsync_enabled = G.Config.CFG_GFX_VSYNC
	OS.vsync_via_compositor = G.Config.CFG_GFX_VSYNC_WINDOWS
	
	# SMOKE
	for i in G.Main.get_node("BG").get_children():
		if i.name.begins_with("smoke"):
			i.visible = G.Config.CFG_GFX_SMOKE
	get_node("FG").visible = G.Config.CFG_GFX_SMOKE
	
	# RENDER
	ProjectSettings.set_setting("rendering/quality/driver/driver_name", G.Config.CFG_GFX_VIDEO_DRIVER)
	ProjectSettings.save()


func a_connect_to_server(idx: int) -> Array:
	if idx == 0:
		return ["localhost", "homelessgunners.tk",
				"93.189.42.243", "80.211.159.106",
				"80.211.244.22"]
	elif idx == 1:
		return ["6666", "6667", "6668"]
	else:
		return []


func a_create_server(idx: int) -> Array:
	if idx == 0:
		return ["6666", "6667", "6668"]
	else:
		return []


func connect_to_server(	host = Net.DEFAULT_HOST,
						port = Net.DEFAULT_SERVER_PORT) -> void:
	if !get_tree().has_network_peer():
		add_child(load("res://assets/scenes/client.tscn").instance()) # это G.Client
		add_child(load("res://assets/scenes/ui.tscn").instance()) # это G.UI
		G.Client.host = host
		G.Client.port = int(port)
		G.Client.create_client(G.Client.host, G.Client.port)
		$Debug/Root/PerfMon.hide()
		update_config()
		emit_signal("start")
		G.Console._set_console_opened(true)
		$temp_splash/splash.hide()

func connect_to_server_bench(	host = Net.DEFAULT_HOST,
								port = Net.DEFAULT_SERVER_PORT) -> void:
	if !get_tree().has_network_peer():
		add_child(load("res://assets/scenes/benchmark/client.tscn").instance()) # это G.Client
		G.Client.host = host
		G.Client.port = int(port)
		G.Client.create_client(G.Client.host, G.Client.port)
		$Debug/Root/PerfMon.hide()
		update_config()
		G.Console._set_console_opened(true)
		$temp_splash/splash.hide()


func create_server(	port = Net.DEFAULT_SERVER_PORT,
					map_seed = null) -> void:
	add_child(load("res://assets/scenes/server.tscn").instance()) # это G.Server
	G.Server.port = int(port)
	G.Server.create_server(G.Server.port, G.Server.max_players, map_seed)
	$Debug/Root/PerfMon.show()
	G.World.get_node("LightMap").queue_free()
	G.Main.get_node("BG").queue_free()
	G.Config.CFG_GFX_POSTPROCESSING = false
	update_config()
	emit_signal("start")
	G.Console._set_console_opened(true)
	$temp_splash/splash.hide()


func create_server_bench(port = Net.DEFAULT_SERVER_PORT) -> void:
	add_child(load("res://assets/scenes/benchmark/server.tscn").instance())
	G.Server.port = int(port)
	G.Server.create_server(G.Server.port, G.Server.max_players)
	G.World.get_node("LightMap").queue_free()
	G.Main.get_node("BG").queue_free()
	G.Config.CFG_GFX_POSTPROCESSING = false
	update_config()
	G.Console._set_console_opened(true)
	$temp_splash/splash.hide()


func go_to_menu() -> void:
	$Debug/Root/PerfMon.hide()
	if G.Client != null:
		G.Client.queue_free()
	if G.UI != null:
		G.UI.queue_free()
	get_tree().network_peer = null
	$GameState.queue_free()
	$ModulesServer.queue_free()
	$FG.queue_free()
	$BG.queue_free()
	$World.queue_free()
	yield(get_tree(), "idle_frame")
	yield(get_tree(), "idle_frame")
	add_child(load("res://assets/scenes/default_state/GameState.tscn").instance())
	add_child(load("res://assets/scenes/default_state/ModulesServer.tscn").instance())
	add_child(load("res://assets/scenes/default_state/BG.tscn").instance())
	add_child(load("res://assets/scenes/default_state/FG.tscn").instance())
	add_child(load("res://assets/scenes/default_state/World.tscn").instance())
	G.Console._set_console_opened(false)
	$temp_splash/splash.show()
	

func console_init() -> void:
	G.Console.register_command(
			"connect_to_server", {
				desc   = "Connect to the server",
				args   = [2, "<host:string> <port:int>"],
				target = self,
				completer_target = self,
				completer_method = "a_connect_to_server"
			}
	)
	
	G.Console.register_command(
			"create_server", {
				desc   = "Create a server",
				args   = [2, "<port:int> <map_seed:int>"],
				target = self,
				completer_target = self,
				completer_method = "a_create_server"
			}
	)
	
	G.Console.register_command(
			"go_to_menu", {
				desc   = "Go to client start state",
				args   = [0],
				target = self
			}
	)
	
	G.Console.register_command(
			"create_server_bench", {
				desc   = "Create a benchmark server",
				args   = [0],
				target = self,
				completer_target = self,
				completer_method = "a_create_server"
			}
	)
	
	G.Console.register_command(
			"connect_to_server_bench", {
				desc   = "Connect to the benchmark server",
				args   = [2, "<host:string> <port:int>"],
				target = self,
				completer_target = self,
				completer_method = "a_connect_to_server"
			}
	)
	
	G.Console.handle_command("cmdlist")
	G.Console._set_console_opened(false)
