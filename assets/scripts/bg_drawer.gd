extends Node2D


var C_COUNT:	int		# изначальное кол-во кругов
var C_RANGE:	int		# диапазон размера круга (randi() % (C_RANGE - C_MIN) + C_MIN)
var C_MIN:		int		# минимальный размер круга
var X_RANGE:	int		# диапазон расположения кругов по оси X
var Y_RANGE:	int		# диапазон расположения кругов по оси Y
var BOX_CHC:	int		# (1-INF, НЕ РАВНО НУЛЮ) шанс появления коробки а не круга (больше - реже)

var C_COUNT_2:	int		# изначальное кол-во кругов (2 проход) (если == 0, то второго прохода не будет)
var C_RANGE_2:	int		# диапазон размера круга (randi() % (C_RANGE - C_MIN) + C_MIN) (2 проход)
var C_MIN_2:	int		# минимальный размер круга (2 проход)
var X_RANGE_2:	int		# диапазон расположения кругов по оси X (2 проход)
var Y_RANGE_2:	int		# диапазон расположения кругов по оси Y (2 проход)
var BOX_CHC_2:	int		# (1-INF, НЕ РАВНО НУЛЮ) шанс появления коробки а не круга (больше - реже) (2 проход)

var N_BIGM:		float	# множитель большого шума (меньше множитель - крупнее шум)
var N_BIGT:		float	# пороговое значение после которого нужно удалить тайл (-1.0 - 1.0) для крупного шума
var N_SMLM:		float	# множитель мелкого шума (больше множитель - мельче шум)
var N_SMLT:		float	# пороговое значение после которого нужно удалить тайл (-1.0 - 1.0) для мелкого шума
var BRG_TRS:	int		# попытки найти остров и построить от него мост
var BRG_MIN:	int		# минимальная длина моста
var BRG_MAX:	int		# максимальная длина моста

var LAB_BMI:	int		# минимальная кисть для генерации лаб
var LAB_BMA:	int		# максимальная кисть для генерации лаб
var LAB_TRS:	int		# количество лаб

var MEGU_CHC:	int		# шанс на мегумин (после прохождения шанса на точку)
var PINT_CHC:	int		# шанс на точку (шанс на начало детекта пов-ти под точку)

const MIN_DISTANCE = 128

export var size = Vector2(1280, 720)
export var count = 8

export var def_color = Color(0.1, 0.1, 0.1, 1)
export var bg_color = Color(0, 0, 0, 1)

export var MAX_TIMEOUT = 10
var timer = 0.0
var timeout = 0.0

var textures = {}
var explosions = {}

class MapInterface:
	var img: Image = null
	var def_color = Color()
	var bg_color = Color()
	var offset = Vector2(0, 0)
	
	var col = Color()
	
	func set_cell(x: int, y: int, id: int):
		if !offset == Vector2(0, 0):
			x += offset.x
			y += offset.y
			if x < 0 or y < 0 or x > offset.x * 2 or y > offset.y * 2:
				return
		match id:
			0: col = def_color
			1: col = bg_color
			-1: col = Color(0, 0, 0, 0)
		
		img.set_pixel(x, y, col)

	func get_cell(x: int, y: int):
		x+=offset.x
		y+=offset.y
		if x < 0 or y < 0 or x > offset.x * 2 or y > offset.y * 2:
			return -1
		if img.get_pixel(x, y).a < 1:
			return -1
		elif is_equal_approx(round(img.get_pixel(x, y).r * 10), def_color.r * 10):
			return 0
		elif is_equal_approx(round(img.get_pixel(x, y).r * 10), bg_color.r * 10):
			return 1




func select_gen_template() -> Vector2:
	var TEMPLATE_N = randi() % BG_GEN_TEMPLATES.TEMPLATES.size()
	var TEMPLATE = BG_GEN_TEMPLATES.TEMPLATES[TEMPLATE_N] as Dictionary
	var raw
	var cook
	
	for key in TEMPLATE.keys():
		raw = TEMPLATE[key] as String
		if raw.count(".") == 1:
			cook = float(raw)
		elif raw.count("-") == 1:
			raw = raw.split("-")
			cook = randi() % (int(raw[1]) - int(raw[0])) + int(raw[0])
		else:
			cook = int(raw)
		set(key, cook)
	
	var img_size = Vector2()
	
	var mx_range = 0
	var my_range = 0
	var mc_range = 0
	
	if X_RANGE > X_RANGE_2:
		mx_range = X_RANGE
	else:
		mx_range = X_RANGE_2
	if Y_RANGE > Y_RANGE_2:
		my_range = Y_RANGE
	else:
		my_range = Y_RANGE_2
	if C_RANGE > C_RANGE_2:
		mc_range = C_RANGE
	else:
		mc_range = C_RANGE_2
	
	img_size.x = mx_range + mc_range * 2
	img_size.y = my_range + mc_range * 2
	
	return img_size


func cook_island(img_size: Vector2) -> ImageTexture:
	var imgtex = ImageTexture.new()
	var img = Image.new()
	
	img.create(
		img_size.x, img_size.y,
		false, Image.FORMAT_RGBA8)
	
	var interface = MapInterface.new()
	interface.img = img
	interface.def_color = def_color
	interface.bg_color = bg_color
	interface.offset = img_size / 2 - Vector2(1, 1)
	img.lock()
	
	G.genlib_instance.generate(interface, randi(), C_COUNT,
		C_RANGE, C_MIN, X_RANGE, Y_RANGE, BOX_CHC,
		C_COUNT_2, C_RANGE_2, C_MIN_2, X_RANGE_2,
		Y_RANGE_2, BOX_CHC_2, N_BIGM, N_BIGT, N_SMLM,
		N_SMLT, BRG_TRS, BRG_MIN, BRG_MAX, LAB_BMI,
		LAB_BMA, LAB_TRS, MEGU_CHC, PINT_CHC, true)
	
	img.unlock()
	imgtex.create_from_image(img, 0)
	
	return imgtex


func _ready() -> void:
	set_process(false)


func generate() -> void:
	var img_size = select_gen_template()
	var cur_pos = Vector2(
			randi() % int(size.x) - img_size.x, 
			randi() % int(size.y) - img_size.y
	)
	var positions = []
	textures = {}
	explosions = {}
	for i in range(count):
		textures[cur_pos] = cook_island(img_size)
		positions.append(cur_pos)
		while true:
			img_size = select_gen_template()
			cur_pos = Vector2(
					randi() % int(size.x) - img_size.x, 
					randi() % int(size.y) - img_size.y
			)
			var fail = false
			for i in positions:
				if i.distance_to(cur_pos) < MIN_DISTANCE:
					fail = true
					break
			if !fail:
				break
	update()
	set_process(true)


func _draw() -> void:
	for i in textures.keys():
		draw_texture(textures[i], i - size / 2)
	for i in explosions.keys():
		draw_circle(i - size / 2, explosions[i], Color(1, 1, 1))


func explosion() -> void:
	var tex_key = textures.keys()[randi() % textures.keys().size()]
	var tex = (textures[tex_key] as ImageTexture)
	var img = tex.get_data()
	
	var pos = Vector2(
			clamp(randi() % int(tex.get_width()), 20, tex.get_width() - 20),
			clamp(randi() % int(tex.get_height()), 20, tex.get_height() - 20)
	)
	var radius = randi() % 6 + 4
	
	var interface = MapInterface.new()
	interface.img = img
	interface.def_color = def_color
	interface.bg_color = bg_color
	interface.offset = Vector2(0, 0)
	img.lock()
	explosions[pos + tex_key] = radius
	G.genlib_instance.circle(interface, pos.x, pos.y, -1, radius)
	img.unlock()
	tex.set_data(img)
	
	update()
	yield(get_tree().create_timer(0.1), "timeout")
	explosions.erase(pos + tex_key)
	update()


func _process(delta: float) -> void:
	timer -= delta
	if timer < 0:
		timer = randi() % MAX_TIMEOUT + 0.1
		explosion()
