extends Module
# Slave


func update_vars():
	t_name = "shotgun"
	d_name = Balance.SHOTGUN_NAME
	d_desc = Balance.SHOTGUN_DESC
	prices = Balance.SHOTGUN_PRICES


func _ready():
	update_vars()
	update_module()


func _exit_tree() -> void:
	weapon.visual.BOOST = ""
	weapon.visual.generate()
	weapon.SHOOT_RATE = Balance.W_DEF_SHOOT_RATE
	weapon.RELOAD_RATE = Balance.W_DEF_RELOAD_RATE
	weapon.SHOTS = Balance.FIRE_DEF_SHOTS
	weapon.SHOT_DELAY = Balance.FIRE_DEF_SHOT_DELAY
	weapon.AMMO_SIZE = Balance.W_DEF_AMMO_SIZE
	weapon.BASE_LVL = 0
	weapon.BASE_TYPE = -1
	
	weapon.ammo = weapon.AMMO * weapon.AMMO_SIZE
	weapon.recalc_damage()


func update_module():
	G.debug_print("[MODULE] " + name + " updated, lvl: " + str(level) + ", enabled: " + str(state))
	if state:
		if level < 3:
			weapon.visual.BOOST = "shotgun"
			weapon.visual.BOOST_LEVEL = "_" + str(level + 1)
			weapon.visual.generate()
			weapon.SHOOT_RATE = Balance.SHOTGUN_SHOOT_RATE_TABLE[level]
			weapon.RELOAD_RATE = Balance.SHOTGUN_RELOAD_RATE_TABLE[level]
			weapon.SHOTS = Balance.SHOTGUN_SHOTS_TABLE[level]
			weapon.SHOT_DELAY = Balance.SHOTGUN_SHOT_DELAY_TABLE[level]
			weapon.AMMO_SIZE = Balance.SHOTGUN_AMMO_SIZE
			weapon.BASE_LVL = level
			weapon.BASE_TYPE = 0
			
			weapon.ammo = weapon.AMMO * weapon.AMMO_SIZE
			weapon.recalc_damage()
	else:
		_exit_tree()
