extends Module
# Master

var _default_hp = Balance.PL_HITBOX_DEFAULT_HP


func update_vars():
	t_name     = "full"
	d_name     = Balance.FULL_NAME
	d_desc     = Balance.FULL_DESC
	prices     = Balance.FULL_PRICES
	exceptions = Balance.FULL_EXCEPTIONS

func _ready():
	update_vars()
	update_module()


onready var _visual_player_parts = [
		player.get_node("visual/body_lt"),
		player.get_node("visual/body_rt"),
		player.get_node("visual/body_rb"),
		player.get_node("visual/body_lb")
	]

func apply_hp(hp):
	for i in range(4):
		player.hitbox_default[i] = hp
		player.hitbox_max[i] = hp
	player.hitbox = player.hitbox_default.duplicate()
	for i in range(4):
		_visual_player_parts[i].get_node("hit").play("upgrade")


func _exit_tree() -> void:
	for i in range(4):
		player.hitbox_default[i] = _default_hp
		player.hitbox_max[i] = _default_hp
	player.hitbox = player.hitbox_default.duplicate()
	for i in range(4):
		_visual_player_parts[i].get_node("hit").play("upgrade")


func update_module():
	G.debug_print("[MODULE] " + name + " updated, lvl: " + str(level) + ", enabled: " + str(state))
	if state:
		process_exceptions()
	else:
		_exit_tree()
