extends Module
# Slave

const LASER_SCRIPT	= "res://assets/scripts/weapon/laser.gd"
const DEF_SCRIPT	= "res://assets/scripts/weapon/firearm.gd"


func update_vars():
	t_name = "laser"
	d_name = Balance.LASER_NAME
	d_desc = Balance.LASER_DESC
	prices = Balance.LASER_PRICES


func _ready():
	update_vars()
	update_module()


func _exit_tree() -> void:
	weapon.script = load(DEF_SCRIPT)
	weapon.visual = weapon.get_node("pivot/visual")
	weapon.visual.BASE = "firearm"
	weapon.visual.BASE_LEVEL = "_1"
	weapon.visual.CLIP = "firearm"
	weapon.visual.BARREL = "firearm"
	weapon.visual.BOLT = "bolt"
	if weapon.visual.RE_BOOST != "":
		weapon.visual.RE_BOOST = "firearm"
		weapon.RELOAD_RATE = Balance.W_DEF_RELOAD_RATE * Balance.RE_BOOST_RATE
	else:
		weapon.RELOAD_RATE = Balance.W_DEF_RELOAD_RATE
	weapon.visual.generate()
	weapon.AMMO_SIZE = Balance.W_DEF_AMMO_SIZE
	weapon.SHOOT_RATE = Balance.W_DEF_SHOOT_RATE
	weapon.RELOAD_RATE = Balance.W_DEF_RELOAD_RATE
	weapon.BASE_LVL = 0
	weapon.BASE_TYPE = -1
	
	weapon.ammo = weapon.AMMO * weapon.AMMO_SIZE
	weapon.owner_id = int(weapon.get_parent().name)
	weapon.get_node("grenade_shooter").owner_id = weapon.owner_id
	weapon.recalc_damage()


func update_module():
	G.debug_print("[MODULE] " + name + " updated, lvl: " + str(level) + ", enabled: " + str(state))
	if state:
		if level < 5:
			weapon.script = load(LASER_SCRIPT)
			if player.team == null:
				yield(player, "changed_team")
			match player.team:
				G.TEAM_1:
					weapon.VISUAL_COLOR = Color(1.0, 0.7, 0.7)
				G.TEAM_2:
					weapon.VISUAL_COLOR = Color(0.7, 0.7, 1.0)
			weapon.visual = weapon.get_node("pivot/visual")
			weapon.visual.BASE = "laser"
			weapon.visual.BASE_LEVEL = "_" + str(level + 1)
			weapon.visual.CLIP = "laser"
			weapon.visual.BARREL = "laser"
			weapon.visual.BOLT = ""
			if weapon.visual.RE_BOOST != "":
				weapon.visual.RE_BOOST = "laser"
				weapon.RELOAD_RATE = Balance.LASER_DEF_RELOAD_RATE * Balance.RE_BOOST_RATE
			else:
				weapon.RELOAD_RATE = Balance.LASER_DEF_RELOAD_RATE
			weapon.visual.generate()
			weapon.AMMO_SIZE = Balance.LASER_DEF_AMMO_SIZE
			weapon.SHOOT_RATE = Balance.LASER_DEF_SHOOT_RATE
			weapon.RELOAD_RATE = Balance.LASER_DEF_RELOAD_RATE
			weapon.BASE_LVL = level
			
			weapon.ammo = weapon.AMMO * weapon.AMMO_SIZE
			weapon.owner_id = int(weapon.get_parent().name)
			weapon.get_node("grenade_shooter").owner_id = weapon.owner_id
			weapon.recalc_damage()
			weapon.init()
	else:
		_exit_tree()
