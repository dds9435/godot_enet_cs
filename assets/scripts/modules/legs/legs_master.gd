extends Module
# Master

var MAX_SPEED_TABLE		= Balance.LEGS_MAX_SPEED_TABLE
var MAX_JUMPS_TABLE		= Balance.LEGS_MAX_JUMPS_TABLE
var MAX_JET_FUEL_TABLE	= Balance.LEGS_MAX_JET_FUEL_TABLE


func update_vars():
	t_name = "legs"
	d_name = Balance.LEGS_NAME
	d_desc = Balance.LEGS_DESC
	prices = Balance.LEGS_PRICES


func _ready():
	update_vars()
	update_module()


func _exit_tree() -> void:
	var _foot_left_tex = load(_graphics_path + _foot_prefix + "01" + _foot_left_postfix)
	var _foot_right_tex = load(_graphics_path + _foot_prefix + "01" + _foot_right_postfix)
	var _foot_d_tex = load(_graphics_path + _foot_prefix + "01" + _foot_d_postfix)
	if _foot_left_tex != null and _foot_right_tex != null and _foot_d_tex != null:
		player.get_node("visual/legs/left").texture = _foot_left_tex
		player.get_node("visual/legs/right").texture = _foot_right_tex
		player.get_node("visual/legs/left/dec").texture = _foot_d_tex
		player.get_node("visual/legs/right/dec").texture = _foot_d_tex
	
	
	player.MAX_SPEED	= Balance.PL_MAX_SPEED
	player.MAX_JUMPS	= Balance.PL_MAX_JUMPS
	player.MAX_JET_FUEL	= Balance.PL_MAX_JET_FUEL
	player.jet_fuel		= player.MAX_JET_FUEL


const _graphics_path		= "res://assets/graphics/chars/body/"
const _foot_prefix			= "foot"
const _foot_left_postfix	= "_l_b.png"
const _foot_right_postfix	= "_r_b.png"
const _foot_d_postfix		= "_d.png"

func update_module():
	G.debug_print("[MODULE] " + name + " updated, lvl: " + str(level) + ", enabled: " + str(state))
	if state:
		var _foot_left_tex = load(_graphics_path + _foot_prefix + str(level + 2).pad_zeros(2) + _foot_left_postfix)
		var _foot_right_tex = load(_graphics_path + _foot_prefix + str(level + 2).pad_zeros(2) + _foot_right_postfix)
		var _foot_d_tex = load(_graphics_path + _foot_prefix + str(level + 2).pad_zeros(2) + _foot_d_postfix)
		if _foot_left_tex != null and _foot_right_tex != null and _foot_d_tex != null:
			player.get_node("visual/legs/left").texture = _foot_left_tex
			player.get_node("visual/legs/right").texture = _foot_right_tex
			player.get_node("visual/legs/left/dec").texture = _foot_d_tex
			player.get_node("visual/legs/right/dec").texture = _foot_d_tex
			player.MAX_SPEED    = MAX_SPEED_TABLE[level]
			player.MAX_JUMPS    = MAX_JUMPS_TABLE[level]
			player.MAX_JET_FUEL = MAX_JET_FUEL_TABLE[level]
			player.jet_fuel		= player.MAX_JET_FUEL
			
	else:
		_exit_tree()
