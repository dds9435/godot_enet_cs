extends Module
# Slave


func update_vars():
	t_name = "barrel"
	d_name = Balance.BARREL_NAME
	d_desc = Balance.BARREL_DESC
	prices = Balance.BARREL_PRICES


func _ready():
	update_vars()
	update_module()


func _exit_tree() -> void:
	weapon.visual.BARREL_LEVEL = "_1"
	weapon.visual.generate()
	weapon.BARREL_LVL = 0
	weapon.recalc_damage()

func update_module():
	G.debug_print("[MODULE] " + name + " updated, lvl: " + str(level) + ", enabled: " + str(state))
	if state:
		if level < 8:
			weapon.visual.BARREL_LEVEL = "_" + str(level + 2)
			weapon.visual.generate()
			weapon.BARREL_LVL = level + 1
			weapon.recalc_damage()
	else:
		_exit_tree()
