extends Module
# Slave


func update_vars():
	t_name = "snipegun"
	d_name = Balance.SNIPEGUN_NAME
	d_desc = Balance.SNIPEGUN_DESC
	prices = Balance.SNIPEGUN_PRICES


func _ready():
	update_vars()
	update_module()


func _exit_tree() -> void:
	weapon.visual.BOOST = ""
	weapon.visual.generate()
	weapon.SHOOT_RATE = Balance.W_DEF_SHOOT_RATE
	weapon.RELOAD_RATE = Balance.W_DEF_RELOAD_RATE
	weapon.AMMO_SIZE = Balance.W_DEF_AMMO_SIZE
	weapon.PENETRATE = Balance.FIRE_DEF_PENETRATE
	weapon.BASE_LVL = 0
	weapon.BASE_TYPE = -1
	
	weapon.ammo = weapon.AMMO * weapon.AMMO_SIZE
	weapon.recalc_damage()


func update_module():
	G.debug_print("[MODULE] " + name + " updated, lvl: " + str(level) + ", enabled: " + str(state))
	if state:
		if level < 3:
			weapon.visual.BOOST = "snipegun"
			weapon.visual.BOOST_LEVEL = "_" + str(level + 1)
			weapon.visual.generate()
			weapon.SHOOT_RATE = Balance.SNIPEGUN_SHOOT_RATE_TABLE[level]
			weapon.RELOAD_RATE = Balance.SNIPEGUN_RELOAD_RATE_TABLE[level]
			weapon.AMMO_SIZE = Balance.SNIPEGUN_AMMO_SIZE
			weapon.PENETRATE = Balance.SNIPEGUN_PENETRATE
			weapon.BASE_LVL = level
			weapon.BASE_TYPE = 1
			
			weapon.ammo = weapon.AMMO * weapon.AMMO_SIZE
			weapon.recalc_damage()
			
	else:
		_exit_tree()
