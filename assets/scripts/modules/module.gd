extends Node
class_name Module


onready var player: Player	= $"../.."
onready var weapon: Weapon	= $"../../weapon"


var t_name		= ""
var d_name		= "Default Module"
var d_desc		= "A useless base default module"
var prices		= [10, 20]
var level		= 0
var temp_level	= 0
var state		= true
var exceptions	= []


func process_exceptions():
	for m_name in exceptions:
		player.module_state(m_name, true, false)


func _ready():
	G.debug_print("[MODULE] " + name + " module on player " +
			str(player.caption) + " (" + player.name + ") loaded.")
