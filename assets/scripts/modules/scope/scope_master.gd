extends Module
# Master


var _pointer: RayCast2D = null
var _pointer_line: Line2D = null
var _weapon_pivot = null


func update_vars():
	t_name = "scope"
	d_name = Balance.SCOPE_NAME
	d_desc = Balance.SCOPE_DESC
	prices = Balance.SCOPE_PRICES


func _ready():
	update_vars()
	update_module()


var _colpoint = null
var _lookpos = Vector2()
var _last_vis = true

func _process(delta: float) -> void:
	if _pointer != null and _pointer_line != null:
		if _pointer.is_colliding():
			_colpoint = _pointer.get_collision_point()
		else:
			_colpoint = null
		if _colpoint:
			_pointer_line.points[1] = Vector2(_pointer.global_position.distance_to(_colpoint), 0)
		else:
			_pointer_line.points[1] = Vector2(16384, 0)
	
	if player.control:
		if (Input.is_action_just_pressed("scope") and
				is_instance_valid(G.UI) and
				!G.UI.block_input and !player.dead):
			_lookpos = weapon.get_global_mouse_position()
		if (Input.is_action_pressed("scope") and
				is_instance_valid(G.UI) and
				!G.UI.block_input and !player.dead):
			player.block_camera = true
			weapon.block_lookat = true
			player.get_node("Camera2D").smoothing_speed = 5
			if _weapon_pivot.rotation == 0:
				if _last_vis != true:
					_pointer_line.show()
					if G.Client != null:
						G.Client.send_set(_pointer_line.get_path(), "visible", true, Net.BROADCAST)
					_last_vis = true
			else:
				if _last_vis != false:
					_pointer_line.hide()
					if G.Client != null:
						G.Client.send_set(_pointer_line.get_path(), "visible", false, Net.BROADCAST)
					_last_vis = false
			if _colpoint != null and _weapon_pivot.rotation == 0:
				player.get_node("Camera2D").global_position = _colpoint
			_lookpos = _lookpos.move_toward(
					weapon.get_global_mouse_position(),
					32)
			weapon.look_at(_lookpos)
		else:
			if _last_vis != false:
				_pointer_line.hide()
				if G.Client != null:
					G.Client.send_set(_pointer_line.get_path(), "visible", false, Net.BROADCAST)
				_last_vis = false
			weapon.block_lookat = false
			player.block_camera = false
	else:
		if _last_vis != false:
			_pointer_line.hide()
			if G.Client != null:
				G.Client.send_set(_pointer_line.get_path(), "visible", false, Net.BROADCAST)
			_last_vis = false
		weapon.block_lookat = false
		player.block_camera = false


func _exit_tree() -> void:
	player.block_camera = false
	weapon.block_lookat = false
	weapon.visual.SCOPE = ""
	weapon.visual.generate()


func update_module():
	G.debug_print("[MODULE] " + name + " updated, lvl: " + str(level) + ", enabled: " + str(state))
	if state:
		_pointer = weapon.get_node("pivot/pointer")
		_pointer_line = weapon.get_node("pivot/pointer/line")
		_weapon_pivot = weapon.get_node("pivot")
		weapon.visual.SCOPE = "scope"
		weapon.visual.generate()
	else:
		_exit_tree()
	set_process(state)
