extends Module
# Slave


var _pointer: RayCast2D = null
var _pointer_line: Line2D = null


func update_vars():
	t_name = "scope"
	d_name = Balance.SCOPE_NAME
	d_desc = Balance.SCOPE_DESC
	prices = Balance.SCOPE_PRICES


func _ready():
	update_vars()
	update_module()


var _colpoint = null

func _physics_process(delta: float) -> void:
	if _pointer != null and _pointer_line != null:
		if _pointer.is_colliding():
			_colpoint = _pointer.get_collision_point()
		else:
			_colpoint = null
		if _colpoint:
			_pointer_line.points[1] = Vector2(_pointer.global_position.distance_to(_colpoint), 0)
		else:
			_pointer_line.points[1] = Vector2(16384, 0)


func _exit_tree() -> void:
	weapon.visual.SCOPE = ""
	weapon.visual.generate()


func update_module():
	G.debug_print("[MODULE] " + name + " updated, lvl: " + str(level) + ", enabled: " + str(state))
	if state:
		_pointer = weapon.get_node("pivot/pointer")
		_pointer_line = weapon.get_node("pivot/pointer/line")
		weapon.visual.SCOPE = "scope"
		weapon.visual.generate()
	else:
		_exit_tree()
	set_physics_process(state)
