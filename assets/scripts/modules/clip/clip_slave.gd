extends Module
# Slave


func update_vars():
	t_name = "clip"
	d_name = Balance.CLIP_NAME
	d_desc = Balance.CLIP_DESC
	prices = Balance.CLIP_PRICES


func _ready():
	update_vars()
	update_module()


func _exit_tree() -> void:
	weapon.visual.CLIP_LEVEL = "_1"
	weapon.visual.generate()
	weapon.AMMO = Balance.W_DEF_AMMO
	weapon.ammo = weapon.AMMO * weapon.AMMO_SIZE


func update_module():
	G.debug_print("[MODULE] " + name + " updated, lvl: " + str(level) + ", enabled: " + str(state))
	if state:
		if level < 8:
			weapon.visual.CLIP_LEVEL = "_" + str(level + 2)
			weapon.visual.generate()
			weapon.AMMO = Balance.CLIP_TABLE[level]
			weapon.ammo = weapon.AMMO * weapon.AMMO_SIZE
	else:
		_exit_tree()
