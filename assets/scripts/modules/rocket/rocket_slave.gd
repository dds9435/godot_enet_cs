extends Module
# Slave

const ROCKET_SCRIPT	= "res://assets/scripts/weapon/rocket.gd"
const DEF_SCRIPT	= "res://assets/scripts/weapon/firearm.gd"


func update_vars():
	t_name = "rocket"
	d_name = Balance.ROCKET_NAME
	d_desc = Balance.ROCKET_DESC
	prices = Balance.ROCKET_PRICES


func _ready():
	update_vars()
	update_module()


func _exit_tree() -> void:
	weapon.script = load(DEF_SCRIPT)
	weapon.visual = weapon.get_node("pivot/visual")
	weapon.visual.BASE = "firearm"
	weapon.visual.BASE_LEVEL = "_1"
	weapon.visual.CLIP = "firearm"
	weapon.visual.BARREL = "firearm"
	weapon.visual.BOLT = "bolt"
	if weapon.visual.RE_BOOST != "":
		weapon.visual.RE_BOOST = "firearm"
		weapon.RELOAD_RATE = Balance.W_DEF_RELOAD_RATE * Balance.RE_BOOST_RATE
	else:
		weapon.RELOAD_RATE = Balance.W_DEF_RELOAD_RATE
	weapon.visual.generate()
	weapon.AMMO_SIZE = Balance.W_DEF_AMMO_SIZE
	weapon.SHOOT_RATE = Balance.W_DEF_SHOOT_RATE
	weapon.RELOAD_RATE = Balance.W_DEF_RELOAD_RATE
	
	weapon.ammo = weapon.AMMO * weapon.AMMO_SIZE
	weapon.owner_id = int(weapon.get_parent().name)
	weapon.get_node("grenade_shooter").owner_id = weapon.owner_id
	weapon.recalc_damage()


func update_module():
	G.debug_print("[MODULE] " + name + " updated, lvl: " + str(level) + ", enabled: " + str(state))
	if state:
		if level < 5:
			weapon.script = load(ROCKET_SCRIPT)
			weapon.visual = weapon.get_node("pivot/visual")
			weapon.visual.BASE = "rocket"
			weapon.visual.BASE_LEVEL = "_1"
			weapon.visual.CLIP = "rocket"
			weapon.visual.BARREL = "rocket"
			weapon.visual.BOLT = ""
			if weapon.visual.RE_BOOST != "":
				weapon.visual.RE_BOOST = "rocket"
				weapon.RELOAD_RATE = Balance.ROCKET_DEF_RELOAD_RATE * Balance.RE_BOOST_RATE
			else:
				weapon.RELOAD_RATE = Balance.ROCKET_DEF_RELOAD_RATE
			weapon.visual.generate()
			weapon.AMMO_SIZE = Balance.ROCKET_DEF_AMMO_SIZE
			weapon.SHOOT_RATE = Balance.ROCKET_DEF_SHOOT_RATE
			
			weapon.ammo = weapon.AMMO * weapon.AMMO_SIZE
			weapon.owner_id = int(weapon.get_parent().name)
			weapon.get_node("grenade_shooter").owner_id = weapon.owner_id
			weapon.recalc_damage()
	else:
		_exit_tree()
