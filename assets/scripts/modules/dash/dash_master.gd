extends Module
# Master


var DASHES			= 0
var DASH_DISTANCE	= 0
var DASH_RATE		= 0
var DASH_RELO		= 0


var rate_timer = 0.0
var relo_timer = 0.0
var dash_count = 0


func update_vars():
	t_name = "dash"
	d_name = Balance.DASH_NAME
	d_desc = Balance.DASH_DESC
	prices = Balance.DASH_PRICES


func _ready():
	update_vars()
	update_module()


func dash(distance: int):
	
	var side = player.get_node("visual").scale.x
	if Input.is_action_pressed("move_left"):
		side = -1
	elif Input.is_action_pressed("move_right"):
		side = 1
	
	var dash_effect_scn = preload("res://assets/scenes/dash_effect.tscn").instance()
	dash_effect_scn.position = player.position
	dash_effect_scn.points[1].x = distance
	dash_effect_scn.get_node("shader").points[1].x = distance
	dash_effect_scn.scale.x = side
	G.World.add_child(dash_effect_scn)
	
	if G.Client != null:
		G.Client.send_call(get_path(), "dash", 
				[player.position, distance, side], 
				Net.BROADCAST)
	
	player.position.x += distance * side
	


func _process(delta: float) -> void:
	if player.control:
		
		if rate_timer > 0:
			rate_timer -= delta
			if rate_timer < 0:
				rate_timer = 0.0
			G.UI.emit_signal("dash_rate", 1.0 - rate_timer / DASH_RATE)
		
		if dash_count < DASHES and relo_timer < DASH_RELO:
			relo_timer += delta
			if relo_timer > DASH_RELO:
				relo_timer = 0
				dash_count += 1
				G.UI.emit_signal("dash_count", dash_count)
			G.UI.emit_signal("dash_relo", relo_timer / DASH_RELO)
		
		if (Input.is_action_just_pressed("dash") and
				rate_timer == 0.0 and dash_count > 0 and
				is_instance_valid(G.UI) and
				!G.UI.block_input and !player.dead):
			var can_dash = false
			var distance = DASH_DISTANCE
			for i in range(DASH_DISTANCE / 8):
				if !player.test_move(
				player.transform.translated(
					Vector2((DASH_DISTANCE - i * 8) * player.get_node("visual").scale.x, 0)
				), Vector2(0,0)):
					can_dash = true
					distance = DASH_DISTANCE - i * 8
					break
			if !can_dash: return
			dash_count -= 1
			rate_timer = DASH_RATE
			relo_timer = 0
			dash(distance)


func _exit_tree() -> void:
	G.UI.emit_signal("dash_state", false)


func update_module():
	G.debug_print("[MODULE] " + name + " updated, lvl: " + str(level) + ", enabled: " + str(state))
	if state:
		DASHES = Balance.DASH_DASHES_TABLE[level]
		DASH_DISTANCE = Balance.DASH_DISTANCE_TABLE[level]
		DASH_RATE = Balance.DASH_RATE_TABLE[level]
		DASH_RELO = Balance.DASH_RELO_TABLE[level]
		dash_count = DASHES
		G.UI.emit_signal("dash_state", true)
		G.UI.emit_signal("dash_relo", relo_timer / DASH_RELO)
		G.UI.emit_signal("dash_rate", 1.0 - rate_timer / DASH_RATE)
		G.UI.emit_signal("dash_count", dash_count)
	set_process(state)
