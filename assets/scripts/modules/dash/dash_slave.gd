extends Module
# Slave


func update_vars():
	t_name = "dash"
	d_name = Balance.DASH_NAME
	d_desc = Balance.DASH_DESC
	prices = Balance.DASH_PRICES


func _ready():
	update_vars()
	update_module()


func dash(pos, length, side):
	var dash_effect_scn = preload("res://assets/scenes/dash_effect.tscn").instance()
	dash_effect_scn.position = pos
	dash_effect_scn.points[1].x = length
	dash_effect_scn.get_node("shader").points[1].x = length
	dash_effect_scn.scale.x = side
	G.World.add_child(dash_effect_scn)


func update_module():
	G.debug_print("[MODULE] " + name + " updated, lvl: " + str(level) + ", enabled: " + str(state))
