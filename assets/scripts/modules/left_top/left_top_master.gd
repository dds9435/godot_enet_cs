extends Module
# Master

var _default_hp = Balance.PL_HITBOX_DEFAULT_HP
onready var _hitbox_id = player.HT_LT


func update_vars():
	t_name = "left_top"
	d_name = Balance.LEFT_TOP_NAME
	d_desc = Balance.LEFT_TOP_DESC
	prices = Balance.LEFT_TOP_PRICES
	exceptions = Balance.LEFT_TOP_EXCEPTIONS


func _ready():
	match player.team:
		G.TEAM_1:
			_hitbox_id = player.HT_LT
			_visual_player_part = player.get_node("visual/body_lt")
		G.TEAM_2:
			_hitbox_id = player.HT_RT
			_visual_player_part = player.get_node("visual/body_rt")
	update_vars()
	update_module()


onready var _visual_player_part = player.get_node("visual/body_lt")

func apply_hp(hp):
	player.hitbox_default[_hitbox_id] = hp
	player.hitbox_max[_hitbox_id] = hp
	player.hitbox = player.hitbox_default.duplicate()
	_visual_player_part.get_node("hit").play("upgrade")


func _exit_tree() -> void:
	player.hitbox_default[_hitbox_id] = _default_hp
	player.hitbox_max[_hitbox_id] = _default_hp
	player.hitbox = player.hitbox_default.duplicate()
	_visual_player_part.get_node("hit").play("upgrade")


func update_module():
	G.debug_print("[MODULE] " + name + " updated, lvl: " + str(level) + ", enabled: " + str(state))
	if state:
		process_exceptions()
	else:
		_exit_tree()
