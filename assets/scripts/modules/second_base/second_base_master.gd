extends Module
# Master


func update_vars():
	t_name = "second_base"
	d_name = Balance.SECOND_BASE_NAME
	d_desc = Balance.SECOND_BASE_DESC
	prices = Balance.SECOND_BASE_PRICES
	exceptions = Balance.SECOND_BASE_EXCEPTIONS


func _ready():
	update_vars()
	update_module()


func _exit_tree() -> void:
	weapon.visual.POSTFIX = ""
	weapon.visual.generate()


func update_module():
	G.debug_print("[MODULE] " + name + " updated, lvl: " + str(level) + ", enabled: " + str(state))
	if state:
		process_exceptions()
		weapon.visual.POSTFIX = "_s"
		weapon.visual.generate()
		if weapon.has_method("init"):
			weapon.init()
	else:
		_exit_tree()
