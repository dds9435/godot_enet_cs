extends Module
# Slave

var _is_init = false


func update_vars():
	t_name = "stealth"
	d_name = Balance.STEALTH_NAME
	d_desc = Balance.STEALTH_DESC
	prices = Balance.STEALTH_PRICES


func _ready():
	update_vars()
	
	player.get_node("stealth_anim").connect("animation_finished", self, "_on_stealth_animation_finished")
	
	update_module()


func _exit_tree() -> void:
	player.get_node("visual/heart").texture = load("res://assets/graphics/chars/body/heart01_b.png")
	player.get_node("visual/heart/dec").texture = load("res://assets/graphics/chars/body/heart01_d.png")
	player.get_node("visual/heart/progress").hide()


func stealth_state(st):
	player.get_node("glitch_mask").set_physics_process(st)
	if st:
		player.get_node("stealth_anim").play("on")
		player.get_node("glitch_mask").set_physics_process(st)
	else:
		player.get_node("stealth_anim").play("off")


func _on_stealth_animation_finished(anim):
	if anim == "on":
		player.hide_from_map = true
	elif anim == "off":
		player.hide_from_map = false
		player.get_node("glitch_mask").set_physics_process(false)


func update_module():
	G.debug_print("[MODULE] " + name + " updated, lvl: " + str(level) + ", enabled: " + str(state))
	if state:
		if !_is_init:
			player.get_node("visual/heart").texture = load("res://assets/graphics/chars/body/heart02_b.png")
			player.get_node("visual/heart/dec").texture = load("res://assets/graphics/chars/body/heart02_d.png")
			player.get_node("visual/heart/progress").show()
			player.get_node("visual/heart/progress").value = 100
			player.get_node("visual/heart/progress").modulate = Color(1, 1, 1, 1)
			_is_init = false
		player.get_node("visual/heart/hit").play("upgrade")
	else:
		_exit_tree()
		_is_init = false
