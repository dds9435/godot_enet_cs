extends Module
# Master

var _is_init = false


var STEALTH_TIMEOUT = 0


var stealth_active = false


func update_vars():
	t_name = "stealth"
	d_name = Balance.STEALTH_NAME
	d_desc = Balance.STEALTH_DESC
	prices = Balance.STEALTH_PRICES


func _ready():
	update_vars()
	
	player.connect("me_hitted", self, "stealth_state", [false])
	player.get_node("stealth_anim").connect("animation_finished", self, "_on_stealth_animation_finished")
	
	update_module()


func _exit_tree() -> void:
	player.get_node("visual/heart").texture = load("res://assets/graphics/chars/body/heart01_b.png")
	player.get_node("visual/heart/dec").texture = load("res://assets/graphics/chars/body/heart01_d.png")
	player.get_node("visual/heart/progress").hide()
	G.UI.emit_signal("stealth_state", false)


func stealth_state(st):
	if G.Client != null:
		G.Client.send_call(get_path(), "stealth_state", 
				[st], 
				Net.BROADCAST)
	if st:
		if !stealth_active:
			player.get_node("stealth_anim").play("on")
			player.get_node("glitch_mask").set_physics_process(true)
	else:
		if stealth_active:
			player.get_node("stealth_anim").play("off")
	stealth_active = st


func _on_stealth_animation_finished(anim):
	if anim == "on":
		player.hide_from_map = true
	elif anim == "off":
		player.hide_from_map = false
		player.get_node("glitch_mask").set_process(false)


var stealth_timer = 0.0
onready var progress = player.get_node("visual/heart/progress") as TextureProgress
onready var mask = player.get_node("glitch_mask")

func _process(delta: float) -> void:
	if (Input.is_action_just_pressed("stealth") and
			!player.dead and player.control and
			is_instance_valid(G.UI) and !G.UI.block_input):
		stealth_state(!stealth_active)
	if Input.is_action_just_pressed("lmb") or Input.is_action_just_pressed("rmb"):
		if stealth_active:
			stealth_state(!stealth_active)
	
	
	if stealth_active:
		mask.TIMEOUT = 0.01 + (1 - stealth_timer / STEALTH_TIMEOUT) * 0.1
		if G.Client != null:
			G.Client.send_set(mask.get_path(), "TIMEOUT", mask.TIMEOUT, Net.BROADCAST)
	progress.value = (stealth_timer / STEALTH_TIMEOUT) * 100
	
	
	if stealth_active:
		stealth_timer -= delta
		if stealth_timer < 0:
			stealth_timer = 0
			stealth_state(false)
		G.UI.emit_signal("stealth_value", stealth_timer / STEALTH_TIMEOUT)
	else:
		if stealth_timer < STEALTH_TIMEOUT:
			stealth_timer += delta
			if stealth_timer > STEALTH_TIMEOUT:
				stealth_timer = STEALTH_TIMEOUT
			G.UI.emit_signal("stealth_value", stealth_timer / STEALTH_TIMEOUT)


func update_module():
	G.debug_print("[MODULE] " + name + " updated, lvl: " + str(level) + ", enabled: " + str(state))
	if state:
		if !_is_init:
			player.get_node("visual/heart").texture = load("res://assets/graphics/chars/body/heart02_b.png")
			player.get_node("visual/heart/dec").texture = load("res://assets/graphics/chars/body/heart02_d.png")
			player.get_node("visual/heart/progress").show()
			player.get_node("visual/heart/progress").modulate = Color(1, 1, 1, 2)
			_is_init = true
		player.get_node("visual/heart/hit").play("upgrade")
		STEALTH_TIMEOUT = Balance.STEALTH_TIMEOUT_TABLE[level]
		stealth_timer = STEALTH_TIMEOUT
		G.UI.emit_signal("stealth_value",  1.0)
		G.UI.emit_signal("stealth_state", true)
	else:
		_exit_tree()
		_is_init = false
	set_process(state)
