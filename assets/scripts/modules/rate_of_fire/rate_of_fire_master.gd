extends Module
# Master


func update_vars():
	t_name = "rate_of_fire"
	d_name = Balance.RATE_OF_FIRE_NAME
	d_desc = Balance.RATE_OF_FIRE_DESC
	prices = Balance.RATE_OF_FIRE_PRICES
	exceptions = Balance.RATE_OF_FIRE_EXCEPTIONS



func _ready():
	update_vars()
	update_module()


func _exit_tree() -> void:
	weapon.visual.BOOST = ""
	weapon.visual.generate()
	weapon.SHOOT_RATE = Balance.W_DEF_SHOOT_RATE


func update_module():
	G.debug_print("[MODULE] " + name + " updated, lvl: " + str(level) + ", enabled: " + str(state))
	if state:
		process_exceptions()
		if level < 8:
			weapon.visual.BOOST = "boost"
			weapon.visual.BOOST_LEVEL = "_" + str(level + 1)
			weapon.visual.generate()
			weapon.SHOOT_RATE = Balance.RATE_OF_FIRE_TABLE[level]
	else:
		_exit_tree()
