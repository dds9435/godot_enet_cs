extends Module
# Slave


func update_vars():
	t_name = "re_boost"
	d_name = Balance.RE_BOOST_NAME
	d_desc = Balance.RE_BOOST_DESC
	prices = Balance.RE_BOOST_PRICES


func _ready():
	update_vars()
	update_module()


func _exit_tree() -> void:
	weapon.visual.RE_BOOST = ""
	weapon.visual.generate()
	match weapon.visual.BASE:
		"firearm":
			var boost_lvl = 0
			if weapon.visual.BOOST_LEVEL != "":
				boost_lvl = int(weapon.visual.BOOST_LEVEL.replace("_", "")) - 1
			match weapon.visual.BOOST:
				"shotgun":
					weapon.RELOAD_RATE = Balance.SHOTGUN_RELOAD_RATE_TABLE[boost_lvl]
				"snipegun":
					weapon.RELOAD_RATE = Balance.SNIPEGUN_RELOAD_RATE_TABLE[boost_lvl]
				_:
					weapon.RELOAD_RATE = Balance.W_DEF_RELOAD_RATE
		"laser":
			weapon.RELOAD_RATE = Balance.LASER_DEF_RELOAD_RATE
		"rocket":
			weapon.RELOAD_RATE = Balance.ROCKET_DEF_RELOAD_RATE


func update_module():
	G.debug_print("[MODULE] " + name + " updated, lvl: " + str(level) + ", enabled: " + str(state))
	if state:
		weapon.visual.RE_BOOST = weapon.visual.BASE
		weapon.visual.generate()
		match weapon.visual.BASE:
			"firearm":
				var boost_lvl = 0
				if weapon.visual.BOOST_LEVEL != "":
					boost_lvl = int(weapon.visual.BOOST_LEVEL.replace("_", "")) - 1
				match weapon.visual.BOOST:
					"shotgun":
						weapon.RELOAD_RATE = Balance.SHOTGUN_RELOAD_RATE_TABLE[boost_lvl] * Balance.RE_BOOST_RATE
					"snipegun":
						weapon.RELOAD_RATE = Balance.SNIPEGUN_RELOAD_RATE_TABLE[boost_lvl] * Balance.RE_BOOST_RATE
					_:
						weapon.RELOAD_RATE = Balance.W_DEF_RELOAD_RATE * Balance.RE_BOOST_RATE
			"laser":
				weapon.RELOAD_RATE = Balance.LASER_DEF_RELOAD_RATE * Balance.RE_BOOST_RATE
			"rocket":
				weapon.RELOAD_RATE = Balance.ROCKET_DEF_RELOAD_RATE * Balance.RE_BOOST_RATE
	else:
		_exit_tree()
