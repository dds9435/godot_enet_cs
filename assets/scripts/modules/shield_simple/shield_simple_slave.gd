extends Module
# Slave

var REGEN_TIMEOUT	= 0
var MAX_HP			= 0

var _is_init = false

onready var hp = MAX_HP
var enabled = true


func update_vars():
	t_name = "shield_simple"
	d_name = Balance.SHIELD_SIMPLE_NAME
	d_desc = Balance.SHIELD_SIMPLE_DESC
	prices = Balance.SHIELD_SIMPLE_PRICES


func _ready():
	update_vars()
	update_module()


var timer = 0.0


func _physics_process(delta: float) -> void:
	if timer > 0:
		timer -= delta
		if timer < 0:
			timer = 0.0
			if !enabled:
				G.Server.send_call(player.get_node("visual/shield_simple").get_path(), "state", 
						[true], 
						Net.BROADCAST)
				enabled = true
				player.get_node("visual/shield_simple/col").disabled = false


func on_shield_hit(point, damage):
	if G.Server != null:
		G.Server.send_call(get_path(), "on_shield_hit", 
				[point, damage], 
				Net.BROADCAST)
		hp -= damage
		if int(hp) <= 0:
			if enabled:
				G.Server.send_call(player.get_node("visual/shield_simple").get_path(), "state", 
						[false], 
						Net.BROADCAST)
				enabled = false
				player.get_node("visual/shield_simple/col").disabled = true
				timer = REGEN_TIMEOUT
			hp = 0
	else:
		player.get_node("visual/shield_simple").hit_anim(point, damage)


func _exit_tree() -> void:
	if player.has_node("visual/shield_simple"):
		player.get_node("visual/shield_simple/anim").play("exit")


func update_module():
	G.debug_print("[MODULE] " + name + " updated, lvl: " + str(level) + ", enabled: " + str(state))
	if state:
		if !_is_init:
			player.get_node("visual").add_child(preload("res://assets/scenes/shield_simple.tscn").instance())
			player.get_node("visual/shield_simple").connect("hit", self, "on_shield_hit")
			player.weapon.add_exception("visual/shield_simple")
			_is_init = true
		elif player.has_node("visual/shield_simple"):
			player.get_node("visual/shield_simple/anim").play("on")
		if G.Server != null:
			player.get_node("visual/shield_simple").callback = self
			set_physics_process(true)
		else:
			set_physics_process(false)
		REGEN_TIMEOUT = Balance.SMPL_REGEN_TIMEOUT_TABLE[level]
		MAX_HP = Balance.SMPL_MAX_HP_TABLE[level]
	else:
		_exit_tree()
		set_physics_process(false)
