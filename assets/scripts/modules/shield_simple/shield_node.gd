extends Area2D

signal hit(point)

var covered_bodies = []
var callback = null

func _ready() -> void:
	if G.Server != null:
		connect("area_entered", self, "_on_area_entered")
		connect("body_entered", self, "_on_body_entered")
		connect("area_exited", self, "_on_area_exited")
		connect("body_exited", self, "_on_body_exited")
	$tex.material = $tex.material.duplicate()
	match get_node("../..").team:
		G.TEAM_1:
			$tex.material.set_shader_param("color", Vector3(1.0, 0.15, 0.15))
		G.TEAM_2:
			$tex.material.set_shader_param("color", Vector3(0.15, 0.15, 1.0))

func state(enabled):
	if enabled:
		$anim.play("on")
	else:
		$anim.play("off")

func hit_anim(point, damage):
	$tw.remove($tex.material, "shader_param/hitForce")
	var pos = ((Vector2.LEFT.rotated(global_position.angle_to_point(point)) * Vector2(get_parent().scale.x, 1) + Vector2(1, 1)) / 2)
	$tex.material.set_shader_param("hitX", pos.x)
	$tex.material.set_shader_param("hitY", pos.y)
	$tw.interpolate_property($tex.material, "shader_param/hitForce", clamp(float(damage) / 100, 0.2, INF), 0.0, 0.15, Tween.TRANS_CIRC, Tween.EASE_OUT)
	$tw.start()

func hit(point, damage, owner_id):
	emit_signal("hit", point, damage)

func get_obj_hp():
	if G.Server != null:
		if callback != null:
			return callback.hp
		else:
			return 0

func get_covered_bodies():
	return covered_bodies

func _on_area_entered(area: Area2D):
	covered_bodies.append(area)

func _on_body_entered(body: Node):
	covered_bodies.append(body)

func _on_area_exited(area: Area2D):
	covered_bodies.erase(area)

func _on_body_exited(body: Node):
	covered_bodies.erase(body)
