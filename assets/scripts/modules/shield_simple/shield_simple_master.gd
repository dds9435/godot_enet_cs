extends Module
# Master

var _is_init = false


func update_vars():
	t_name = "shield_simple"
	d_name = Balance.SHIELD_SIMPLE_NAME
	d_desc = Balance.SHIELD_SIMPLE_DESC
	prices = Balance.SHIELD_SIMPLE_PRICES
	exceptions = Balance.SHIELD_SIMPLE_EXCEPTIONS


func _ready():
	update_vars()
	update_module()


func on_shield_hit(point, damage):
	player.get_node("visual/shield_simple").hit_anim(point, damage)


func _exit_tree() -> void:
	if player.has_node("visual/shield_simple"):
		player.get_node("visual/shield_simple/anim").play("exit")


func update_module():
	G.debug_print("[MODULE] " + name + " updated, lvl: " + str(level) + ", enabled: " + str(state))
	if state:
		process_exceptions()
		if !_is_init:
			player.get_node("visual").add_child(preload("res://assets/scenes/shield_simple.tscn").instance())
			player.get_node("visual/shield_simple").connect("hit", self, "on_shield_hit")
			player.weapon.add_exception("visual/shield_simple")
			_is_init = true
		elif player.has_node("visual/shield_simple"):
			player.get_node("visual/shield_simple/anim").play("on")
	else:
		_exit_tree()
