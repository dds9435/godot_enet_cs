extends Module
# Slave

var _default_hp = Balance.PL_HITBOX_DEFAULT_HP
onready var _hitbox_id = player.HT_RT


var RATE = Balance.SINGLE_HT_RATE


func update_vars():
	t_name = "right_top"
	d_name = Balance.RIGHT_TOP_NAME
	d_desc = Balance.RIGHT_TOP_DESC
	prices = Balance.RIGHT_TOP_PRICES


func _ready():
	match player.team:
		G.TEAM_1:
			_hitbox_id = player.HT_RT
			_visual_player_part = player.get_node("visual/body_rt")
		G.TEAM_2:
			_hitbox_id = player.HT_LT
			_visual_player_part = player.get_node("visual/body_lt")
	update_vars()
	update_module()


onready var _visual_player_part = player.get_node("visual/body_rt")

func apply_hp(hp):
	if G.Server != null:
		G.Server.send_call(get_path(), "apply_hp", [hp], Net.BROADCAST)
	player.hitbox_default[_hitbox_id] = hp
	player.hitbox_max[_hitbox_id] = hp
	player.hitbox = player.hitbox_default.duplicate()
	_visual_player_part.get_node("hit").play("upgrade")


func _exit_tree() -> void:
	player.hitbox_default[_hitbox_id] = _default_hp
	player.hitbox_max[_hitbox_id] = _default_hp
	player.hitbox = player.hitbox_default.duplicate()
	_visual_player_part.get_node("hit").play("upgrade")


func update_module():
	G.debug_print("[MODULE] " + name + " updated, lvl: " + str(level) + ", enabled: " + str(state))
	if state:
		if G.Server != null:
			apply_hp(_default_hp + (level + 1) * RATE)
	else:
		_exit_tree()
