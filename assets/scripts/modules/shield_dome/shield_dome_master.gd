extends Module
# Master

var _is_init = false


func update_vars():
	t_name = "shield_dome"
	d_name = Balance.SHIELD_DOME_NAME
	d_desc = Balance.SHIELD_DOME_DESC
	prices = Balance.SHIELD_DOME_PRICES
	exceptions = Balance.SHIELD_DOME_EXCEPTIONS


func _ready():
	update_vars()
	update_module()


func on_shield_hit(point, damage):
	player.get_node("visual/shield_dome").hit_anim(point, damage)


func _exit_tree() -> void:
	if player.has_node("visual/shield_dome"):
		player.get_node("visual/shield_dome/anim").play("exit")


func update_module():
	G.debug_print("[MODULE] " + name + " updated, lvl: " + str(level) + ", enabled: " + str(state))
	if state:
		process_exceptions()
		if !_is_init:
			player.get_node("visual").add_child(preload("res://assets/scenes/shield_dome.tscn").instance())
			player.get_node("visual/shield_dome").connect("hit", self, "on_shield_hit")
			player.weapon.add_exception("visual/shield_dome")
		elif player.has_node("visual/shield_dome"):
			player.get_node("visual/shield_dome/anim").play("on")
		_is_init = true
	else:
		_exit_tree()
