extends Module
# Master


func update_vars():
	t_name = "grenade"
	d_name = Balance.GRENADE_NAME
	d_desc = Balance.GRENADE_DESC
	prices = Balance.GRENADE_PRICES


func _ready():
	update_vars()
	update_module()


func _exit_tree() -> void:
	weapon.visual.GRENADE = ""
	weapon.visual.generate()


func update_module():
	G.debug_print("[MODULE] " + name + " updated, lvl: " + str(level) + ", enabled: " + str(state))
	if state:
		weapon.visual.GRENADE = "grenade"
		weapon.visual.generate()
		G.UI.emit_signal("grenade_timer", 1)
	else:
		_exit_tree()
