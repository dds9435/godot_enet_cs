extends Weapon

const B_PATH		= "res://assets/scenes/firearm_bullet.tscn"


var VISUAL_WIDTH	= Balance.FIRE_DEF_VISUAL_WIDTH
var VISUAL_COLOR	= Balance.FIRE_DEF_VISUAL_COLOR
var DAMAGE			= Balance.DMG_TABLE_FIREARM[BARREL_LVL]
var PENETRATE		= Balance.FIRE_DEF_PENETRATE
var SHOTS			= Balance.FIRE_DEF_SHOTS
var SHOT_DELAY		= Balance.FIRE_DEF_SHOT_DELAY
var GLOW_SIZE		= Balance.FIRE_DEF_GLOW_SIZE

var BASE_LVL		= 0
var BASE_TYPE		= BASE_SIMPLE

const BASE_SHOTGUN	= 0
const BASE_SNIPEGUN	= 1
const BASE_SIMPLE	= -1

var DAMAGE_BASE = 0
var DAMAGE_ADD = 0


var shot_counter = 0

var collider = null
var colpoint = null


var fx_scn = preload("res://assets/scenes/hit_fx.tscn")


func recalc_damage() -> void:
	var add = 0
	match BASE_TYPE:
		BASE_SHOTGUN:
			add = Balance.SHOTGUN_DMG_ADD_TABLE[BASE_LVL]
			DAMAGE = Balance.DMG_TABLE_SHOTGUN[BARREL_LVL] + add
			DAMAGE_BASE = Balance.DMG_TABLE_SHOTGUN[BARREL_LVL]
		BASE_SNIPEGUN:
			add = Balance.SNIPEGUN_DMG_ADD_TABLE[BASE_LVL]
			DAMAGE = Balance.DMG_TABLE_SNIPEGUN[BARREL_LVL] + add
			DAMAGE_BASE = Balance.DMG_TABLE_SNIPEGUN[BARREL_LVL]
		_:
			DAMAGE = Balance.DMG_TABLE_FIREARM[BARREL_LVL]
			DAMAGE_BASE = Balance.DMG_TABLE_FIREARM[BARREL_LVL]
	DAMAGE_ADD = add


func shoot() -> void:
	shot_counter = 0
	while shot_counter < SHOTS:
		for shooter in active_shooters:
			if G.Client != null:
				# client
				if PENETRATE:
					shooting_timer = SHOOT_RATE
				else:
					shooting_timer = clamp(shooting_timer + (float(SHOOT_RATE) / SHOTS) / active_shooters.size(), 0, 2)
				if $"..".control:
					apply_recoil(clamp(DAMAGE * SHOTS, 4, 30))
				process_smoke(DAMAGE)
				process_on_shoot()
				process_glow(VISUAL_COLOR, GLOW_SIZE * DAMAGE)
				
				if !PENETRATE and !is_slave:
					collider = shooter.get_collider()
					
					if collider == null:
						colpoint = shooter.global_position + Vector2(shooter.cast_to.x,
								0).rotated(shooter.global_rotation * $"../visual".scale.x)
					else:
						colpoint = shooter.get_collision_point()
					
					if (is_equal_approx(
						stepify(
							shooter.global_position.angle_to_point(colpoint),
								0.1),
						stepify(
							shooter.get_node("barrel").\
								global_position.angle_to_point(colpoint),
								0.1))):
						local_proj([
								shooter.get_node("barrel").global_position,
								colpoint])
					else:
						local_proj([colpoint])
				continue
			
			# server
			
			rewind_players(G.Server.recv_timer)
			
#			var shooter_pos = shooter.global_position
#			var pl_motion = ($"..".motion *
#				G.Server.connections_rtt.get(int($"..".name), 1.0))
#			if !$"..".test_move($"..".transform, pl_motion):
#				shooter.global_position += pl_motion
			
			var temp_damage = DAMAGE
			while temp_damage > 0:
				shooter.force_raycast_update()
				collider = shooter.get_collider()
				
				if collider == null:
					temp_damage = 0
					colpoint = shooter.global_position + Vector2(shooter.cast_to.x,
							0).rotated(shooter.global_rotation * $"../visual".scale.x)
				else:
					colpoint = shooter.get_collision_point()
					if collider.has_method("hit") and collider.has_method("get_hp"):
						var to_set_damage = collider.get_hp(colpoint)
						var block_dmg = temp_damage
						if collider.has_method("update_dirty_quadrants"):
							collider.update_dirty_quadrants()
							if PENETRATE:
								block_dmg = temp_damage * Balance.SNIPEGUN_BLOCK_DAMAGE_MULT
						collider.hit(
								colpoint,
								shooter.global_rotation,
								block_dmg
								)
						if PENETRATE:
							temp_damage -= to_set_damage / Balance.SNIPEGUN_BLOCK_DAMAGE_MULT
						else:
							temp_damage -= to_set_damage
						if collider.has_method("update_dirty_quadrants"):
							collider.update_dirty_quadrants()
					elif collider.has_method("hit"):
						var to_set_damage = temp_damage
						if collider.has_method("get_obj_hp"):
							to_set_damage = collider.get_obj_hp()
						collider.hit(
							colpoint,
							temp_damage,
							owner_id
						)
						temp_damage -= to_set_damage
					else:
						temp_damage = 0
				
				if !PENETRATE:
					break
			
			if (is_equal_approx(
				stepify(
					shooter.global_position.angle_to_point(colpoint),
						0.1),
				stepify(
					shooter.get_node("barrel").\
						global_position.angle_to_point(colpoint),
						0.1))):
				emit_signal("serv_projectile", self, [
								shooter.get_node("barrel").global_position,
								colpoint])
			else:
				emit_signal("serv_projectile", self, [colpoint])
			unrewind_players()
#			shooter.global_position = shooter_pos
		
		shot_counter += 1
		if shot_counter != SHOTS:
			yield(get_tree().create_timer(SHOT_DELAY), "timeout")


func rewind_players(time: float) -> void:
	for pl in G.World.players.get_children():
		pl.rewind(time)


func unrewind_players() -> void:
	for pl in G.World.players.get_children():
		pl.unrewind()


func local_proj(data: Array) -> void:
	var b = preload(B_PATH).instance()
	b.get_node("anim").get_animation("idle").track_set_key_value(0, 0, VISUAL_WIDTH)
	b.default_color = VISUAL_COLOR
	b.points = data
	var fx = fx_scn.instance()
	fx.position = data.back()
	fx.rotation = data.back().angle_to_point(data[0])
	G.World.add_child(fx)
	G.World.add_child(b)


func proj_request(data: Array) -> void:
	if !is_slave:
		$"..".motion -= Vector2(clamp(DAMAGE * 25, 0, 150), 0).rotated(global_rotation)
	
	var b = preload(B_PATH).instance()
	b.get_node("anim").get_animation("idle").track_set_key_value(0, 0, VISUAL_WIDTH)
	b.default_color = VISUAL_COLOR
	b.points = data
	var fx = fx_scn.instance()
	fx.position = data.back()
	fx.rotation = data.back().angle_to_point(data[0])
	G.World.add_child(fx)
	G.World.add_child(b)
