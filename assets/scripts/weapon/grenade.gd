extends Node2D

signal serv_grenade_projectile(weapon, data)

const G_PATH	= "res://assets/scenes/grenade.tscn"
const TR_PATH	= "res://assets/scenes/trail.tscn"

const NEW		= 0
const NEW_LOCAL	= 1


var RADIUS		= Balance.GRENADE_DEF_RADIUS
var DAMAGE		= Balance.GRENADE_DEF_DAMAGE


var owner_id = -1

var counter = 0


func shoot() -> void:
	if G.Server == null:
		# client
		return
	
	# server
	
#	var shooter_pos = global_position
#	var pl_motion = ($"../..".motion *
#		G.Server.connections_rtt.get(int($"../..".name), 1.0))
#	if !$"../..".test_move($"../..".transform, pl_motion):
#		global_position += pl_motion
	
	var rocket_name = "g_" + str(counter)
	counter += 1
	if counter == 100:
		counter = 0
	
	proj_request([NEW_LOCAL, global_position,
			global_rotation, rocket_name])
	emit_signal("serv_grenade_projectile", self, [NEW, global_position,
			global_rotation, rocket_name])
	
#	global_position = shooter_pos


func proj_request(data) -> void:
	match data[0]:
		NEW_LOCAL:
			var b = preload(G_PATH).instance()
			b.position = data[1]
			b.rotation = data[2]
			for ex in get_node("../..").hit_colliders:
				if get_node("../..").get_node(ex) is PhysicsBody2D:
					b.add_collision_exception_with(get_node("../..").get_node(ex))
			b.linear_velocity = Vector2(Balance.GRENADE_DEF_IMPULSE, 0).rotated(data[2])
			b.angular_velocity = randf() - 0.5 * 10
			b.name = data[3]
			b.DAMAGE = DAMAGE
			b.RADIUS = RADIUS
			b.owner_id = owner_id
		
			G.World.add_child(b)
		
		NEW:
			var b = preload(G_PATH).instance()
			var tr = preload(TR_PATH).instance()
			tr.obj = b
			b.position = data[1]
			b.rotation = data[2]
			for ex in get_node("../..").hit_colliders:
				if get_node("../..").get_node(ex) is PhysicsBody2D:
					b.add_collision_exception_with(get_node("../..").get_node(ex))
			b.linear_velocity = Vector2(Balance.GRENADE_DEF_IMPULSE, 0).rotated(data[2])
			b.angular_velocity = randf() - 0.5 * 10
			b.integrate = G.Client.PP
			b.name = data[3]
			
			if !get_parent().is_slave:
				$"../..".motion -= Vector2(clamp(DAMAGE * 25, 0, 300), 0).rotated(get_parent().rotation)
		
			G.World.add_child(b)
			G.World.add_child(tr)
