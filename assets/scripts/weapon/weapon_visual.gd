extends Node2D
class_name Weapon_Visual

signal update_shooters(data)

const WEAPONARY_PATH	= "res://assets/scenes/weapons/"
const SHOOTERS_NAMES	= ["0", "1"]

const BASE_PATH			= "base/"
const CLIP_PATH			= "clip/"
const REBOOST_PATH		= "re_boost/"
const BOOST_PATH		= "boost/"
const BARREL_PATH		= "barrel/"
const MISC_PATH			= "misc/"

const TSCN				= ".tscn"

# tweakable (start state)

var BASE				= "firearm"		# \
var CLIP				= "firearm"		# - firearm, laser, rocket (all)
var BARREL				= "firearm"		# /

var RE_BOOST			= ""			# firearm, laser, rocket, empty (all)
var POSTFIX				= ""			# _s, empty (firearm, laser)

var BOOST				= ""			# boost, shotgun, snipegun, empty (firearm)
var BOLT				= "bolt"		# bolt, empty (firearm)

var GRENADE				= ""			# grenade, empty (all)
var SCOPE				= ""			# scope, empty (all)

var BASE_LEVEL			= "_1"			# _1 (firearm, rocket); _[1-5] (laser)
var CLIP_LEVEL			= "_1"			# _[1-9] (all)
var BARREL_LEVEL		= "_1"			# _[1-6] (all)

var BOOST_LEVEL			= "_1"			# _[1-8] (firearm, boost); _[1-3] (firearm, shot/snipe gun)  

# don't touch this

var GRENADE_TYPE		= ""
var SCOPE_TYPE			= ""
var RE_BOOST_TYPE		= ""

# runtime

var base				= null
var barrel				= null
var clip				= null
var base_s				= null
var clip_s				= null
var boost				= null
var re_boost			= null
var grenade				= null
var scope				= null

# for outside anim purposes

var bolt = null
var clip_indicator_scale = 0.0
var clip_glow_scale = 0.0


var bolt_dpos = Vector2(0,0)

var prev_gen = []


func _ready() -> void:
	
	# testing enviroment
	if get_tree().current_scene.name == "weapon":
		get_tree().current_scene.position = Vector2(250, 250)
		get_tree().current_scene.scale = Vector2(8,8)
		VisualServer.set_default_clear_color(Color(0.5,0.7,0.5))
		
		set_process(true)
	else:
		set_process(false)
	yield(get_tree(), "idle_frame")
	generate()


func generate() -> void:
	
	var c_gen = []
	
	# clear
	
	for i in get_children():
		if i.name != "tw":
			i.queue_free()
	
	# grenade type matching
	
	if BASE == "rocket":
		GRENADE_TYPE = "_3"
	elif RE_BOOST != "" and BOOST != "":
		GRENADE_TYPE = "_2"
	elif RE_BOOST != "":
		GRENADE_TYPE = "_1"
	else:
		GRENADE_TYPE = "_4"
	
	# scope type matching
	
	if BASE == "rocket":
		SCOPE_TYPE = "_1"
	else:
		SCOPE_TYPE = "_2"
	
	# re_boost laser matching
	
	if BASE == "laser":
		RE_BOOST_TYPE = BASE_LEVEL
	else:
		RE_BOOST_TYPE = "_1"
	
	# loading all needed scenes
	
	base		= load(WEAPONARY_PATH + BASE_PATH + BASE + BASE_LEVEL + TSCN).instance()
	barrel		= load(WEAPONARY_PATH + BARREL_PATH + BARREL + POSTFIX + BARREL_LEVEL + TSCN).instance()
	clip		= load(WEAPONARY_PATH + CLIP_PATH + CLIP + CLIP_LEVEL + TSCN).instance()
	base_s		= null
	clip_s		= null
	boost		= null
	re_boost	= null
	grenade		= null
	scope		= null
	
	if POSTFIX != "":
		clip_s		= load(WEAPONARY_PATH + CLIP_PATH + CLIP + CLIP_LEVEL + TSCN).instance()
		base_s		= load(WEAPONARY_PATH + BASE_PATH + BASE + POSTFIX + BASE_LEVEL + TSCN).instance()
	if GRENADE != "":
		grenade		= load(WEAPONARY_PATH + BASE_PATH + GRENADE + GRENADE_TYPE + TSCN).instance()
	if BOOST != "":
		boost		= load(WEAPONARY_PATH + BOOST_PATH + BOOST + POSTFIX + BOOST_LEVEL + TSCN).instance()
	if BOLT != "":
		bolt		= load(WEAPONARY_PATH + MISC_PATH + BOLT + POSTFIX + TSCN).instance()
	if RE_BOOST != "":
		re_boost	= load(WEAPONARY_PATH + REBOOST_PATH + RE_BOOST + POSTFIX + RE_BOOST_TYPE + TSCN).instance()
	if SCOPE != "":
		scope		= load(WEAPONARY_PATH + MISC_PATH + SCOPE + SCOPE_TYPE + TSCN).instance()
	
	# starting build chain step by step
	
	var last_chain = base
	
	add_child(base)
	
	clip.position = clip_pos(clip, base)
	if clip.has_node("indicator"):
		clip_indicator_scale = clip.get_node("indicator").scale.y
	if clip.has_node("glow"):
		clip_glow_scale = clip.get_node("glow").scale.y
	add_child(clip)
	
	if BOLT != "":
		bolt.position = bolt_pos(bolt, base)
		bolt_dpos = bolt.position
		add_child(bolt)
	
	if POSTFIX != "":
		base_s.position = chain_pos(base_s, last_chain)
		last_chain = base_s
		add_child(base_s)
		clip_s.position = clip_pos(clip_s, base_s)
		add_child(clip_s)
	
	if SCOPE != "":
		if POSTFIX != "" and BASE == "laser":
			scope.position = scope_pos(scope, base_s)
		else:
			scope.position = scope_pos(scope, base)
		add_child(scope)
	
	if GRENADE != "":
		if POSTFIX != "":
			grenade.position = grenade_pos(grenade, base_s)
		else:
			grenade.position = grenade_pos(grenade, base)
		add_child(grenade)
	
	if RE_BOOST != "":
		re_boost.position = chain_pos(re_boost, last_chain)
		last_chain = re_boost
		add_child(re_boost)
	
	if BOOST != "":
		boost.position = chain_pos(boost, last_chain)
		last_chain = boost
		add_child(boost)

	barrel.position = chain_pos(barrel, last_chain)
	last_chain = barrel
	add_child(barrel)
	
	var shooters_data = {}
	
	for i in barrel.get_children():
		if i.name in SHOOTERS_NAMES:
			shooters_data[i.name] = base.position + i.position
			shooters_data[i.name + "/barrel"] = barrel.position - base.position
	if grenade != null:
		shooters_data["grenade_shooter"] = (
				grenade.position +
				grenade.get_node("grenade_shooter").position +
				position)
	
	if scope != null:
		$"../pointer".show()
		$"../pointer".position = scope.get_node("front").position + scope.position + position
	else:
		$"../pointer".hide()
	
	
	if base != null:
		c_gen.append(BASE_PATH + BASE + BASE_LEVEL)
	if barrel != null:
		c_gen.append(BARREL_PATH + BARREL + POSTFIX + BARREL_LEVEL)
	if clip != null:
		c_gen.append(CLIP_PATH + CLIP + CLIP_LEVEL)
	if base_s != null:
		c_gen.append(BASE_PATH + BASE + POSTFIX + BASE_LEVEL)
	if clip_s != null:
		c_gen.append(CLIP_PATH + CLIP + CLIP_LEVEL)
	if boost != null:
		c_gen.append(BOOST_PATH + BOOST + POSTFIX + BOOST_LEVEL)
	if re_boost != null:
		c_gen.append(REBOOST_PATH + RE_BOOST + POSTFIX + RE_BOOST_TYPE)
	if grenade != null:
		c_gen.append(BASE_PATH + GRENADE + GRENADE_TYPE)
	if scope != null:
		c_gen.append(MISC_PATH + SCOPE + SCOPE_TYPE)
	
	var c_nodes = []
	
	for i in c_gen:
		if !(i in prev_gen):
			if i.begins_with(BASE_PATH) and BASE_PATH != "":
				if base != null:
					c_nodes.append(base)
				if base_s != null:
					c_nodes.append(base_s)
			elif i.begins_with(BARREL_PATH) and BARREL_PATH != "":
				if barrel != null:
					c_nodes.append(barrel)
			elif i.begins_with(CLIP_PATH) and CLIP_PATH != "":
				if clip != null:
					c_nodes.append(clip)
				if clip_s != null:
					c_nodes.append(clip_s)
			elif i.begins_with(BOOST_PATH) and BOOST_PATH != "":
				if boost != null:
					c_nodes.append(boost)
			elif i.begins_with(REBOOST_PATH) and REBOOST_PATH != "":
				if re_boost != null:
					c_nodes.append(re_boost)
			elif i.begins_with(BASE_PATH + GRENADE) and !"" in [BASE_PATH, GRENADE]:
				if grenade != null:
					c_nodes.append(grenade)
			elif i.begins_with(MISC_PATH + SCOPE) and !"" in [MISC_PATH, SCOPE]:
				if scope != null:
					c_nodes.append(scope)
	
	for i in c_nodes:
		$tw.remove(i, "modulate")
		$tw.interpolate_property(
			i, "modulate",
			Color(10, 10, 10, 10), Color(1, 1, 1, 1),
			0.5, Tween.TRANS_CUBIC, Tween.EASE_OUT
		)
	if c_nodes.size() > 0:
		$tw.start()
	
	prev_gen = c_gen
	
	emit_signal("update_shooters", shooters_data)


# testing purposes

func _process(delta: float) -> void:
	if Input.is_action_just_pressed("lmb"):
		on_shoot(0.5)
	if Input.is_action_just_pressed("rmb"):
		generate()


# bolt animation

func on_shoot(duration) -> void:
	if BOLT != "":
		$tw.remove(bolt, "position")
		$tw.interpolate_property(bolt, "position", bolt_dpos -
				(bolt.get_node("right").position - bolt.get_node("left").position),
				bolt_dpos,
				duration, Tween.TRANS_CUBIC, Tween.EASE_OUT)
		
		$tw.start()


# pos matchers

func scope_pos(_scope: Node, _base: Node) -> Vector2:
	var ret = Vector2(0,0)
	
	ret = (_base.position +
			_base.get_node("scope").position -
			_scope.get_node("bottom").position)
	
	return ret


func grenade_pos(_grenade: Node, _base: Node) -> Vector2:
	var ret = Vector2(0,0)
	
	ret = (_base.position +
			_base.get_node("grenade").position -
			_grenade.get_node("top").position)
	
	return ret


func bolt_pos(_bolt: Node, _base: Node) -> Vector2:
	var ret = Vector2(0,0)
	
	ret = (_base.position +
			_base.get_node("bolt").position -
			_bolt.get_node("left").position)
	
	return ret


func clip_pos(_clip: Node, _base: Node) -> Vector2:
	var ret = Vector2(0,0)
	
	ret = (_base.position +
			_base.get_node("clip").position -
			_clip.get_node("top").position)
	
	return ret


func chain_pos(_target: Node, _last: Node) -> Vector2:
	var ret = Vector2(0,0)
	
	ret = (_last.position +
			_last.get_node("right").position -
			_target.get_node("left").position)
	
	return ret
