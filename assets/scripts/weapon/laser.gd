extends Weapon

const B_PATH		= "res://assets/scenes/laser_beam.tscn"


var VISUAL_WIDTH	= Balance.LASER_DEF_VISUAL_WIDTH
var VISUAL_COLOR	= Color(0,0,0)
var GLOW_SIZE		= Balance.LASER_DEF_GLOW_SIZE
var DAMAGE			= Balance.DMG_TABLE_LASER[BARREL_LVL]
var SHIELD_DMG_MULT	= Balance.LASER_SHIELD_DAMAGE_MULT

var BASE_LVL		= 0
var DAMAGE_BASE = 0
var DAMAGE_ADD = 0


var shooting = false
var collider = null
var colpoint = null


var fx_scn = preload("res://assets/scenes/laser_hit_fx.tscn")
var fx_saved = {}


func recalc_damage() -> void:
	DAMAGE_BASE = Balance.DMG_TABLE_LASER[BARREL_LVL]
	DAMAGE_ADD = Balance.LASER_DMG_ADD_TABLE[BASE_LVL]
	DAMAGE = Balance.DMG_TABLE_LASER[BARREL_LVL] + Balance.LASER_DMG_ADD_TABLE[BASE_LVL]


func shoot() -> void:
	shooting = true
	for shooter in active_shooters:
		if G.Client != null:
			# client
			shooter.get_node("barrel/laser_beam").show()
			process_smoke(DAMAGE)
			process_glow(VISUAL_COLOR, GLOW_SIZE * DAMAGE, false)
			continue
		
		# server
		
		rewind_players(G.Server.recv_timer)
		
#		var shooter_pos = shooter.global_position
#		var pl_motion = ($"..".motion *
#			G.Server.connections_rtt.get(int($"..".name), 1.0))
#		if !$"..".test_move($"..".transform, pl_motion):
#			shooter.global_position += pl_motion
		
		collider = shooter.get_collider()
		if collider == null:
			colpoint = shooter.global_position + Vector2(shooter.cast_to.x,
					0).rotated(shooter.global_rotation * $"../visual".scale.x)
		else:
			colpoint = shooter.get_collision_point()
			if collider.has_method("hit") and collider.has_method("get_hp"):
				collider.hit(
					colpoint,
					shooter.global_rotation,
					DAMAGE
				)
			elif collider.has_method("hit"):
				if collider.name.begins_with("shield"):
					collider.hit(
						colpoint,
						DAMAGE * SHIELD_DMG_MULT,
						owner_id
					)
				else:
					collider.hit(
						colpoint,
						DAMAGE,
						owner_id
					)
		unrewind_players()
#		shooter.global_position = shooter_pos


func rewind_players(time: float) -> void:
	for pl in G.World.players.get_children():
		pl.rewind(time)


func unrewind_players() -> void:
	for pl in G.World.players.get_children():
		pl.unrewind()


var _indicator_on = false

func ammo_change(ammo: int, is_shoot) -> void:
	if ammo > 0 and !_indicator_on:
		if visual.base != null and visual.base.has_node("indicator"):
			visual.base.get_node("indicator").show()
		if visual.base_s != null and visual.base_s.has_node("indicator"):
			visual.base_s.get_node("indicator").show()
		_indicator_on = true
	elif ammo == 0 and _indicator_on:
		if visual.base != null and visual.base.has_node("indicator"):
			visual.base.get_node("indicator").hide()
		if visual.base_s != null and visual.base_s.has_node("indicator"):
			visual.base_s.get_node("indicator").hide()
		_indicator_on = false
	if visual.clip != null and visual.clip.has_node("indicator") and visual.clip.has_node("glow"):
		visual.clip.get_node("indicator").scale.y = (float(ammo) / (AMMO * AMMO_SIZE)) * visual.clip_indicator_scale
		visual.clip.get_node("glow").scale.y = (float(ammo) / (AMMO * AMMO_SIZE)) * visual.clip_glow_scale
	if visual.clip_s != null and visual.clip_s.has_node("indicator") and visual.clip_s.has_node("glow"):
		visual.clip_s.get_node("indicator").scale.y = (float(ammo) / (AMMO * AMMO_SIZE)) * visual.clip_indicator_scale
		visual.clip_s.get_node("glow").scale.y = (float(ammo) / (AMMO * AMMO_SIZE)) * visual.clip_glow_scale


func every_frame() -> void:
	if !shooting or G.Server != null: return
	for shooter in active_shooters:
		collider = shooter.get_collider()
		if collider == null:
			colpoint = shooter.global_position + Vector2(shooter.cast_to.x,
					0).rotated(shooter.global_rotation * $"../visual".scale.x)
		else:
			colpoint = shooter.get_collision_point()
		shooter.get_node("barrel/laser_beam").points[1] = Vector2(
				shooter.global_position.distance_to(colpoint), 0)
		fx_saved[shooter].emitting = true
		fx_saved[shooter].get_node("smoke").emitting = true
		fx_saved[shooter].get_node("glow").show()
		fx_saved[shooter].position = colpoint
		fx_saved[shooter].rotation = rotation


func release() -> void:
	shooting = false
	for shooter in active_shooters:
		shooter.get_node("barrel/laser_beam").hide()
		process_glow(VISUAL_COLOR, GLOW_SIZE * DAMAGE, true)
		fx_saved[shooter].emitting = false
		fx_saved[shooter].get_node("smoke").emitting = false
		fx_saved[shooter].get_node("glow").hide()


func on_exit() -> void:
	for shooter in active_shooters:
		shooter.get_node("barrel/laser_beam").queue_free()
		process_glow(VISUAL_COLOR, GLOW_SIZE * DAMAGE, true)
	for fx in fx_saved.values():
		fx.queue_free()


func init() -> void:
	for fx in fx_saved.values():
		fx.queue_free()
	for shooter in active_shooters:
		var b = preload(B_PATH).instance()
		b.get_node("anim").get_animation("idle").track_set_key_value(0, 0, VISUAL_WIDTH)
		b.get_node("anim").get_animation("idle").track_set_key_value(0, 1, VISUAL_WIDTH / 2)
		b.default_color = VISUAL_COLOR
		b.hide()
		var fx = fx_scn.instance()
		fx_saved[shooter] = fx
		G.World.add_child(fx)
		shooter.get_node("barrel").add_child(b)
