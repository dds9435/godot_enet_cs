extends Weapon

const R_PATH	= "res://assets/scenes/rocket.tscn"

const NEW		= 0
const NEW_LOCAL	= 1


var RADIUS		= Balance.DMG_TABLE_ROCKET[BARREL_LVL]
var DAMAGE		= Balance.RAD_TABLE_ROCKET[BARREL_LVL]


var counter = 0

func recalc_damage() -> void:
	RADIUS = Balance.RAD_TABLE_ROCKET[BARREL_LVL]
	DAMAGE = Balance.DMG_TABLE_ROCKET[BARREL_LVL]


func shoot() -> void:
	for shooter in active_shooters:
		if G.Server == null:
			# client
			continue
		
		# server
		
#		var shooter_pos = shooter.global_position
#		var pl_motion = ($"..".motion *
#			G.Server.connections_rtt.get(int($"..".name), 1.0))
#		if !$"..".test_move($"..".transform, pl_motion):
#			shooter.global_position += pl_motion
		
		var rocket_name = "r_" + str(counter)
		counter += 1
		if counter == 100:
			counter = 0
		
		proj_request([NEW_LOCAL, shooter.global_position,
				shooter.global_rotation * get_parent().visual.scale.x, rocket_name])
		emit_signal("serv_projectile", self, [NEW, shooter.global_position,
				shooter.global_rotation * get_parent().visual.scale.x, rocket_name])
		
#		shooter.global_position = shooter_pos


func proj_request(data) -> void:
	match data[0]:
		NEW_LOCAL:
			var b = preload(R_PATH).instance()
			b.position = data[1]
			b.rotation = data[2]
			for ex in get_parent().hit_colliders:
				if get_parent().get_node(ex) is PhysicsBody2D:
					b.add_collision_exception_with(get_parent().get_node(ex))
			b.linear_velocity = Vector2(Balance.ROCKET_DEF_IMPULSE, 0).rotated(data[2])
			b.name = data[3]
			b.DAMAGE = DAMAGE
			b.RADIUS = RADIUS
			b.owner_id = owner_id
		
			G.World.add_child(b)
		
		NEW:
			var b = preload(R_PATH).instance()
			b.position = data[1]
			b.rotation = data[2]
			for ex in get_parent().hit_colliders:
				if get_parent().get_node(ex) is PhysicsBody2D:
					b.add_collision_exception_with(get_parent().get_node(ex))
			b.linear_velocity = Vector2(Balance.ROCKET_DEF_IMPULSE, 0).rotated(data[2])
			b.integrate = G.Client.PP
			b.name = data[3]
			
			if !is_slave:
				$"..".motion -= Vector2(clamp(DAMAGE * 25, 0, 300), 0).rotated(rotation)
		
		
			G.World.add_child(b)
