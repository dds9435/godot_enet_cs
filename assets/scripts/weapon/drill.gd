extends Node2D

const DRILL_RECOIL		= Balance.DRILL_RECOIL

const DRILL_DMG			= Balance.DRILL_DMG
const DRILL_BLOCK_DMG	= Balance.DRILL_BLOCK_DMG


var is_slave = true
var owner_id = -1


var timer = 0.0
var collider = null
var colpoint = null
var pressed = false
var is_colliding = false

func _physics_process(delta: float) -> void:
	rotation = $"../weapon".rotation
	$tex.playing = pressed
	$tex.visible = pressed
	$tex.scale.y = Vector2.RIGHT.rotated(rotation).sign().x
	
	if G.Server == null:
		$smoke.emitting = false
		$sparks.emitting = false
		if pressed:
			if $anim.current_animation != "process":
				$anim.play("process")
			for ptr in get_children():
				if ptr is RayCast2D:
					if ptr.is_colliding():
						$smoke.emitting = true
						$sparks.emitting = true
		else:
			if $anim.current_animation != "idle":
				$anim.play("idle")
		return
	
	if pressed and timer == 0.0:
		timer = DRILL_RECOIL
		for ptr in get_children():
			if ptr is RayCast2D:
				if ptr.is_colliding():
					if !is_colliding:
						is_colliding = true
					collider = ptr.get_collider()
					if collider == null:
						colpoint = ptr.global_position + Vector2(ptr.cast_to.x,
								0).rotated(ptr.global_rotation)
					else:
						colpoint = ptr.get_collision_point()
						if collider.has_method("hit") and collider.has_method("get_hp"):
							collider.hit(
									colpoint,
									ptr.global_rotation,
									DRILL_BLOCK_DMG
									)
							if collider.has_method("update_dirty_quadrants"):
								collider.update_dirty_quadrants()
						elif collider.has_method("hit"):
							collider.hit(
								colpoint,
								DRILL_DMG,
								owner_id
							)
	
	if timer > 0:
		timer -= delta
		if timer < 0:
			timer = 0.0


func _ready() -> void:
	owner_id = int(get_parent().name)
	if !is_slave:
		$"../weapon".connect("me_pressed_rmb", self, "rmb_pressed")
		$"../weapon".connect("me_released_rmb", self, "rmb_released")


func rmb_pressed(weapon = null):
	$"../weapon/pivot/visual".hide()
	pressed = true


func rmb_released(weapon = null):
	$"../weapon/pivot/visual".show()
	pressed = false
