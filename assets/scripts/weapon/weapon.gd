extends Node2D
class_name Weapon

signal me_changed_ammo(weapon, ammo, shooted)
signal me_changed_wrot(weapon, rot)
signal me_changed_wprot(pivot, rot)
signal me_released(weapon)
signal me_shooted_grenade(weapon)
signal serv_projectile(weapon, data)
signal serv_changed_wprot(pivot, rot)
signal serv_changed_wrot(weapon, rot)
signal me_pressed_rmb(weapon)
signal me_released_rmb(weapon)


var AMMO				= Balance.W_DEF_AMMO
var AMMO_SIZE			= Balance.W_DEF_AMMO_SIZE
var SHOOT_RATE			= Balance.W_DEF_SHOOT_RATE
var RELOAD_RATE			= Balance.W_DEF_RELOAD_RATE
var AFTERSHOOT_DELAY	= 0.1
var AFTERSHOOT_SMOKE	= Balance.W_DEF_AFTERSHOOT_SMOKE
var SHOT_COOLDOWN_SPEED	= Balance.W_DEF_SHOT_COOLDOWN_SPEED
var GRENADE_ENABLED		= Balance.W_DEF_GRENADE_ENABLED
var GRENADE_RATE		= Balance.W_DEF_GRENADE_RATE
var BARREL_LVL			= 0


var is_slave = true

onready var active_shooters = [ get_node("pivot/shooters/0") ]
onready var visual: Weapon_Visual = get_node("pivot/visual")

var ammo = AMMO * AMMO_SIZE
var shoot_timer = 0.0
var reload_timer = 0.0
var aftershoot_timer = 0.0
var aftershoot_smoke_timer = 0.0
var shooting_timer = 0.0
var grenade_timer = 0.0
var prev_released = false

var last_pivrot = 0.0
var last_wrot = 0.0

var last_pivot_scale = 1

var owner_id = -1

var block_lookat = false

var rot_update_timer = 0.0
var tanadd = 0

func apply_recoil(recoil) -> void:
	recoil = recoil / 2
	if abs($pivot.rotation_degrees) + recoil < 60:
		var add = (randf() * recoil - recoil / 2) * shooting_timer
		tanadd = recoil
		G.UI.emit_signal("cursor_scale", 
			tan(
				deg2rad(
					tanadd *
				shooting_timer)) *
			position.distance_to(get_local_mouse_position())
		)
		if Vector2(1,0).rotated(global_rotation).x > 0:
			$pivot.rotation_degrees -= add
		else:
			$pivot.rotation_degrees += add
		$pivot.rotation_degrees = clamp($pivot.rotation_degrees, -recoil / 2, recoil / 2)
	aftershoot_timer = AFTERSHOOT_DELAY


func _physics_process(delta: float) -> void:
	if self.has_method("every_frame"):
		self.call("every_frame")
	if aftershoot_smoke_timer > 0:
		aftershoot_smoke_timer -= delta
		if aftershoot_smoke_timer < 0:
			for i in active_shooters:
				(i.get_node("barrel/smoke") as Particles2D).emitting = false
			aftershoot_smoke_timer = 0


func integrate_wrot(rot: float) -> void:
	var timer = 0.0
	if G.Client != null:
		if $"..".control: return
		timer = clamp(G.Client.RTT, 0.016, 1.0)
	else:
		timer = clamp(G.Server.connections_rtt.get(int($"..".name), 1.0), 0.016, 1.0)
	$rot_tween.remove(self, "rotation_degrees")
	$rot_tween.interpolate_property(self, "rotation_degrees", null, rot, timer, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	$rot_tween.start()
	
	$pivot.scale.y = Vector2.RIGHT.rotated(rotation).sign().x
	if last_pivot_scale != $pivot.scale.y:
		if $pivot.scale.y == 1:
			$pivot.rotation_degrees = - abs($pivot.rotation_degrees)
		else:
			$pivot.rotation_degrees = abs($pivot.rotation_degrees)
		last_pivot_scale = $pivot.scale.y


func _on_tween_step(object: Object, key: NodePath, elapsed: float, value: Object):
	emit_signal("serv_changed_wrot", self, rotation_degrees)


func _process(delta: float) -> void:
	if !get_parent().dead:
		if !block_lookat and is_instance_valid(G.UI) and !G.UI.block_input:
			look_at(get_global_mouse_position())
			rotation_degrees = stepify(rotation_degrees, 0.01)
	else:
		look_at(global_position + Vector2.RIGHT * get_parent().get_node("visual").scale.x)
	
	if rotation_degrees < 0:
		rotation_degrees = 360.0
	elif rotation_degrees > 360:
		rotation_degrees = 0.0
	
	if last_wrot != stepify(rotation_degrees, 0.01):
		emit_signal("me_changed_wrot", self, stepify(rotation_degrees, 0.01))
		last_wrot = stepify(rotation_degrees, 0.01)
	
	if $pivot.rotation_degrees != last_pivrot:
		emit_signal("me_changed_wprot", $pivot, $pivot.rotation_degrees)
		last_pivrot = $pivot.rotation_degrees
	
	$pivot.scale.y = Vector2.RIGHT.rotated(rotation).sign().x
	if (Input.is_action_pressed("mmb") and is_instance_valid(G.UI)
			and !G.UI.block_input and
			grenade_timer == 0 and
			!get_parent().dead and
			GRENADE_ENABLED):
				grenade_timer = GRENADE_RATE
				emit_signal("me_shooted_grenade", $grenade_shooter)
	
	if (Input.is_action_pressed("lmb") and is_instance_valid(G.UI) and
			!G.UI.block_input and
			shoot_timer == 0 and
			!get_parent().dead and
			$pivot/visual.visible):
		if ammo > 0:
			prev_released = false
			shoot_timer = SHOOT_RATE
			ammo -= 1
			$pivot/visual.on_shoot(SHOOT_RATE)
			self.call("shoot")
			emit_signal("me_changed_ammo", self, ammo, true)
			if self.has_method("ammo_change"):
				self.call("ammo_change", ammo, false)
		elif !prev_released:
			if self.has_method("release"):
				emit_signal("me_released", self)
				self.call("release")
			prev_released = true
	
	if (Input.is_action_just_pressed("rmb") and
			!get_parent().dead):
		emit_signal("me_pressed_rmb", self)
	
	if (Input.is_action_just_released("rmb")):
		emit_signal("me_released_rmb", self)
	
	if (Input.is_action_just_released("lmb") and
			self.has_method("release")):
		emit_signal("me_released", self)
		self.call("release")
	
	if aftershoot_timer > 0:
		aftershoot_timer = clamp(aftershoot_timer - delta, 0, INF)
	if shooting_timer > 0 and aftershoot_timer == 0:
		shooting_timer = clamp(shooting_timer - delta, 0, INF)
		G.UI.emit_signal("cursor_scale", 
			tan(
				deg2rad(
					tanadd *
				shooting_timer)) *
			position.distance_to(get_local_mouse_position())
		)
	if shoot_timer > 0:
		shoot_timer = clamp(shoot_timer - delta, 0, INF)
	if reload_timer > 0 and shoot_timer == 0:
		reload_timer = clamp(reload_timer - delta, 0, INF)
	if grenade_timer > 0:
		grenade_timer = clamp(grenade_timer - delta, 0, INF)
		G.UI.emit_signal("grenade_timer", ((GRENADE_RATE - grenade_timer) / GRENADE_RATE))
	
	
	if last_pivot_scale != $pivot.scale.y:
		if $pivot.scale.y == 1:
			$pivot.rotation_degrees = - abs($pivot.rotation_degrees)
		else:
			$pivot.rotation_degrees = abs($pivot.rotation_degrees)
		last_pivot_scale = $pivot.scale.y
	
	
	if aftershoot_timer == 0:
		AFTERSHOOT_DELAY = 0.1
		if round($pivot.rotation_degrees) != 0:
			$pivot.rotation_degrees = move_toward($pivot.rotation_degrees, 0, delta * SHOT_COOLDOWN_SPEED)
		else:
			$pivot.rotation_degrees = 0
	
	
	if ammo < AMMO * AMMO_SIZE and reload_timer == 0:
		reload_timer = RELOAD_RATE
		ammo += 1
		emit_signal("me_changed_ammo", self, ammo, false)
		if self.has_method("ammo_change"):
			self.call("ammo_change", ammo, false)


func add_exception(path) -> void:
	var all_shooters = []
	for i in $pivot/shooters.get_children():
		if i is RayCast2D:
			all_shooters.append(i)
	all_shooters.append($"../drill/ptr1")
	all_shooters.append($"../drill/ptr2")
	all_shooters.append($"../drill/ptr3")
	for ray in all_shooters:
		ray.add_exception(get_parent().get_node(path))


func update_shooters(data: Dictionary) -> void:
	GRENADE_ENABLED = false
	if !is_slave and is_instance_valid(G.UI):
		G.UI.emit_signal("grenade_timer", 0)
	active_shooters = []
	for i in $pivot/shooters.get_children():
		i.hide()
	for i in data.keys():
		if i == "grenade_shooter":
			GRENADE_ENABLED = true
			if !is_slave:
				G.UI.emit_signal("grenade_timer", 1)
			$grenade_shooter.position = data[i]
		elif i.ends_with("/barrel"):
			var shooter_barrel = get_node_or_null("pivot/shooters/" + i)
			if shooter_barrel != null:
				shooter_barrel.position = data[i]
		else:
			var shooter = get_node_or_null("pivot/shooters/" + i)
			if shooter != null:
				shooter.position = data[i]
				shooter.show()
				active_shooters.append(shooter)
	if visual.base != null and visual.base.has_node("indicator"):
		visual.base.get_node("indicator").modulate = self.VISUAL_COLOR
	if visual.base_s != null and visual.base_s.has_node("indicator"):
		visual.base_s.get_node("indicator").modulate = self.VISUAL_COLOR
	if visual.clip != null and visual.clip.has_node("indicator") and visual.clip.has_node("glow"):
		visual.clip.get_node("indicator").modulate = self.VISUAL_COLOR
	if visual.clip_s != null and visual.clip_s.has_node("indicator") and visual.clip_s.has_node("glow"):
		visual.clip_s.get_node("indicator").modulate = self.VISUAL_COLOR


func process_on_shoot() -> void:
	for i in active_shooters:
		if !(i.get_node("barrel/onShoot") as Particles2D).emitting:
			(i.get_node("barrel/onShoot") as Particles2D).emitting = true
			yield(get_tree().create_timer(0.1), "timeout")
			(i.get_node("barrel/onShoot") as Particles2D).emitting = false


func process_smoke(damage) -> void:
	for i in active_shooters:
		if !(i.get_node("barrel/smoke") as Particles2D).emitting:
			(i.get_node("barrel/smoke") as Particles2D).emitting = true
			(i.get_node("barrel/smoke") as Particles2D).lifetime = damage / 3
			(i.get_node("barrel/smoke") as Particles2D).amount = (damage / 3) * 16
	aftershoot_smoke_timer = AFTERSHOOT_SMOKE


func process_glow(color: Color, size: float, autooff: bool = true) -> void:
	for i in active_shooters:
		i.get_node("barrel/glow").scale = Vector2(clamp(size, 0, 0.25), clamp(size, 0, 0.25))
		i.get_node("barrel/glow").modulate = color
		if autooff:
			$glow_tween.remove(i.get_node("barrel/glow"), "modulate")
			$glow_tween.interpolate_property(i.get_node("barrel/glow"),
					"modulate", color,
					Color(1, 1, 1, 0), clamp(size, 0.25, 1.0),
					Tween.TRANS_CIRC, Tween.EASE_OUT)
	if autooff:
		$glow_tween.start()


func reset() -> void:
	grenade_timer = 0.0
	ammo = AMMO * AMMO_SIZE
	for shooter in active_shooters:
		if shooter.has_node("barrel/laser_beam"):
			shooter.get_node("barrel/laser_beam").hide()
	
	if G.Client != null:
		G.UI.emit_signal("grenade_timer", ((GRENADE_RATE -
				grenade_timer) / GRENADE_RATE))
		emit_signal("me_changed_ammo", self, ammo, false)
		emit_signal("me_released_rmb", self)


func _ready() -> void:
	set_process(!is_slave)
	set_physics_process(true)
	$pivot/visual.connect("update_shooters", self, "update_shooters")
	if get_parent().name != "root":
		var all_shooters = []
		for i in $pivot/shooters.get_children():
			if i is RayCast2D:
				all_shooters.append(i)
		all_shooters.append($"../drill/ptr1")
		all_shooters.append($"../drill/ptr2")
		all_shooters.append($"../drill/ptr3")
		all_shooters.append($"pivot/pointer")
		for ray in all_shooters:
			for ex in get_parent().hit_colliders:
				ray.add_exception(get_parent().get_node(ex))
		owner_id = int(get_parent().name)
		$grenade_shooter.owner_id = owner_id
	if self.has_method("init"):
		self.call("init")
	if G.Server != null:
		$rot_tween.connect("tween_step", self, "_on_tween_step")
