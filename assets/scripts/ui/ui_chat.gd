extends Control

const MAX_LINES = 5
const DIM_AFTER_MSG = 1.0

const OPEN_TAGS = ["[b]", "[i]", "[u]", "[s]", "[code]", "[center]", "[right]", "[fill]", "[indent]", "[url]", "[img]", "[url=", "[font=", "[color="]
const CLOSE_TAGS = ["[/b]", "[/i]", "[/u]", "[/s]", "[/code]", "[/center]", "[/right]", "[/fill]", "[/indent]", "[/url]", "[/img]", "[/url]", "[/font]", "[/color]"]

onready var chat = $chat
onready var chat_bg = $chat_bg
onready var chat_exposed = $chat_exposed_placeholder
onready var chat_unexposed = $chat_unexposed_placeholder
onready var tw = $tw

onready var msg = $chat/vbox/msg
onready var who = $chat/vbox/hbox/who
onready var input = $chat/vbox/hbox/input

var exposed = false
var chat_who = false


var dim_timeout = 0.0


func update_exposed() -> void:
	tw.remove(chat, "rect_position")
	tw.remove(chat, "rect_size")
	tw.remove(chat, "modulate")
	G.UI.block_input = exposed
	if exposed:
		dim_timeout = 0.0
		tw.interpolate_property(
			chat, "modulate", null,
			Color(1, 1, 1),
			0.2, Tween.TRANS_CUBIC, Tween.EASE_OUT
		)
		tw.interpolate_property(
			chat, "rect_position", null,
			chat_exposed.rect_position,
			0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
		)
		tw.interpolate_property(
			chat, "rect_size", null,
			chat_exposed.rect_size,
			0.2, Tween.TRANS_CUBIC, Tween.EASE_OUT
		)
		tw.interpolate_property(
			chat_bg, "modulate", null,
			Color(1, 1, 1),
			0.3, Tween.TRANS_CUBIC, Tween.EASE_OUT
		)
		tw.interpolate_property(
			chat_bg, "rect_position", null,
			chat_exposed.rect_position,
			0.5, Tween.TRANS_CUBIC, Tween.EASE_OUT
		)
		tw.interpolate_property(
			chat_bg, "rect_size", null,
			chat_exposed.rect_size,
			0.3, Tween.TRANS_CUBIC, Tween.EASE_OUT
		)
	else:
		var a = 0.5
		if dim_timeout == 0.0:
			a = 0.0
		tw.interpolate_property(
			chat, "modulate", null,
			Color(1, 1, 1, a),
			0.2, Tween.TRANS_CUBIC, Tween.EASE_OUT
		)
		tw.interpolate_property(
			chat, "rect_position", null,
			chat_unexposed.rect_position,
			0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
		)
		tw.interpolate_property(
			chat, "rect_size", null,
			chat_unexposed.rect_size,
			0.3, Tween.TRANS_CUBIC, Tween.EASE_OUT
		)
		tw.interpolate_property(
			chat_bg, "modulate", null,
			Color(1, 1, 1, a),
			0.3, Tween.TRANS_CUBIC, Tween.EASE_OUT
		)
		tw.interpolate_property(
			chat_bg, "rect_position", null,
			chat_unexposed.rect_position,
			0.5, Tween.TRANS_CUBIC, Tween.EASE_OUT
		)
		tw.interpolate_property(
			chat_bg, "rect_size", null,
			chat_unexposed.rect_size,
			0.3, Tween.TRANS_CUBIC, Tween.EASE_OUT
		)
	tw.start()

var who_rect_pos = Vector2()
var who_rect_size = Vector2()
var input_rect_pos = Vector2()
var input_rect_size = Vector2()

func update_who():
	tw.remove(who, "rect_position")
	tw.remove(who, "rect_size")
	tw.remove(input, "rect_position")
	tw.remove(input, "rect_size")
	who_rect_pos = who.rect_position
	who_rect_size = who.rect_size
	input_rect_pos = input.rect_position
	input_rect_size = input.rect_size
	
	if chat_who:
		who.text = "ALL: "
	else:
		who.text = "TEAM: "
	_on_input_text_changed()
	yield(get_tree(), "idle_frame")
	
	tw.interpolate_property(who, "rect_position",
			who_rect_pos, who.rect_position, 0.2,
			Tween.TRANS_CUBIC, Tween.EASE_OUT)
	tw.interpolate_property(who, "rect_size",
			who_rect_size, who.rect_size, 0.2,
			Tween.TRANS_CUBIC, Tween.EASE_OUT)
	tw.interpolate_property(input, "rect_position",
			input_rect_pos, input.rect_position, 0.2,
			Tween.TRANS_CUBIC, Tween.EASE_OUT)
	tw.interpolate_property(input, "rect_size",
			input_rect_size, input.rect_size, 0.2,
			Tween.TRANS_CUBIC, Tween.EASE_OUT)
	tw.start()

var shift_pressed = false

func _input(event: InputEvent) -> void:
	if $"../pause".showed or !exposed: return
	if event.is_action_pressed("chat_shift"):
		shift_pressed = true
	if event.is_action_released("chat_shift"):
		shift_pressed = false
	if event.is_action_pressed("chat_who"):
		if shift_pressed:
			chat_who = !chat_who
			update_who()
	if event.is_action_pressed("chat_enter"):
		if !shift_pressed:
			send_message()
	if event.is_action_pressed("chat_esc"):
		dim_timeout = DIM_AFTER_MSG
		exposed = false
		input.release_focus()
		update_exposed()
	if event.is_action_pressed("ui_up"):
		yield(get_tree(), "idle_frame")
		input.grab_focus()
		input.cursor_set_line(input.cursor_get_line() - 1)
	if event.is_action_pressed("ui_down"):
		yield(get_tree(), "idle_frame")
		input.grab_focus()
		input.cursor_set_line(input.cursor_get_line() + 1)

func send_message():
	if input.text.length() < 1:
		yield(get_tree(), "idle_frame")
		input.text = ""
		_on_input_text_changed()
		return
	G.Client.send_msg(input.text, Net.SERVER_ID, chat_who)
	yield(get_tree(), "idle_frame")
	input.text = ""
	_on_input_text_changed()

func new_message(message: String, to_all: bool, id: int) -> void:
	
	var pl = G.World.players.get_node_or_null(str(id))
	var my_pl = G.my_player
	if pl == null or my_pl == null: return
	if !to_all and pl.team != my_pl.team:
		return
	
	if message == "F":
		emit_signal("oof")
	
	var cooked = "[color="
	
	if pl.team == G.TEAM_1:
		if to_all:
			cooked += "#" + $"..".TEAM_1_COLOR.to_html(false)
		else:
			cooked += "#" + $"..".TEAM_1_COLOR.darkened(0.25).to_html(false)
	else:
		if to_all:
			cooked += "#" + $"..".TEAM_2_COLOR.to_html(false)
		else:
			cooked += "#" + $"..".TEAM_2_COLOR.darkened(0.25).to_html(false)
	cooked += "]"
	
	if to_all:
		cooked += "[ALL] "
	else:
		cooked += "[TEAM] "
	
	cooked += pl.caption + "[/color]: "
	cooked += message
	
	var how_much_pop = 0
	
	for i in range(OPEN_TAGS.size()):
		how_much_pop += message.count(OPEN_TAGS[i]) - message.count(CLOSE_TAGS[i])
	
	msg.append_bbcode(cooked + "\n")
	for i in range(how_much_pop):
		msg.pop()
	
	if exposed: return
	dim_timeout = DIM_AFTER_MSG
	tw.remove(chat, "modulate")
	tw.interpolate_property(
		chat, "modulate", null,
		Color(1, 1, 1, 0.5),
		0.2, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.interpolate_property(
		chat_bg, "modulate", null,
		Color(1, 1, 1, 0.5),
		0.3, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.start()

func _process(delta: float) -> void:
	if dim_timeout > 0 and !exposed:
		dim_timeout -= delta
		if dim_timeout < 0:
			tw.remove(chat, "modulate")
			tw.interpolate_property(
				chat, "modulate", null,
				Color(1, 1, 1, 0.0),
				0.2, Tween.TRANS_CUBIC, Tween.EASE_OUT
			)
			tw.interpolate_property(
				chat_bg, "modulate", null,
				Color(1, 1, 1, 0.0),
				0.3, Tween.TRANS_CUBIC, Tween.EASE_OUT
			)
			tw.start()

func _on_chat_focus():
	if exposed: return
	exposed = true
	yield(get_tree(), "idle_frame")
	input.grab_focus()
	update_exposed()

func _on_hud_resized():
	yield(get_tree(), "idle_frame")
	update_exposed()

onready var input_font = input.get_font("font")
onready var input_min_size = input_font.get_height() +\
			input.get("custom_constants/line_spacing")
var input_scrollbar_width = 0

func _on_input_text_changed():
	if input_scrollbar_width == 0:
		for i in input.get_children():
			if i is VScrollBar:
				input_scrollbar_width = i.rect_size.x + 8
	if input.rect_size.x - input_scrollbar_width > 0:
		input.rect_min_size.y = clamp(
			stepify(
				(input_font\
					.get_wordwrap_string_size(
						input.text, input.rect_size.x - input_scrollbar_width
					).y / input_font.get_height()) * input_min_size,
				input_min_size),
			input_min_size, input_min_size * MAX_LINES)

func _ready() -> void:
	show()
	chat.rect_size = Vector2(0,0)
	chat.rect_position = rect_size
	chat_exposed.hide()
	chat_unexposed.hide()
	if G.Main == null: return
	yield(G.Client, "my_player_added")
	G.UI.connect("chat_focus", self, "_on_chat_focus")
	connect("resized", self, "_on_hud_resized")
	input.connect("text_changed", self, "_on_input_text_changed")
	update_exposed()
	update_who()
