extends Control

onready var tw = $tw

func _on_panic_on():
	tw.remove(self, "modulate:a")
	tw.interpolate_property(
		self, "modulate:a",
		null, 1.0, 0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.start()

func _on_panic_off():
	tw.remove(self, "modulate:a")
	tw.interpolate_property(
		self, "modulate:a",
		null, 0.0, 0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.start()


func _ready() -> void:
	get_parent().connect("panic_on", self, "_on_panic_on")
	get_parent().connect("panic_off", self, "_on_panic_off")
