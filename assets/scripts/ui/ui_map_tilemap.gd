extends Node2D

var texture: ImageTexture = null
var image: Image = null
var used_rect: Rect2
var draw_offset = Vector2(0,0)

func _on_map_received(img: Image, rect: Rect2) -> void:
	image = img
	used_rect = rect
	texture = ImageTexture.new()
	texture.create_from_image(image, 0)
	update()

func _on_map_process_cell(pos: Vector2, is_bg: bool) -> void:
	if is_bg:
		image.set_pixelv(pos, COLORSCHEME.MINIMAP_TILE_BACK)
	else:
		image.set_pixelv(pos, Color(0, 0, 0, 0))

func _on_map_process_cell_start() -> void:
	image.lock()

func _on_map_process_cell_end() -> void:
	image.unlock()
	texture.create_from_image(image, 0)
	update()

func _ready() -> void:
	if G.Main == null: return
	yield(G.Client, "client_created")
	G.World.map.connect("received_map", self, "_on_map_received")
	G.World.map.connect("process_cell", self, "_on_map_process_cell")
	G.World.map.connect("start_process_cell", self, "_on_map_process_cell_start")
	G.World.map.connect("end_process_cell", self, "_on_map_process_cell_end")

func _draw() -> void:
	if texture != null:
		draw_offset = used_rect.end + used_rect.position
		draw_texture(texture, - used_rect.size / 2 + draw_offset / 2)
