extends Node2D

var players = {}
var seats = {}
var points = {}

class map_point:
	var pos = Vector2(0,0)
	var state = -1

class map_player:
	var pos = Vector2(0,0)
	var team = -1
	var mine = false
	var crouch = false
	var hide = false

func _on_new_point(idx: int, pos: Vector2):
	points[idx] = map_point.new()
	points[idx].pos = pos / 32
	points[idx].state = Points.STATE_NONE
	update()

func _on_point_ev(idx: int, state: int):
	if points.has(idx):
		points[idx].state = state
		update()

func _on_player_changed_crouch(id: int, val: bool) -> void:
	if players.has(id):
		players[id].crouch = val
		update()

func _on_player_hide_from_map(id: int, val: bool) -> void:
	if players.has(id):
		players[id].hide = val
		update()

func _on_player_change_pos(id: int, pos: Vector2) -> void:
	if players.has(id):
		if pos != Vector2(NAN, NAN):
			players[id].pos = (pos / 32) + Vector2(0, 0.5)
		update()

func _on_player_added(id: int) -> void:
	var pl = G.World.players.get_node_or_null(str(id))
	if is_instance_valid(pl) and !players.has(id):
		players[id] = map_player.new()
		players[id].pos = pl.position
		players[id].team = pl.team
		players[id].mine = (pl == G.my_player)
		if pl.mon_seatoni == "":
			yield(pl, "changed_seat")
		seats[id] = get_node(pl.mon_seatoni).position / 32
		pl.connect("pl_changed_position", self, "_on_player_change_pos")
		pl.connect("pl_hide_from_map", self, "_on_player_hide_from_map")
		pl.connect("pl_changed_crouch", self, "_on_player_changed_crouch")
		update()


func _on_player_deleted(id: int) -> void:
	if players.has(id):
		players.erase(id)
		seats.erase(id)
		update()
		

var _c = Color()
var _p = null
var eflag = false

func _draw() -> void:
	for i in points.keys():
		_p = points[i]
		eflag = false
		match _p.state:
			Points.STATE_ACTIVE:
				_c = COLORSCHEME.MINIMAP_POINT_ACTIVE
			Points.STATE_CAPPED_1:
				_c = COLORSCHEME.MINIMAP_POINT_CAPPED_1
			Points.STATE_CAPPED_2:
				_c = COLORSCHEME.MINIMAP_POINT_CAPPED_2
			Points.STATE_CAP_1:
				_c = COLORSCHEME.MINIMAP_POINT_CAP_1
			Points.STATE_CAP_2:
				_c = COLORSCHEME.MINIMAP_POINT_CAP_2
			_:
				eflag = true
		if eflag:
			continue
		draw_circle(
			_p.pos, COLORSCHEME.MINIMAP_POINT_RADIUS, _c
			)
	
	for i in seats.keys():
		draw_rect(
				Rect2(seats[i] - Vector2(2.5, -1.75), Vector2(5, 2)),
				COLORSCHEME.MINIMAP_SEAT
			)
	
	for i in players.keys():
		_p = players[i]
		if _p.mine:
			if _p.team == G.TEAM_1:
				if _p.hide:
					_c = COLORSCHEME.MINIMAP_PL_ST_TEAM_1
				else:
					_c = COLORSCHEME.MINIMAP_PL_MY_TEAM_1
			elif _p.team == G.TEAM_2:
				if _p.hide:
					_c = COLORSCHEME.MINIMAP_PL_ST_TEAM_2
				else:
					_c = COLORSCHEME.MINIMAP_PL_MY_TEAM_2
		else:
			if _p.hide:
				continue
			if _p.team == G.TEAM_1:
				_c = COLORSCHEME.MINIMAP_PL_TEAM_1
			elif _p.team == G.TEAM_2:
				_c = COLORSCHEME.MINIMAP_PL_TEAM_2
		
		if _p.crouch:
			draw_rect(
					Rect2(players[i].pos - Vector2(1, 1.5), Vector2(2, 2)),
					_c
				)
		else:
			draw_rect(
					Rect2(players[i].pos - Vector2(1, 1.5), Vector2(2, 2.5)),
					_c
				)

func _ready() -> void:
	if G.Main == null: return
	G.Client.connect("player_added", self, "_on_player_added")
	G.Client.connect("player_deleted", self, "_on_player_deleted")
	yield(G.Client, "client_created")
	G.World.points.connect("map_new_point", self, "_on_new_point")
	G.World.points.connect("map_point_ev", self, "_on_point_ev")
	yield(G.Client, "my_player_added")
	_on_player_added(int(G.my_player.name))
