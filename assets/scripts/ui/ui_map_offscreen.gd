extends Node2D

const INDICATOR_GAP = 8

var points = {}

onready var tilemap = $"../tilemap"
onready var parent = $".."
onready var tw = $tw

var _p = null
var _c = Color()
var _pos = Vector2()
var _rrect = Rect2()
var eflag = false

class map_point:
	var pos = Vector2(0,0)
	var state = -1

func _on_tab(val: bool):
	if val:
		tw.remove(self, "modulate")
		modulate.a = 0.0

func _on_map_exposed_end(val: bool):
	if !val:
		tw.remove(self, "modulate")
		tw.interpolate_property(self, "modulate", null, Color(1, 1, 1, 1), 0.2,
				Tween.TRANS_LINEAR, Tween.EASE_OUT)
		tw.start()
		update_indicators(true)

var tween_i_pos = Vector2(0,0)

func update_indicators(with_tween=false):
	for i in get_children():
		if i == tw: continue
		_p = points[i.name]
		eflag = false
		match _p.state:
			Points.STATE_ACTIVE:
				_c = COLORSCHEME.MINIMAP_POINT_ACTIVE
			Points.STATE_CAPPED_1:
				_c = COLORSCHEME.MINIMAP_POINT_CAPPED_1
			Points.STATE_CAPPED_2:
				_c = COLORSCHEME.MINIMAP_POINT_CAPPED_2
			Points.STATE_CAP_1:
				_c = COLORSCHEME.MINIMAP_POINT_CAP_1
			Points.STATE_CAP_2:
				_c = COLORSCHEME.MINIMAP_POINT_CAP_2
			_:
				eflag = true
		_pos = points[i.name].pos * tilemap.scale.x + tilemap.position
		i.visible = !(eflag ||
			Rect2(Vector2(0,0), parent.rect_size).has_point(_pos))
		if !i.visible:
			continue
		if with_tween:
			tween_i_pos = i.position
		if _pos.x > parent.rect_size.x && _pos.y < 0:
			i.position = Vector2(parent.rect_size.x, 0)
		elif _pos.x < 0 && _pos.y > parent.rect_size.y:
			i.position = Vector2(0, parent.rect_size.y)
		elif _pos.x > 0 && _pos.y > 0:
			_rrect = Rect2(Vector2(0,0),
				parent.rect_size).clip(Rect2(Vector2(0,0), _pos))
			i.position = _rrect.end
		else:
			_rrect = Rect2(Vector2(0,0),
				parent.rect_size).clip(Rect2(_pos, parent.rect_size - _pos))
			i.position = _rrect.position
		i.modulate = _c
		i.modulate.a = clamp(
			_pos.distance_to(parent.rect_size / 2) /
			clamp(i.position.distance_to(parent.rect_size / 2) - 1.0, 0.1, INF),
			0.0, 1.0)
		i.rotation = (parent.rect_size / 2).angle_to_point(_pos) + PI
		i.position -= Vector2(INDICATOR_GAP, 0).rotated(i.rotation)
		if with_tween:
			tw.remove(i, "position")
			tw.interpolate_property(i, "position", tween_i_pos,
					i.position, 0.2, Tween.TRANS_CUBIC, Tween.EASE_OUT)
			i.position = tween_i_pos
	if with_tween:
		tw.start()

var n

func _on_point_ev(idx: int, state: int):
	n = str(idx)
	if points.has(n):
		points[n].state = state
		update_indicators()

func _on_new_point(idx: int, pos: Vector2):
	n = str(idx)
	points[n] = map_point.new()
	points[n].pos = pos / 32
	points[n].state = Points.STATE_NONE
	var indicator = Sprite.new()
	indicator.name = n
	indicator.texture = preload("res://assets/graphics/ui/offscreen_indicator.png")
	add_child(indicator)


func _ready() -> void:
	if G.Main == null: return
	yield(G.Client, "client_created")
	G.World.points.connect("map_new_point", self, "_on_new_point")
	G.World.points.connect("map_point_ev", self, "_on_point_ev")
	G.UI.connect("map_exposed_end", self, "_on_map_exposed_end")
	G.UI.connect("tab_pressed", self, "_on_tab")
	
