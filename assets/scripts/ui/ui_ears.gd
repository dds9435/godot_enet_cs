extends Control


onready var left = $left
onready var right = $right
onready var left_vbox = $left/vbox
onready var right_vbox = $right/vbox
onready var left_placeholder = $left_placeholder
onready var right_placeholder = $right_placeholder
onready var tw = $tw

var exposed = false

func push_pl_info(to: Panel, nickname_str: String,
		avatar_str: String, acs_arr: Array,
		stat_arr: Array, dead: bool):
	var holder: Control = to.get_node("holder")
	
	var nickname: Label = holder.get_node("nickname")	
	var avatar: TextureRect = holder.get_node("avatar")
	var acs: Control = holder.get_node("acs")
	var acs_cont: Array = acs.get_children()
	var stat: Label = holder.get_node("stat")
	
	var side: int = 0
	
	if to.get_parent() == right_vbox:
		side = 1
		
	
	for i in acs_cont:
		i.hide()
	stat.hide()
	
	if nickname_str == "":
		nickname.text = "..."
		avatar.texture = preload("res://icon.png")
		
		holder.modulate.a = 0.2
	else:
		nickname.text = nickname_str
		if avatar_str == "":
			avatar.texture = preload("res://icon.png")
		else:
			avatar.texture = load(avatar_str)
		stat.rect_position = acs.rect_position
		if side == 1:
			stat.rect_position = acs.rect_position -\
					Vector2(stat.rect_size.x, 0) + Vector2(53, 0)
		
		for i in range(acs_arr.size()):
			if side == 1:
				i = 2 - i
				stat.rect_position.x -= 20
			else:
				stat.rect_position = acs.rect_position +\
						acs_cont[i].position + Vector2(20, 0)
			acs_cont[i].show()
			# TODO ACS
		
		stat.text = ( str(stat_arr[0]) + "/" +
				str(stat_arr[1]) + "/" +
				str(stat_arr[2]) + "/" +
				str(stat_arr[3]) + "/" +
				str(int(stat_arr[4] * 10) / 10) )
		
		stat.show()
		
		if dead:
			holder.modulate.a = 0.5
		else:
			holder.modulate.a = 1.0


func update_players_list():
	var lidx = 0
	var ridx = 0
	
	# TODO SORTING BY STATS
	
	for i in left_vbox.get_children():
		push_pl_info(i, "", "", [], [], false)
	for i in right_vbox.get_children():
		push_pl_info(i, "", "", [], [], false)
	for i in G.World.players.get_children():
		var cvbox = null
		match (i as Player).team:
			G.TEAM_1:
				cvbox = left_vbox.get_node_or_null(str(lidx))
				lidx += 1
			G.TEAM_2:
				cvbox = right_vbox.get_node_or_null(str(ridx))
				ridx += 1
		if cvbox == null: continue
		push_pl_info(cvbox, (i as Player).caption, "",
			range(randi()%4), (i as Player).stats,
			(i as Player).dead)


func _on_player_changed_stats(pl: Player, stats: Array):
	update_players_list()

func _on_player_resurrect(pl: Player):
	update_players_list()

func _on_player_dead(pl: Player):
	update_players_list()

func _on_player_added(id: int) -> void:
	var pl = G.World.players.get_node_or_null(str(id))
	if pl != null:
		pl.connect("changed_stats", self, "_on_player_changed_stats")
		pl.connect("dead", self, "_on_player_dead")
		pl.connect("resurrect", self, "_on_player_resurrect")
	update_players_list()

func _on_player_deleted(id: int) -> void:
	update_players_list()

const SEPARATE = 40

func update_exposed() -> void:
	tw.remove(left, "rect_position")
	tw.remove(right, "rect_position")
	tw.remove(right, "rect_size")
	tw.remove(right, "rect_size")
	tw.remove(left, "modulate")
	tw.remove(right, "modulate")
	tw.remove(left_vbox, "custom_constants/separation")
	tw.remove(right_vbox, "custom_constants/separation")
	var sepsize = int(($left_placeholder.rect_size.y - SEPARATE * 8) / 7)
	var comp = (($left_placeholder.rect_size.y -
			(SEPARATE + sepsize / 2) * 8) / 7 -
			int(($left_placeholder.rect_size.y -
			(SEPARATE + sepsize / 2) * 8) / 7))
	var holder = null
	var avatar = null
	for i in left_vbox.get_children():
		holder = i.get_node("holder")
		avatar = i.get_node("holder/avatar")
		i.rect_min_size.y = SEPARATE + sepsize / 2
		i.rect_size.y = i.rect_min_size.y
		holder.rect_position.x = avatar.rect_position.y - 5
		holder.rect_size.x = i.rect_size.x - holder.rect_position.x
	for i in right_vbox.get_children():
		holder = i.get_node("holder")
		avatar = i.get_node("holder/avatar")
		i.rect_min_size.y = SEPARATE + sepsize / 2
		i.rect_size.y = i.rect_min_size.y
		holder.rect_position.x = - (avatar.rect_position.y - 5)
		holder.rect_size.x = i.rect_size.x - holder.rect_position.x
	if exposed:
		tw.interpolate_property(
			self, "modulate", null,
			Color(1, 1, 1),
			0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
		)
		tw.interpolate_property(
			left, "modulate", null,
			Color(1, 1, 1),
			0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
		)
		tw.interpolate_property(
			left, "rect_position", null,
			left_placeholder.rect_position,
			0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
		)
		tw.interpolate_property(
			left, "rect_size", null,
			left_placeholder.rect_size,
			0.2, Tween.TRANS_CUBIC, Tween.EASE_OUT
		)
		tw.interpolate_property(
			right, "modulate", null,
			Color(1, 1, 1),
			0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
		)
		tw.interpolate_property(
			right, "rect_position", null,
			right_placeholder.rect_position,
			0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
		)
		tw.interpolate_property(
			right, "rect_size", null,
			right_placeholder.rect_size,
			0.2, Tween.TRANS_CUBIC, Tween.EASE_OUT
		)
		tw.interpolate_property(left_vbox, "custom_constants/separation",
			null, ($left_placeholder.rect_size.y - (SEPARATE + sepsize / 2) * 8) / 7,
			 0.3, Tween.TRANS_CUBIC, Tween.EASE_OUT, 0.075)
		tw.interpolate_property(right_vbox, "custom_constants/separation",
			null, ($right_placeholder.rect_size.y - (SEPARATE + sepsize / 2) * 8) / 7,
			 0.3, Tween.TRANS_CUBIC, Tween.EASE_OUT, 0.075)
	else:
		tw.interpolate_property(
			self, "modulate", null,
			Color(1, 1, 1, 0),
			0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
		)
		tw.interpolate_property(
			left, "modulate", null,
			Color(1, 1, 1, 0),
			0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
		)
		tw.interpolate_property(
			left, "rect_position", null,
			(left_placeholder.rect_position -
				Vector2(left_placeholder.rect_size.x, 0)),
			0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
		)
		tw.interpolate_property(
			left, "rect_size", null,
			left_placeholder.rect_size,
			0.2, Tween.TRANS_CUBIC, Tween.EASE_OUT
		)
		tw.interpolate_property(
			right, "modulate", null,
			Color(1, 1, 1, 0),
			0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
		)
		tw.interpolate_property(
			right, "rect_position", null,
			(right_placeholder.rect_position +
				Vector2(right_placeholder.rect_size.x, 0)),
			0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
		)
		tw.interpolate_property(
			right, "rect_size", null,
			right_placeholder.rect_size,
			0.2, Tween.TRANS_CUBIC, Tween.EASE_OUT
		)
		tw.interpolate_property(left_vbox, "custom_constants/separation",
			null, - (SEPARATE + sepsize / 2), 0.2, Tween.TRANS_CUBIC, Tween.EASE_OUT)
		tw.interpolate_property(right_vbox, "custom_constants/separation",
			null, - (SEPARATE + sepsize / 2), 0.2, Tween.TRANS_CUBIC, Tween.EASE_OUT)
	tw.start()


func _on_tab(val: bool):
	exposed = val
	update_exposed()

func _on_hud_resized():
	yield(get_tree(), "idle_frame")
	update_exposed()

func _ready() -> void:
	show()
	left.modulate.a = 0.0
	right.modulate.a = 0.0
	left_placeholder.hide()
	right_placeholder.hide()
	if G.Main == null: return
	yield(G.Client, "my_player_added")
	G.Client.connect("player_added", self, "_on_player_added")
	G.Client.connect("player_deleted", self, "_on_player_deleted")
	G.UI.connect("tab_pressed", self, "_on_tab")
	connect("resized", self, "_on_hud_resized")
	update_exposed()
	update_players_list()
	G.my_player.connect("changed_stats", self, "_on_player_changed_stats")
	G.my_player.connect("dead", self, "_on_player_dead")
	G.my_player.connect("resurrect", self, "_on_player_resurrect")
