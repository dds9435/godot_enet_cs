extends Control

enum {
	RECOIL_MODE_LINE,
	RECOIL_MODE_CROSS
}

var recoil_mode:   int   = RECOIL_MODE_LINE	setget _set_recoil_mode
var recoil_len:    int   = 0				setget _set_recoil_len			# длина линии разброса
var recoil_offset: int   = 0				setget _set_recoil_offset		# отступ от центра для линии разброса
var recoil_color:  Color = Color(1, 1, 1)	setget _set_recoil_color		# цвет линии разброса
var recoil_width:  int   = 0				setget _set_recoil_width		# ширина линии разброса
var recoil_aa:     bool  = false			setget _set_recoil_aa			# сглаживание линий разрброса

var dot_size:      int   = 0                setget _set_dot_size			# радиус центральной точки
var dot_color:     Color = Color(1, 1, 1)   setget _set_dot_color			# цвет центральной точки

var ammo_size:     int   = 0				setget _set_ammo_size			# размер индикатора аммуниции
var gren_offset:   int   = 0				setget _set_gren_offset			# отступ для индикатора гранат
var cross_offset:  int   = 0				setget _set_cross_offset		# отступ от центра для индикаторов аммуниции в режиме перекрестия
var cross_space:   int   = 0				setget _set_cross_space			# отступ между индиакторами аммуниции и гранат в режиме перекрестия
var ammo_thicc:    int   = 0				setget _set_ammo_thicc			# ширина линии индикатора аммуниции
var gren_thicc:    int   = 0				setget _set_gren_thicc			# ширина линии индикатора аммуниции
var cross_aslope:  float = 0.0				setget _set_cross_aslope		# наклон для индикатора аммуниции в режиме перекрестия
var cross_gslope:  float = 0.0				setget _set_cross_gslope		# наклон для индикатора гранат в режиме перекрестия

var on_hit_delay:  float = 0.0					# длительность показа хитмаркера
var on_hit_offset: int   = 0					# отступ линии хитмаркера
var on_hit_len:    int   = 0					# длина линии хитмаркера
var on_hit_color:  Color = Color(1, 0, 0)		# цвет хитмаркера
var on_hit_width:  int   = 0					# ширина линии хитмаркера
var on_hit_aa:     bool  = false                # сглаживание линий хитмаркера

var crecoil = 0.0

var ppos = Vector2(0,0)
var crot = 0.0
var last_ppos = Vector2(0,0)
var last_side = false

var line_u = Vector2(0,0)
var line_b = Vector2(0,0)
var dline_u = 0.0
var dline_b = 0.0

var hit_timer = 0.0

var block_input = false
var map_exposed = false
var is_respawned = false

var grenade_state = false

onready var tw = $tw
onready var line_a = $line_ammo
onready var line_g = $line_grenade
onready var cross_a = $cross_ammo
onready var cross_g = $cross_grenade
onready var line_a_mat = $line_ammo.material
onready var line_g_mat = $line_grenade.material
onready var cross_a_mat = $cross_ammo.material
onready var cross_g_mat = $cross_grenade.material

const DEFAULT_TEMPLATES = [
	{
		"recoil_mode":		RECOIL_MODE_LINE,
		"recoil_len":		32,
		"recoil_offset":	8,
		"recoil_color":		Color(1, 1, 1),
		"recoil_width":		2,
		"recoil_aa":		false,
		
		"dot_size":			2,
		"dot_color":		Color(1, 1, 1),
		
		"ammo_size":		64,
		"gren_offset":		24,
		"ammo_thicc":		16,
		"gren_thicc":		20,
		
		"on_hit_delay":		0.1,
		"on_hit_offset":	4,
		"on_hit_len":		8,
		"on_hit_color":		Color(1, 0, 0),
		"on_hit_width":		2,
		"on_hit_aa":		false
	},
	{
		"recoil_mode":		RECOIL_MODE_CROSS,
		"recoil_len":		16,
		"recoil_offset":	4,
		"recoil_color":		Color(1, 1, 1),
		"recoil_width":		2,
		"recoil_aa":		false,
		
		"dot_size":			2,
		"dot_color":		Color(1, 1, 1, 0.5),
		
		"ammo_size":		24,
		"gren_offset":		8,
		"cross_offset":		16,
		"cross_space":		8,
		"ammo_thicc":		4,
		"gren_thicc":		2,
		"cross_aslope":		0.3,
		"cross_gslope":		0.3,
		
		"on_hit_delay":		0.1,
		"on_hit_offset":	4,
		"on_hit_len":		8,
		"on_hit_color":		Color(1, 0, 0),
		"on_hit_width":		2,
		"on_hit_aa":		false
	}
]

func _draw() -> void:
	# dot
	draw_circle(Vector2(0,0), dot_size, dot_color)
	# hit
	if hit_timer > 0:
		draw_line(
			Vector2(on_hit_offset, 0).rotated(deg2rad(-45)),
			Vector2(on_hit_len + on_hit_offset, 0).rotated(deg2rad(-45)),
			on_hit_color, on_hit_width, on_hit_aa)
		draw_line(
			Vector2(on_hit_offset, 0).rotated(deg2rad(45)),
			Vector2(on_hit_len + on_hit_offset, 0).rotated(deg2rad(45)),
			on_hit_color, on_hit_width, on_hit_aa)
		draw_line(
			Vector2(on_hit_offset, 0).rotated(deg2rad(-135)),
			Vector2(on_hit_len + on_hit_offset, 0).rotated(deg2rad(-135)),
			on_hit_color, on_hit_width, on_hit_aa)
		draw_line(
			Vector2(on_hit_offset, 0).rotated(deg2rad(135)),
			Vector2(on_hit_len + on_hit_offset, 0).rotated(deg2rad(135)),
			on_hit_color, on_hit_width, on_hit_aa)
	# recoil
	match recoil_mode:
		RECOIL_MODE_LINE:
			line_u = Vector2(recoil_offset, crecoil).rotated(crot)
			line_b = Vector2(recoil_offset, -crecoil).rotated(crot)
			dline_u = clamp((ppos*2).distance_to(line_u), 0.1, INF)
			dline_b = clamp((ppos*2).distance_to(line_b), 0.1, INF)
			
			draw_line(
				line_u.linear_interpolate(ppos*2,
					clamp(recoil_len, 0, dline_u) / dline_u),
				line_u,
				recoil_color,
				recoil_width, recoil_aa)
			draw_line(
				line_b.linear_interpolate(ppos*2,
					clamp(recoil_len, 0, dline_b) / dline_b),
				line_b,
				recoil_color,
				recoil_width, recoil_aa)
			
		RECOIL_MODE_CROSS:
			draw_line(
				Vector2(-recoil_offset - crecoil, 0),
				Vector2(-recoil_offset - recoil_len - crecoil, 0),
				recoil_color,
				recoil_width, recoil_aa
			)
			draw_line(
				Vector2(recoil_offset + crecoil, 0),
				Vector2(recoil_offset + recoil_len + crecoil, 0),
				recoil_color,
				recoil_width, recoil_aa
			)
			draw_line(
				Vector2(0, -recoil_offset - crecoil),
				Vector2(0, -recoil_offset - recoil_len - crecoil),
				recoil_color,
				recoil_width, recoil_aa
			)
			draw_line(
				Vector2(0, recoil_offset + crecoil),
				Vector2(0, recoil_offset + recoil_len + crecoil),
				recoil_color,
				recoil_width, recoil_aa
			)


func update_indicators() -> void:
	line_a.rect_size = Vector2(1,1) * ammo_size
	line_a.rect_position = -line_a.rect_size / 2
	line_a.rect_pivot_offset = -line_a.rect_position
	line_g.rect_size = Vector2(1,1) * (ammo_size + gren_offset)
	line_g.rect_position = - line_g.rect_size / 2
	line_g.rect_pivot_offset = -line_g.rect_position
	
	line_a_mat.set_shader_param("thickness", ammo_thicc)
	line_g_mat.set_shader_param("thickness", gren_thicc)
	
	cross_a.rect_size = Vector2(1,1) * ammo_size
	cross_g.rect_size = Vector2(ammo_size - gren_offset, ammo_size)
	
	cross_a.rect_position = (- cross_a.rect_size / 2 -
		Vector2(cross_offset, - cross_space / 2 - ammo_thicc))
	cross_g.rect_position = (- cross_a.rect_size / 2 -
		Vector2(cross_offset - gren_offset, cross_space / 2 + gren_thicc))
	
	cross_a_mat.set_shader_param("thickness",
		float(ammo_thicc) / cross_a.rect_size.x)
	cross_g_mat.set_shader_param("thickness",
		float(gren_thicc) / cross_a.rect_size.x)
	
	cross_a_mat.set_shader_param("slope", cross_aslope)
	cross_g_mat.set_shader_param("slope", cross_gslope)


func update_states() -> void:
	tw.remove(self, "modulate")
	if (!block_input && !map_exposed && is_respawned):
		tw.interpolate_property(
			self, "modulate", null, Color(1, 1, 1, 1),
			0.1, Tween.TRANS_LINEAR, Tween.EASE_OUT)
		get_parent().emit_signal("pointer_state", false)
	else:
		tw.interpolate_property(
			self, "modulate", null, Color(1, 1, 1, 0),
			0.1, Tween.TRANS_LINEAR, Tween.EASE_OUT)
		get_parent().emit_signal("pointer_state", true)
	tw.start()
	if recoil_mode == RECOIL_MODE_CROSS:
		line_a.hide()
		line_g.hide()
		cross_a.show()
		cross_g.visible = grenade_state
	elif recoil_mode == RECOIL_MODE_LINE:
		line_a.show()
		line_g.visible = grenade_state
		cross_a.hide()
		cross_g.hide()


func _set_cross_gslope(slope: float) -> void:
	cross_gslope = slope
	update_indicators()

func _set_cross_aslope(slope: float) -> void:
	cross_aslope = slope
	update_indicators()

func _set_gren_thicc(thicc: int) -> void:
	gren_thicc = thicc
	update_indicators()

func _set_ammo_thicc(thicc: int) -> void:
	ammo_thicc = thicc
	update_indicators()

func _set_cross_space(space: int) -> void:
	cross_space = space
	update_indicators()

func _set_cross_offset(offset: int) -> void:
	cross_offset = offset
	update_indicators()

func _set_gren_offset(offset: int) -> void:
	gren_offset = offset
	update_indicators()

func _set_ammo_size(size: int) -> void:
	ammo_size = size
	update_indicators()

func _set_dot_color(color: Color) -> void:
	dot_color = color
	update()

func _set_dot_size(size: int) -> void:
	dot_size = size
	update()

func _set_recoil_aa(val: bool) -> void:
	recoil_aa = val
	update()

func _set_recoil_width(width: int) -> void:
	recoil_width = width
	update()

func _set_recoil_color(color: Color) -> void:
	recoil_color = color
	update()

func _set_recoil_offset(offset: int) -> void:
	recoil_offset = offset
	update()

func _set_recoil_len(l: int) -> void:
	recoil_len = l
	update()

func _set_recoil_mode(mode: int) -> void:
	recoil_mode = mode
	update()
	update_states()

func _on_changed_cursor_scale(val: float) -> void:
	crecoil = val
	update()

func _on_changed_cursor_ammo(val: float) -> void:
	if recoil_mode == RECOIL_MODE_LINE:
		tw.remove(line_a_mat, "shader_param/value")
		tw.interpolate_property(
			line_a_mat, "shader_param/value", null, 
			val * 50, 0.1, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	elif recoil_mode == RECOIL_MODE_CROSS:
		tw.remove(cross_a_mat, "shader_param/value")
		tw.interpolate_property(
			cross_a_mat, "shader_param/value", null, 
			val * 100, 0.1, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	tw.start()

func _on_changed_grenade_timer(val: float) -> void:
	if recoil_mode == RECOIL_MODE_LINE:
		tw.remove(line_g_mat, "shader_param/value")
		tw.interpolate_property(
			line_g_mat, "shader_param/value", null, 
			val * 15, 0.1, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	elif recoil_mode == RECOIL_MODE_CROSS:
		tw.remove(cross_g_mat, "shader_param/value")
		tw.interpolate_property(
			cross_g_mat, "shader_param/value", null, 
			val * 100, 0.1, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	tw.start()

func _on_changed_cursor_grenade_state(val: bool) -> void:
	grenade_state = val
	update_states()

func _on_hit():
	hit_timer = on_hit_delay
	update()

func _on_respawn():
	ppos = G.my_player.position - G.my_player.camera.global_position
	is_respawned = true
	update()
	update_states()

func _on_death():
	is_respawned = false
	update()
	update_states()

func _on_block_input(val: bool):
	block_input = val
	update()
	update_states()

func _on_map_exposed(val: bool):
	map_exposed = val
	update()
	update_states()

func _on_changed_ammo(who, ammo: int, shoot: bool) -> void:
	_on_changed_cursor_ammo((float(ammo) / (who.AMMO * who.AMMO_SIZE)))

func _process(delta: float) -> void:
	rect_position = get_global_mouse_position()
	
	if G.my_player != null and !G.my_player.dead:
		ppos = G.my_player.position - G.my_player.camera.global_position
		if ppos != last_ppos:
			last_ppos = ppos
			crot = ppos.angle_to_point(Vector2(0,0))
			$line_ammo.rect_rotation = rad2deg(crot) + 180
			$line_grenade.rect_rotation = rad2deg(crot) + 180
			if ppos.x < 0:
				if !last_side:
					line_a_mat.set_shader_param("side", true)
					line_a_mat.set_shader_param("offset", 50)
					line_g_mat.set_shader_param("side", true)
					line_g_mat.set_shader_param("offset", 50)
					
					cross_a.rect_scale.x = 1
					cross_g.rect_scale.x = 1
					
					cross_a.rect_position = (- cross_a.rect_size / 2 -
						Vector2(cross_offset,
						- cross_space / 2 - ammo_thicc))
					cross_g.rect_position = (- cross_a.rect_size / 2 -
						Vector2(cross_offset - gren_offset,
						cross_space / 2 + gren_thicc))
					last_side = true
			else:
				if last_side:
					line_a_mat.set_shader_param("side", false)
					line_a_mat.set_shader_param("offset", 0)
					line_g_mat.set_shader_param("side", false)
					line_g_mat.set_shader_param("offset", 0)
					
					cross_a.rect_scale.x = -1
					cross_g.rect_scale.x = -1
					
					cross_a.rect_position = ((- cross_a.rect_size / 2 -
						Vector2(cross_offset,
						- cross_space / 2 - ammo_thicc)) *
						Vector2(-1, 1))
					cross_g.rect_position = ((- cross_a.rect_size / 2 -
						Vector2(cross_offset - gren_offset,
						cross_space / 2 + gren_thicc)) *
						Vector2(-1, 1))
					last_side = false
					
			if recoil_mode == RECOIL_MODE_LINE:
				update()
	if hit_timer > 0:
		hit_timer -= delta
		if hit_timer < 0:
			hit_timer = 0
			update()

func cursor_template(i: String) -> void:
	for key in DEFAULT_TEMPLATES[int(i)].keys():
		set(key, DEFAULT_TEMPLATES[int(i)][key])

func a_cursor_template(idx: int) -> Array:
	return ["0", "1"]

func _ready() -> void:
	set_process(false)
	cursor_template("0")
	if G.Main == null: return
	G.Console.register_command(
			"cursor_template", {
				desc   = "",
				args   = [1, "<template:int>"],
				target = self,
				completer_target = self,
				completer_method = "a_cursor_template"
			}
	)
	var p = get_parent()
	p.connect("cursor_scale", self, "_on_changed_cursor_scale")
	p.connect("cursor_ammo", self, "_on_changed_cursor_ammo")
	p.connect("grenade_timer", self, "_on_changed_grenade_timer")
	p.connect("cursor_grenade_state", self, "_on_changed_cursor_grenade_state")
	p.connect("cursor_on_hit", self, "_on_hit")
	p.connect("on_respawn", self, "_on_respawn")
	p.connect("on_death", self, "_on_death")
	p.connect("block_input", self, "_on_block_input")
	p.connect("map_exposed", self, "_on_map_exposed")
	yield(G.Client, "my_player_added")
	G.my_player.weapon.connect("me_changed_ammo", self, "_on_changed_ammo")
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	update_indicators()
	set_process(true)
