extends Control

const ICONS_PATH = "res://assets/graphics/ui/upgrade/"

enum {
	BUTTON_BUY,
	BUTTON_CRED,
	BUTTON_REMEMBER
}

const CATEGORIES_NAMES : Dictionary = {
	"Q" : "[center]HP: ",
	"W" : "[center]SHIELDS: ",
	"E" : "[center]BODY: ",
	"A" : "[center]WEAPON SPECIALS: ",
	"S" : "[center]FIREARM BOOSTS: ",
	"D" : "[center]WEAPON BASES: "
}

const CATEGORIES : Dictionary = {
	"Q" : {
		"Q" : "left_top", 
		"W" : "left_bottom",
		"E" : "full", 
		"A" : "right_top",
		"S" : "right_bottom"
	},
	"W" : {
		"Q" : "shield_simple", 
		"W" : "shield_dome"
	},
	"E" : {
		"Q" : "stealth", 
		"W" : "dash", 
		"E" : "legs"
	},
	"A" : {
		"Q" : "scope", 
		"W" : "re_boost", 
		"E" : "barrel",
		"A" : "clip", 
		"S" : "grenade"
	},
	"S" : {
		"Q" : "rate_of_fire", 
		"W" : "snipegun", 
		"E" : "shotgun"
	},
	"D" : {
		"Q" : "laser", 
		"W" : "rocket", 
		"E" : "second_base"
	}
}

onready var info_placeholder = $info_placeholder
onready var respawn_placeholder = $respawn_placeholder
onready var back_placeholder = $back_placeholder
onready var left_placeholder = $left_placeholder
onready var right_placeholder = $right_placeholder
onready var respawn = $respawn
onready var respawn_bg = $respawn_bg
onready var back = $back
onready var back_bg = $back_bg
onready var left = $left
onready var right = $right
onready var info = $info
onready var info_bg = $info_bg
onready var popup_menu = $menu
onready var popup_menu_bg = $menu_bg

onready var tw = $tw

var showed = false

var page = 0
var last_choice = ""

var credited_session = []
var remembered = []

var curnode = null
var fg = null

var pl = null
var we = null

func update_info() -> void:
	if pl == null or we == null: 
		return

	var pl_cooked_str = "[b][PLAYER][/b]\n"
	var we_cooked_str = "[b][WEAPON][/b]\n"
	
	pl_cooked_str += "[b]Left top:[/b] " + str(pl.hitbox_max[G.my_player.HT_LT]) + "\n"
	pl_cooked_str += "[b]Right top:[/b] " + str(pl.hitbox_max[G.my_player.HT_RT]) + "\n"
	pl_cooked_str += "[b]Left bottom:[/b] " + str(pl.hitbox_max[G.my_player.HT_LB]) + "\n"
	pl_cooked_str += "[b]Right bottom:[/b] " + str(pl.hitbox_max[G.my_player.HT_RB]) + "\n"
	pl_cooked_str += "\n"
	pl_cooked_str += "[b]Speed:[/b] " + str(pl.MAX_SPEED) + "\n"
	pl_cooked_str += "[b]Fuel:[/b] " + str(pl.MAX_JET_FUEL) + "\n"
	pl_cooked_str += "[b]Jumps:[/b] " + str(pl.MAX_JUMPS) + "\n"
	
	we_cooked_str += "[b]Shoot rate:[/b] " + str(we.SHOOT_RATE) + "\n"
	we_cooked_str += "[b]Reload rate:[/b] " + str(we.RELOAD_RATE) + "\n"
	we_cooked_str += "[b]Ammo:[/b] " + str(we.AMMO * we.AMMO_SIZE) + "\n"
	we_cooked_str += "\n"
	if we.visual.POSTFIX == "_s":
		we_cooked_str += "[b]Double base[/b]\n"
	match we.visual.BASE:
		"firearm":
			match we.visual.BOOST:
				"snipegun", "shotgun":
					we_cooked_str += "[b]Type:[b] " + we.visual.BOOST + "\n"
					we_cooked_str += ("[b]Damage:[/b] " + str(we.DAMAGE) +
					" (" + str(we.DAMAGE_BASE) + " + " + 
					str(we.DAMAGE_ADD) + ")" +
					"\n")
					if we.visual.BOOST == "shotgun":
						we_cooked_str += "[b]Shots:[/b] " + str(we.SHOTS) + "\n"
				_:
					we_cooked_str += "[b]Type:[/b] " + we.visual.BASE + "\n"
					we_cooked_str += "[b]Damage:[/b] " + str(we.DAMAGE) + "\n"
		"laser":
			we_cooked_str += "[b]Type:[/b] " + we.visual.BASE + "\n"
			we_cooked_str += ("[b]Damage:[/b] " + str(we.DAMAGE) +
			" (" + str(we.DAMAGE_BASE) + " + " +
			str(we.DAMAGE_ADD) + ")" +
			"\n")
		"rocket":
			we_cooked_str += "[b]Type:[/b] " + we.visual.BASE + "\n"
			we_cooked_str += "[b]Damage:[/b] " + str(we.DAMAGE) + "\n"
			we_cooked_str += "[b]Radius:[/b] " + str(we.RADIUS) + "\n"
	if we.GRENADE_ENABLED:
		we_cooked_str += "\n"
		we_cooked_str += "[b]Grenade damage:[/b] " + str(Balance.GRENADE_DEF_DAMAGE) + "\n"
		we_cooked_str += "[b]Grenade radius:[/b] " + str(Balance.GRENADE_DEF_RADIUS) + "\n"
	
	
	$"info/weapon_11".bbcode_text = we_cooked_str
	$"info/player_11".bbcode_text = pl_cooked_str
	$"info/weapon_14".bbcode_text = we_cooked_str
	$"info/player_14".bbcode_text = pl_cooked_str

func back_pressed():
	if page == 1:
		page = 0
		last_choice = ""
		update_page()


func popup_menu_pressed(id: int):
	if !last_choice in CATEGORIES.keys(): return
	if curnode == null || !curnode.name in CATEGORIES[last_choice].keys(): return
	
	var m_name = CATEGORIES[last_choice][curnode.name]
	
	match id:
		0:
			G.Client.sell_module(m_name)
			yield(G.my_player, "me_deleted_module")
			update_page()
			update_info()
		1:
			var state = G.my_player.get_module_state(m_name)
			if state != null:
				G.my_player.module_state(m_name, true, !state)
				yield(get_tree(), "idle_frame")
				update_page()
				update_info()


func button_pressed(from: Panel, kind: int):
	if !last_choice in CATEGORIES.keys(): return
	if !from.name in CATEGORIES[last_choice].keys(): return
	
	var m_name = CATEGORIES[last_choice][from.name]
	
	match kind:
		BUTTON_BUY:
			G.Client.buy_module(m_name)
		BUTTON_CRED:
			G.Client.credit_module(m_name)
			credited_session.append(m_name)
			if remembered.has(m_name):
				remembered.append(m_name)
		BUTTON_REMEMBER:
			if remembered.has(m_name):
				for i in remembered:
					remembered.erase(i)
				from.get_node("holder/remember").pressed = false
			else:
				for i in range(credited_session.count(m_name)):
					remembered.append(m_name)
				from.get_node("holder/remember").pressed = true
	
	var a = 0.0
	if keyboard_mode: a = 0.25
	tw.remove(curnode, "modulate:a")
	tw.interpolate_property(
		from.get_node("fg"), "modulate:a", 1, a,
		0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT)
	tw.start()
	yield(G.my_player, "me_added_module")
	update_page()
	update_info()


func build_module(node: Panel, m_name: String):
	var m : Module = G.ModulesServer.get_module(m_name)
	
	var icon = node.get_node("holder/icon")
	var buy = node.get_node("holder/buy")
	var credit = node.get_node("holder/credit")
	var module_text = node.get_node("holder/module_text")
	var category_text = node.get_node("holder/category_text")
	var remember = node.get_node("holder/remember")
	
	buy.mouse_filter = Control.MOUSE_FILTER_STOP
	credit.mouse_filter = Control.MOUSE_FILTER_STOP
	remember.mouse_filter = Control.MOUSE_FILTER_STOP
	
	tw.remove(icon, "modulate:a")
	tw.remove(buy, "modulate:a")
	tw.remove(credit, "modulate:a")
	tw.remove(module_text, "modulate:a")
	tw.remove(category_text, "modulate:a")
	tw.remove(remember, "modulate:a")
	
	tw.interpolate_property(
		icon, "modulate:a", null, 1,
		0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.interpolate_property(
		buy, "modulate:a", null, 1,
		0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.interpolate_property(
		credit, "modulate:a", null, 1,
		0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.interpolate_property(
		module_text, "modulate:a", null, 1,
		0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.interpolate_property(
		category_text, "modulate:a", null, 0,
		0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.interpolate_property(
		remember, "modulate:a", null, 1,
		0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.start()
	
	if m == null: 
		return []

	var m_state = G.my_player.get_module_state(m_name)
	var module_level = G.ModulesServer.get_module_level(m_name, G.my_player)
	var buy_price = G.ModulesServer.get_module_price(
			m_name,
			G.my_player, 
			GameState.OPERATION_BUY)
	var credit_price = G.ModulesServer.get_module_price(
			m_name,
			G.my_player, 
			GameState.OPERATION_CREDIT)
	
	buy.text = str(buy_price) + "$"
	credit.text = str(credit_price) + "P"
	buy.disabled = false
	credit.disabled = false
	remember.disabled = true
	remember.modulate = Color(1, 1, 1, 0.5)
	
	remember.pressed = remembered.has(m_name)
	
	if m_name in credited_session:
		remember.disabled = false
		remember.modulate = Color(1, 1, 1, 1)
	
	if G.my_player.money < buy_price:
		buy.disabled = true
	if G.my_player.credit < credit_price:
		credit.disabled = true
	
	var module_str = ""
	
	if node.name in ["Q", "W", "E"]:
		module_str += "[right]"
	
	if m_state == null:
		module_str += \
			m.d_name + " (" +\
			str(module_level[0]) +\
			"/" + str(module_level[1]) + ")"
	else:
		module_str += \
			m.d_name + " (" +\
			str(module_level[0] + 1) +\
			"/" + str(module_level[1]) + ")"
		
		if module_level[0] + 1 == module_level[1]:
			buy.text = "MAX"
			credit.text = "MAX"
			buy.disabled = true
			credit.disabled = true
	
	module_text.bbcode_text = module_str
	icon.texture = load(ICONS_PATH + m_name + ".png")
	
	

func build_modules_menu():
	if !last_choice in CATEGORIES.keys(): return
	for i in left.get_children():
		if CATEGORIES[last_choice].has(i.name):
			if i.name != last_choice:
				tw.remove(i, "modulate:a")
				tw.interpolate_property(
					i, "modulate:a", null, 1,
					0.2, Tween.TRANS_CUBIC, Tween.EASE_OUT
				)
				tw.start()
			build_module(i, CATEGORIES[last_choice][i.name])
		else:
			tw.remove(i, "modulate:a")
			tw.interpolate_property(
				i, "modulate:a", null, 0,
				0.2, Tween.TRANS_CUBIC, Tween.EASE_OUT
			)
			tw.start()
	for i in right.get_children():
		if CATEGORIES[last_choice].has(i.name):
			if i.name != last_choice:
				tw.remove(i, "modulate:a")
				tw.interpolate_property(
					i, "modulate:a", null, 1,
					0.2, Tween.TRANS_CUBIC, Tween.EASE_OUT
				)
				tw.start()
			build_module(i, CATEGORIES[last_choice][i.name])
		else:
			tw.remove(i, "modulate:a")
			tw.interpolate_property(
				i, "modulate:a", null, 0,
				0.2, Tween.TRANS_CUBIC, Tween.EASE_OUT
			)
			tw.start()


func build_category(node: Panel):
	var icon = node.get_node("holder/icon")
	var buy = node.get_node("holder/buy")
	var credit = node.get_node("holder/credit")
	var module_text = node.get_node("holder/module_text")
	var category_text = node.get_node("holder/category_text")
	var remember = node.get_node("holder/remember")
	
	buy.mouse_filter = Control.MOUSE_FILTER_IGNORE
	credit.mouse_filter = Control.MOUSE_FILTER_IGNORE
	remember.mouse_filter = Control.MOUSE_FILTER_IGNORE
	
	tw.remove(icon, "modulate:a")
	tw.remove(buy, "modulate:a")
	tw.remove(credit, "modulate:a")
	tw.remove(module_text, "modulate:a")
	tw.remove(category_text, "modulate:a")
	tw.remove(remember, "modulate:a")
	
	tw.interpolate_property(
		icon, "modulate:a", null, 0,
		0.2, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.interpolate_property(
		buy, "modulate:a", null, 0,
		0.2, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.interpolate_property(
		credit, "modulate:a", null, 0,
		0.2, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.interpolate_property(
		module_text, "modulate:a", null, 0,
		0.2, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.interpolate_property(
		category_text, "modulate:a", null, 1,
		0.2, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.interpolate_property(
		remember, "modulate:a", null, 0,
		0.2, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.start()
	
	category_text.bbcode_text =\
		CATEGORIES_NAMES[node.name] + str(CATEGORIES[node.name].size())


func build_categories_menu():
	for i in left.get_children():
		if i.modulate.a == 0.0:
			tw.remove(i, "modulate:a")
			tw.interpolate_property(
				i, "modulate:a", null, 1,
				0.2, Tween.TRANS_CUBIC, Tween.EASE_OUT
			)
			tw.start()
		build_category(i)
	for i in right.get_children():
		if i.modulate.a == 0.0:
			tw.remove(i, "modulate:a")
			tw.interpolate_property(
				i, "modulate:a", null, 1,
				0.2, Tween.TRANS_CUBIC, Tween.EASE_OUT
			)
			tw.start()
		build_category(i)


func update_page():
	match page:
		0:
			tw.remove(back, "modulate:a")
			tw.remove(back_bg, "modulate:a")
			tw.interpolate_property(
				back, "modulate:a", null, 0,
				0.2, Tween.TRANS_CUBIC, Tween.EASE_OUT
			)
			tw.interpolate_property(
				back_bg, "modulate:a", null, 0,
				0.2, Tween.TRANS_CUBIC, Tween.EASE_OUT
			)
			tw.start()
			build_categories_menu()
		1:
			tw.remove(back, "modulate:a")
			tw.remove(back_bg, "modulate:a")
			tw.interpolate_property(
				back, "modulate:a",
				null, 1,
				0.2, Tween.TRANS_CUBIC, Tween.EASE_OUT
			)
			tw.interpolate_property(
				back_bg, "modulate:a",
				null, 1,
				0.2, Tween.TRANS_CUBIC, Tween.EASE_OUT
			)
			tw.start()
			build_modules_menu()


func popup_menu_show():
	
	popup_menu.clear()
	popup_menu.add_item("Sell")
	
	if last_choice in CATEGORIES.keys():
		if curnode != null && curnode.name in CATEGORIES[last_choice].keys():
			
			var m_name = CATEGORIES[last_choice][curnode.name]
			var state = G.my_player.get_module_state(m_name)
			if state != null:
				if state:
					popup_menu.add_item("Disable")
				else:
					popup_menu.add_item("Enable")
	
	popup_menu.rect_position = get_local_mouse_position()
	popup_menu_bg.rect_position = popup_menu.rect_position
	popup_menu.rect_size = Vector2(0,0)
	popup_menu_bg.rect_size = Vector2(0,0)
	tw.remove(popup_menu, "modulate:a")
	tw.remove(popup_menu_bg, "modulate:a")
	tw.remove(popup_menu_bg, "rect_size")
	tw.interpolate_property(
		popup_menu, "modulate:a", 0, 1,
		0.2, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.interpolate_property(
		popup_menu_bg, "modulate:a", 0, 1,
		0.2, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.interpolate_property(
		popup_menu_bg, "rect_size",
		null, popup_menu.rect_size,
		0.2, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.start()


func popup_menu_hide():
	popup_menu.show()
	tw.remove(popup_menu, "modulate:a")
	tw.remove(popup_menu_bg, "modulate:a")
	tw.interpolate_property(
		popup_menu, "modulate:a", 1, 0,
		0.2, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.interpolate_property(
		popup_menu_bg, "modulate:a", 1, 0,
		0.2, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.start()
	yield(get_tree().create_timer(0.2), "timeout")
	popup_menu.hide()


var keyboard_mode = false

func _input(event: InputEvent) -> void:
	if G.UI.block_input: return
	if event.is_action_pressed("lmb"):
		if curnode == null or page == 1: return
		tw.remove(curnode.get_node("fg"), "modulate:a")
		tw.interpolate_property(
			curnode.get_node("fg"), "modulate:a", 1, 0,
			0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
		)
		tw.start()
		if page == 0:
			page = 1
			last_choice = curnode.name
			update_page()
	if event.is_action_pressed("rmb") and page == 1 and curnode != null:
		if last_choice in CATEGORIES.keys():
			if curnode != null && curnode.name in CATEGORIES[last_choice].keys():
				
				var m_name = CATEGORIES[last_choice][curnode.name]
				var state = G.my_player.get_module_state(m_name)
				if state != null:
					popup_menu.popup()
	
	if event.is_action_pressed("space"):
		_on_respawn_pressed()
	
	if event is InputEventMouse and keyboard_mode:
		if curnode != null:
			mouse_exited(curnode)
		keyboard_mode = false
	
	if event is InputEventKey and event.is_pressed() and event.as_text() in\
						["Q", "W", "E", "A", "S", "D", "Escape"]:
		keyboard_mode = true
		if page == 0 and event.as_text() != "Escape":
			curnode = find_node(event.as_text())
			tw.remove(curnode.get_node("fg"), "modulate:a")
			tw.interpolate_property(
				curnode.get_node("fg"), "modulate:a", 1, 0,
				0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
			)
			tw.start()
			curnode = null
			page = 1
			last_choice = event.as_text()
			update_page()
		elif page == 1:
			if event.as_text() == "Escape":
				if curnode == null:
					back_pressed()
				else:
					mouse_exited(curnode)
			else:
				if curnode == null:
					mouse_entered(find_node(event.as_text()))
				else:
					match event.as_text():
						"Q":
							button_pressed(curnode, BUTTON_BUY)
						"W":
							button_pressed(curnode, BUTTON_CRED)
						"E":
							button_pressed(curnode, BUTTON_REMEMBER)
						"A":
							popup_menu_pressed(0)
						"S":
							popup_menu_pressed(1)

func mouse_entered(node: Panel):
	curnode = node
	fg = node.get_node("fg")
	if !keyboard_mode and page == 1: return
	tw.remove(fg, "modulate:a")
	tw.interpolate_property(
		fg, "modulate:a",
		null, 0.25,
		0.2, Tween.TRANS_CUBIC, Tween.EASE_OUT)
	tw.start()

func mouse_exited(node: Panel):
	if popup_menu.visible: return
	curnode = null
	fg = node.get_node("fg")
	tw.remove(fg, "modulate:a")
	tw.interpolate_property(
		fg, "modulate:a", null, 0,
		0.2, Tween.TRANS_CUBIC, Tween.EASE_OUT)
	tw.start()


const SEPARATE = -30

func update_exposed():
	tw.remove(respawn, "rect_position")
	tw.remove(respawn_bg, "rect_position")
	tw.remove(back, "rect_position")
	tw.remove(back_bg, "rect_position")
	tw.remove(left, "rect_position")
	tw.remove(right, "rect_position")
	tw.remove(left, "rect_size")
	tw.remove(right, "rect_size")
	tw.remove(info, "rect_position")
	tw.remove(info_bg, "rect_position")
	tw.remove(left, "custom_constants/separation")
	tw.remove(right, "custom_constants/separation")
	var sepsize = ($left_placeholder.rect_size.y - SEPARATE * 3) / 2
	var holder = null
	var icon = null
	for i in left.get_children():
		holder = i.get_node("holder")
		icon = i.get_node("holder/icon")
		i.rect_min_size.y = SEPARATE + sepsize / 2
		i.rect_size.y = i.rect_min_size.y
		holder.rect_position.x = icon.rect_position.y
		holder.rect_size.x = i.rect_size.x - icon.rect_position.y * 2 - 2.5
	for i in right.get_children():
		holder = i.get_node("holder")
		icon = i.get_node("holder/icon")
		i.rect_min_size.y = SEPARATE + sepsize / 2
		i.rect_size.y = i.rect_min_size.y
		holder.rect_position.x = icon.rect_position.y + 5
		holder.rect_size.x = i.rect_size.x - icon.rect_position.y * 2
	tw.interpolate_property(
		respawn, "rect_position", null,
		respawn_placeholder.rect_position,
		0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.interpolate_property(
		respawn_bg, "rect_position", null,
		respawn_placeholder.rect_position,
		0.5, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.interpolate_property(
		back, "rect_position", null,
		back_placeholder.rect_position,
		0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.interpolate_property(
		back_bg, "rect_position", null,
		back_placeholder.rect_position,
		0.5, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.interpolate_property(
		left, "rect_position", null,
		left_placeholder.rect_position,
		0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.interpolate_property(
		right, "rect_position", null,
		right_placeholder.rect_position,
		0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.interpolate_property(
		left, "rect_size", null,
		left_placeholder.rect_size,
		0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.interpolate_property(
		right, "rect_size", null,
		right_placeholder.rect_size,
		0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.interpolate_property(
		info, "rect_position", null,
		info_placeholder.rect_position,
		0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.interpolate_property(
		info_bg, "rect_position", null,
		info_placeholder.rect_position,
		0.5, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.interpolate_property(
		info, "rect_size", null,
		info_placeholder.rect_size,
		0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.interpolate_property(
		info_bg, "rect_size", null,
		info_placeholder.rect_size,
		0.5, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.interpolate_property(left, "custom_constants/separation",
		null, ($left_placeholder.rect_size.y - (SEPARATE + sepsize / 2) * 3) / 2,
		 0.2, Tween.TRANS_CUBIC, Tween.EASE_OUT)
	tw.interpolate_property(right, "custom_constants/separation",
		null, ($left_placeholder.rect_size.y - (SEPARATE + sepsize / 2) * 3) / 2,
		 0.2, Tween.TRANS_CUBIC, Tween.EASE_OUT)
	tw.start()


func on_show():
	set_process_input(true)
	credited_session = []
	page = 0
	if curnode != null:
		mouse_exited(curnode)
	curnode = null
	last_choice = ""
	for i in remembered:
		G.Client.credit_module(i)
		yield(G.my_player, "me_added_module")
	showed = true
	modulate = Color(1, 1, 1, 0)
	show()
	tw.remove(self, "modulate:a")
	tw.remove($"../hud", "modulate:a")
	tw.interpolate_property(
		self, "modulate:a", null, 1,
		0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.interpolate_property(
		$"../hud", "modulate:a", null, 0,
		0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.start()
	update_page()


func on_hide():
	set_process_input(false)
	showed = false
	tw.remove(self, "modulate:a")
	tw.remove($"../hud", "modulate:a")
	tw.interpolate_property(
		self, "modulate:a", null, 0,
		0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.interpolate_property(
		$"../hud", "modulate:a", null, 1,
		0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.start()
	yield(get_tree().create_timer(0.4), "timeout")
	hide()


func _on_hud_resized():
	yield(get_tree(), "idle_frame")
	update_exposed()
	if rect_size > Vector2(1600, 900):
		info.get_node("player_14").show()
		info.get_node("weapon_14").show()
		info.get_node("player_11").hide()
		info.get_node("weapon_11").hide()
	else:
		info.get_node("player_14").hide()
		info.get_node("weapon_14").hide()
		info.get_node("player_11").show()
		info.get_node("weapon_11").show()

func _on_respawn_pressed():
	if respawn.modulate.a != 1.0: return
	
	if G.UI.show_respawn:
		G.UI.respawn()

func chrespawn(state: bool) -> void:
	if state:
		tw.remove(respawn, "modulate:a")
		tw.remove(respawn_bg, "modulate:a")
		tw.interpolate_property(respawn, "modulate:a",
		null, 1, 0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT)
		tw.interpolate_property(respawn_bg, "modulate:a",
		null, 1, 0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT)
		tw.start()
	else:
		tw.remove(respawn, "modulate:a")
		tw.remove(respawn_bg, "modulate:a")
		tw.interpolate_property(respawn, "modulate:a",
		null, 0, 0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT)
		tw.interpolate_property(respawn_bg, "modulate:a",
		null, 0, 0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT)
		tw.start()

func _on_tab(val: bool):
	if !showed: return
	tw.remove(self, "modulate")
	if val:
		tw.interpolate_property(
			self, "modulate", null,
			Color(1, 1, 1, 0),
			0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
		)
	else:
		tw.interpolate_property(
			self, "modulate", null,
			Color(1, 1, 1, 1),
			0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
		)
	tw.start()

func _ready() -> void:
	info_placeholder.hide()
	respawn_placeholder.hide()
	back_placeholder.hide()
	left_placeholder.hide()
	right_placeholder.hide()
	hide()
	if G.Main == null: return
	yield(G.Client, "my_player_added")
	connect("resized", self, "_on_hud_resized")
	respawn.connect("pressed", self, "_on_respawn_pressed")
	G.UI.connect("tab_pressed", self, "_on_tab")
	
	left.get_node("Q").connect("mouse_entered", self, "mouse_entered", [left.get_node("Q")])
	left.get_node("W").connect("mouse_entered", self, "mouse_entered", [left.get_node("W")])
	left.get_node("E").connect("mouse_entered", self, "mouse_entered", [left.get_node("E")])
	right.get_node("A").connect("mouse_entered", self, "mouse_entered", [right.get_node("A")])
	right.get_node("S").connect("mouse_entered", self, "mouse_entered", [right.get_node("S")])
	right.get_node("D").connect("mouse_entered", self, "mouse_entered", [right.get_node("D")])
	left.get_node("Q").connect("mouse_exited", self, "mouse_exited", [left.get_node("Q")])
	left.get_node("W").connect("mouse_exited", self, "mouse_exited", [left.get_node("W")])
	left.get_node("E").connect("mouse_exited", self, "mouse_exited", [left.get_node("E")])
	right.get_node("A").connect("mouse_exited", self, "mouse_exited", [right.get_node("A")])
	right.get_node("S").connect("mouse_exited", self, "mouse_exited", [right.get_node("S")])
	right.get_node("D").connect("mouse_exited", self, "mouse_exited", [right.get_node("D")])
	
	left.get_node("Q/holder/buy").connect("pressed", self, "button_pressed", [left.get_node("Q"), BUTTON_BUY])
	left.get_node("W/holder/buy").connect("pressed", self, "button_pressed", [left.get_node("W"), BUTTON_BUY])
	left.get_node("E/holder/buy").connect("pressed", self, "button_pressed", [left.get_node("E"), BUTTON_BUY])
	right.get_node("A/holder/buy").connect("pressed", self, "button_pressed", [right.get_node("A"), BUTTON_BUY])
	right.get_node("S/holder/buy").connect("pressed", self, "button_pressed", [right.get_node("S"), BUTTON_BUY])
	right.get_node("D/holder/buy").connect("pressed", self, "button_pressed", [right.get_node("D"), BUTTON_BUY])
	left.get_node("Q/holder/credit").connect("pressed", self, "button_pressed", [left.get_node("Q"), BUTTON_CRED])
	left.get_node("W/holder/credit").connect("pressed", self, "button_pressed", [left.get_node("W"), BUTTON_CRED])
	left.get_node("E/holder/credit").connect("pressed", self, "button_pressed", [left.get_node("E"), BUTTON_CRED])
	right.get_node("A/holder/credit").connect("pressed", self, "button_pressed", [right.get_node("A"), BUTTON_CRED])
	right.get_node("S/holder/credit").connect("pressed", self, "button_pressed", [right.get_node("S"), BUTTON_CRED])
	right.get_node("D/holder/credit").connect("pressed", self, "button_pressed", [right.get_node("D"), BUTTON_CRED])
	left.get_node("Q/holder/remember").connect("pressed", self, "button_pressed", [left.get_node("Q"), BUTTON_REMEMBER])
	left.get_node("W/holder/remember").connect("pressed", self, "button_pressed", [left.get_node("W"), BUTTON_REMEMBER])
	left.get_node("E/holder/remember").connect("pressed", self, "button_pressed", [left.get_node("E"), BUTTON_REMEMBER])
	right.get_node("A/holder/remember").connect("pressed", self, "button_pressed", [right.get_node("A"), BUTTON_REMEMBER])
	right.get_node("S/holder/remember").connect("pressed", self, "button_pressed", [right.get_node("S"), BUTTON_REMEMBER])
	right.get_node("D/holder/remember").connect("pressed", self, "button_pressed", [right.get_node("D"), BUTTON_REMEMBER])
	
	back.connect("pressed", self, "back_pressed")
	
	popup_menu.connect("about_to_show", self, "popup_menu_show")
	popup_menu.connect("popup_hide", self, "popup_menu_hide")
	popup_menu.connect("id_pressed", self, "popup_menu_pressed")
	
	pl = G.my_player
	we = G.my_player.weapon
	
	update_exposed()
	update_page()
	update_info()
