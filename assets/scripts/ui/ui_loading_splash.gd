extends Panel

onready var tw = $tw

func _ready() -> void:
	show()
	yield(G.Client, "my_player_added")
	yield(get_tree().create_timer(1.5), "timeout")
	tw.remove(self, "modulate")
	tw.interpolate_property(
		self, "modulate",
		null, Color(1, 1, 1, 0),
		0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.start()
	yield(get_tree().create_timer(0.4), "timeout")
	hide()
