extends Label

const UPDATE_TIME = 0.5

var timer = 0.0

func _ready() -> void:
	set_process(false)
	yield(G, "_added")
	set_process(true)

func _input(event: InputEvent) -> void:
	if event.is_action_pressed("perfmon"):
		visible = !visible

func _process(delta: float) -> void:
	if visible:
		timer -= delta
		if timer < 0:
			if G.Server != null:
				text = (
					"FPS: " + str(Performance.get_monitor(Performance.TIME_FPS)) + ", " +
					"OBJECTS: " + str(Performance.get_monitor(Performance.OBJECT_NODE_COUNT)) + ", " +
					"DELTA: " + str(Performance.get_monitor(Performance.TIME_PROCESS)) + ", " +
					"PHYS DELTA: " + str(Performance.get_monitor(Performance.TIME_PHYSICS_PROCESS))
					)
			else:
				if !is_instance_valid(G.Client): return
				text = (
					"FPS: " + str(Performance.get_monitor(Performance.TIME_FPS)) + ", " +
					"RTT/PP: " + str(int(G.Client.RTT * 1000)) + "/" + str(int(G.Client.PP * 1000)) + ", " +
					"PANIC: " + str(stepify(G.Client.panic, 0.1)) + ", " +
					"OBJECTS: " + str(Performance.get_monitor(Performance.OBJECT_NODE_COUNT)) + ", " +
					"DELTA: " + str(Performance.get_monitor(Performance.TIME_PROCESS)) + ", " +
					"PHYS DELTA: " + str(Performance.get_monitor(Performance.TIME_PHYSICS_PROCESS)) + ", " +
					"MEMORY: " + str(Performance.get_monitor(Performance.MEMORY_STATIC))
					)
			timer = UPDATE_TIME
