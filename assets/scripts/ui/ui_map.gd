extends ScrollContainer

const UNEXPOSED_SCALE = 2
const EXPOSED_USED_RECT_OFFSET = 256


onready var tilemap = $tilemap
onready var indicators = $indicators
onready var offscreen = $offscreen
onready var tw = $tw
onready var exposed_placeholder = $"../map_exposed_placeholder"
onready var unexposed_placeholder = $"../map_unexposed_placeholder"
onready var decorator = $"../map_decorator"
onready var decorator_bg = $"../map_decorator_bg"

var exposed = false
var pos = Vector2(0,0)

func calc_scale() -> float:
	var rs = tilemap.used_rect.size
	if rs.x > rs.y:
		return (exposed_placeholder.rect_size.x /
				(rs.x + EXPOSED_USED_RECT_OFFSET))
	else:
		return (exposed_placeholder.rect_size.y /
				(rs.y + EXPOSED_USED_RECT_OFFSET))

func update_exposed() -> void:
	tw.remove(self, "rect_position")
	tw.remove(self, "rect_size")
	tw.remove($"..", "modulate")
	tw.remove(decorator_bg, "rect_position")
	tw.remove(decorator_bg, "rect_size")
	tw.remove(tilemap, "scale")
	tw.remove(indicators, "scale")
	tw.remove(tilemap, "position")
	tw.remove(indicators, "position")
	if exposed:
		var calced_scale = calc_scale()
		tw.interpolate_property(
			self, "rect_position", null,
			exposed_placeholder.rect_position + Vector2(2,2),
			0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
		)
		tw.interpolate_property(
			self, "rect_size", null,
			exposed_placeholder.rect_size - Vector2(4,4),
			0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
		)
		tw.interpolate_property(
			decorator_bg, "rect_position", null,
			exposed_placeholder.rect_position,
			0.5, Tween.TRANS_CUBIC, Tween.EASE_OUT
		)
		tw.interpolate_property(
			decorator_bg, "rect_size", null,
			exposed_placeholder.rect_size,
			0.5, Tween.TRANS_CUBIC, Tween.EASE_OUT
		)
		tw.interpolate_property(
			tilemap, "scale", null, Vector2(1,1) *
			calced_scale,
			0.2, Tween.TRANS_CUBIC, Tween.EASE_OUT
		)
		tw.interpolate_property(
			indicators, "scale", null, Vector2(1,1) *
			calced_scale,
			0.2, Tween.TRANS_CUBIC, Tween.EASE_OUT
		)
		tw.interpolate_property(
			tilemap, "position", null, exposed_placeholder.rect_size / 2,
			0.2, Tween.TRANS_CUBIC, Tween.EASE_OUT
		)
		tw.interpolate_property(
			indicators, "position", null, exposed_placeholder.rect_size / 2,
			0.2, Tween.TRANS_CUBIC, Tween.EASE_OUT
		)
		tw.interpolate_property(
			$"..", "modulate",
			null, Color(1, 1, 1, 1),
			0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
		)
	else:
		if G.UI.show_respawn:
			tw.interpolate_property(
				$"..", "modulate",
				null, Color(1, 1, 1, 0),
				0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
			)
		else:
			tw.interpolate_property(
				$"..", "modulate",
				null, Color(1, 1, 1, 1),
				0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
			)
		tw.interpolate_property(
			self, "rect_position", null,
			unexposed_placeholder.rect_position + Vector2(2,2),
			0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
		)
		tw.interpolate_property(
			self, "rect_size", null,
			unexposed_placeholder.rect_size - Vector2(4,4),
			0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
		)
		tw.interpolate_property(
			decorator_bg, "rect_position", null,
			unexposed_placeholder.rect_position,
			0.5, Tween.TRANS_CUBIC, Tween.EASE_OUT
		)
		tw.interpolate_property(
			decorator_bg, "rect_size", null,
			unexposed_placeholder.rect_size,
			0.5, Tween.TRANS_CUBIC, Tween.EASE_OUT
		)
		tw.interpolate_property(
			tilemap, "position", null, pos,
			0.2, Tween.TRANS_CUBIC, Tween.EASE_OUT
		)
		tw.interpolate_property(
			indicators, "position", null, pos,
			0.2, Tween.TRANS_CUBIC, Tween.EASE_OUT
		)
		tw.interpolate_property(
			tilemap, "scale", null, Vector2(1,1) * UNEXPOSED_SCALE,
			0.2, Tween.TRANS_CUBIC, Tween.EASE_OUT
		)
		tw.interpolate_property(
			indicators, "scale", null, Vector2(1,1) * UNEXPOSED_SCALE,
			0.2, Tween.TRANS_CUBIC, Tween.EASE_OUT
		)
	tw.start()


func _on_resized():
	decorator.rect_position = rect_position - Vector2(2, 2)
	decorator.rect_size = rect_size + Vector2(4, 4)


func _on_hud_resized():
	yield(get_tree(), "idle_frame")
	update_exposed()
	_on_changed_position(G.my_player.position)


func _on_changed_position(p: Vector2) -> void:
	offscreen.update_indicators()
	pos = (
			- (
				p / (32 / UNEXPOSED_SCALE)
			) + unexposed_placeholder.rect_size / 2
		)
	if !exposed:
		tw.remove(tilemap, "position")
		tw.remove(indicators, "position")
		tw.interpolate_property(
			tilemap, "position", null, pos,
			0.1, Tween.TRANS_CUBIC, Tween.EASE_OUT
		)
		tw.interpolate_property(
			indicators, "position", null, pos,
			0.1, Tween.TRANS_CUBIC, Tween.EASE_OUT
		)
		tw.start()


func _on_tab(val: bool):
	exposed = val
	G.UI.emit_signal("map_exposed", exposed)
	update_exposed()


func _on_change_respawn(val: bool):
	yield(get_tree(), "idle_frame")
	update_exposed()


func _tween_completed(object: Object, key: NodePath):
	if object == self and key == ":rect_size":
		G.UI.emit_signal("map_exposed_end", exposed)


func _ready() -> void:
	get_parent().show()
	exposed_placeholder.hide()
	unexposed_placeholder.hide()
	tilemap.scale = Vector2(1,1) * UNEXPOSED_SCALE
	indicators.scale = Vector2(1,1) * UNEXPOSED_SCALE
	if G.Main == null: return
	yield(G.Client, "my_player_added")
	G.my_player.connect("me_changed_position", self, "_on_changed_position")
	G.UI.connect("tab_pressed", self, "_on_tab")
	G.UI.connect("change_respawn", self, "_on_change_respawn")
	get_parent().connect("resized", self, "_on_hud_resized")
	connect("resized", self, "_on_resized")
	tw.connect("tween_completed", self, "_tween_completed")
	_on_resized()
	decorator.show()
	decorator_bg.show()
	update_exposed()
