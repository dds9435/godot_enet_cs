extends Panel


onready var resume = $resume
onready var fullscreen = $fullscreen
onready var exit = $exit
onready var tw = $tw

var showed = false


func _on_fullscreen_toggle(val: bool) -> void:
	G.Config.CFG_GFX_FULLSCREEN = val
	G.Main.update_config()

func _on_resume():
	showed = false
	get_parent().block_input = false
	tw.remove(self, "modulate")
	tw.interpolate_property(
		self, "modulate",
		null, Color(1, 1, 1, 0),
		0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.start()
	yield(get_tree().create_timer(0.4), "timeout")
	if !showed:
		hide()

func on_show():
	showed = true
	get_parent().block_input = true
	show()
	tw.remove(self, "modulate")
	tw.interpolate_property(
		self, "modulate",
		null, Color(1, 1, 1, 1),
		0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.start()

func _on_exit():
	G.Main.go_to_menu()

func _input(event: InputEvent) -> void:
	if event.is_action_pressed("back"):
		if $"../upgrade".page == 0 and !$"../chat".exposed:
			if !showed:
				on_show()
			else:
				_on_resume()


func _ready() -> void:
	hide()
	modulate.a = 0.0
	fullscreen.pressed = G.Config.CFG_GFX_FULLSCREEN
	
	fullscreen.connect("toggled", self, "_on_fullscreen_toggle")
	resume.connect("pressed", self, "_on_resume")
	exit.connect("pressed", self, "_on_exit")
