extends CanvasLayer

signal cursor_scale(val) # float
signal cursor_ammo(val) # float
signal cursor_grenade_state(val) # bool
signal cursor_on_hit() # void
signal on_respawn() # void
signal on_death() # void
signal pointer_state(val) # bool

signal jet_value(val) # float

signal dash_state(val) # bool
signal dash_relo(val) # float
signal dash_rate(val) # float
signal dash_count(val) # int

signal grenade_timer(val) # float

signal stealth_state(val) # bool
signal stealth_value(val) # float

signal block_input(val) # bool
signal change_respawn(val) # bool
signal tab_pressed(val) # bool
signal map_exposed(val) # bool
signal map_exposed_end(val) # bool

signal panic_on() # void
signal panic_off() # void

signal chat_focus() #bool

var block_input = true setget _on_block_input
var show_respawn = false

func _on_block_input(val: bool) -> void:
	block_input = val
	emit_signal("block_input", block_input)


func _on_changed_jet_value(val: float) -> void:
	$hud.jet_val = val
	$hud.update_hud()

# enable/disable dash indicator
func _on_changed_dash_state(val: bool) -> void:
	print("DASH_STATE " + str(val))

func _on_changed_dash_rate(val: float) -> void:
	print("DASH_RATE " + str(val))

func _on_changed_dash_relo(val: float) -> void:
	print("DASH_RELO " + str(val))

func _on_changed_dash_count(val: int) -> void:
	print("DASH_COUNT " + str(val))

# enable/disable stealth indicator
func _on_changed_stealth_state(val: bool) -> void:
	print("STEALTH_STATE " + str(val))

# stealth bar 0.0 - 1.0
func _on_changed_stealth_value(val: float) -> void:
	print("STEALTH_VALUE " + str(val))

# money value
func _on_changed_money(player, val: int) -> void:
	$panel.money_val = val
	$panel.update_money()

# credit value
func _on_changed_credit(player, val: int) -> void:
	$panel.credit_val = val
	$panel.update_money()

# main timer value
func _on_changed_timer(val: float) -> void:
	$panel.timer = int(val)
	$panel.update_timer()

# teams score value
func _on_changed_score(val, type) -> void:
	if G.GameState.score_max == 0:
		yield(G.GameState, "changed_score_max")
	match type:
		null:
			var score_red = val[0]
			var score_blu = val[1]
			$panel.score_red_val_num = score_red
			$panel.score_blue_val_num = score_blu
			$panel.score_red_val = float(score_red) / G.GameState.score_max
			$panel.score_blue_val = float(score_blu) / G.GameState.score_max
			$panel.update_score()
		0:
			var score_red = val
			$panel.score_red_val_num = score_red
			$panel.score_red_val = float(score_red) / G.GameState.score_max
			$panel.update_score()
		1:
			var score_blu = val
			$panel.score_blue_val_num = score_blu
			$panel.score_blue_val = float(score_blu) / G.GameState.score_max
			$panel.update_score()


# game state value
func _on_changed_state(val: int) -> void:
	if val > GameState.STATE_NONE:
		$tw.remove($waiting_for_players, "modulate:a")
		$tw.interpolate_property($waiting_for_players, "modulate:a",
		null, 0, 0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT)
		$tw.start()
		yield(get_tree().create_timer(0.4), "timeout")
		$waiting_for_players.hide()
	else:
		if !$waiting_for_players.visible:
			$tw.remove($waiting_for_players, "modulate:a")
			$tw.interpolate_property($waiting_for_players, "modulate:a",
			null, 1, 0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT)
			$tw.start()
			$waiting_for_players.show()
	
	match val:
		GameState.STATE_WAIT:
			_on_show_respawn_screen()
			$upgrade.chrespawn(false)
		GameState.STATE_GAME:
			if G.my_player.dead:
				_on_show_respawn_screen()
			$upgrade.chrespawn(true)
		GameState.STATE_FREP:
			if G.my_player.dead:
				_on_show_respawn_screen()
			$upgrade.chrespawn(true)


func respawn():
	if G.GameState.state < GameState.STATE_GAME:
		return
	
	$upgrade.on_hide()
	G.my_player.resurrect()
	show_respawn = false
	emit_signal("change_respawn", show_respawn)


func chat_message(message: String, to_all: bool, id: int) -> void:
	$chat.new_message(message, to_all, id)

# on player death
func _on_show_respawn_screen(player=int(G.my_player.name)) -> void:
	if G.GameState.state == GameState.STATE_NONE or show_respawn:
		return # don't show if game didn't started already
	
	yield(get_tree().create_timer(1.0), "timeout")
	$upgrade.on_show()
	show_respawn = true
	emit_signal("change_respawn", show_respawn)

# feed events processing

enum {
	FEED_RESP,
	FEED_KILL,
	FEED_HIT,
	FEED_CAP,
	FEED_PKILL,
	FEED_WIN
}

const TEAM_1_COLOR		= Color(1.0, 0.6, 0.5)
const TEAM_2_COLOR		= Color(0.5, 0.6, 1.0)

const CAP_ICON	= "res://assets/graphics/ui/feed/cap.png"
const FALL_ICON	= "res://assets/graphics/ui/feed/fall.png"
const KILL_ICON	= "res://assets/graphics/ui/feed/kill.png"

func _on_feed_event(feed_id : int, feed_data=[]) -> void:
	var feed_event = "[right]"
	
	match feed_id:
		FEED_CAP:
			var point_id: int = feed_data[0]
			var capturers: PoolIntArray = feed_data[1]
			var team: int = feed_data[2]
			
			var message = "[color=#"
			
			match team:
				G.TEAM_1:
					message += TEAM_1_COLOR.to_html(false)
				G.TEAM_2:
					message += TEAM_2_COLOR.to_html(false)
			
			message += "]"
			
			if int(G.my_player.name) in capturers:
				if capturers.size() == 1:
					message += G.my_player.caption
				else:
					message += G.my_player.caption + " and " + str(capturers.size()) + " players"
			else:
				message += str(capturers.size()) + " players"
			
			message += "[/color] "
			message += "[img]" + CAP_ICON + "[/img] "
			var point = G.World.points.get_node_or_null(str(point_id))
			if point:
				message += str(point_id)
			else:
				return
			
			feed_event += message
			
		FEED_KILL:
			var player_id: int = feed_data[0]
			var killed_id: int = feed_data[1]
			
			if player_id == int(G.my_player.name):
				emit_signal("on_death")
			
			var message = "[color=#"
			
			if player_id == killed_id:
				var player = G.World.players.get_node_or_null(str(player_id))
				if player:
					match player.team:
						G.TEAM_1:
							message += TEAM_1_COLOR.to_html(false)
						G.TEAM_2:
							message += TEAM_2_COLOR.to_html(false)
					message += "] " + player.caption + "[/color] "
					message += "[img]" + FALL_ICON + "[/img]"
				else:
					return
				
			else:
				var player = G.World.players.get_node_or_null(str(player_id))
				var killer = G.World.players.get_node_or_null(str(killed_id))
				if killer:
					match killer.team:
						G.TEAM_1:
							message += TEAM_1_COLOR.to_html(false)
						G.TEAM_2:
							message += TEAM_2_COLOR.to_html(false)
					message += "] " + killer.caption + "[/color] "
					message += "[img]" + KILL_ICON + "[/img] " + "[color=#"
				else:
					return
				if player:
					match player.team:
						G.TEAM_1:
							message += TEAM_1_COLOR.to_html(false)
						G.TEAM_2:
							message += TEAM_2_COLOR.to_html(false)
					message += "] " + player.caption + "[/color]"
				else:
					return
					
			feed_event += message
		FEED_HIT:
			var player_id: int = feed_data[0]
			
			if player_id == int(G.my_player.name):
				emit_signal("cursor_on_hit")
		FEED_RESP:
			var player_id: int = feed_data[0]
			
			if player_id == int(G.my_player.name):
				emit_signal("on_respawn")

	if !(feed_id in [FEED_CAP, FEED_KILL]):
		return

	feed_event += "[/right]"
	$panel/feed/text.append_bbcode(feed_event)


func _on_player_added(id: int) -> void:
	var player = G.World.players.get_node_or_null(str(id)) as Player
	if player == null: return
	if player != G.my_player:
		yield(player, "changed_team")
	var team: int = player.team
	
	var message = "[right][color=#"
	
	match team:
		G.TEAM_1:
			message += TEAM_1_COLOR.to_html(false)
		G.TEAM_2:
			message += TEAM_2_COLOR.to_html(false)
	
	message += "]"
	message += str(id)
	message += "[/color]"
	message += " joined[/right]"
	
	$panel/feed/text.append_bbcode(message)


func _on_player_deleted(id: int) -> void:
	var player = G.World.players.get_node_or_null(str(id)) as Player
	if !is_instance_valid(player): return
	var team: int = player.team
	
	var message = "[right][color=#"
	
	match team:
		G.TEAM_1:
			message += TEAM_1_COLOR.to_html(false)
		G.TEAM_2:
			message += TEAM_2_COLOR.to_html(false)
	
	message += "]"
	message += str(id)
	message += "[/color]"
	message += " disconnected[/right]"
	
	$panel/feed/text.append_bbcode(message)


func _on_points_update(red: int, blu: int, state: int) -> void:
	
	$panel.points_red.text = str(red)
	$panel.points_blu.text = str(blu)
	
	match state:
		Points.MESSAGE_TAKE:
			$panel/teach/text.bbcode_text = ($panel.MESSAGES["take"])
		Points.MESSAGE_KILL:
			match G.my_player.team:
				G.TEAM_1:
					$panel/teach/text.bbcode_text = ($panel.MESSAGES["kill_blu"])
				G.TEAM_2:
					$panel/teach/text.bbcode_text = ($panel.MESSAGES["kill_red"])
		Points.MESSAGE_HOLD:
			$panel/teach/text.bbcode_text = ($panel.MESSAGES["hold"])

func _input(event: InputEvent) -> void:
	if event.is_action_pressed("hide_ui"):
		for i in get_children():
			i.visible = !i.visible
	if !block_input:
		if event.is_action_pressed("tab"):
			emit_signal("tab_pressed", true)
		elif event.is_action_released("tab"):
			emit_signal("tab_pressed", false)
		
		if event.is_action_pressed("chat_focus"):
			emit_signal("chat_focus")

func _ready() -> void:
	connect("jet_value", self, "_on_changed_jet_value")
	connect("dash_state", self, "_on_changed_dash_state")
	connect("dash_relo", self, "_on_changed_dash_relo")
	connect("dash_rate", self, "_on_changed_dash_rate")
	connect("dash_count", self, "_on_changed_dash_count")
	connect("stealth_state", self, "_on_changed_stealth_state")
	connect("stealth_value", self, "_on_changed_stealth_value")
	
	G.UI = self
	G.emit_signal("ui_added")
	if G.Main == null: return
	yield(G.World, "map_client_added")
	yield(G.World.map, "gen_end")
	
	G.Client.connect("player_added", self, "_on_player_added")
	G.Client.connect("player_deleted", self, "_on_player_deleted")
	G.Client.connect("my_player_added", self, "_on_player_added", [int(G.my_player.name)])
	G.World.points.connect("points_update", self, "_on_points_update")
	
	yield(G.Client, "my_player_added")
	G.my_player.connect("dead", self, "_on_show_respawn_screen")
	G.my_player.connect("changed_money", self, "_on_changed_money")
	G.my_player.connect("changed_credit", self, "_on_changed_credit")
	
	G.GameState.connect("changed_timer", self, "_on_changed_timer")
	G.GameState.connect("changed_state", self, "_on_changed_state")
	G.GameState.connect("changed_score", self, "_on_changed_score")
	G.GameState.connect("feed_ev", self, "_on_feed_event")
