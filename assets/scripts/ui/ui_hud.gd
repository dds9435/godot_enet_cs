extends Control


onready var fuel = $fuel
onready var fuel_bg = $fuel_bg
onready var shield = $shield
onready var stealth = $stealth
onready var dash = $dash

onready var fuel_placeholder = $fuel_placeholder
onready var shield_placeholder = $shield_placeholder
onready var stealth_placeholder = $stealth_placeholder
onready var dash_placeholder = $dash_placeholder

onready var tw = $tw

var jet_val = 0.0


func update_hud() -> void:
	tw.remove(fuel, "rect_size")
	tw.interpolate_property(
		fuel, "rect_size", null,
		fuel_placeholder.rect_size * Vector2(jet_val, 1),
		0.2, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.start()

func update_exposed() -> void:
	tw.remove(fuel, "rect_position")
	tw.remove(fuel_bg, "rect_position")
	tw.remove(fuel_bg, "rect_size")
	tw.remove(shield, "rect_position")
	tw.remove(stealth, "rect_position")
	tw.remove(dash, "rect_position")
	tw.interpolate_property(
		fuel, "rect_position", null,
		fuel_placeholder.rect_position,
		0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.interpolate_property(
		fuel_bg, "rect_position", null,
		fuel_placeholder.rect_position,
		0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.interpolate_property(
		fuel_bg, "rect_size", null,
		fuel_placeholder.rect_size,
		0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.interpolate_property(
		shield, "rect_position", null,
		shield_placeholder.rect_position,
		0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.interpolate_property(
		stealth, "rect_position", null,
		stealth_placeholder.rect_position,
		0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.interpolate_property(
		dash, "rect_position", null,
		dash_placeholder.rect_position,
		0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.start()

func _on_hud_resized():
	yield(get_tree(), "idle_frame")
	update_exposed()

func _ready() -> void:
	fuel_placeholder.hide()
	shield_placeholder.hide()
	stealth_placeholder.hide()
	dash_placeholder.hide()
	fuel.rect_position = Vector2(rect_size.x / 2, rect_size.y)
	shield.rect_position = Vector2(rect_size.x / 2, rect_size.y)
	stealth.rect_position = Vector2(rect_size.x / 2, rect_size.y)
	dash.rect_position = Vector2(rect_size.x / 2, rect_size.y)
	if G.Main == null: return
	yield(G.Client, "my_player_added")
	connect("resized", self, "_on_hud_resized")
	update_exposed()
