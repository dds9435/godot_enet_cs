extends Control

const MESSAGES = {
	"take" : "[center]Take points",
	"kill_blu" : "[center]Kill [color=blue]blue[/color] points",
	"kill_red" : "[center]Kill [color=red]red[/color] points",
	"hold" : "[center]Hold your points" 
}


onready var teach = $teach
onready var teach_bg = $teach_bg
onready var score = $score
onready var score_bg = $score_bg
onready var money = $money
onready var money_bg = $money_bg
onready var feed = $feed
onready var feed_bg = $feed_bg

onready var score_bg_panel = $score/bg
onready var score_red_panel = $score/red
onready var score_blue_panel = $score/blue
onready var score_red_number = $score/red_s
onready var score_blue_number = $score/blu_s
onready var points_red = $score/red_p
onready var points_blu = $score/blu_p

onready var money_text = $money/text

onready var teach_placeholder = $teach_placeholder
onready var score_placeholder = $score_placeholder
onready var money_placeholder = $money_placeholder
onready var feed_placeholder = $feed_placeholder

onready var tw = $tw

var score_red_val = 0.0
var score_blue_val = 0.0
var score_red_val_num = 0
var score_blue_val_num = 0

var money_val = 0
var credit_val = 0

var timer = 0


func update_money() -> void:
	money_text.bbcode_text = \
	"[center]%d$\n%d[img]res://assets/graphics/ui/rubles.png[/img]" %\
	[money_val, credit_val]


func update_timer() -> void:
	$score/time.text = "%02d:%02d" % [timer / 60, timer % 60]


func update_score() -> void:
	tw.remove(score_red_panel, "rect_position")
	tw.remove(score_red_panel, "rect_size")
	tw.remove(score_blue_panel, "rect_position")
	tw.remove(score_blue_panel, "rect_size")
	score_red_number.text = str(score_red_val_num)
	score_blue_number.text = str(score_blue_val_num)
	tw.interpolate_property(
		score_red_panel, "rect_position",
		null, score_bg_panel.rect_position,
		0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.interpolate_property(
		score_red_panel, "rect_size",
		null,
		score_bg_panel.rect_size * Vector2(score_red_val, 1),
		0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.interpolate_property(
		score_blue_panel, "rect_position",
		null,
		(score_bg_panel.rect_position +
			Vector2(score_bg_panel.rect_size.x -
				score_bg_panel.rect_size.x * score_blue_val
			, 0)),
		0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.interpolate_property(
		score_blue_panel, "rect_size",
		null,
		score_bg_panel.rect_size * Vector2(score_blue_val, 1),
		0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.start()


func update_exposed() -> void:
	tw.remove(teach, "rect_position")
	tw.remove(teach_bg, "rect_position")
	tw.remove(score, "rect_position")
	tw.remove(score_bg, "rect_position")
	tw.remove(money, "rect_position")
	tw.remove(money_bg, "rect_position")
	tw.remove(feed, "rect_position")
	tw.remove(feed_bg, "rect_position")
	tw.remove(teach, "rect_size")
	tw.remove(teach_bg, "rect_size")
	tw.remove(score, "rect_size")
	tw.remove(score_bg, "rect_size")
	tw.remove(money, "rect_size")
	tw.remove(money_bg, "rect_size")
	tw.remove(feed, "rect_size")
	tw.remove(feed_bg, "rect_size")
	tw.interpolate_property(
		teach, "rect_position", null,
		teach_placeholder.rect_position,
		0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.interpolate_property(
		teach_bg, "rect_position", null,
		teach_placeholder.rect_position,
		0.5, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.interpolate_property(
		teach, "rect_size", null,
		teach_placeholder.rect_size,
		0.2, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.interpolate_property(
		teach_bg, "rect_size", null,
		teach_placeholder.rect_size,
		0.3, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.interpolate_property(
		score, "rect_position", null,
		score_placeholder.rect_position,
		0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.interpolate_property(
		score_bg, "rect_position", null,
		score_placeholder.rect_position,
		0.5, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.interpolate_property(
		score, "rect_size", null,
		score_placeholder.rect_size,
		0.2, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.interpolate_property(
		score_bg, "rect_size", null,
		score_placeholder.rect_size,
		0.3, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.interpolate_property(
		money, "rect_position", null,
		money_placeholder.rect_position,
		0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.interpolate_property(
		money_bg, "rect_position", null,
		money_placeholder.rect_position,
		0.5, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.interpolate_property(
		money, "rect_size", null,
		money_placeholder.rect_size,
		0.2, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.interpolate_property(
		money_bg, "rect_size", null,
		money_placeholder.rect_size,
		0.3, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.interpolate_property(
		feed, "rect_position", null,
		feed_placeholder.rect_position,
		0.4, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.interpolate_property(
		feed_bg, "rect_position", null,
		feed_placeholder.rect_position,
		0.5, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.interpolate_property(
		feed, "rect_size", null,
		feed_placeholder.rect_size,
		0.2, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.interpolate_property(
		feed_bg, "rect_size", null,
		feed_placeholder.rect_size,
		0.3, Tween.TRANS_CUBIC, Tween.EASE_OUT
	)
	tw.start()
	yield(get_tree().create_timer(0.4), "timeout")
	update_score()

func _on_hud_resized():
	yield(get_tree(), "idle_frame")
	update_exposed()


func _ready() -> void:
	teach_placeholder.hide()
	score_placeholder.hide()
	money_placeholder.hide()
	feed_placeholder.hide()
	teach.rect_position = Vector2(rect_size.x / 2, 0)
	score.rect_position = Vector2(rect_size.x / 2, 0)
	money.rect_position = Vector2(rect_size.x / 2, 0)
	feed.rect_position = Vector2(rect_size.x / 2, 0)
	teach.rect_size = Vector2(0, 0)
	score.rect_size = Vector2(0, 0)
	money.rect_size = Vector2(0, 0)
	feed.rect_size = Vector2(0, 0)
	if G.Main == null: return
	yield(G.Client, "my_player_added")
	connect("resized", self, "_on_hud_resized")
	update_exposed()
