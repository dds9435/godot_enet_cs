extends Control

var POINTER_SIZE =  8				setget _set_pointer_size
var POINTER_THICC = 1				setget _set_pointer_thicc
var POINTER_ANGLE = 0				setget _set_pointer_angle
var POINTER_COLOR = Color(1, 1, 1)	setget _set_pointer_color
var POINTER_AA =    false			setget _set_pointer_aa

var state = true

const DEFAULT_TEMPLATES = [
	{
		"POINTER_SIZE" :  8,
		"POINTER_THICC" : 2,
		"POINTER_ANGLE" : 0,
		"POINTER_COLOR" : Color(1, 1, 1),
		"POINTER_AA" :    false
	},
	{
		"POINTER_SIZE" :  8,
		"POINTER_THICC" : 3,
		"POINTER_ANGLE" : 45,
		"POINTER_COLOR" : Color(1, 1, 1, 0.5),
		"POINTER_AA" :    false
	}
]

func _set_pointer_aa(aa: bool):
	POINTER_AA = aa
	update()

func _set_pointer_color(color: Color):
	POINTER_COLOR = color
	update()

func _set_pointer_angle(angle: int):
	POINTER_ANGLE = angle
	update()

func _set_pointer_thicc(thicc: int):
	POINTER_THICC = thicc
	update()

func _set_pointer_size(size: int):
	POINTER_SIZE = size
	update()

func _draw() -> void:
	if state:
		draw_line((Vector2(0, 1) * POINTER_SIZE).rotated(deg2rad(POINTER_ANGLE)),
				(Vector2(0, -1) * POINTER_SIZE).rotated(deg2rad(POINTER_ANGLE)),
				POINTER_COLOR, POINTER_THICC, POINTER_AA)
		draw_line((Vector2(1, 0) * POINTER_SIZE).rotated(deg2rad(POINTER_ANGLE)),
				(Vector2(-1, 0) * POINTER_SIZE).rotated(deg2rad(POINTER_ANGLE)),
				POINTER_COLOR, POINTER_THICC, POINTER_AA)

func _on_pointer_state(val: bool) -> void:
	state = val
	update()

func _process(delta: float) -> void:
	rect_position = get_global_mouse_position()


func pointer_template(i: String) -> void:
	for key in DEFAULT_TEMPLATES[int(i)].keys():
		set(key, DEFAULT_TEMPLATES[int(i)][key])

func a_pointer_template(idx: int) -> Array:
	return ["0", "1"]

func _ready() -> void:
	pointer_template("0")
	if G.Main == null:
		Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
		return
	G.Console.register_command(
			"pointer_template", {
				desc   = "",
				args   = [1, "<template:int>"],
				target = self,
				completer_target = self,
				completer_method = "a_pointer_template"
			}
	)
	get_parent().connect("pointer_state", self, "_on_pointer_state")
	_on_pointer_state(true)
