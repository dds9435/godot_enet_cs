extends Client # self -> Client -> Net
# Игровой клиент

signal my_player_added()
signal player_added(id)
signal player_deleted(id)
signal client_created()


var is_alive: bool = false
var is_init: bool = false

var a_list_enabled_modules_cache = []
var a_list_disabled_modules_cache = []
var rules
const rules_scn = preload("res://assets/scenes/client_rules.tscn")
var timer = 1.0
var last_timestamp = 0


func list_modules() -> void:
	var f_text = ""
	var children = G.my_player.get_node("Modules").get_children()
	for i in children:
		f_text += i.d_name + ", level: " + str(i.level) + " (temp level: " + str(i.temp_level) + ")" + "\n"
	f_text = f_text.left(f_text.length() - 1)
	if children.size() == 0:
		f_text = "No modules active!"
	G.Console.message(f_text)


func a_list_modules(idx: int) -> Array:
	if idx == 0:
		return G.ModulesServer.available
	else:
		return []


func a_list_enabled_modules(idx: int) -> Array:
	if idx == 0:
		if a_list_enabled_modules_cache.size() > 0:
			return a_list_enabled_modules_cache
		var m_nodes = G.my_player.get_node("Modules").get_children()
		var m_cook = []
		for i in m_nodes:
			if i.state == true:
				m_cook.append(i.t_name)
		return m_cook
	else:
		return []


func a_list_disabled_modules(idx: int) -> Array:
	if idx == 0:
		if a_list_disabled_modules_cache.size() > 0:
			return a_list_disabled_modules_cache
		var m_nodes = G.my_player.get_node("Modules").get_children()
		var m_cook = []
		for i in m_nodes:
			if i.state == false:
				m_cook.append(i.t_name)
		return m_cook
	else:
		return []


func disable_module(m_name: String) -> void:
	G.my_player.module_state(m_name, true, false)
	a_list_enabled_modules_cache = []


func enable_module(m_name: String) -> void:
	G.my_player.module_state(m_name, true, true)
	a_list_disabled_modules_cache = []


func buy_module(m_name: String) -> void:
	send_call(G.GameState.get_path(), "approve_module_ev", 
			[int(G.my_player.name), m_name, GameState.OPERATION_BUY], 
			Net.SERVER_ID)


func credit_module(m_name: String) -> void:
	send_call(G.GameState.get_path(), "approve_module_ev", 
			[int(G.my_player.name), m_name, GameState.OPERATION_CREDIT], 
			Net.SERVER_ID)


func sell_module(m_name: String) -> void:
	send_call(G.GameState.get_path(), "approve_module_ev", 
			[int(G.my_player.name), m_name, GameState.OPERATION_SELL], 
			Net.SERVER_ID)


func kill_player() -> void:
	G.my_player.kill(G.my_player.name)


func resurrect_player() -> void:
	G.my_player.resurrect()


func init_console() -> void:
	G.Console.register_command(
			"disable_module", {
				desc   = "Try to disable a module",
				args   = [1, "<name:string>"],
				target = self,
				completer_target = self,
				completer_method = "a_list_enabled_modules"
			})
	G.Console.register_command(
			"enable_module", {
				desc   = "Try to enable a module",
				args   = [1, "<name:string>"],
				target = self,
				completer_target = self,
				completer_method = "a_list_disabled_modules"
			})
	G.Console.register_command(
			"buy_module", {
				desc   = "Try to buy a module",
				args   = [1, "<name:string>"],
				target = self,
				completer_target = self,
				completer_method = "a_list_modules"
			})
	G.Console.register_command(
			"credit_module", {
				desc   = "Try to credit a module",
				args   = [1, "<name:string>"],
				target = self,
				completer_target = self,
				completer_method = "a_list_modules"
			})
	
	G.Console.register_command(
			"sell_module", {
				desc   = "Try to sell a module",
				args   = [1, "<name:string>"],
				target = self,
				completer_target = self,
				completer_method = "a_list_modules"
			})
	G.Console.register_command(
			"list_modules", {
				desc   = "List all current modules",
				args   = [0],
				target = self
			})
	G.Console.register_command(
			"kill_player", {
				desc   = "Kills your player",
				args   = [0],
				target = self
			})
	G.Console.register_command(
			"resurrect_player", {
				desc   = "Resurrects your player",
				args   = [0],
				target = self
			})


func _ready() -> void:
	set_process(false)
	G.Client = self
	G.emit_signal("client_added")
	G.emit_signal("_added")
	rules = rules_scn.instance()
	call_deferred("add_child", rules)
	peer.connect("peer_connected", self, "add_player")
	peer.connect("peer_disconnected", self, "del_player")
	init_console()
	yield(peer, "connection_succeeded")
	set_process(true)


func create_client(host: String, port: int) -> void:
	G.World.map.get_node("Destruct").tile_set = load("res://assets/tilemaps/destruct.tres")
	G.World.map.tile_set = load("res://assets/tilemaps/tilemap_client.tres")
	G.World.map.script = load("res://assets/scripts/map/map_client.gd")
	G.World.points.script = load("res://assets/scripts/points/points_client.gd")
	G.World.map.connect("gen_end", self, "_on_player_ready")
	emit_signal("client_created")
	.create_client(host, port)


func del_player(id: int) -> void:
	emit_signal("player_deleted", id)
	G.World.del_player(id)


func _on_server_disconnected() -> void:
	is_alive = false
	._on_server_disconnected()


func add_player(id: int) -> void:
	if id == SERVER_ID: # когда происходит подключение с сервером, вместо спавна игрока для сервера
		# спавним игрока для себя (под нашим номером), для это вызываем эту же ф-ции
		add_player(get_tree().get_network_unique_id())
	else: # если добавляется не сервер
		var player = G.World.add_player(id)
		yield(player, "tree_entered") # выход и возврат при включении игрока в древо
		is_alive = true
		
		# любой игрок
		# pass
		
		if id == get_tree().get_network_unique_id(): # наш игрок
			G.Client.cur_connections.append(1)
			G.my_player = player
			player.connect("me_changed_position", self, "_on_me_changed_position")
			player.connect("me_changed_motion", self, "_on_me_changed_motion")
			player.connect("me_changed_crouch", self, "_on_me_changed_crouch")
			player.connect("me_changed_jet", self, "_on_me_changed_jet")
			player.connect("me_changed_jet_fuel", self, "_on_me_changed_jet_fuel")
			player.connect("me_added_module", self, "_on_me_added_module")
			player.connect("me_deleted_module", self, "_on_me_deleted_module")
			player.connect("me_module_state", self, "_on_me_module_state")
			player.connect("changed_flip", self, "_on_me_changed_player_flip")
			player.connect("resurrect", self, "_on_me_resurrect")
			player.connect("changed_team", self, "_on_me_changed_team")
			player.connect("changed_seat", self, "_on_me_changed_seat")
			player.get_node("weapon").is_slave = false
			player.get_node("drill").is_slave = false
			yield(player, "ready")
			player.weapon.connect("me_changed_ammo", self, "_on_me_changed_ammo")
			player.weapon.connect("me_changed_wrot", self, "_on_me_changed_wrot")
			player.weapon.connect("me_changed_wprot", self, "_on_me_changed_wprot")
			player.weapon.connect("me_released", self, "_on_me_released")
			player.weapon.connect("me_shooted_grenade", self, "_on_me_shooted_grenade")
			player.weapon.connect("me_pressed_rmb", self, "_on_me_pressed_rmb")
			player.weapon.connect("me_released_rmb", self, "_on_me_released_rmb")
			player.emit_signal("dead", player)
			
			var camera = Camera2D.new()
			camera.name = "Camera2D"
			player.add_child(camera)
			player.camera = camera
			player.camera.current = true
			player.camera.smoothing_enabled = true
			player.camera.smoothing_speed = 50
			
			player.control = true
			
			if player.team == null:
				yield(player, "changed_team")
			if player.mon_seatoni == "":
				yield(player, "changed_seat")
			
			emit_signal("my_player_added")
			player.completely_added = true
			player.emit_signal("completely_added")
			is_init = true
			
		else: # другой игрок, тогда синхронизация по серверу
			G.Client.cur_connections.append(id)
			var path = str(player.get_path())
			send_sync(path, "dead", SERVER_ID)
			send_sync(path, "team", SERVER_ID)
			send_sync(path, "seat", SERVER_ID)
			send_sync(path, "position", SERVER_ID)
			if player.team == null:
				yield(player, "changed_team")
			if player.mon_seatoni == "":
				yield(player, "changed_seat")
			G.my_player.send_modules(id)
			emit_signal("player_added", id)
			player.completely_added = true
			player.emit_signal("completely_added")


func _on_player_ready(arg_from_gen_end) -> void:
	send_call("/root/Main/Server", "player_ready", [get_tree().get_network_unique_id()], Net.SERVER_ID)
	ready = true


func _on_me_resurrect(player: Player) -> void:
	if !is_init or !is_alive: return
	send_set(player.get_path(), "dead", player.dead, Net.BROADCAST)


func _on_me_pressed_rmb(weapon: Weapon) -> void:
	if !is_init or !is_alive: return
	send_call(weapon.get_node("../drill").get_path(), "rmb_pressed", [], Net.BROADCAST)


func _on_me_released_rmb(weapon: Weapon) -> void:
	if !is_init or !is_alive: return
	send_call(weapon.get_node("../drill").get_path(), "rmb_released", [], Net.BROADCAST)


func _on_me_shooted_grenade(weapon: Node2D) -> void:
	if !is_init or !is_alive: return
	send_call(weapon.get_path(), "shoot", [], Net.BROADCAST)


func _on_me_released(weapon: Weapon) -> void:
	if !is_init or !is_alive: return
	send_call(weapon.get_path(), "release", [], Net.BROADCAST)


func _on_me_changed_wrot(weapon: Weapon, rot: float) -> void:
	if !is_init or !is_alive: return
	send_call(weapon.get_path(), "integrate_wrot", [rot], Net.SERVER_ID, NetworkedMultiplayerPeer.TRANSFER_MODE_UNRELIABLE_ORDERED)


func _on_me_changed_wprot(pivot: Object, rot: float) -> void:
	if !is_init or !is_alive: return
	send_set(pivot.get_path(), "rotation_degrees", rot, Net.BROADCAST)


func _on_me_changed_ammo(weapon: Weapon, ammo: int, shooted: bool) -> void:
	if !is_alive: return
	send_set(weapon.get_path(), "ammo", ammo, Net.BROADCAST)
	if shooted:
		send_call(weapon.get_path(), "shoot", [], Net.BROADCAST)
	send_call(weapon.get_path(), "ammo_change", [ammo, shooted], Net.BROADCAST)


func _on_me_changed_player_flip(player: Player, tex, val: bool) -> void:
	if !is_alive: return
	send_set(tex.get_path(), "flip_h", val, Net.BROADCAST)


func _on_me_changed_team(player: Player, val: int) -> void:
	if !is_alive: return
	send_set(player.get_path(), "team", val, Net.BROADCAST)


func _on_me_changed_seat(player: Player, team: int, val: int) -> void:
	if !is_alive: return
	send_set(player.get_path(), "seat", val, Net.BROADCAST)


func _on_me_module_state(m_name: String, state: bool, send_to: int) -> void:
	if !is_alive: return
	send_call(G.my_player.get_path(), "module_state", [m_name, false, state], send_to)


func _on_me_added_module(m_name: String, level: int, temp_level: int, send_to: int) -> void:
	if !is_alive: return
	send_call(G.my_player.get_path(), "add_module", [m_name, false, level, temp_level], send_to)


func _on_me_deleted_module(m_name: String, send_to: int) -> void:
	if !is_alive: return
	send_call(G.my_player.get_path(), "del_module", [m_name, false], send_to)


func _on_me_changed_motion(motion: Vector2) -> void:
	if !is_alive: return
	send_set(G.my_player.get_path(), "motion", motion, Net.BROADCAST, NetworkedMultiplayerPeer.TRANSFER_MODE_UNRELIABLE_ORDERED)


func _on_me_changed_position(pos: Vector2) -> void:
	if !is_alive: return
	send_call(G.my_player.get_path(), "integrate_position", [pos], Net.SERVER_ID, NetworkedMultiplayerPeer.TRANSFER_MODE_UNRELIABLE_ORDERED)


func _on_me_changed_crouch(crouch: bool) -> void:
	if !is_alive: return
	send_set(G.my_player.get_path(), "crouch", crouch, Net.BROADCAST)


func _on_me_changed_jet_fuel(jet_fuel: float) -> void:
	if !is_alive: return
	send_set(G.my_player.get_path(), "jet_fuel", jet_fuel, Net.BROADCAST, NetworkedMultiplayerPeer.TRANSFER_MODE_UNRELIABLE_ORDERED)


func _on_me_changed_jet(jet: bool) -> void:
	if !is_alive: return
	send_set(G.my_player.get_path(), "jet_mode", jet, Net.BROADCAST)
