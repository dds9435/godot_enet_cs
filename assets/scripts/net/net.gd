extends Node
class_name Net
# Базовый сетевой код


signal msg(text, to_all, id)
signal received_echo(id)
signal received_timer(timer)

const SIMULATE_PING			= 0		# == 0 - off, ms

const PACKETS_TIMER			= 5		# == 0 - off, sec
const PACKETS_PRINT			= false	# print count of packets for each type

const DEFAULT_HOST			= "127.0.0.1"
const DEFAULT_SERVER_PORT	= 6666
const DEFAULT_MAX_PLAYERS	= 16

const PANIC_TIMEOUT			= 10.0

enum {
	BROADCAST,
	SERVER_ID
}

enum {
	PCK_MSG,
	PCK_ECHO,
	PCK_TIMER,
	PCK_SET,
	PCK_GET,
	PCK_SYNC,
	PCK_CALL
}

var host: String = DEFAULT_HOST
var port: int = DEFAULT_SERVER_PORT

var peer
var cur_connections = []

var pack_timer = 0
var recv_timer = 0
var user_timer = 0


func _ready() -> void:
	get_tree().multiplayer.connect("network_peer_packet", self, "_on_packet_received")
	create_peer()


func create_peer() -> NetworkedMultiplayerENet:
	peer = NetworkedMultiplayerENet.new()
	peer.connect("peer_connected", self, "_on_peer_connected")
	peer.connect("peer_disconnected", self, "_on_peer_disconnected")
	return peer


func send_sync(
		node: String, property: String, id:int = SERVER_ID, 
		transfer_mode = NetworkedMultiplayerPeer.TRANSFER_MODE_RELIABLE
) -> void:
	_send_data([PCK_SYNC, node, property], id, transfer_mode)


func send_get(
		node: String, property: String, node2: String, property2: String, id:int = SERVER_ID, 
		transfer_mode = NetworkedMultiplayerPeer.TRANSFER_MODE_RELIABLE
) -> void:
	_send_data([PCK_GET, node, property, node2, property2], id, transfer_mode)


func send_set(
		node: String, property: String, val, id:int = SERVER_ID, 
		transfer_mode = NetworkedMultiplayerPeer.TRANSFER_MODE_RELIABLE
) -> void:
	_send_data([PCK_SET, node, property, val], id, transfer_mode)


func send_call(
		node: String, func_name: String, args=[], id:int = SERVER_ID, 
		transfer_mode = NetworkedMultiplayerPeer.TRANSFER_MODE_RELIABLE
) -> void:
	_send_data([PCK_CALL, node, func_name, args], id, transfer_mode)


func send_msg(
		msg: String, id:int = SERVER_ID, to_all = true,
		transfer_mode = NetworkedMultiplayerPeer.TRANSFER_MODE_RELIABLE
) -> void:
	_send_data([PCK_MSG, msg, to_all], id, transfer_mode)


func send_echo(
		id:int = SERVER_ID, 
		transfer_mode = NetworkedMultiplayerPeer.TRANSFER_MODE_UNRELIABLE_ORDERED,
		rtt:float = 0
) -> void:
	_send_data([PCK_ECHO, rtt], id, transfer_mode)


func send_timer(
		id:int = SERVER_ID,
		transfer_mode = NetworkedMultiplayerPeer.TRANSFER_MODE_RELIABLE,
		timer:float = 0
) -> void:
	_send_data([PCK_TIMER, timer], id, transfer_mode)

var spacket_counter = 0
var rpacket_counter = 0

var SPPS = 0
var RPPS = 0

func _send_data(
		data:Array, id:int = BROADCAST, 
		transfer_mode = NetworkedMultiplayerPeer.NetworkedMultiplayerPeer.TRANSFER_MODE_RELIABLE
) -> void:
	if G.Server != null and SIMULATE_PING > 0:
		yield(get_tree().create_timer(float(SIMULATE_PING) / 1000), "timeout")
	if G.Server != null:
		data.append(pack_timer)
	elif G.Client != null:
		data.append(user_timer)
	if id == BROADCAST:
		for id_peer in cur_connections:
			get_tree().multiplayer.send_bytes(var2bytes(data), id_peer, transfer_mode)
	else:
		get_tree().multiplayer.send_bytes(var2bytes(data), id, transfer_mode)
	
	spacket_counter += 1
	if PACKETS_PRINT:
		if spackets.has(data[0]):
			spackets[data[0]] += 1
		else:
			spackets[data[0]] = 1


var sec_timer = 0.0
var info_timer = 0.0
var rpackets = {}
var spackets = {}

func print_packet_mon(packets: Dictionary) -> void:
	var packets_str = ""
	for i in packets.keys():
		match i:
			PCK_CALL:
				packets_str += "PCK_CALL:  " + str(packets[i]) + "\n"
			PCK_ECHO:
				packets_str += "PCK_ECHO:  " + str(packets[i]) + "\n"
			PCK_GET:
				packets_str += "PCK_GET:   " + str(packets[i]) + "\n"
			PCK_MSG:
				packets_str += "PCK_MSG:   " + str(packets[i]) + "\n"
			PCK_SET:
				packets_str += "PCK_SET:   " + str(packets[i]) + "\n"
			PCK_SYNC:
				packets_str += "PCK_SYNC:  " + str(packets[i]) + "\n"
			PCK_TIMER:
				packets_str += "PCK_TIMER: " + str(packets[i]) + "\n"
	
	print(packets_str)

func _physics_process(delta: float) -> void:
	if PACKETS_TIMER == 0:
		set_physics_process(false)
		return
	sec_timer += delta
	info_timer += delta
	if sec_timer > 1:
		sec_timer = 0
		SPPS = spacket_counter
		RPPS = rpacket_counter
		spacket_counter = 0
		rpacket_counter = 0
	if info_timer > PACKETS_TIMER:
		if SPPS > 0 or RPPS > 0:
			G.debug_print("[INFO] " + str(SPPS) + "/" + str(RPPS) + " SPPS/RPPS")
			
			if PACKETS_PRINT:
				print("RECEIVED PACKETS IN " + str(PACKETS_TIMER) + " SECONDS:")
				print_packet_mon(rpackets)
				print("SENT PACKETS IN " + str(PACKETS_TIMER) + " SECONDS:")
				print_packet_mon(spackets)
				rpackets = {}
				spackets = {}
			
		info_timer = 0.0


func _on_peer_connected(id: int) -> void:
	G.debug_print(["CONNECT:", id])
	# look at game_server/client add_player func for cur_con append


func _on_peer_disconnected(id: int) -> void:
	G.debug_print(["DISCONNECT:", id])
	cur_connections.erase(id)


# обработка входящих пакетов
func _on_packet_received(id: int, packet: PoolByteArray) -> void:
	if G.Server != null and SIMULATE_PING > 0:
		yield(get_tree().create_timer(float(SIMULATE_PING) / 1000), "timeout")
	
	var data = bytes2var(packet) # [type_pck, data1, ..., dataN, timestamp]
	var type_pck: int = data.front()
	var timestamp = data.back()
	if timestamp is float:
		recv_timer = timestamp
	
	rpacket_counter += 1
	if PACKETS_PRINT:
		if rpackets.has(type_pck):
			rpackets[type_pck] += 1
		else:
			rpackets[type_pck] = 1
	
	
	# от всех
	match type_pck: 
		PCK_ECHO:
			if id == SERVER_ID:
				send_echo(id)
			emit_signal("received_echo", id, data[1], timestamp)
		PCK_MSG:
			var type = "[ALL]:"
			if !data[2]: type = "[TEAM]:"
			G.debug_print([id, "MSG", type, data[1]])
			emit_signal("msg", data[1], data[2], id)
		PCK_TIMER:
			emit_signal("received_timer", data[1])
		PCK_SET: #[type, obj, property, val]
			if has_node(data[1]): get_node(data[1]).set(data[2], data[3])
			#else: G.debug_print(["PACKET: ", data, " INVALID!"])
			#G.debug_print([id, "SET:", data[1], data[2]])
		PCK_GET:
			G.Peer.send_set(data[3], data[4], get_node(data[1]).get(data[2]), id)
		PCK_SYNC: #[type, obj, property, val]
			G.Peer.send_set(data[1], data[2], get_node(data[1]).get(data[2]), id)
			#G.debug_print([id, "SYNC:", data[1], data[2]])
		PCK_CALL: #[type, obj, func_name, args]
			if has_node(data[1]) and get_node(data[1]).has_method(data[2]):
				get_node(data[1]).callv(data[2], data[3])
			#else: G.debug_print(["PACKET: ", data, " INVALID!"])
			#G.debug_print([id, "CALL:", data[1], data[2]])
		_:
			G.debug_print([id, "???", data])
