extends Server # self -> Server -> Net
# Игровой сервер

var rules

var teams = {
		G.TEAM_1: [], 
		G.TEAM_2: []
}

const rules_scn = preload("res://assets/scenes/server_rules.tscn")

func _ready() -> void:
	G.Server = self
	G.emit_signal("_added")
	rules = rules_scn.instance()
	call_deferred("add_child", rules)
	var tmp_arr = [null]
	tmp_arr.resize(max_players / 2)
	G.debug_print(["MAX_PLAYERS:", max_players])
	G.Server.teams[G.TEAM_1] = tmp_arr.duplicate()
	G.Server.teams[G.TEAM_2] = tmp_arr.duplicate()

	peer.connect("peer_connected", self, "add_player")
	peer.connect("peer_disconnected", self, "del_player")
	
	G.GameState.script = load("res://assets/scripts/game_state/game_state_server.gd")
	G.GameState.connect("changed_timer", self, "_on_changed_timer")
	G.GameState.connect("changed_stage", self, "_on_changed_stage")
	G.GameState.connect("changed_state", self, "_on_changed_state")
	G.GameState.connect("changed_score", self, "_on_changed_score")
	G.GameState.connect("changed_score_max", self, "_on_changed_score_max")
	G.GameState.connect("feed_ev", self, "_on_feed_event")
	G.GameState.connect("exit_win", self, "_on_exit_win")
	G.GameState.connect("approved_module_op", self, "send_module_op")
	
	G.Server.connect("msg", self, "_on_msg")
	G.World.world_camera.current = true


func create_server(s_port=port, s_max_players=max_players, s_map_seed=null) -> void:
	randomize()
	G.World.map.script = load("res://assets/scripts/map/map_server.gd")
	G.World.points.script = load("res://assets/scripts/points/points_server.gd")
	G.World.points.connect("point_ev", self, "_on_point_event")
	G.World.map.connect("hit", self, "_on_map_hit")
	G.World.map.connect("expl", self, "_on_map_expl")
	if s_map_seed != null:
		G.World.map.map_seed = int(s_map_seed)
	G.World.map.init_seed()
	while true:
		G.World.map.generate()
		if G.World.map.gen_success == 1:
			break
		elif G.World.map.gen_success == 2:
			get_tree().quit()
			return
	for p in G.World.points.get_children():
		p.register()
	G.World.points.set_physics_process(true)
	.create_server(s_port, s_max_players)


func player_ready(id: int) -> void:
	G.debug_print("LOADED SIGNAL: " + str(id))
	G.Server.cur_connections.append(id)


func del_player(id: int) -> void:
	G.World.del_player(id)
	var t = get_player_team(id)
	del_player_from_team(id)
	rules.del_player(id)
	
	
	# Чтобы седло СНАЧАЛА удалилось и только потом с ним были новые операции
	yield(G.World.players.get_node(str(id)), "tree_exited")
	yield(get_tree(), "idle_frame")
	if get_players_in_team(t).size() == 0:
		if G.GameState.timer > 30:
			swap_player_team(-1, true)
		elif G.GameState.state == GameState.STATE_GAME:
			G.GameState.timer = 0.0
	
	G.World.points.recalc_points()


func add_player(id: int) -> void:
	var info = {
			'team': add_player_to_team(id),
			'seat': get_seat(id)
	}
	var player = G.World.add_player(id, info)
	yield(player, "tree_entered") # выход и возврат при включении игрока в древо
	var path = str(player.get_path())
	# отправка новому клиенту информации о карте
	send_map(int(player.name))
	send_points(int(player.name))
	
	var load_timeout_timer = 30.0
	while true:
		if G.Server.cur_connections.has(id):
			break
		yield(get_tree().create_timer(0.1), "timeout")
		load_timeout_timer -= 0.1
		if load_timeout_timer < 0:
			G.debug_print("LOAD TIMEOUT: " + str(id))
			return
	
	# отправка новому клиенту информации о его команде и месте
	send_set(path, "team", player.team, int(player.name))
	send_set(path, "seat", player.seat, int(player.name))
	# карма при первом спауне
	player.stats[G.GameState.STAT_KARMA] = G.GameState.DEFAULT_KARMA
	send_set(path, "stats", player.stats, int(player.name))
	
	player.money = Rules.NEW_ARRIVAL * clamp(G.GameState.stage, 1, INF)
	send_set(path, "money", player.money, int(player.name))
	
	player.connect("changed_money", self ,"_on_changed_money_player")
	player.connect("changed_credit", self ,"_on_changed_credit_player")
	player.connect("changed_stats", self ,"_on_changed_stats_player")
	player.connect("serv_hitbox", self, "_on_player_hitbox")
	player.connect("serv_changed_position", self, "_on_player_changed_position")
	player.connect("dead", self, "_on_dead_player")
	player.connect("resurrect", self, "_on_resurrect_player")
	player.weapon.connect("serv_projectile", self, "_on_player_projectile")
	player.weapon.connect("serv_changed_wprot", self, "_on_player_changed_wprot")
	player.weapon.connect("serv_changed_wrot", self, "_on_player_changed_wrot")
	player.get_node("weapon/grenade_shooter").connect("serv_grenade_projectile", self, "_on_player_grenade_projectile")
	player.completely_added = true
	
	rules.add_player(id)
	# send all to new player
	
	for property in ["timer", "state", "stage", "score_max"]:
		send_set(G.GameState.get_path(), property, G.GameState.get(property), id)
	send_call(G.GameState.get_path(), "set_score", [G.GameState.score], id)
	
	G.World.points.recalc_points()


func send_big_array(path: String, func_name: String, to_id: int, array: Array, meta_front: Array = [], meta_end: Array = [], transfer = NetworkedMultiplayerPeer.TRANSFER_MODE_RELIABLE):
	var packet = []
	for cell in array:
		if packet.size() < 4096:
			packet.append(cell)
		else:
			send_call(
					path,
					func_name, meta_front + [packet] + meta_end,
					to_id, transfer)
			packet = [cell]
	if packet.size() > 0:
		send_call(
				path,
				func_name, meta_front + [packet] + meta_end,
				to_id, transfer)


func send_map(p_id: int) -> void:
	var pcks = {
			Map.PCK_CELLS_DEFA: G.World.map.get_used_cells_by_id(Map.ID_DEFA),
			Map.PCK_CELLS_BACK: G.World.map.get_used_cells_by_id(Map.ID_BACK),
			Map.PCK_CELLS_HARD: G.World.map.get_used_cells_by_id(Map.ID_HARD),
			Map.PCK_CELLS_FENC: G.World.map.get_used_cells_by_id(Map.ID_FENC),
			Map.PCK_CELLS_LABO: G.World.map.get_used_cells_by_id(Map.ID_LABO),
			Map.PCK_CELLS_MEGU: G.World.map.get_used_cells_by_id(Map.ID_MEGU),
			Map.PCK_HITS:    [G.World.map.hits.keys(), G.World.map.hits.values()]
			}

	for pck_meta in pcks:
		send_big_array(
			G.World.map.get_path(),
			"load_map", p_id,
			pcks[pck_meta], [pck_meta])

	send_call(
		G.World.map.get_path(),
		"load_map", [Map.PCK_SEATS,
		G.World.map.SEATS], p_id)
	
	send_call(
		G.World.map.get_path(),
		"load_map", [Map.PCK_TERMINATE,
		[
			G.World.map.LINE_SIZE,
			G.World.map.C_RANGE,
			G.World.map.X_RANGE,
			G.World.map.Y_RANGE,
			G.World.map.C_RANGE_2,
			G.World.map.X_RANGE_2,
			G.World.map.Y_RANGE_2
		]
		], p_id)


func send_points(p_id: int) -> void:
	for point in G.World.points.get_children():
		send_call(G.World.points.get_path(), "new_point", [point.ID, point.SCN, point.position, point.rotation], p_id)
	for point in G.World.points.points.values():
		if point.state != Points.STATE_NONE:
			send_call(G.World.points.get_path(), "point_event", [point.id, Points.PROP_STATE, point.state], p_id)
			send_call(G.World.points.get_path(), "point_event", [point.id, Points.PROP_TIMER, point.timer], p_id)
			send_call(G.World.points.get_path(), "point_event", [point.id, Points.PROP_HP, point.hp], p_id)

func _on_msg(msg, id, to_all): send_call("/root/Main/ui/", "chat_message", [msg, id, to_all], Net.BROADCAST)
func _on_changed_timer(val): send_set(G.GameState.get_path(), "timer", val, Net.BROADCAST)
func _on_changed_score(val, type_score): send_call(G.GameState.get_path(), "set_score", [val, type_score], Net.BROADCAST)
func _on_changed_score_max(val): send_set(G.GameState.get_path(), "score_max", val, Net.BROADCAST)
func _on_changed_stage(val): send_set(G.GameState.get_path(), "stage", val, Net.BROADCAST)
func _on_changed_state(val): send_set(G.GameState.get_path(), "state", val, Net.BROADCAST)
func _on_feed_event(feed_id, feed_data): send_call(G.GameState.get_path(), "feed_event", [feed_id, feed_data], Net.BROADCAST)
func _on_point_event(point_idx, point_property, data): send_call(G.World.points.get_path(), "point_event", [point_idx, point_property, data], Net.BROADCAST)

func _on_player_changed_wrot(weapon, rot):
	send_call(weapon.get_path(), "integrate_wrot", [rot], Net.BROADCAST, NetworkedMultiplayerPeer.TRANSFER_MODE_UNRELIABLE_ORDERED)
func _on_player_changed_position(player, pos):
	send_call(player.get_path(), "integrate_position", [pos], Net.BROADCAST, NetworkedMultiplayerPeer.TRANSFER_MODE_UNRELIABLE_ORDERED)
func _on_player_grenade_projectile(weapon, data): send_call(weapon.get_path(), "proj_request", [data], Net.BROADCAST)
func _on_player_changed_wprot(pivot, rot): send_set(pivot.get_path(), "rotation_degrees", rot, Net.BROADCAST)
func _on_player_projectile(weapon, data): send_call(weapon.get_path(), "proj_request", [data], Net.BROADCAST)
func _on_changed_money_player(player, val): send_set(player.get_path(), "money", player.money, Net.BROADCAST)
func _on_changed_credit_player(player, val): send_set(player.get_path(), "credit", player.credit, Net.BROADCAST)
func _on_changed_stats_player(player, val): send_set(player.get_path(), "stats", player.stats, Net.BROADCAST)
func _on_dead_player(player, first = false):
	if (player.stats[G.GameState.STAT_KARMA] < 0 and
			is_leading_team(get_player_team(int(player.name)))):
		swap_player_team(int(player.name))
	send_set(player.get_path(), "dead", player.dead, Net.BROADCAST)
	send_call(player.get_path(), "delete_credit_modules", [], int(player.name))
func _on_resurrect_player(player):
	G.GameState.feed_event(GameState.FEED_RESP, [int(player.name)])

func _on_player_hitbox(player, id, hp):
	send_call(player.get_path(), "process_hitbox_from_server", [id, hp], Net.BROADCAST)

func _on_map_hit(point, hp):
	send_call(G.World.map.get_path(), "hit", [point, 0, hp], Net.BROADCAST)

func _on_map_expl(point, radius, owner_id):
	send_call(G.World.map.get_path(), "explosion", [point, radius, 0, owner_id], Net.BROADCAST)

func shutdown_rocket(path: String):
	send_call(path, "client_shutdown", [], Net.BROADCAST) 

func send_module_op(player, m_name, operation_type, data):
	match operation_type:
		GameState.OPERATION_BUY, GameState.OPERATION_CREDIT:
			send_call(player.get_path(), "add_module", 
					[m_name, true, data[0], data[1]], int(player.name))
		GameState.OPERATION_SELL:
			send_call(player.get_path(), "del_module", 
					[m_name, true], int(player.name))


func _on_exit_win(val): 
	send_set(G.GameState.get_path(), "win_team", val, Net.BROADCAST)
	yield(get_tree().create_timer(1.0), "timeout")
	get_tree().quit()


# Получить номер места игрока в команде
func get_seat(id: int) -> int:
	var team = get_player_team(id)
	return teams[team].find(id) + 1


# Удалить игрока из команды
func del_player_from_team(id: int) -> void:
	var team = get_player_team(id)
	var pos = teams[team].find(id) #< null
	teams[team][pos] = null


# Сменить команду игрока на противоположную
func swap_player_team(id: int, force = false): # id == -1 - рандомный игрок, force - убить игрока и возродить за другую команду	
	if id == -1:
		if G.GameState.players_count < 2: return
		id = int(G.World.players.get_children()[randi() %
				G.GameState.players_count].name)
	var p = G.World.players.get_node_or_null(str(id))
	if p == null: return
	if force and p.dead == false:
		send_set(p.get_path(), "dead", true, id)
	var t = get_player_team(id)
	if t == null:
		swap_player_team(-1, true)
		return
	else:
		if get_players_in_team(t).size() == 1: return
		match t:
			G.TEAM_1:
				send_set(p.get_path(), "team", G.TEAM_2, id)
				add_player_to_team(id, G.TEAM_2)
			G.TEAM_2:
				send_set(p.get_path(), "team", G.TEAM_1, id)
				add_player_to_team(id, G.TEAM_1)
	send_set(p.get_path(), "seat", get_seat(id), id)
	
	
	# KARMA RESET
	p.stats[G.GameState.STAT_KARMA] = G.GameState.DEFAULT_KARMA
	send_set(p.get_path(), "stats", p.stats, id)


# Добавить игрока в команду
func add_player_to_team(id: int, team = null) -> int: # принудительно в группу - указать team, иначе бросает в меньшую
	if team:
		var pos
		del_player_from_team(id)
		pos = teams[team].find(null)
		teams[team][pos] = id
		return team
	else:
		var pos
		if get_players_in_team(G.TEAM_2).size() >= get_players_in_team(G.TEAM_1).size():
			pos = teams[G.TEAM_1].find(null)
			teams[G.TEAM_1][pos] = id
			return G.TEAM_1
		else:
			pos = teams[G.TEAM_2].find(null)
			teams[G.TEAM_2][pos] = id
			return G.TEAM_2


func is_leading_team(team: int):
	var opp_team = -1
	match team:
		G.TEAM_1:
			opp_team = G.TEAM_2
		G.TEAM_2:
			opp_team = G.TEAM_1
		_:
			return false
	if G.GameState.score[team] > G.GameState.score[opp_team]:
		return true
	else:
		return false


# Получить команду игрока
func get_player_team(id: int):
	for player in get_players_in_team(G.TEAM_1):
		if player == id:
			return G.TEAM_1
	for player in get_players_in_team(G.TEAM_2):
		if player == id:
			return G.TEAM_2
	return null


# Получить игроков из команды
func get_players_in_team(team: int):
	var result = []
	for player in teams[team]:
		if player != null:
			result.append(player)
	return result
