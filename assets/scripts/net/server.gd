extends Net
class_name Server
# Базовый класс сервера (ничего не знает о игре и игровой логике, только транспорт)

const MAX_RTT		= 0.3	# если пинг игрока > 300 мс
							# в течение PANIC_TIMEOUT секунд он идет нахуй

var max_players: int = DEFAULT_MAX_PLAYERS

func reset_cfg() -> void:
	host = DEFAULT_HOST
	port = DEFAULT_SERVER_PORT
	max_players = DEFAULT_MAX_PLAYERS


func create_server(s_port=port, s_max_players=max_players) -> void:
	G.debug_print("START SERVER ON " + str(host) + ":" + str(port))
	connect("received_echo", self, "received_echo")
	peer.create_server(s_port, s_max_players)
	get_tree().set_network_peer(peer)
	G.Peer = self
	OS.set_window_title("SERVER")


var echo_timer				= 1.0
var echo_timestamp			= 0.0
var connections_rtt			= {}
var connections_timeouts	= {}


func received_echo(id: int, rtt: int, timestamp: float) -> void:
	connections_rtt[id] = pack_timer - timestamp


func echo() -> void:
	for i in cur_connections:
		send_echo(i, NetworkedMultiplayerPeer.TRANSFER_MODE_UNRELIABLE_ORDERED,
				connections_rtt[i])
		connections_rtt.erase(i)
	

var last_pack_timer = 0.0

func _physics_process(delta: float) -> void:
	if cur_connections.size() == 0:
		pack_timer = 0
		return
	pack_timer += delta
	if round(pack_timer) != last_pack_timer:
		send_timer(BROADCAST, NetworkedMultiplayerPeer.TRANSFER_MODE_RELIABLE, pack_timer)
		last_pack_timer = round(pack_timer)
	if echo_timer > 0:
		echo_timer -= delta
		if echo_timer < 0:
			for i in cur_connections:
				if !(connections_rtt.has(i)):
					connections_rtt[i] = MAX_RTT + 0.1
				if !(connections_timeouts.has(i)):
					connections_timeouts[i] = 0
			for i in connections_rtt.keys():
				if !(i in cur_connections):
					connections_rtt.erase(i)
					continue
				if connections_rtt[i] > MAX_RTT:
					connections_timeouts[i] += 1
				else:
					if connections_timeouts[i] > 0:
						connections_timeouts[i] -= 1
				if connections_timeouts[i] > PANIC_TIMEOUT:
					if cur_connections.has(i):
						G.debug_print("[PANIC] " +
							str(i) +
							" DIDN'T REPLY FOR " + str(PANIC_TIMEOUT) + " SECs!")
						peer.disconnect_peer(i, true)
					connections_timeouts.erase(i)
					connections_rtt.erase(i)
			for i in connections_timeouts.keys():
				if !(i in cur_connections):
					connections_timeouts.erase(i)
			echo()
			echo_timer = 1.0
