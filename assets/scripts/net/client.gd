extends Net
class_name Client
# Базовый класс клиента (ничего не знает о игре и игровой логике, только транспорт)


func create_client(host: String, port: int) -> void:
	G.debug_print("START CLIENT")
	connect("received_echo", self, "received_echo")
	peer.connect("server_disconnected", self, "_on_server_disconnected")
	peer.connect("connection_succeeded", self, "_on_connection_succeeded")
	peer.connect("connection_failed", self, "_on_connection_failed")
	peer.create_client(host, port)
	connect("received_timer", self, "_on_received_timer")
	get_tree().set_network_peer(peer)
	G.Peer = self
	
	
	OS.set_window_title("CLIENT:" + str(get_tree().get_network_unique_id()))


const PANIC_INTENSIFIES = 2.0

var RTT	= 0.0
var PP	= 0.0

var panic = 0.0
var ready = false


func _on_received_timer(timer: float) -> void:
	user_timer = timer

func _physics_process(delta: float) -> void:
	if !ready: return
	user_timer += delta
	
	if panic < PANIC_INTENSIFIES and (panic + delta) >= PANIC_INTENSIFIES:
		if G.UI != null:
			G.UI.emit_signal("panic_on")
	
	panic += delta
	
	if panic > PANIC_TIMEOUT:
		print("[PANIC] NO REPLY FROM SERVER FOR " + str(PANIC_TIMEOUT) + " SECs!")
		G.Main.go_to_menu()


func received_echo(id: int, rtt: float, timestamp: float) -> void:
	RTT = rtt
	PP = rtt / 2
	if panic >= PANIC_INTENSIFIES:
		if G.UI != null:
			G.UI.emit_signal("panic_off")
	panic = 0.0


# подключение не удалось
func _on_connection_failed() -> void:
	G.debug_print("Connection failed")
	G.Main.go_to_menu()


# подключение удалось
func _on_connection_succeeded() -> void:
	G.debug_print("Connection succeeded")


# отсоединился сервер
func _on_server_disconnected() -> void:
	G.debug_print("Server disconnected")
	G.Main.go_to_menu()
