extends Node
class_name COLORSCHEME

# Список цветов для всего (TODO - запихать все цвета "из головы" сюда)

const MINIMAP_TILE_DEFA =		Color(0.51, 0.51, 0.51)
const MINIMAP_TILE_BACK =		Color(0.3, 0.3, 0.3)
const MINIMAP_TILE_HARD =		Color(0.81, 0.81, 0.81)
const MINIMAP_TILE_FENC =		Color(0.34, 0.34, 0.3)
const MINIMAP_TILE_LABO =		Color(0.25, 0.25, 0.25)

const MINIMAP_PL_TEAM_1 =		Color(0.9, 0.0, 0.0)
const MINIMAP_PL_TEAM_2 =		Color(0.0, 0.0, 0.9)
const MINIMAP_PL_MY_TEAM_1 =	Color(0.9, 0.45, 0.45)
const MINIMAP_PL_MY_TEAM_2 = 	Color(0.45, 0.45, 0.9)
const MINIMAP_PL_ST_TEAM_1 =	Color(0.9, 0.45, 0.45, 0.25)
const MINIMAP_PL_ST_TEAM_2 =	Color(0.45, 0.45, 0.9, 0.25)

const MINIMAP_SEAT =			Color(0.6, 0.6, 0.6)

const MINIMAP_POINT_ACTIVE =	Color(1.0, 1.0, 1.0)
const MINIMAP_POINT_CAPPED_1 =	Color(1.0, 0.3, 0.3)
const MINIMAP_POINT_CAPPED_2 =	Color(0.3, 0.3, 1.0)
const MINIMAP_POINT_CAP_1 =		Color(1.0, 0.6, 0.6)
const MINIMAP_POINT_CAP_2 =		Color(0.6, 0.6, 1.0)

const MINIMAP_POINT_RADIUS =	2
