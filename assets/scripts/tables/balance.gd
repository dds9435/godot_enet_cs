extends Node
class_name Balance
# Константы для баланса

# player

const PL_MOTION_BIAS				= 50
const PL_GRAVITY					= 2700
const PL_STOPFORCE					= 50
const PL_MIN_SPEED					= 250
const PL_MIN_JUMP					= 600

const PL_MAX_SPEED					= 650
const PL_MAX_JUMP					= 1200
const PL_MAX_JUMPS					= 2
const PL_MAX_JET_FUEL				= 6

const PL_ACCEL						= 10
const PL_JUMP_ACCEL					= 200
const PL_JET						= 1100

const PL_JET_ACTIVATION				= 0.25
const PL_JET_REFUEL_FACT			= 15

const PL_DEATH_EXPLODE_RADIUS		= 3
const PL_DEATH_EXPLODE_DAMAGE		= 5

const PL_HITBOX_DEFAULT_HP			= 10
const PL_HITBOX_DEFAULT_CE_HP		= 3

const PL_HITBOX_BLOW_IMPULSE		= 300
const PL_HITBOX_BLOW_TORQUE			= 1000

# drill

const DRILL_RECOIL					= 0.1

const DRILL_DMG						= 1
const DRILL_BLOCK_DMG				= 4

# explosion

const EXPL_LISTENER_TIMEOUT			= 0.15

const MEGU_RADIUS					= 4
const MEGU_DAMAGE					= 7

# weapons

const W_DEF_AMMO					= 3
const W_DEF_AMMO_SIZE				= 5
const W_DEF_SHOOT_RATE				= 0.5
const W_DEF_RELOAD_RATE				= 0.4
const W_DEF_AFTERSHOOT_SMOKE		= 0.5
const W_DEF_SHOT_COOLDOWN_SPEED		= 48
const W_DEF_GRENADE_ENABLED			= false
const W_DEF_GRENADE_RATE			= 1.0

const FIRE_DEF_VISUAL_WIDTH			= 10
const FIRE_DEF_VISUAL_COLOR			= Color(1,1,1)
const FIRE_DEF_GLOW_SIZE			= 0.025

const FIRE_DEF_PENETRATE			= false
const FIRE_DEF_SHOTS				= 1
const FIRE_DEF_SHOT_DELAY			= 0.05

const LASER_DEF_VISUAL_WIDTH		= 6

const LASER_DEF_SHOOT_RATE			= 0.1
const LASER_DEF_RELOAD_RATE			= 0.5
const LASER_DEF_AMMO_SIZE			= 15
const LASER_DEF_GLOW_SIZE			= 0.0125

const GRENADE_DEF_RADIUS			= 3
const GRENADE_DEF_DAMAGE			= 30
const GRENADE_DEF_IMPULSE			= 1070

const ROCKET_DEF_RELOAD_RATE		= 0.7
const ROCKET_DEF_SHOOT_RATE			= 0.8
const ROCKET_DEF_AMMO_SIZE			= 1
const ROCKET_DEF_IMPULSE			= 1900

# modules

const BARREL_NAME					= "Barrel"
const BARREL_DESC					= "Barrel module"
const BARREL_PRICES					= [250,250,250,250,250]

const CLIP_NAME						= "CLIP"
const CLIP_DESC						= "CLIP module"
const CLIP_PRICES					= [100,100,100,100,100,100,100]

const DASH_NAME						= "DASH"
const DASH_DESC						= "DASH module"
const DASH_PRICES					= [200,200,200,200]

const GRENADE_NAME					= "GRENADE"
const GRENADE_DESC					= "GRENADE module"
const GRENADE_PRICES				= [500]

const LASER_NAME					= "LASER"
const LASER_DESC					= "LASER module"
const LASER_PRICES					= [250,500,500]
const LASER_EXCEPTIONS				= ["rocket", "boost", "shotgun", "snipegun"]

const FULL_NAME						= "FULL"
const FULL_DESC						= "FULL module"
const FULL_PRICES					= [350,350,350,350,350,350]
const FULL_EXCEPTIONS				= ["left_top", "right_top", "right_bottom", "left_bottom"]

const LEFT_BOTTOM_NAME				= "LEFT_BOTTOM"
const LEFT_BOTTOM_DESC				= "LEFT_BOTTOM module"
const LEFT_BOTTOM_PRICES			= [200,200,200,200]
const LEFT_BOTTOM_EXCEPTIONS		= ["full"]

const LEFT_TOP_NAME					= "LEFT_TOP"
const LEFT_TOP_DESC					= "LEFT_TOP module"
const LEFT_TOP_PRICES				= [200,200,200,200]
const LEFT_TOP_EXCEPTIONS			= ["full"]

const RIGHT_BOTTOM_NAME				= "RIGHT_BOTTOM"
const RIGHT_BOTTOM_DESC				= "RIGHT_BOTTOM module"
const RIGHT_BOTTOM_PRICES			= [200,200,200,200]
const RIGHT_BOTTOM_EXCEPTIONS		= ["full"]

const RIGHT_TOP_NAME				= "RIGHT_TOP"
const RIGHT_TOP_DESC				= "RIGHT_TOP module"
const RIGHT_TOP_PRICES				= [200,200,200,200]
const RIGHT_TOP_EXCEPTIONS			= ["full"]

const LEGS_NAME						= "LEGS"
const LEGS_DESC						= "LEGS module"
const LEGS_PRICES					= [150,150,150,150,150]

const RATE_OF_FIRE_NAME				= "RATE_OF_FIRE"
const RATE_OF_FIRE_DESC				= "RATE_OF_FIRE module"
const RATE_OF_FIRE_PRICES			= [260,300,300,300,300,300,300,300]
const RATE_OF_FIRE_EXCEPTIONS		= ["shotgun", "snipegun", "laser", "rocket"]

const RE_BOOST_NAME					= "RE_BOOST"
const RE_BOOST_DESC					= "RE_BOOST module"
const RE_BOOST_PRICES				= [500]

const ROCKET_NAME					= "ROCKET"
const ROCKET_DESC					= "ROCKET module"
const ROCKET_PRICES					= [1000]
const ROCKET_EXCEPTIONS				= ["laser", "second_base", "boost", "shotgun", "snipegun"]

const SCOPE_NAME					= "SCOPE"
const SCOPE_DESC					= "SCOPE module"
const SCOPE_PRICES					= [6050]

const SECOND_BASE_NAME				= "SECOND_BASE"
const SECOND_BASE_DESC				= "SECOND_BASE module"
const SECOND_BASE_PRICES			= [5200]
const SECOND_BASE_EXCEPTIONS		= ["rocket"]

const SHIELD_DOME_NAME				= "SHIELD_DOME"
const SHIELD_DOME_DESC				= "SHIELD_DOME module"
const SHIELD_DOME_PRICES			= [4005]
const SHIELD_DOME_EXCEPTIONS		= ["shield_simple"]

const SHIELD_SIMPLE_NAME			= "SHIELD_SIMPLE"
const SHIELD_SIMPLE_DESC			= "SHIELD_SIMPLE module"
const SHIELD_SIMPLE_PRICES			= [400,400,400,400,400]
const SHIELD_SIMPLE_EXCEPTIONS		= ["shield_dome"]

const SHOTGUN_NAME					= "SHOTGUN"
const SHOTGUN_DESC					= "SHOTGUN module"
const SHOTGUN_PRICES				= [250,500,750]
const SHOTGUN_EXCEPTIONS			= ["rate_of_fire", "snipegun", "laser", "rocket"]

const SNIPEGUN_NAME					= "SNIPEGUN"
const SNIPEGUN_DESC					= "SNIPEGUN module"
const SNIPEGUN_PRICES				= [250,500,750]
const SNIPEGUN_EXCEPTIONS			= ["rate_of_fire", "shotgun", "laser", "rocket"]

const STEALTH_NAME					= "STEALTH"
const STEALTH_DESC					= "STEALTH module"
const STEALTH_PRICES				= [300,300,300]


const FULL_HT_RATE					= 5
const SINGLE_HT_RATE				= 10

const DASH_DASHES_TABLE				= [2,    4,    6,    6  ]
const DASH_DISTANCE_TABLE			= [256,  384,  384,  512 ]
const DASH_RATE_TABLE				= [1.0,  1.0,  1.0,  1.0 ]
const DASH_RELO_TABLE				= [2.0,  3.0,  4.0,  5.0]

const DOME_REGEN_TIMEOUT_TABLE		= [4.0]
const DOME_MAX_HP_TABLE				= [200]

const SMPL_REGEN_TIMEOUT_TABLE		= [4.0]
const SMPL_MAX_HP_TABLE				= [20]

const STEALTH_TIMEOUT_TABLE			= [10.0, 15.0, 20.0]

const LEGS_MAX_SPEED_TABLE			= [750,  800,  850,   900,   950]
const LEGS_MAX_JUMPS_TABLE			= [2,     2,     3,     4,     5]
const LEGS_MAX_JET_FUEL_TABLE		= [9,     12,     15,    18,    21]

const CLIP_TABLE					= [4,  5,  6,  7,  8,  9,  10,  11]

# shotgun/snipegun dmg +
const SHOTGUN_DMG_ADD_TABLE			= [0.4,   0.4,   0.4]
const SNIPEGUN_DMG_ADD_TABLE		= [5,  15,  25]
# laser module dmg +
const LASER_DMG_ADD_TABLE			= [1.0,   1.1,   1.2 ]
const LASER_SHIELD_DAMAGE_MULT		= 1.3
const SNIPEGUN_BLOCK_DAMAGE_MULT	= 4.0

# barrel module table for each weapon (0 index - without barrel (e.g. default damage))
const DMG_TABLE_FIREARM				= [4,     5,     6,     7,    8,    10]
const DMG_TABLE_SHOTGUN				= [0.4,   0.5,   0.6,   0.7,   0.8,   0.9 ]
const DMG_TABLE_SNIPEGUN			= [5,     7,    11,    16,    19,    21]
const DMG_TABLE_LASER				= [0.5,     0.7,    0.9,     1.1,     1.3,     1.5]
const DMG_TABLE_ROCKET				= [45,    50,    60,    70,    80,   90]
const RAD_TABLE_ROCKET				= [5,     6,     7,    8,    9,    10]

const RATE_OF_FIRE_TABLE			= [0.349,  0.258,  0.205,  0.17,  0.145,  0.127,  0.112,  0.1]

const SHOTGUN_SHOOT_RATE_TABLE		= [0.9,   0.9,   0.9  ]
const SHOTGUN_RELOAD_RATE_TABLE		= [0.7,   0.7,   0.7  ]
const SHOTGUN_SHOTS_TABLE			= [10,     20,    30   ]
const SHOTGUN_SHOT_DELAY_TABLE		= [0.001, 0.001, 0.001]
const SHOTGUN_AMMO_SIZE				= 3

const SNIPEGUN_SHOOT_RATE_TABLE		= [1.0,   1.0,   1.0 ]
const SNIPEGUN_RELOAD_RATE_TABLE	= [1.0,   1.0,   1.0 ]
const SNIPEGUN_PENETRATE			= true
const SNIPEGUN_AMMO_SIZE			= 1

const RE_BOOST_RATE					= 0.5
