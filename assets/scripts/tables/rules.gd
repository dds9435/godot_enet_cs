extends Node
class_name Rules
# Правила

# timers
const MATCH_TIME			= 480.0 # sec
const WAIT_TIME				= 15.0 # sec
const FREP_TIME				= 5.0 # sec

# stages
const STAGE_COUNT			= 4
const STAGE_DURATION		= 120.0 # sec
const STAGE_P_WEIGHT		= 4
const MIN_PLAYERS			= 2

# feed
const REWARD_KILL			= 100 # mult by stage
const REWARD_CAP			= 150
const REWARD_PKILL			= 150
const REWARD_DMG_MULT		= 2
const KARMA_POINT			= 0.0075

# score
const MAX_SCORE				= 1000 # mult by stage

# money
const MAX_MONEY				= 1000 # mult by stage
const NEW_ARRIVAL			= 1000 # mult by stage

# points
const POINT_ONE_PLAYER_PERC	= 0.03
const POINT_DEFAULT_HP		= 10.0
const POINT_DEFAULT_TIME	= 3.0
const POINT_CAP_DISTANCE	= 400


var TEST_MODE				= false

func _ready() -> void:
	for arg in OS.get_cmdline_args():
		if arg == "--test":
			TEST_MODE = true
