extends Node2D
# Скрипт мира. Этот скрипт для событий независящих от того сетевой или локальный режим

signal map_client_added()

onready var points       = $"Points"
onready var players      = $"Players"
onready var seats        = $"Seats"
onready var map          = $"Map"
onready var world_camera = $"Camera2D"

const PLAYER = preload("res://assets/scenes/player.tscn")

func _ready() -> void:
	G.World = self
	G.emit_signal("world_added")


# info - информация о игроке (Server Only).
# На клиенте аргумент == null
func add_player(id: int, info = null) -> Object: 
	G.debug_print(['SPAWN: ', id])
	
	var player = PLAYER.instance()
	player.name = str(id)
	
	if info:
		for property in info:
			player.set(property, info[property])

	players.call_deferred("add_child", player)

	return player


func del_player(id: int) -> void:
	G.debug_print(['DESPAWN: ', id])
	# освобождаем объект игрока
	players.get_node(str(id)).queue_free()
	
