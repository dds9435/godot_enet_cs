extends Points
# Логика точек (клиентская часть)

signal map_new_point(idx, pos)
signal map_point_ev(idx, state)
signal points_update(red, blu, state)

const POINTS_PATH =		"res://assets/scenes/points/"

const COLOR_ACTIVE		= Color(1, 1, 1)
const COLOR_CAP_1	= Color(1, 0.75, 0.75)
const COLOR_CAP_2	= Color(0.75, 0.75, 1)
const COLOR_CAPPED_1	= Color(1, 0.5, 0.5)
const COLOR_CAPPED_2	= Color(0.5, 0.5, 1)

var def_scale = Vector2(1,1) * Rules.POINT_CAP_DISTANCE / 512.0

var points_state = 0
var red = 0
var blu = 0
var neutral = 0

func points_update():
	red = 0
	blu = 0
	neutral = 0
	for i in points.values():
		match i.get("fixed_state"):
			STATE_ACTIVE:
				neutral += 1
			STATE_CAPPED_1:
				red += 1
			STATE_CAPPED_2:
				blu += 1
	
	if neutral > 0:
		points_state = MESSAGE_TAKE
	match G.my_player.team:
		G.TEAM_1:
			if blu > red and blu > neutral:
				points_state = MESSAGE_KILL
			elif red > 0 and blu == 0 and neutral == 0:
				points_state = MESSAGE_HOLD
		G.TEAM_2:
			if red > blu and red > neutral:
				points_state = MESSAGE_KILL
			elif blu > 0 and red == 0 and neutral == 0:
				points_state = MESSAGE_HOLD
	
	emit_signal("points_update", red, blu, points_state)



func animate_point(idx: int) -> void:
	var p_anim = get_node(str(idx) + "/anim") as AnimationPlayer
	var hp = points[idx].hp
	var timer = points[idx].timer
	var progress = (((Rules.POINT_DEFAULT_TIME - timer) / Rules.POINT_DEFAULT_TIME) *
					(hp / Rules.POINT_DEFAULT_HP) *
					p_anim.get_animation("process").length)
	
	if p_anim.current_animation != "process":
		p_anim.current_animation = "process"
	p_anim.seek(progress, true)
	p_anim.stop(false)


func color_point(idx: int, color: Color) -> void:
	var p = get_node(str(idx))
	for i in p.get_children():
		if (i is AnimatedSprite or
			i is Sprite):
			for j in i.get_children():
				if (j.name.begins_with("bar") or
					j.name.begins_with("glow")):
					j.modulate = color


func new_point(idx: int, scn: String, pos: Vector2, rot: float) -> void:
	var p = load(POINTS_PATH + scn + ".tscn").instance()
	var pc = Point.new()
	p.rotation = rot
	p.position = pos
	p.name = str(idx)
	p.get_node("glow").scale = def_scale
	pc.id = idx
	add_child(p)
	points[idx] = pc
	var p_anim = p.get_node("anim")
	var p_gl = p.get_node("glow")
	var p_tw = p.get_node("tw")
	p_gl.material.set_shader_param("opacity", 0.2)
	p_tw.remove(p_gl, "modulate")
	p_tw.remove(p_gl, "scale")
	p_tw.interpolate_property(p_gl, "modulate",
		null, Color(1,1,1,0), 0.5,
		Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
	p_tw.interpolate_property(p_gl, "scale",
		def_scale, Vector2(0,0), 0.5,
		Tween.TRANS_CUBIC, Tween.EASE_OUT)
	p_tw.start()
	emit_signal("map_new_point", idx, pos)


func point_event(idx: int, prop: int, data) -> void:
	match prop:
		PROP_TIMER:
			points[idx].timer = data
			animate_point(idx)
		PROP_HP:
			points[idx].hp = data
			animate_point(idx)
		PROP_STATE:
			var p = get_node(str(idx))
			var p_tw = p.get_node("tw") as Tween
			var p_gl = p.get_node("glow") as Sprite
			points[idx].state = data
			
			if data in [STATE_ACTIVE, STATE_CAPPED_1, STATE_CAPPED_2, STATE_NONE]:
				points[idx].fixed_state = data
				points_update()
			
			p_tw.remove(p_gl, "modulate")
			p_tw.remove(p_gl, "scale")
			
			match data:
				STATE_ACTIVE:
					color_point(idx,	COLOR_ACTIVE)
					p_tw.interpolate_property(p_gl, "modulate",
						null,	COLOR_ACTIVE, 0.5,
						Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
					p_tw.interpolate_property(p_gl, "scale",
						null, def_scale, 0.5,
						Tween.TRANS_CUBIC, Tween.EASE_OUT)
				STATE_CAP_1:
					color_point(idx,	COLOR_CAP_1)
					p_tw.interpolate_property(p_gl, "modulate",
						null,	COLOR_CAP_1, 0.3,
						Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
					p_tw.interpolate_property(p_gl, "scale",
						def_scale * 2, def_scale, 0.75,
						Tween.TRANS_CUBIC, Tween.EASE_OUT)
				STATE_CAP_2:
					color_point(idx,	COLOR_CAP_2)
					p_tw.interpolate_property(p_gl, "modulate",
						null,	COLOR_CAP_2, 0.3,
						Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
					p_tw.interpolate_property(p_gl, "scale",
						def_scale * 2, def_scale, 0.75,
						Tween.TRANS_CUBIC, Tween.EASE_OUT)
				STATE_CAPPED_1:
					color_point(idx,	COLOR_CAPPED_1)
					p_tw.interpolate_property(p_gl, "modulate",
						null,	COLOR_CAPPED_1, 0.3,
						Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
					p_tw.interpolate_property(p_gl, "scale",
						def_scale * 2, def_scale, 1.5,
						Tween.TRANS_CUBIC, Tween.EASE_OUT)
				STATE_CAPPED_2:
					color_point(idx,	COLOR_CAPPED_2)
					p_tw.interpolate_property(p_gl, "modulate",
						null,	COLOR_CAPPED_2, 0.3,
						Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
					p_tw.interpolate_property(p_gl, "scale",
						def_scale * 2, def_scale, 1.5,
						Tween.TRANS_CUBIC, Tween.EASE_OUT)
				STATE_NONE:
					p_tw.interpolate_property(p_gl, "modulate",
						null, Color(1,1,1,0), 0.5,
						Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
					p_tw.interpolate_property(p_gl, "scale",
						def_scale, Vector2(0,0), 0.5,
						Tween.TRANS_CUBIC, Tween.EASE_OUT)
			
			p_tw.start()
			emit_signal("map_point_ev", idx, data)
