extends StaticBody2D

var callback = null
var ID = -1
var SCN = ""

var state = -1

func register() -> void:
	if (G.Server != null and
		callback != null):
		callback.register_point(ID, position)


func hit(point, damage, owner_id):
	if (G.Server != null and
		callback != null):
		callback.hit_point(ID, damage, owner_id)
