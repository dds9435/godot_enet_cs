extends Points
# Логика точек (серверная часть)

signal point_ev(point_idx, point_property, data)


func register_point(idx: int, pos: Vector2) -> void:
	var p = Point.new()
	p.id = idx
	p.pos = pos
	points[idx] = p


func recalc_points() -> void:
	var counter = 0
	var needed = clamp(int((G.GameState.players_count / 2) *
						points.size() *
						Rules.POINT_ONE_PLAYER_PERC),
						1, points.size())
	for i in points.values():
		if i.state != STATE_NONE:
			counter += 1
	
	while counter > needed:
		for i in points.values():
			if i.state == STATE_ACTIVE:
				disable_point(i.id)
				counter -= 1
				if counter == needed:
					break
				else:
					continue
			if i.state != STATE_NONE:
				disable_point(i.id)
				counter -= 1
				if counter == needed:
					break
	
	if counter < needed:
		var i
		while true:
			i = points.values()[randi()%points.values().size()]
			if i.state == STATE_NONE:
				enable_point(i.id)
				counter += 1
				if counter == needed:
					break
	


func enable_point(idx: int) -> void:
	var p = points[idx]
	p.hp = Rules.POINT_DEFAULT_HP
	p.timer = Rules.POINT_DEFAULT_TIME
	p.state = STATE_ACTIVE
	emit_signal("point_ev", idx, PROP_HP, p.hp)
	emit_signal("point_ev", idx, PROP_TIMER, p.timer)
	emit_signal("point_ev", idx, PROP_STATE, p.state)


func disable_point(idx: int) -> void:
	var p = points[idx]
	p.hp = 0
	p.timer = 0.0
	p.state = STATE_NONE
	emit_signal("point_ev", idx, PROP_HP, p.hp)
	emit_signal("point_ev", idx, PROP_TIMER, p.timer)
	emit_signal("point_ev", idx, PROP_STATE, p.state)


func hit_point(idx: int, damage: float, owner_id: int) -> void:
	var p = points[idx]
	if (p.state == STATE_NONE or
		p.state == STATE_ACTIVE or
		((p.state == STATE_CAP_1 or p.state == STATE_CAPPED_1) and
		G.World.players.get_node(str(owner_id)).team == G.TEAM_1) or
		((p.state == STATE_CAP_2 or p.state == STATE_CAPPED_2) and
		G.World.players.get_node(str(owner_id)).team == G.TEAM_2)): return
	
	p.hp -= damage
	if p.hp < 0:
		disable_point(idx)
		recalc_points()
		G.GameState.feed_event(GameState.FEED_PKILL, [idx, owner_id])
	emit_signal("point_ev", idx, PROP_HP, p.hp)


func capturers_point(idx: int) -> PoolIntArray:
	var ret = []
	var pt = points[idx]
	for pl in G.World.players.get_children():
		if pt.pos.distance_to(pl.position) < Rules.POINT_CAP_DISTANCE:
			ret.append(int(pl.name))
	
	return ret


func _physics_process(delta: float) -> void:
	for pt in points.values():
		if (pt.state == STATE_NONE or
			pt.state == STATE_CAPPED_1 or
			pt.state == STATE_CAPPED_2): continue
		
		pt.capturers = 0
		
		for pl in G.World.players.get_children():
			if pt.pos.distance_to(pl.position) < Rules.POINT_CAP_DISTANCE:
				if pt.state == STATE_ACTIVE:
					if pl.team == G.TEAM_1:
						pt.state = STATE_CAP_1
					elif pl.team == G.TEAM_2:
						pt.state = STATE_CAP_2
					emit_signal("point_ev", pt.id, PROP_STATE, pt.state)
				
				elif ((pl.team == G.TEAM_1 and pt.state == STATE_CAP_2) or
					  (pl.team == G.TEAM_2 and pt.state == STATE_CAP_1)):
						pt.capturers = 0
						break
				
				pt.capturers += 1
				
				if pt.timer > 0:
					pt.timer -= delta * pt.capturers
					if pt.timer < 0:
						pt.timer = 0.0
						var ct = -1
						if pt.state == STATE_CAP_1:
							pt.state = STATE_CAPPED_1
							ct = G.TEAM_1
						elif pt.state == STATE_CAP_2:
							pt.state = STATE_CAPPED_2
							ct = G.TEAM_2
						emit_signal("point_ev", pt.id,  PROP_STATE, pt.state)
						G.GameState.feed_event(GameState.FEED_CAP,
								[pt.id, capturers_point(pt.id), ct])

						
					emit_signal("point_ev", pt.id, PROP_TIMER, pt.timer)
		
		if pt.capturers == 0:
			
			if pt.state != STATE_ACTIVE:
				pt.state = STATE_ACTIVE
				emit_signal("point_ev", pt.id, PROP_STATE, pt.state)
			
			if pt.timer < Rules.POINT_DEFAULT_TIME:
				pt.timer += delta / 2
				if pt.timer > Rules.POINT_DEFAULT_TIME:
					pt.timer = Rules.POINT_DEFAULT_TIME
				
				emit_signal("point_ev", pt.id, PROP_TIMER, pt.timer)
