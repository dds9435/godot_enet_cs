extends Node2D
class_name Points
# Логика точек (общая)

enum {
	MESSAGE_TAKE,
	MESSAGE_KILL,
	MESSAGE_HOLD
}

enum {
	STATE_NONE,
	STATE_ACTIVE,
	STATE_CAP_1,
	STATE_CAP_2,
	STATE_CAPPED_1,
	STATE_CAPPED_2
}

enum {
	PROP_TIMER,
	PROP_STATE,
	PROP_HP
}

class Point:
	var id: int = -1
	var capturers: int = 0
	var timer: float = 0.0
	var hp: float = 0.0
	var state = STATE_NONE
	var fixed_state = STATE_NONE
	var pos = Vector2()

var points = {}
