extends Node
class_name GEN_TEMPLATES

const TEMPLATES = [
	{
		"C_COUNT"  : "8-16",   # короба ебаные 0
		"C_RANGE"  : "35",
		"C_MIN"    : "25",
		"X_RANGE"  : "10-40",
		"Y_RANGE"  : "150-190",
		"BOX_CHC"  : "1",
	
		"C_COUNT_2"  : "5-10",
		"C_RANGE_2"  : "13",
		"C_MIN_2"  : "8",
		"X_RANGE_2"  : "100-150",
		"Y_RANGE_2"  : "150-190",
		"BOX_CHC_2"  : "1",
	
		"N_BIGM"  : "0.05",
		"N_BIGT"  : "0.15",
		"N_SMLM"  : "0.2",
		"N_SMLT"  : "0.2",
	
		"BRG_TRS"  : "300",
		"BRG_MIN"  : "4",
		"BRG_MAX"  : "80",
	
		"LAB_BMI"  : "4",
		"LAB_BMA"  : "6",
		"LAB_TRS"  : "8-20",
		
		"MEGU_CHC" : "2-8",
		"PINT_CHC" : "4",
	
		"MIN_CELLS"  : "3000",
		"MIN_PDIST" : "8"
	},
	{
		"C_COUNT"  : "4-10",   #1
		"C_RANGE"  : "30",
		"C_MIN"    : "20",
		"X_RANGE"  : "5-80",
		"Y_RANGE"  : "5-50",
		"BOX_CHC"  : "6",

		"C_COUNT_2"  : "10-40",
		"C_RANGE_2"  : "12",
		"C_MIN_2"  : "8",
		"X_RANGE_2"  : "200-270",
		"Y_RANGE_2"  : "5-100",
		"BOX_CHC_2"  : "1",

		"N_BIGM"  : "0.05",
		"N_BIGT"  : "0.15",
		"N_SMLM"  : "0.2",
		"N_SMLT"  : "0.1",

		"BRG_TRS"  : "100",
		"BRG_MIN"  : "6",
		"BRG_MAX"  : "48",

		"LAB_BMI"  : "4",
		"LAB_BMA"  : "6",
		"LAB_TRS"  : "0-3",
		
		"MEGU_CHC" : "2-8",
		"PINT_CHC" : "4",

		"MIN_CELLS"  : "3000",
		"MIN_PDIST" : "8"
	},
	{
		"C_COUNT"  : "4-10",   #2
		"C_RANGE"  : "30",
		"C_MIN"    : "20",
		"X_RANGE"  : "5-80",
		"Y_RANGE"  : "5-50",
		"BOX_CHC"  : "1",

		"C_COUNT_2"  : "15-40",
		"C_RANGE_2"  : "12",
		"C_MIN_2"  : "8",
		"X_RANGE_2"  : "200-270",
		"Y_RANGE_2"  : "40-100",
		"BOX_CHC_2"  : "1",

		"N_BIGM"  : "0.05",
		"N_BIGT"  : "0.15",
		"N_SMLM"  : "0.2",
		"N_SMLT"  : "0.1",

		"BRG_TRS"  : "100",
		"BRG_MIN"  : "6",
		"BRG_MAX"  : "48",

		"LAB_BMI"  : "4",
		"LAB_BMA"  : "6",
		"LAB_TRS"  : "1-2",
		
		"MEGU_CHC" : "2-8",
		"PINT_CHC" : "4",
		
		"MIN_CELLS"  : "3000",
		"MIN_PDIST" : "8"
	},
	{
		"C_COUNT"  : "4-10",    #3
		"C_RANGE"  : "30",
		"C_MIN"    : "20",
		"X_RANGE"  : "5-80",
		"Y_RANGE"  : "5-50",
		"BOX_CHC"  : "1",

		"C_COUNT_2"  : "15-40",
		"C_RANGE_2"  : "12",
		"C_MIN_2"  : "8",
		"X_RANGE_2"  : "190-200",
		"Y_RANGE_2"  : "100-120",
		"BOX_CHC_2"  : "1",

		"N_BIGM"  : "0.03",
		"N_BIGT"  : "0.15",
		"N_SMLM"  : "0.2",
		"N_SMLT"  : "0.2",

		"BRG_TRS"  : "100",
		"BRG_MIN"  : "4",
		"BRG_MAX"  : "48",

		"LAB_BMI"  : "4",
		"LAB_BMA"  : "6",
		"LAB_TRS"  : "1-6",
		
		"MEGU_CHC" : "2-6",
		"PINT_CHC" : "4",
		
		"MIN_CELLS"  : "2000",
		"MIN_PDIST" : "8"
	},
	{
		"C_COUNT"  : "4-10",  #высокий   4
		"C_RANGE"  : "30",
		"C_MIN"    : "20",
		"X_RANGE"  : "5-10",
		"Y_RANGE"  : "5-10",
		"BOX_CHC"  : "1",

		"C_COUNT_2"  : "15-40",
		"C_RANGE_2"  : "12",
		"C_MIN_2"  : "8",
		"X_RANGE_2"  : "90-120",
		"Y_RANGE_2"  : "140-180",
		"BOX_CHC_2"  : "1",

		"N_BIGM"  : "0.03",
		"N_BIGT"  : "0.15",
		"N_SMLM"  : "0.2",
		"N_SMLT"  : "0.2",

		"BRG_TRS"  : "100",
		"BRG_MIN"  : "4",
		"BRG_MAX"  : "48",

		"LAB_BMI"  : "4",
		"LAB_BMA"  : "6",
		"LAB_TRS"  : "2-6",
		
		"MEGU_CHC" : "2-8",
		"PINT_CHC" : "4",
		
		"MIN_CELLS"  : "2000",
		"MIN_PDIST" : "8"
	},
	{
		"C_COUNT"  : "2-5", #высокий с кругом  5
		"C_RANGE"  : "40",
		"C_MIN"    : "30",
		"X_RANGE"  : "5-10",
		"Y_RANGE"  : "5-10",
		"BOX_CHC"  : "1000",

		"C_COUNT_2"  : "15-40",
		"C_RANGE_2"  : "12",
		"C_MIN_2"  : "8",
		"X_RANGE_2"  : "140-180",
		"Y_RANGE_2"  : "140-180",
		"BOX_CHC_2"  : "1",

		"N_BIGM"  : "0.03",
		"N_BIGT"  : "0.15",
		"N_SMLM"  : "0.2",
		"N_SMLT"  : "0.2",

		"BRG_TRS"  : "100",
		"BRG_MIN"  : "4",
		"BRG_MAX"  : "48",

		"LAB_BMI"  : "4",
		"LAB_BMA"  : "6",
		"LAB_TRS"  : "0-6",
		
		"MEGU_CHC" : "2-8",
		"PINT_CHC" : "4",
		
		"MIN_CELLS"  : "2000",
		"MIN_PDIST" : "8"
	},
	{
		"C_COUNT"  : "2-5",    #6
		"C_RANGE"  : "40",
		"C_MIN"    : "30",
		"X_RANGE"  : "5-10",
		"Y_RANGE"  : "5-10",
		"BOX_CHC"  : "1000",

		"C_COUNT_2"  : "25-40",
		"C_RANGE_2"  : "12",
		"C_MIN_2"  : "8",
		"X_RANGE_2"  : "200-250",
		"Y_RANGE_2"  : "90-110",
		"BOX_CHC_2"  : "2",

		"N_BIGM"  : "0.03",
		"N_BIGT"  : "0.15",
		"N_SMLM"  : "0.2",
		"N_SMLT"  : "0.2",

		"BRG_TRS"  : "100",
		"BRG_MIN"  : "4",
		"BRG_MAX"  : "48",

		"LAB_BMI"  : "4",
		"LAB_BMA"  : "6",
		"LAB_TRS"  : "0-6",
		
		"MEGU_CHC" : "2-8",
		"PINT_CHC" : "4",
		
		"MIN_CELLS"  : "2000",
		"MIN_PDIST" : "8"
	},
	{
		"C_COUNT"  : "0", #острова   7
		"C_RANGE"  : "0",
		"C_MIN"    : "0",
		"X_RANGE"  : "0",
		"Y_RANGE"  : "0",
		"BOX_CHC"  : "0",

		"C_COUNT_2"  : "25-60",
		"C_RANGE_2"  : "12",
		"C_MIN_2"  : "8",
		"X_RANGE_2"  : "250-350",
		"Y_RANGE_2"  : "100-150",
		"BOX_CHC_2"  : "2",

		"N_BIGM"  : "0.03",
		"N_BIGT"  : "0.15",
		"N_SMLM"  : "0.2",
		"N_SMLT"  : "0.2",

		"BRG_TRS"  : "300",
		"BRG_MIN"  : "4",
		"BRG_MAX"  : "60",

		"LAB_BMI"  : "4",
		"LAB_BMA"  : "6",
		"LAB_TRS"  : "0-8",
		
		"MEGU_CHC" : "2-8",
		"PINT_CHC" : "4",
		
		"MIN_CELLS"  : "4000",
		"MIN_PDIST" : "8"
	},
	{
		"C_COUNT"  : "1",   #куб в центре    8
		"C_RANGE"  : "40",
		"C_MIN"    : "30",
		"X_RANGE"  : "10-30",
		"Y_RANGE"  : "10-30",
		"BOX_CHC"  : "1",

		"C_COUNT_2"  : "30-40",
		"C_RANGE_2"  : "13",
		"C_MIN_2"  : "6",
		"X_RANGE_2"  : "200-250",
		"Y_RANGE_2"  : "150-170",
		"BOX_CHC_2"  : "10",

		"N_BIGM"  : "0.03",
		"N_BIGT"  : "0.15",
		"N_SMLM"  : "0.2",
		"N_SMLT"  : "0.2",

		"BRG_TRS"  : "300",
		"BRG_MIN"  : "4",
		"BRG_MAX"  : "60",

		"LAB_BMI"  : "4",
		"LAB_BMA"  : "6",
		"LAB_TRS"  : "6-12",
		
		"MEGU_CHC" : "2-8",
		"PINT_CHC" : "4",
		
		"MIN_CELLS"  : "4000",
		"MIN_PDIST" : "8"
	},
	{
		"C_COUNT"  : "4-12",   #шары    9
		"C_RANGE"  : "40",
		"C_MIN"    : "30",
		"X_RANGE"  : "160-270",
		"Y_RANGE"  : "10-60",
		"BOX_CHC"  : "100",

		"C_COUNT_2"  : "2-6",
		"C_RANGE_2"  : "13",
		"C_MIN_2"  : "6",
		"X_RANGE_2"  : "200-250",
		"Y_RANGE_2"  : "150-170",
		"BOX_CHC_2"  : "2",

		"N_BIGM"  : "0.05",
		"N_BIGT"  : "0.15",
		"N_SMLM"  : "0.2",
		"N_SMLT"  : "0.2",

		"BRG_TRS"  : "300",
		"BRG_MIN"  : "4",
		"BRG_MAX"  : "80",

		"LAB_BMI"  : "3",
		"LAB_BMA"  : "6",
		"LAB_TRS"  : "0-3",
		
		"MEGU_CHC" : "2-8",
		"PINT_CHC" : "4",
		
		"MIN_CELLS"  : "4500",
		"MIN_PDIST" : "8"
	},
	{
		"C_COUNT"  : "2-10",   #шары    10
		"C_RANGE"  : "40",
		"C_MIN"    : "30",
		"X_RANGE"  : "160-270",
		"Y_RANGE"  : "10-60",
		"BOX_CHC"  : "1000",

		"C_COUNT_2"  : "4-10",
		"C_RANGE_2"  : "13",
		"C_MIN_2"  : "8",
		"X_RANGE_2"  : "200-250",
		"Y_RANGE_2"  : "150-170",
		"BOX_CHC_2"  : "2",

		"N_BIGM"  : "0.05",
		"N_BIGT"  : "0.15",
		"N_SMLM"  : "0.2",
		"N_SMLT"  : "0.2",

		"BRG_TRS"  : "300",
		"BRG_MIN"  : "4",
		"BRG_MAX"  : "80",

		"LAB_BMI"  : "4",
		"LAB_BMA"  : "6",
		"LAB_TRS"  : "1-3",
		
		"MEGU_CHC" : "2-8",
		"PINT_CHC" : "4",
		
		"MIN_CELLS"  : "5500",
		"MIN_PDIST" : "8"
	},
	{
		"C_COUNT"  : "40-45",   #полоска говна   11
		"C_RANGE"  : "4",
		"C_MIN"    : "2",
		"X_RANGE"  : "350-390",
		"Y_RANGE"  : "40-100",
		"BOX_CHC"  : "1000",

		"C_COUNT_2"  : "60-85",
		"C_RANGE_2"  : "6",
		"C_MIN_2"  : "4",
		"X_RANGE_2"  : "350-390",
		"Y_RANGE_2"  : "15-45",
		"BOX_CHC_2"  : "2",

		"N_BIGM"  : "0.03",
		"N_BIGT"  : "0.15",
		"N_SMLM"  : "0.2",
		"N_SMLT"  : "0.3",

		"BRG_TRS"  : "10-300",
		"BRG_MIN"  : "4",
		"BRG_MAX"  : "80",

		"LAB_BMI"  : "0",
		"LAB_BMA"  : "0",
		"LAB_TRS"  : "0",
		
		"MEGU_CHC" : "2-8",
		"PINT_CHC" : "4",
		
		"MIN_CELLS"  : "500",
		"MIN_PDIST" : "8"
	},
	{
		"C_COUNT"  : "8-20",   #крест 12
		"C_RANGE"  : "13",
		"C_MIN"    : "4",
		"X_RANGE"  : "10-40",
		"Y_RANGE"  : "150-170",
		"BOX_CHC"  : "1000",

		"C_COUNT_2"  : "17-26",
		"C_RANGE_2"  : "13",
		"C_MIN_2"  : "3",
		"X_RANGE_2"  : "350-370",
		"Y_RANGE_2"  : "10-70",
		"BOX_CHC_2"  : "20",

		"N_BIGM"  : "0.07",
		"N_BIGT"  : "0.15",
		"N_SMLM"  : "0.2",
		"N_SMLT"  : "0.3",

		"BRG_TRS"  : "300",
		"BRG_MIN"  : "4",
		"BRG_MAX"  : "80",

		"LAB_BMI"  : "4",
		"LAB_BMA"  : "6",
		"LAB_TRS"  : "1",
		
		"MEGU_CHC" : "2-8",
		"PINT_CHC" : "4",
		
		"MIN_CELLS"  : "2500",
		"MIN_PDIST" : "8"
	},
	{
		"C_COUNT"  : "0",   #мосты   13
		"C_RANGE"  : "0",
		"C_MIN"    : "0",
		"X_RANGE"  : "0",
		"Y_RANGE"  : "0",
		"BOX_CHC"  : "0",

		"C_COUNT_2"  : "55-60",
		"C_RANGE_2"  : "8",
		"C_MIN_2"  : "7",
		"X_RANGE_2"  : "190-260",
		"Y_RANGE_2"  : "190-260",
		"BOX_CHC_2"  : "20",

		"N_BIGM"  : "0.02",
		"N_BIGT"  : "0.15",
		"N_SMLM"  : "0.2",
		"N_SMLT"  : "0.3",

		"BRG_TRS"  : "5000",
		"BRG_MIN"  : "4",
		"BRG_MAX"  : "120",

		"LAB_BMI"  : "0",
		"LAB_BMA"  : "0",
		"LAB_TRS"  : "0",
		
		"MEGU_CHC" : "2-8",
		"PINT_CHC" : "4",
		
		"MIN_CELLS"  : "1500",
		"MIN_PDIST" : "8"
		},
		{
		"C_COUNT"  : "0",   #башня   14  возможно бкдет пробленой, выпилить при надобности
		"C_RANGE"  : "0",
		"C_MIN"    : "0",
		"X_RANGE"  : "0",
		"Y_RANGE"  : "0",
		"BOX_CHC"  : "0",

		"C_COUNT_2"  : "45-75",
		"C_RANGE_2"  : "14",
		"C_MIN_2"  : "6",
		"X_RANGE_2"  : "60-80",
		"Y_RANGE_2"  : "220-225",
		"BOX_CHC_2"  : "30",

		"N_BIGM"  : "0.04",
		"N_BIGT"  : "0.15",
		"N_SMLM"  : "0.2",
		"N_SMLT"  : "0.3",

		"BRG_TRS"  : "600",
		"BRG_MIN"  : "4",
		"BRG_MAX"  : "80",

		"LAB_BMI"  : "4",
		"LAB_BMA"  : "6",
		"LAB_TRS"  : "1-4",
		
		"MEGU_CHC" : "2-8",
		"PINT_CHC" : "4",
		
		"MIN_CELLS"  : "2500",
		"MIN_PDIST" : "8"
		},
		{
		"C_COUNT"  : "10-16",   # башня в островах 15
		"C_RANGE"  : "13",
		"C_MIN"    : "5",
		"X_RANGE"  : "10-30",
		"Y_RANGE"  : "150-170",
		"BOX_CHC"  : "1000",

		"C_COUNT_2"  : "23-34",
		"C_RANGE_2"  : "13",
		"C_MIN_2"  : "3",
		"X_RANGE_2"  : "350-370",
		"Y_RANGE_2"  : "150-170",
		"BOX_CHC_2"  : "20",

		"N_BIGM"  : "0.07",
		"N_BIGT"  : "0.15",
		"N_SMLM"  : "0.2",
		"N_SMLT"  : "0.3",

		"BRG_TRS"  : "300",
		"BRG_MIN"  : "4",
		"BRG_MAX"  : "80",

		"LAB_BMI"  : "4",
		"LAB_BMA"  : "6",
		"LAB_TRS"  : "1",
		
		"MEGU_CHC" : "2-8",
		"PINT_CHC" : "4",
		
		"MIN_CELLS"  : "2500",
		"MIN_PDIST" : "8"
		},
		{
		"C_COUNT"  : "25-75",   #хуева туча мелких островов   16
		"C_RANGE"  : "4",
		"C_MIN"    : "3",
		"X_RANGE"  : "220-250",
		"Y_RANGE"  : "220-225",
		"BOX_CHC"  : "1000",

		"C_COUNT_2"  : "15-20",
		"C_RANGE_2"  : "14",
		"C_MIN_2"  : "6",
		"X_RANGE_2"  : "130-150",
		"Y_RANGE_2"  : "130-150",
		"BOX_CHC_2"  : "2",

		"N_BIGM"  : "0.04",
		"N_BIGT"  : "0.15",
		"N_SMLM"  : "0.2",
		"N_SMLT"  : "0.3",

		"BRG_TRS"  : "10",
		"BRG_MIN"  : "4",
		"BRG_MAX"  : "80",

		"LAB_BMI"  : "4",
		"LAB_BMA"  : "6",
		"LAB_TRS"  : "0",
		
		"MEGU_CHC" : "4-8",
		"PINT_CHC" : "4",
		
		"MIN_CELLS" : "1500",
		"MIN_PDIST" : "8"
	}

]
