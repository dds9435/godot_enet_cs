extends Node
class_name BG_GEN_TEMPLATES

const TEMPLATES = [
	{
		"C_COUNT"  : "2-8",
		"C_RANGE"  : "12",
		"C_MIN"    : "8",
		"X_RANGE"  : "30-60",
		"Y_RANGE"  : "20-40",
		"BOX_CHC"  : "100",
	
		"C_COUNT_2"  : "0",
		"C_RANGE_2"  : "0",
		"C_MIN_2"  : "0",
		"X_RANGE_2"  : "0",
		"Y_RANGE_2"  : "0",
		"BOX_CHC_2"  : "0",
	
		"N_BIGM"  : "0.05",
		"N_BIGT"  : "0.0",
		"N_SMLM"  : "0.2",
		"N_SMLT"  : "0.2",
	
		"BRG_TRS"  : "100",
		"BRG_MIN"  : "8",
		"BRG_MAX"  : "32",
	
		"LAB_BMI"  : "4",
		"LAB_BMA"  : "6",
		"LAB_TRS"  : "8",
		
		"MEGU_CHC" : "1",
		"PINT_CHC" : "1"
	}
]
