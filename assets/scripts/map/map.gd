extends TileMap
class_name Map
# Общий код для классов карты

# Типы пакетов для приема/передачи карты 

enum {
	PCK_CELLS_DEFA, PCK_CELLS_BACK,
	PCK_CELLS_HARD, PCK_CELLS_FENC,
	PCK_CELLS_LABO, PCK_CELLS_MEGU,
	PCK_HITS, PCK_SEATS, PCK_TERMINATE
}

enum {
	ID_DEFA, ID_BACK,
	ID_HARD, ID_FENC,
	ID_LABO, ID_PINT,
	ID_MEGU
}


const DEFAULT_HP = 10


signal hit(pos, hp)
signal expl(pos, radius, owner_id)
signal cell_destroy(pos)

var hits = {}

func explosion(pos, radius, damage, owner_id) -> void:
	var e = preload("res://assets/scenes/expl.tscn").instance()
	e.get_node("visual").radius = radius * cell_size.x
	e.get_node("visual").pos = pos
	e.get_node("listener/col").shape.radius = radius * cell_size.x
	if G.Server != null:
		e.get_node("listener").receiver = self
		e.get_node("listener").damage = damage
		e.get_node("listener").center = pos
		e.get_node("listener").radius = radius * cell_size.x
		e.get_node("listener").owner_id = owner_id
	else:
		var p = world_to_map(pos)
		var _p = Vector2(0,0)
		var r = int(radius / 2)
		var c = -1
		
		var N = 2 * r + 1
		for i in range(N):
			for j in range(N):
				_p.x = i - r
				_p.y = j - r
				if _p.x * _p.x + _p.y * _p.y <= r * r + 1:
					c = get_cellv(_p + p)
					if c >= 6 * 8 or (c >= 3 * 8 and c < 6 * 8):
						set_cellv(_p + p, -1)
						self.cell_proc_ext.append(_p + p)
		
		self.call("process_cells")
		
		for i in get_children():
			if i.position == pos:
				if !i is TileMap:
					i.queue_free()
	call_deferred("add_child", e)
	emit_signal("expl", pos, radius, owner_id)


func explosion_bodies(bodies, damage, center, radius, owner_id) -> void:
	if G.Server != null:
		for i in bodies:
			if is_instance_valid(i):
				if i.has_method("get_covered_bodies"):
					for j in i.get_covered_bodies():
						if (j in bodies) and !(j.has_method("get_covered_bodies")):
							bodies.erase(j)
		for i in bodies:
			if is_instance_valid(i):
				if i.has_method("hit") and !i.has_method("get_hp"):
					i.hit(
						center,
						int( clamp(damage * (1 - 
								center.distance_to(i.global_position) # myrzik ya teb lublu
								/ clamp(radius, 0.1, INF) )
							, 0, INF)),
						owner_id
					)


func get_hp(point: Vector2):
	if G.Server == null: return
	var p = world_to_map(point)
	
	if hits.has(p):
		return hits[p]
	else:
		return DEFAULT_HP


func hit_exact_cell(p, hp) -> void:
	if G.Server != null:
		if get_cellv(p) != ID_DEFA:
			return
		if hp > 0:
			hits[p] = hp
		else:
			set_cellv(p, ID_BACK)
			if hits.has(p):
				hits.erase(p)
		
		emit_signal("hit", p, hp)


var dpart = preload("res://assets/scenes/destruct_particles.tscn")

func hit(point: Vector2, angle: float, damage) -> void:
	if G.Server != null:
		var p_a = [
		world_to_map(point - Vector2(4,0).rotated(angle)),
		world_to_map(point),
		world_to_map(point + Vector2(4,0).rotated(angle))
		]
		var p = null
		for i in p_a:
			if get_cellv(i) == ID_DEFA:
				p = i
				break
		if p == null: return
		
		if hits.has(p):
			hits[p] -= damage
		else:
			hits[p] = DEFAULT_HP - damage
		emit_signal("hit", p, hits[p])
		if hits[p] < 1:
			hits.erase(p)
			set_cellv(p, -1)
		if get_cellv(p) == -1 and hits.has(p):
			hits.erase(p)
	else:
		if get_cellv(point) < 10 * 8: return # < чем линии с деф тайлами HARDCODE
		if damage < 1:
			set_cellv(point, (randi() % 3 + 7) * 8 + randi() % 8) # рандомный тайл из трех возможных линий HARDCODE
			$Destruct.set_cellv(point, -1)
			emit_signal("cell_destroy", point)
		else:
			var tid = int(float(damage) /
					(float(DEFAULT_HP) /
					3)) + randi()%4 * 3
			if ($Destruct.get_cellv(point) != tid and
				!get_cellv(point) == -1):
				$Destruct.set_cellv(point, tid,
						bool(randi()%2),
						bool(randi()%2),
						bool(randi()%2))
