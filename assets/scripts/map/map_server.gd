extends Map

signal generation_end()

var genlib_instance = null

const POINTS_ONEL = [
	"res://assets/scenes/points/1_canister.tscn",
	"res://assets/scenes/points/1_drill.tscn",
	"res://assets/scenes/points/1_inside_drill.tscn"
]
const POINTS_TWOL = [
	"res://assets/scenes/points/2_boer.tscn",
	"res://assets/scenes/points/2_bunker.tscn",
	"res://assets/scenes/points/2_engine.tscn",
	"res://assets/scenes/points/2_fire_point.tscn",
	"res://assets/scenes/points/2_inside_canister.tscn"
]

const SEAT_Y_RANGE	= 1024
const SEAT_X_RANGE	= 1024
const SEAT_X_OFFSET	= 1024
const SEAT_Y_OFFSET	= -1024
const SEAT_M_DIST	= 256

const MAX_TRIES		= 8 # максимальное количество попыток генерации
const LINE_SIZE		= 8 # длина линии в тайлсете

var C_COUNT:	int		# изначальное кол-во кругов
var C_RANGE:	int		# диапазон размера круга (randi() % (C_RANGE - C_MIN) + C_MIN)
var C_MIN:		int		# минимальный размер круга
var X_RANGE:	int		# диапазон расположения кругов по оси X
var Y_RANGE:	int		# диапазон расположения кругов по оси Y
var BOX_CHC:	int		# (1-INF, НЕ РАВНО НУЛЮ) шанс появления коробки а не круга (больше - реже)

var C_COUNT_2:	int		# изначальное кол-во кругов (2 проход) (если == 0, то второго прохода не будет)
var C_RANGE_2:	int		# диапазон размера круга (randi() % (C_RANGE - C_MIN) + C_MIN) (2 проход)
var C_MIN_2:	int		# минимальный размер круга (2 проход)
var X_RANGE_2:	int		# диапазон расположения кругов по оси X (2 проход)
var Y_RANGE_2:	int		# диапазон расположения кругов по оси Y (2 проход)
var BOX_CHC_2:	int		# (1-INF, НЕ РАВНО НУЛЮ) шанс появления коробки а не круга (больше - реже) (2 проход)

var N_BIGM:		float	# множитель большого шума (меньше множитель - крупнее шум)
var N_BIGT:		float	# пороговое значение после которого нужно удалить тайл (-1.0 - 1.0) для крупного шума
var N_SMLM:		float	# множитель мелкого шума (больше множитель - мельче шум)
var N_SMLT:		float	# пороговое значение после которого нужно удалить тайл (-1.0 - 1.0) для мелкого шума
var BRG_TRS:	int		# попытки найти остров и построить от него мост
var BRG_MIN:	int		# минимальная длина моста
var BRG_MAX:	int		# максимальная длина моста

var LAB_BMI:	int		# минимальная кисть для генерации лаб
var LAB_BMA:	int		# максимальная кисть для генерации лаб
var LAB_TRS:	int		# количество лаб

var MEGU_CHC:	int		# шанс на мегумин (после прохождения шанса на точку)
var PINT_CHC:	int		# шанс на точку (шанс на начало детекта пов-ти под точку)

var MIN_CELLS:	int		# минимальное количество заполненых тайлов (если генератор сделал меньше этого числа - генерация идет заново, использовать с осторожностью!)

var MIN_PDIST:	int		# минимальное расстояние между точками (в тайлах)

var BOTTOM: int = 0

var SEATS:		PoolVector2Array

var RNG = RandomNumberGenerator.new()
var map_seed

var gen_success = 0


func init_seed():
	if !map_seed:
		RNG.randomize()
	else:
		RNG.seed = map_seed
	G.debug_print(["MAP SEED:", RNG.seed])


func place_seats():
	var _used_rect = get_used_rect()
	var _used_rect_real = Rect2(map_to_world(_used_rect.position), map_to_world(_used_rect.size))
	
	var y_side = 0
	match RNG.randi() % 3:
		0:
			y_side = _used_rect_real.size.y / 8
		1:
			y_side = _used_rect_real.size.y / 4
		2:
			y_side = _used_rect_real.size.y / 2
	var c_pos = Vector2(0,0)
	var cond = false
	
	for i in range(8):
		while true:
			cond = false
			c_pos = Vector2(-SEAT_X_OFFSET +
				RNG.randi()%(SEAT_X_RANGE) - SEAT_X_RANGE,
				y_side + RNG.randi()%(SEAT_Y_RANGE) - SEAT_Y_RANGE / 2)
			
			for j in SEATS:
				if j.distance_to(c_pos) < SEAT_M_DIST:
					cond = true
			
			if cond:
				continue
			else:
				SEATS.append(c_pos)
				break
	
	for i in range(8):
		SEATS.append(Vector2(_used_rect_real.size.x - SEATS[i].x,
			abs(SEATS[i].y - _used_rect_real.size.y) + SEAT_Y_OFFSET) +
			_used_rect_real.position)
		G.World.seats.get_node("Team_2/Seat_" + str(i + 1)).position = SEATS[SEATS.size() - 1]
		SEATS[i] += Vector2(0, SEAT_Y_OFFSET) + _used_rect_real.position
		G.World.seats.get_node("Team_1/Seat_" + str(i + 1)).position = SEATS[i]

func place_points():
	var ppool = get_used_cells_by_id(5)
	var used_pool = []
	var used_roots = []
	var fail = false
	var start = 0
	var end = 0
	var l = 0
	var r = Vector2()
	var p
	var p_counter = 0
	for i in ppool:
		if used_pool.has(i): continue
		if get_cellv(i + Vector2(0, 1)) in [0, 2]:
			r = i
			end = 0
			start = -1
			while true:
				if ppool.has(i + Vector2(end, 0)):
					used_pool.append(i + Vector2(end, 0))
					end += 1
				else:
					end -= 1
					break
			while true:
				if ppool.has(i + Vector2(start, 0)):
					used_pool.append(i + Vector2(start, 0))
					start -= 1
				else:
					start += 1
					break
			l = end - start
			r = r + Vector2(start, 0)
			fail = false
			for r_p in used_roots:
				if r_p.distance_to(r) < MIN_PDIST:
					fail = true
					break
			if fail: continue
			used_roots.append(r)
			for j in range(start - 1, end + 2):
				set_cellv(r + Vector2(j, 1), ID_HARD)
			if l == 0:
				p = load(POINTS_ONEL[RNG.randi() % POINTS_ONEL.size()]).instance()
				p.position.y = 16
			elif l == 1:
				p = load(POINTS_TWOL[RNG.randi() % POINTS_TWOL.size()]).instance()
				p.position = Vector2(16, 16)
			else: continue
			p.position += map_to_world(r) + cell_size / 2
			p.script = preload("res://assets/scripts/points/point.gd")
			p.ID = p_counter
			p.SCN = p.name
			p.callback = G.World.points
			p.name = str(p_counter)
			G.World.points.add_child(p)
			p_counter += 1
		elif get_cellv(i + Vector2(1, 0)) in [0, 2]:
			r = i
			end = 0
			start = -1
			while true:
				if ppool.has(i + Vector2(0, end)):
					used_pool.append(i + Vector2(0, end))
					end += 1
				else:
					end -= 1
					break
			while true:
				if ppool.has(i + Vector2(0, start)):
					used_pool.append(i + Vector2(0, start))
					start -= 1
				else:
					start += 1
					break
			l = end - start
			r = r + Vector2(0, end)
			fail = false
			for r_p in used_roots:
				if r_p.distance_to(r) < MIN_PDIST:
					fail = true
					break
			if fail: continue
			used_roots.append(r)
			for j in range(start - 1, end + 2):
				set_cellv(r + Vector2(1, j), ID_HARD)
			if l == 0:
				p = load(POINTS_ONEL[RNG.randi() % POINTS_ONEL.size()]).instance()
				p.position.x = 16
			elif l == 1:
				p = load(POINTS_TWOL[RNG.randi() % POINTS_TWOL.size()]).instance()
				p.position = Vector2(16, -16)
			else: continue
			p.rotation_degrees = -90
			p.position += map_to_world(r) + cell_size / 2
			p.script = preload("res://assets/scripts/points/point.gd")
			p.ID = p_counter
			p.SCN = p.name
			p.callback = G.World.points
			p.name = str(p_counter)
			G.World.points.add_child(p)
			p_counter += 1
		elif get_cellv(i - Vector2(1, 0)) in [0, 2]:
			r = i
			end = 0
			start = -1
			while true:
				if ppool.has(i + Vector2(0, end)):
					used_pool.append(i + Vector2(0, end))
					end += 1
				else:
					end -= 1
					break
			while true:
				if ppool.has(i + Vector2(0, start)):
					used_pool.append(i + Vector2(0, start))
					start -= 1
				else:
					start += 1
					break
			l = end - start
			r = r + Vector2(0, start)
			fail = false
			for r_p in used_roots:
				if r_p.distance_to(r) < MIN_PDIST:
					fail = true
					break
			if fail: continue
			used_roots.append(r)
			for j in range(start - 1, end + 2):
				set_cellv(r + Vector2(-1, j), ID_HARD)
			if l == 0:
				p = load(POINTS_ONEL[RNG.randi() % POINTS_ONEL.size()]).instance()
				p.position.x = -16
			elif l == 1:
				p = load(POINTS_TWOL[RNG.randi() % POINTS_TWOL.size()]).instance()
				p.position = Vector2(-16, 16)
			else: continue
			p.rotation_degrees = 90
			p.position += map_to_world(r) + cell_size / 2
			p.script = preload("res://assets/scripts/points/point.gd")
			p.ID = p_counter
			p.SCN = p.name
			p.callback = G.World.points
			p.name = str(p_counter)
			G.World.points.add_child(p)
			p_counter += 1
	var s = 0
	var c = -1
	for i in ppool:
		s = 0
		for j in range(2):
			c = get_cellv(i + Vector2(1 - j * 2, 0))
			if c == ID_BACK or c == ID_LABO:
				s+=1
		for j in range(2):
			c = get_cellv(i + Vector2(0, 1 - j * 2))
			if c == ID_BACK or c == ID_LABO:
				s+=1
		if s > 1:
			set_cellv(i, ID_BACK)

func place_megu():
	var m_counter = 0
	for i in get_used_cells_by_id(ID_MEGU):
		var m = preload("res://assets/scenes/megumin.tscn").instance()
		m.name = "megu_" + str(m_counter)
		m.position = map_to_world(i) + cell_size / 2
		add_child(m)
		m_counter += 1

func select_gen_template():
	var TEMPLATE_N = RNG.randi() % GEN_TEMPLATES.TEMPLATES.size()
	var TEMPLATE = GEN_TEMPLATES.TEMPLATES[TEMPLATE_N] as Dictionary
	var raw
	var cook
	
	for key in TEMPLATE.keys():
		raw = TEMPLATE[key] as String
		if raw.count(".") == 1:
			cook = float(raw)
		elif raw.count("-") == 1:
			raw = raw.split("-")
			cook = RNG.randi() % (int(raw[1]) - int(raw[0])) + int(raw[0])
		else:
			cook = int(raw)
		set(key, cook)
	
	return TEMPLATE_N

func generate():
	gen_success = 0
	if !genlib_instance:
		var genlib = preload("res://assets/lib/gen.gdns")
		print(genlib.library.get_name())
		if genlib.library.get_current_library_path() == "":
			G.debug_print("[ERROR] Can't find a lib for current platform!")
			gen_success = 2
			return
		genlib_instance = genlib.new()
	for i in get_children():
		i.queue_free()
	for i in G.World.points.get_children():
		i.queue_free()
	SEATS = [] as PoolVector2Array
	clear()
	tile_set = load("res://assets/tilemaps/tilemap_server.tres")

	var TEMPLATE = select_gen_template()
	var TRIES = 0
	
	while true:
		genlib_instance.generate(self, map_seed, C_COUNT,
		C_RANGE, C_MIN, X_RANGE, Y_RANGE, BOX_CHC,
		C_COUNT_2, C_RANGE_2, C_MIN_2, X_RANGE_2,
		Y_RANGE_2, BOX_CHC_2, N_BIGM, N_BIGT, N_SMLM,
		N_SMLT, BRG_TRS, BRG_MIN, BRG_MAX, LAB_BMI,
		LAB_BMA, LAB_TRS, MEGU_CHC, PINT_CHC, false)
		if get_used_cells().size() > MIN_CELLS:
			break
		clear()
		TRIES += 1
		if TRIES > MAX_TRIES:
			G.debug_print("[MAP] FAILED TO GENERATE! TEMPLATE: " + str(TEMPLATE))
			emit_signal("generation_end")
			return
	
	place_megu()
	place_points()
	place_seats()
	
	var _used_rect = get_used_rect()
	var _used_rect_real = Rect2(map_to_world(_used_rect.position), map_to_world(_used_rect.size))
	BOTTOM = _used_rect_real.size.y + _used_rect_real.position.y + 4096
	gen_success = 1


func explosion(pos, radius, damage, owner_id):
	var p = world_to_map(pos)
	var _p = Vector2(0,0)
	var r = int(radius)
	
	var N = 2 * r + 1
	for i in range(N):
		for j in range(N):
			_p.x = i - r
			_p.y = j - r
			if _p.x * _p.x + _p.y * _p.y <= r * r + 1:
				hit_exact_cell(_p + p, int(((_p.length() - 1) / r) * 10 - randi() % 5 - 5))
	r /= 2
	N = 2 * r + 1
	for i in range(N):
		for j in range(N):
			_p.x = i - r
			_p.y = j - r
			if _p.x * _p.x + _p.y * _p.y <= r * r + 1:
				if get_cellv(_p + p) in [ID_BACK, ID_LABO, ID_MEGU, ID_FENC]:
					set_cellv(_p + p, -1)
	
	.explosion(pos, radius, damage, owner_id)
