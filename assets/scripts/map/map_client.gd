extends Map
# Обработка загруженной карты (клинтская часть)

signal seat_status(pos, flag)
signal received_map(texture)
signal process_cell(cell, is_bg)
signal start_process_cell()
signal end_process_cell()
signal gen_end(used_rect)

var genlib_instance = null

func _init() -> void:
	get_parent().emit_signal("map_client_added")
	connect("cell_destroy", self, "_on_cell_destroy")
	set_process(true)

func load_map(type, packet):
	match type:
		PCK_CELLS_DEFA:
			for i in packet:
				set_cellv(i, ID_DEFA)
		PCK_CELLS_BACK:
			for i in packet:
				set_cellv(i, ID_BACK)
		PCK_CELLS_HARD:
			for i in packet:
				set_cellv(i, ID_HARD)
		PCK_CELLS_FENC:
			for i in packet:
				set_cellv(i, ID_FENC)
		PCK_CELLS_LABO:
			for i in packet:
				set_cellv(i, ID_LABO)
		PCK_CELLS_MEGU:
			pass
			var m_counter = 0
			for i in packet:
				var m = preload("res://assets/scenes/megumin.tscn").instance()
				m.name = "megu_" + str(m_counter)
				m.position = map_to_world(i) + cell_size / 2
				add_child(m)
				m_counter += 1
		PCK_HITS:
			var dict = {}
			for i in range(packet.front().size()):
				dict[packet[0][i]] = packet[1][i]
			for p in dict:
				var tid = int(float(dict[p]) /
						(float(DEFAULT_HP) /
						3)) + randi()%4 * 3
				if $Destruct.get_cellv(p) != tid:
					$Destruct.set_cellv(p, tid,
							bool(randi()%2),
							bool(randi()%2),
							bool(randi()%2))
		PCK_SEATS:
			for i in range(8):
				G.World.seats.get_node("Team_2/Seat_" + str(i + 1)).position = packet[i + 8]
				G.World.seats.get_node("Team_1/Seat_" + str(i + 1)).position = packet[i]
		PCK_TERMINATE:
			cook_lightmap()
			emit_signal("received_map", cook_minimap(), _used_rect)
			packet.insert(0, self)
			G.genlib_instance.callv("place", packet) # LEAKING
			G.Main.get_node("BG/islands_bg/drawer").generate() # LEAKING
			G.Main.get_node("BG/islands_mg/drawer").generate() # LEAKING
			G.Main.get_node("BG/islands_fg/drawer").generate() # LEAKING
			G.Main.get_node("BG").colorize()
			update_dirty_quadrants()
			emit_signal("gen_end", get_used_rect())

var timer = 0.0
var cell_proc = []
var cell_proc_ext = []

var _lightmap_texture: ImageTexture = null
var _lightmap_image: Image = null
var _used_rect: Rect2
var _used_rect_real: Rect2

func _process(delta: float) -> void:
	if timer > 0:
		timer -= delta
		if timer < 0:
			timer = 0
			process_cells()

func process_cells():
	_lightmap_image.lock()
	emit_signal("start_process_cell")
	for i in cell_proc:
		_lightmap_image.set_pixelv(i - _used_rect.position, Color(0.5, 0.5, 0.5))
		emit_signal("process_cell", i - _used_rect.position, true)
	for i in cell_proc_ext:
		_lightmap_image.set_pixelv(i - _used_rect.position, Color(1, 1, 1, 0))
		emit_signal("process_cell", i - _used_rect.position, false)
	cell_proc.clear()
	_lightmap_image.unlock()
	emit_signal("end_process_cell")
	_lightmap_texture.create_from_image(_lightmap_image, 4)
	$"../LightMap".texture = _lightmap_texture

func _on_cell_destroy(p):
	cell_proc.append(p)
	if timer == 0:
		timer = 0.1

func cook_minimap():
	var _minimap_image = Image.new()
	_minimap_image.create(_used_rect.size.x, _used_rect.size.y, false, Image.FORMAT_RGBA8)
	
	_minimap_image.lock()
	
	var _c = Color()
	var _t = -1
	
	for i in (_used_rect.size.x):
		for j in (_used_rect.size.y):
			_t = get_cellv(Vector2(i, j) + _used_rect.position)
			
			match _t:
				PCK_CELLS_BACK:
					_c = COLORSCHEME.MINIMAP_TILE_BACK
				PCK_CELLS_DEFA:
					_c = COLORSCHEME.MINIMAP_TILE_DEFA
				PCK_CELLS_FENC:
					_c = COLORSCHEME.MINIMAP_TILE_FENC
				PCK_CELLS_HARD:
					_c = COLORSCHEME.MINIMAP_TILE_HARD
				PCK_CELLS_LABO:
					_c = COLORSCHEME.MINIMAP_TILE_LABO
				_:
					_c = Color(0, 0, 0, 0)
			
			_minimap_image.set_pixel(i, j, _c)
		
	_minimap_image.unlock()
	return _minimap_image

func cook_lightmap():
	
	_used_rect = get_used_rect().grow(4)
	_used_rect_real = Rect2(map_to_world(_used_rect.position), map_to_world(_used_rect.size))
	_lightmap_texture = ImageTexture.new()
	_lightmap_image = Image.new()
	_lightmap_image.create(_used_rect.size.x, _used_rect.size.y, false, Image.FORMAT_RGBA8)
	
	_lightmap_image.lock()
	
	var _c = Color()
	var _t = -1
	
	for i in (_used_rect.size.x):
		for j in (_used_rect.size.y):
			_t = get_cellv(Vector2(i, j) + _used_rect.position)
			
			if _t in [0, 2]:
				_c = Color(0, 0, 0)
			elif _t in [1, 4, 5, 6]:
				_c = Color(0.5, 0.5, 0.5)
			else:
				_c = Color(1, 1, 1, 0)
			
			_lightmap_image.set_pixel(i, j, _c)
	
	_lightmap_image.unlock()
	
	_lightmap_texture.create_from_image(_lightmap_image, 4)
	
	$"../LightMap".texture = _lightmap_texture
	$"../LightMap".position = _used_rect_real.position
	$"../LightMap".scale = cell_size
