extends ParallaxBackground


func _ready() -> void:
	get_viewport().connect("size_changed", self, "_on_viewport_size_changed")
	_on_viewport_size_changed()


func colorize() -> void:
	randomize()
	var color = COLORZ.COLORS[randi() % COLORZ.COLORS.size()]
	
	$img.modulate = color
	
	for i in get_children():
		if i.has_node("smoke"):
			i.get_node("smoke").modulate = color.lightened(0.56)
			i.get_node("smoke").modulate.a = 0.6
	var _used_rect = G.World.map.get_used_rect().grow(255)
	var _used_rect_real = Rect2(
			G.World.map.map_to_world(_used_rect.position), 
			G.World.map.map_to_world(_used_rect.size)
	)
	
	$"../FG/smoke".modulate = color.lightened(0.6)
	$"../FG/smoke".modulate.a = 0.4
	$"../FG/smoke".rect_position = _used_rect_real.position
	$"../FG/smoke".rect_size = _used_rect_real.size
	
	G.World.points.modulate = color.lightened(0.34)
	
	G.World.map.modulate = color
	VisualServer.set_default_clear_color(color)


func _on_viewport_size_changed() -> void:
	for i in get_children():
		if i.has_node("smoke"):
			(i.get_node("smoke") as TextureRect).rect_size = get_viewport().size
			(i.get_node("smoke") as TextureRect).rect_position = -get_viewport().size / 2
