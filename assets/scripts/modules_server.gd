extends Node
class_name ModulesServer


const MODULES_PATH = "res://assets/scripts/modules"
var available = []


func _ready() -> void:
	update_available_modules()
	G.ModulesServer = self


func get_module_level(m_name: String, player: Player) -> Array:
	var module = player.get_node_or_null("Modules/" + m_name)
	if module == null:
		module = get_module(m_name)
		if module == null:
			return [0, 0]
		return[0, module.prices.size()]
	else:
		return[module.level, module.prices.size()]


func get_module_price(m_name: String, player: Player, operation_type: int):
	var module = player.get_node_or_null("Modules/" + m_name)
	var price = 0
	var sell  = 0
	if module == null:
		module = get_module(m_name)
		if module == null:
			return -1
		price = module.prices[0]
		sell  = module.prices[0] / 2
	else:
		var level = module.level
		if module.prices.size() < level + 2:
			if operation_type == GameState.OPERATION_SELL and\
										!module.temp_level > 0:
				return module.prices[level - 1] / 2
			return 0
		price = module.prices[level + 1]
		sell  = module.prices[level] / 2
	
	match operation_type:
		GameState.OPERATION_BUY, GameState.OPERATION_CREDIT:
			return price
		GameState.OPERATION_SELL:
			if module.temp_level > 0:
				return 0
			else:
				return sell


func get_module(m_name: String, not_slave: bool = false) -> Node:
	if !available.has(m_name):
		G.debug_print("[Modules Server] Can't find module named " + m_name + "!")
		return null
	var node = Node.new()
	var type = "slave"
	if not_slave: type = "master"
	node.name = m_name
	node.script = load(MODULES_PATH + "/" + m_name + "/" + m_name + "_" + type + ".gd")
	if node.has_method("update_vars"):
		node.update_vars()
	else:
		G.debug_print("[Modules Server] " + m_name + "didn't have vars updater method!'")
	return node


func update_available_modules() -> void:
	var dir = Directory.new()
	if dir.open(MODULES_PATH) == OK:
		dir.list_dir_begin(true, true)
		var file_name = dir.get_next()
		while file_name != "":
			if dir.current_is_dir():
				if check_folder(file_name):
					available.append(file_name)
				elif check_folder(file_name, ".gd"):
					available.append(file_name)
			file_name = dir.get_next()
	else:
		G.debug_print("[Modules Server] can't open " + MODULES_PATH + "!")
		return
	
	if available.size() == 0:
		G.debug_print("[Modules Server] Modules updated, but there is no proper modules!")
		return
	var msg = "[Modules Server] Modules updated, " + str(available.size()) + " modules loaded: "
	for i in available:
		msg += i + ", "
	msg = msg.left(msg.length() - 2)
	G.debug_print(msg)


func check_folder(module_name: String, extension: String = ".gdc") -> bool:
	var dir = Directory.new()
	if dir.open(MODULES_PATH + "/" + module_name) == OK:
		dir.list_dir_begin(true, true)
		var file_name = dir.get_next()
		var required = [module_name + "_slave" + extension, module_name + "_master" + extension]
		while file_name != "":
			if !dir.current_is_dir():
				if required.has(file_name):
					required.erase(file_name)
			file_name = dir.get_next()
		if required.size() == 0:
			return true
		else:
			return false
	else:
		G.debug_print("[Modules Server] can't open " + MODULES_PATH + "/" + module_name + "!")
		return false
