extends Node2D

const genlib = preload("res://assets/lib/gen.gdns")
onready var genlib_instance = genlib.new()

const POINTS_ONEL = [
	"res://assets/scenes/points/1_canister.tscn",
	"res://assets/scenes/points/1_drill.tscn",
	"res://assets/scenes/points/1_inside_drill.tscn"
]
const POINTS_TWOL = [
	"res://assets/scenes/points/2_boer.tscn",
	"res://assets/scenes/points/2_bunker.tscn",
	"res://assets/scenes/points/2_engine.tscn",
	"res://assets/scenes/points/2_fire_point.tscn",
	"res://assets/scenes/points/2_inside_canister.tscn"
]

const SEAT_Y_RANGE	= 1024
const SEAT_X_RANGE	= 1024
const SEAT_X_OFFSET	= 1024
const SEAT_Y_OFFSET	= -1024
const SEAT_M_DIST	= 256

const MAX_TRIES		= 8 # максимальное количество попыток генерации
const LINE_SIZE		= 8 # длина линии в тайлсете

var C_COUNT:	int		# изначальное кол-во кругов
var C_RANGE:	int		# диапазон размера круга (randi() % (C_RANGE - C_MIN) + C_MIN)
var C_MIN:		int		# минимальный размер круга
var X_RANGE:	int		# диапазон расположения кругов по оси X
var Y_RANGE:	int		# диапазон расположения кругов по оси Y
var BOX_CHC:	int		# (1-INF, НЕ РАВНО НУЛЮ) шанс появления коробки а не круга (больше - реже)

var C_COUNT_2:	int		# изначальное кол-во кругов (2 проход) (если == 0, то второго прохода не будет)
var C_RANGE_2:	int		# диапазон размера круга (randi() % (C_RANGE - C_MIN) + C_MIN) (2 проход)
var C_MIN_2:	int		# минимальный размер круга (2 проход)
var X_RANGE_2:	int		# диапазон расположения кругов по оси X (2 проход)
var Y_RANGE_2:	int		# диапазон расположения кругов по оси Y (2 проход)
var BOX_CHC_2:	int		# (1-INF, НЕ РАВНО НУЛЮ) шанс появления коробки а не круга (больше - реже) (2 проход)

var N_BIGM:		float	# множитель большого шума (меньше множитель - крупнее шум)
var N_BIGT:		float	# пороговое значение после которого нужно удалить тайл (-1.0 - 1.0) для крупного шума
var N_SMLM:		float	# множитель мелкого шума (больше множитель - мельче шум)
var N_SMLT:		float	# пороговое значение после которого нужно удалить тайл (-1.0 - 1.0) для мелкого шума
var BRG_TRS:	int		# попытки найти остров и построить от него мост
var BRG_MIN:	int		# минимальная длина моста
var BRG_MAX:	int		# максимальная длина моста

var LAB_BMI:	int		# минимальная кисть для генерации лаб
var LAB_BMA:	int		# максимальная кисть для генерации лаб
var LAB_TRS:	int		# количество лаб

var MEGU_CHC:	int		# шанс на мегумин (после прохождения шанса на точку)
var PINT_CHC:	int		# шанс на точку (шанс на начало детекта пов-ти под точку)

var MIN_CELLS:	int		# минимальное количество заполненых тайлов (если генератор сделал меньше этого числа - генерация идет заново, использовать с осторожностью!)

var MIN_PDIST:	int		# минимальное расстояние между точками (в тайлах)

var SEATS:		PoolVector2Array

enum {
	ID_DEFA, ID_BACK,
	ID_HARD, ID_FENC,
	ID_LABO, ID_PINT,
	ID_MEGU
}

func _ready():
	randomize()
	get_viewport().connect("size_changed", self, "_on_viewport_size_changed")
	generate()
	colorize()
	$BG/islands_bg/drawer.genlib_instance = genlib_instance
	$BG/islands_bg/drawer.generate()
	$BG/islands_mg/drawer.genlib_instance = genlib_instance
	$BG/islands_mg/drawer.generate()
	$BG/islands_fg/drawer.genlib_instance = genlib_instance
	$BG/islands_fg/drawer.generate()
	_on_viewport_size_changed()

var gen_s = false
var zmode = false

func _process(delta: float) -> void:
	
	if Input.is_action_just_pressed("a"):
		$BG/islands_bg/drawer.genlib_instance = genlib_instance
		$BG/islands_bg/drawer.generate()
		$BG/islands_mg/drawer.genlib_instance = genlib_instance
		$BG/islands_mg/drawer.generate()
		$BG/islands_fg/drawer.genlib_instance = genlib_instance
		$BG/islands_fg/drawer.generate()
	
	if Input.is_action_just_pressed("F") and !gen_s:
		generate()
		colorize()
	
	if Input.is_action_just_pressed("e"):
		colorize()
	
	if Input.is_action_just_pressed("lmb"):
		zmode = !zmode
		$Tween.remove($Camera2D, "zoom")
		$Tween.remove($Camera2D, "position")
		if !zmode:
			$Tween.interpolate_property($Camera2D, "zoom", null, Vector2(8,8), 1.0, Tween.TRANS_CUBIC, Tween.EASE_OUT)
			$Tween.interpolate_property($Camera2D, "position", null, Vector2(0,0), 1.0, Tween.TRANS_CUBIC, Tween.EASE_OUT)
		else:
			$Tween.interpolate_property($Camera2D, "zoom", null, Vector2(1, 1), 1.0, Tween.TRANS_CUBIC, Tween.EASE_OUT)
			$Tween.interpolate_property($Camera2D, "position", null, get_global_mouse_position(), 1.0, Tween.TRANS_CUBIC, Tween.EASE_OUT)
		$Tween.start()
	
	if Input.is_action_just_pressed("hide_ui"):
		$CanvasLayer/Info.visible = !$CanvasLayer/Info.visible
	
	if Input.is_action_just_pressed("s"):
		OS.window_fullscreen = !OS.window_fullscreen
	
	if zmode:
		$Camera2D.position = $Camera2D.position.move_toward(get_global_mouse_position(), $Camera2D.position.distance_to(get_global_mouse_position()) / 250)

func place_seats():
	var _used_rect = $TileMap.get_used_rect()
	var _used_rect_real = Rect2($TileMap.map_to_world(_used_rect.position), $TileMap.map_to_world(_used_rect.size))
	
	var y_side = 0
	match randi() % 3:
		0:
			y_side = _used_rect_real.size.y / 8
		1:
			y_side = _used_rect_real.size.y / 4
		2:
			y_side = _used_rect_real.size.y / 2
	var c_pos = Vector2(0,0)
	var cond = false
	
	for i in range(8):
		while true:
			cond = false
			c_pos = Vector2(-SEAT_X_OFFSET +
				randi()%(SEAT_X_RANGE) - SEAT_X_RANGE,
				y_side + randi()%(SEAT_Y_RANGE) - SEAT_Y_RANGE / 2)
			
			for j in SEATS:
				if j.distance_to(c_pos) < SEAT_M_DIST:
					cond = true
			
			if cond:
				continue
			else:
				SEATS.append(c_pos)
				break
	
	for i in range(8):
		SEATS.append(Vector2(_used_rect_real.size.x - SEATS[i].x,
			abs(SEATS[i].y - _used_rect_real.size.y) + SEAT_Y_OFFSET) +
			_used_rect_real.position)
		SEATS[i] += Vector2(0, SEAT_Y_OFFSET) + _used_rect_real.position
	
	update()

func _draw() -> void:
	pass
	#for i in SEATS:
	#	draw_circle(i, 32, Color.red)

func place_points():
	var ppool = $TileMap.get_used_cells_by_id(5)
	var used_pool = []
	var used_roots = []
	var fail = false
	var start = 0
	var end = 0
	var l = 0
	var r = Vector2()
	var p
	var p_counter = 0
	for i in ppool:
		if used_pool.has(i): continue
		if $TileMap.get_cellv(i + Vector2(0, 1)) in [0, 2]:
			r = i
			end = 0
			start = -1
			while true:
				if ppool.has(i + Vector2(end, 0)):
					used_pool.append(i + Vector2(end, 0))
					end += 1
				else:
					end -= 1
					break
			while true:
				if ppool.has(i + Vector2(start, 0)):
					used_pool.append(i + Vector2(start, 0))
					start -= 1
				else:
					start += 1
					break
			l = end - start
			r = r + Vector2(start, 0)
			fail = false
			for r_p in used_roots:
				if r_p.distance_to(r) < MIN_PDIST:
					fail = true
					break
			if fail: continue
			used_roots.append(r)
			for j in range(start - 1, end + 2):
				$TileMap.set_cellv(r + Vector2(j, 1), ID_HARD)
			if l == 0:
				p = load(POINTS_ONEL[randi() % POINTS_ONEL.size()]).instance()
				p.position.y = 16
			elif l == 1:
				p = load(POINTS_TWOL[randi() % POINTS_TWOL.size()]).instance()
				p.position = Vector2(16, 16)
			else: continue
			p.position += $TileMap.map_to_world(r) + $TileMap.cell_size / 2
			p.name = str(p_counter)
			$Points.add_child(p)
			p_counter += 1
		elif $TileMap.get_cellv(i + Vector2(1, 0)) in [0, 2]:
			r = i
			end = 0
			start = -1
			while true:
				if ppool.has(i + Vector2(0, end)):
					used_pool.append(i + Vector2(0, end))
					end += 1
				else:
					end -= 1
					break
			while true:
				if ppool.has(i + Vector2(0, start)):
					used_pool.append(i + Vector2(0, start))
					start -= 1
				else:
					start += 1
					break
			l = end - start
			r = r + Vector2(0, end)
			fail = false
			for r_p in used_roots:
				if r_p.distance_to(r) < MIN_PDIST:
					fail = true
					break
			if fail: continue
			used_roots.append(r)
			for j in range(start - 1, end + 2):
				$TileMap.set_cellv(r + Vector2(1, j), ID_HARD)
			if l == 0:
				p = load(POINTS_ONEL[randi() % POINTS_ONEL.size()]).instance()
				p.position.x = 16
			elif l == 1:
				p = load(POINTS_TWOL[randi() % POINTS_TWOL.size()]).instance()
				p.position = Vector2(16, -16)
			else: continue
			p.rotation_degrees = -90
			p.position += $TileMap.map_to_world(r) + $TileMap.cell_size / 2
			p.name = str(p_counter)
			$Points.add_child(p)
			p_counter += 1
		elif $TileMap.get_cellv(i - Vector2(1, 0)) in [0, 2]:
			r = i
			end = 0
			start = -1
			while true:
				if ppool.has(i + Vector2(0, end)):
					used_pool.append(i + Vector2(0, end))
					end += 1
				else:
					end -= 1
					break
			while true:
				if ppool.has(i + Vector2(0, start)):
					used_pool.append(i + Vector2(0, start))
					start -= 1
				else:
					start += 1
					break
			l = end - start
			r = r + Vector2(0, start)
			fail = false
			for r_p in used_roots:
				if r_p.distance_to(r) < MIN_PDIST:
					fail = true
					break
			if fail: continue
			used_roots.append(r)
			for j in range(start - 1, end + 2):
				$TileMap.set_cellv(r + Vector2(-1, j), ID_HARD)
			if l == 0:
				p = load(POINTS_ONEL[randi() % POINTS_ONEL.size()]).instance()
				p.position.x = -16
			elif l == 1:
				p = load(POINTS_TWOL[randi() % POINTS_TWOL.size()]).instance()
				p.position = Vector2(-16, 16)
			else: continue
			p.rotation_degrees = 90
			p.position += $TileMap.map_to_world(r) + $TileMap.cell_size / 2
			p.name = str(p_counter)
			$Points.add_child(p)
			p_counter += 1
	var s = 0
	var c = -1
	for i in ppool:
		s = 0
		for j in range(2):
			c = $TileMap.get_cellv(i + Vector2(1 - j * 2, 0))
			if c == ID_BACK or c == ID_LABO:
				s+=1
		for j in range(2):
			c = $TileMap.get_cellv(i + Vector2(0, 1 - j * 2))
			if c == ID_BACK or c == ID_LABO:
				s+=1
		if s > 1:
			$TileMap.set_cellv(i, ID_BACK)
		else:
			$TileMap.set_cellv(i, -1)


func place_megu():
	var m_counter = 0
	for i in $TileMap.get_used_cells_by_id(6):
		var m = preload("res://assets/scenes/megumin.tscn").instance()
		m.name = "megu_" + str(m_counter)
		m.position = $TileMap.map_to_world(i) + $TileMap.cell_size / 2
		$TileMap.add_child(m)
		m_counter += 1

func select_gen_template():
	var TEMPLATE_N = randi() % GEN_TEMPLATES.TEMPLATES.size()
	var TEMPLATE = GEN_TEMPLATES.TEMPLATES[TEMPLATE_N] as Dictionary
	var raw
	var cook
	
	for key in TEMPLATE.keys():
		raw = TEMPLATE[key] as String
		if raw.count(".") == 1:
			cook = float(raw)
		elif raw.count("-") == 1:
			raw = raw.split("-")
			cook = randi() % (int(raw[1]) - int(raw[0])) + int(raw[0])
		else:
			cook = int(raw)
		set(key, cook)
	
	return TEMPLATE_N

func generate():
	gen_s = true
	for i in $TileMap.get_children():
		i.queue_free()
	for i in $Points.get_children():
		i.queue_free()
	SEATS = [] as PoolVector2Array
	$TileMap.clear()
	$TileMap.tile_set = load("res://assets/tilemaps/tilemap_server.tres")

	var TEMPLATE = select_gen_template()
	var TRIES = 0
	
	var t = OS.get_system_time_msecs()
	while true:
		genlib_instance.generate($TileMap, randi(), C_COUNT,
		C_RANGE, C_MIN, X_RANGE, Y_RANGE, BOX_CHC,
		C_COUNT_2, C_RANGE_2, C_MIN_2, X_RANGE_2,
		Y_RANGE_2, BOX_CHC_2, N_BIGM, N_BIGT, N_SMLM,
		N_SMLT, BRG_TRS, BRG_MIN, BRG_MAX, LAB_BMI,
		LAB_BMA, LAB_TRS, MEGU_CHC, PINT_CHC, false)
		if $TileMap.get_used_cells().size() > MIN_CELLS:
			break
		$TileMap.clear()
		TRIES += 1
		if TRIES > MAX_TRIES:
			$CanvasLayer/Info.text = ("FAILED TO GENERATE!\n\nTEMPLATE : %d\nUSED_CELLS : %d\nMIN_CELLS : %d\nTRIES : %d\n\nC_COUNT : %d\nC_RANGE : %d\nC_MIN : %d\nX_RANGE : %d\nY_RANGE : %d\nBOX_CHC : %d\n\nC_COUNT_2 : %d\nC_RANGE_2 : %d\nC_MIN_2 : %d\nX_RANGE_2 : %d\nY_RANGE_2 : %d\nBOX_CHC_2 : %d\n\nN_BIGM : %f\nN_BIGT : %f\nN_SMLM : %f\nN_SMLT : %f\n\nBRG_TRS : %d\nBRG_MIN : %d\nBRG_MAX : %d\n\nLAB_BMI : %d\nLAB_BMA : %d\nLAB_TRS : %d\n\nGEN TIME : %d" %
			[TEMPLATE, $TileMap.get_used_cells().size(), MIN_CELLS, TRIES, C_COUNT, C_RANGE, C_MIN, X_RANGE, Y_RANGE, BOX_CHC, C_COUNT_2, C_RANGE_2, C_MIN_2, X_RANGE_2, Y_RANGE_2, BOX_CHC_2, N_BIGM, N_BIGT, N_SMLM, N_SMLT, BRG_TRS, BRG_MIN, BRG_MAX, LAB_BMI, LAB_BMA, LAB_TRS, OS.get_system_time_msecs() - t])
			gen_s = false
			return
	
	cook_lightmap()
	place_megu()
	place_points()
	place_seats()
	
	$TileMap.tile_set = load("res://assets/tilemaps/tilemap_client.tres")
	genlib_instance.place($TileMap, LINE_SIZE, C_RANGE, X_RANGE, Y_RANGE, C_RANGE_2, X_RANGE_2, Y_RANGE_2)
	
	$CanvasLayer/Info.text = ("TEMPLATE : %d\nUSED_CELLS : %d\nMIN_CELLS : %d\nTRIES : %d\n\nC_COUNT : %d\nC_RANGE : %d\nC_MIN : %d\nX_RANGE : %d\nY_RANGE : %d\nBOX_CHC : %d\n\nC_COUNT_2 : %d\nC_RANGE_2 : %d\nC_MIN_2 : %d\nX_RANGE_2 : %d\nY_RANGE_2 : %d\nBOX_CHC_2 : %d\n\nN_BIGM : %f\nN_BIGT : %f\nN_SMLM : %f\nN_SMLT : %f\n\nBRG_TRS : %d\nBRG_MIN : %d\nBRG_MAX : %d\n\nLAB_BMI : %d\nLAB_BMA : %d\nLAB_TRS : %d\n\nGEN TIME : %d\n\nMEGU_CHC : %d\nPINT_CHC : %d\n\nMIN_PDIST : %d" %
	[TEMPLATE, $TileMap.get_used_cells().size(), MIN_CELLS, TRIES, C_COUNT, C_RANGE, C_MIN, X_RANGE, Y_RANGE, BOX_CHC, C_COUNT_2, C_RANGE_2, C_MIN_2, X_RANGE_2, Y_RANGE_2, BOX_CHC_2, N_BIGM, N_BIGT, N_SMLM, N_SMLT, BRG_TRS, BRG_MIN, BRG_MAX, LAB_BMI, LAB_BMA, LAB_TRS, OS.get_system_time_msecs() - t, MEGU_CHC, PINT_CHC, MIN_PDIST])
	
	gen_s = false

func cook_lightmap():
	
	var _used_rect = $TileMap.get_used_rect()
	var _used_rect_real = Rect2($TileMap.map_to_world(_used_rect.position), $TileMap.map_to_world(_used_rect.size))
	var _lightmap_texture = ImageTexture.new()
	var _lightmap_image = Image.new()
	_lightmap_image.create(_used_rect.size.x, _used_rect.size.y, false, Image.FORMAT_RGBA8)
	
	_lightmap_image.lock()
	
	var _c = Color()
	var _t = -1
	
	for i in (_used_rect.size.x):
		for j in (_used_rect.size.y):
			_t = $TileMap.get_cellv(Vector2(i, j) + _used_rect.position)
			
			if _t in [0, 2]:
				_c = Color(0, 0, 0)
			elif _t in [1, 4, 5, 6]:
				_c = Color(0.5, 0.5, 0.5)
			else:
				_c = Color(1, 1, 1, 0)
			
			_lightmap_image.set_pixel(i, j, _c)
	
	_lightmap_image.unlock()
	
	_lightmap_texture.create_from_image(_lightmap_image, 4)
	
	$LightMap.texture = _lightmap_texture
	$LightMap.position = _used_rect_real.position
	$LightMap.scale = $TileMap.cell_size

func colorize():
	randomize()
	var color = COLORZ.COLORS[randi()%COLORZ.COLORS.size()]
	
	$BG/img.modulate = color
	
	for i in $BG.get_children():
		if i.has_node("smoke"):
			i.get_node("smoke").modulate = color.lightened(0.56)
			i.get_node("smoke").modulate.a = 0.6
	var _used_rect = $TileMap.get_used_rect().grow(255)
	var _used_rect_real = Rect2($TileMap.map_to_world(_used_rect.position), $TileMap.map_to_world(_used_rect.size))
	
	$"FG/tex".modulate = color.lightened(0.6)
	$"FG/tex".modulate.a = 0.4
	$"FG/tex".rect_position = _used_rect_real.position
	$"FG/tex".rect_size = _used_rect_real.size
	
	$Points.modulate = color.lightened(0.34)
	
	$TileMap.modulate = color
	VisualServer.set_default_clear_color(color)

func _on_viewport_size_changed():
	for i in $BG.get_children():
		if i.has_node("smoke"):
			(i.get_node("smoke") as TextureRect).rect_size = get_viewport().size
			(i.get_node("smoke") as TextureRect).rect_position = - get_viewport().size / 2
