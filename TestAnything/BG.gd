extends Node2D


export var BASE_CIRCLES_COUNT				= 20 # количество базовых кругов
export var BASE_CIRCLE_X_RANGE				= 80 # диапазон для основной точки круга по X
export var BASE_CIRCLE_Y_RANGE				= 10 # диапазон для основной точки круга по Y
export var BASE_CIRCLE_RADIUS_MIN			= 20 # минимальный радиус круга
export var BASE_CIRCLE_RADIUS_RANGE			= 10 # диапазон радиуса круга (суммируется с минимальным)
export var BASE_CIRCLE_DEF_RANGE			= 20 # диапазон переменной, отвечающей за деформацию круга ("ромбирование")

const NOISE_BIG_OCTAVES						= 5 # октавы основного шума
const NOISE_BIG_PERIOD_MIN					= 30 # минимальный период основного шума
const NOISE_BIG_PERIOD_RANGE				= 15 # диапазон периода основного шума (суммируется с минимальным)
const NOISE_BIG_THRESHOLD					= 0.01 # порог значения основного шума для удаления тайлов


const MIN_DISTANCE							= 128

export var size = Vector2(1280, 720)
export var count = 8

export var solid_color = Color(0,0,0,1)


export var MAX_TIMEOUT = 10
var timer = 0.0
var timeout = 0.0


var RNG = RandomNumberGenerator.new()
var noise = OpenSimplexNoise.new()
var textures = {}
var explosions = {}

var img_size = Vector2(BASE_CIRCLE_X_RANGE + (BASE_CIRCLE_RADIUS_MIN + BASE_CIRCLE_RADIUS_RANGE) * 2,
	BASE_CIRCLE_Y_RANGE + (BASE_CIRCLE_RADIUS_MIN + BASE_CIRCLE_RADIUS_RANGE) * 2)


func explosion():
	var tex_key = textures.keys()[randi()%textures.keys().size()]
	var tex = (textures[tex_key] as ImageTexture)
	var img = tex.get_data()
	
	var pos = (Vector2(clamp(randi()%int(img_size.x), 20, img_size.x - 20),
			clamp(randi()%int(img_size.y), 20, img_size.y - 20)))
	var radius = randi()%15 + 5
	
	img.lock()
	if img.get_pixel(pos.x, pos.y).is_equal_approx(solid_color):
		explosions[pos + tex_key] = radius
		circle(img, pos.x, pos.y, radius)
	else:
		img.unlock()
		return
	img.unlock()
	tex.set_data(img)
	
	update()
	yield(get_tree().create_timer(0.1), "timeout")
	explosions.erase(pos + tex_key)
	update()
	


func _ready() -> void:
	set_process(false)
	RNG.randomize()
	noise.seed = RNG.randi()
	noise.octaves = NOISE_BIG_OCTAVES
	noise.period = NOISE_BIG_PERIOD_MIN + (RNG.randf() - 0.5) * NOISE_BIG_PERIOD_RANGE
	var cur_pos = Vector2(RNG.randi()% int(size.x) - img_size.x, RNG.randi() % int(size.y) - img_size.y)
	var posintions = []
	for i in range(count):
		textures[cur_pos] = cook_island()
		posintions.append(cur_pos)
		while true:
			cur_pos = Vector2(RNG.randi()% int(size.x) - img_size.x, RNG.randi()% int(size.y) - img_size.y)
			var fail = false
			for i in posintions:
				if i.distance_to(cur_pos) < MIN_DISTANCE:
					fail = true
					break
			if !fail:
				break
	update()
	set_process(true)

func _process(delta: float) -> void:
	timer -= delta
	if timer < 0:
		timer = randi()%MAX_TIMEOUT + 0.1
		explosion()

func cook_island() -> ImageTexture:
	var imgtex = ImageTexture.new()
	var img = Image.new()
	var noise_offset = Vector2(RNG.randi()%1024, RNG.randi()%1024)
	img.create(
		img_size.x, img_size.y,
		false, Image.FORMAT_RGBA8)
	
	img.lock()
	for i in range(BASE_CIRCLES_COUNT):
		circle(img,
			RNG.randi() % BASE_CIRCLE_X_RANGE + BASE_CIRCLE_RADIUS_MIN + BASE_CIRCLE_RADIUS_RANGE,
			RNG.randi() % BASE_CIRCLE_Y_RANGE + BASE_CIRCLE_RADIUS_MIN + BASE_CIRCLE_RADIUS_RANGE,
			BASE_CIRCLE_RADIUS_MIN
			+RNG.randi() % BASE_CIRCLE_RADIUS_RANGE-BASE_CIRCLE_RADIUS_RANGE / 2,
			true, true,
			RNG.randi() % BASE_CIRCLE_DEF_RANGE
		)
	
	for i in range(img_size.x):
		for j in range(img_size.y):
			if img.get_pixel(i, j) == Color(0,0,0,0):
				continue
			if noise.get_noise_2d(i + noise_offset.x, j + noise_offset.y) > NOISE_BIG_THRESHOLD:
				img.set_pixel(i, j, Color(0,0,0,0))
		
	
	img.unlock()
	imgtex.create_from_image(img)
	
	return imgtex
	
func circle(img : Image, x0, y0, radius, fill = false, full_circle=true, d = 1):
	var x = radius - 1
	var y = 0
	var dx = d
	var dy = d
	var err = dx - (radius << 1)

	while x >= y:
		
		for i in range(x):
			if fill:
				img.set_pixel(x0 + i, y0 + y, solid_color)
				img.set_pixel(x0 + y, y0 + i, solid_color)
				img.set_pixel(x0 - y, y0 + i, solid_color)
				img.set_pixel(x0 - i, y0 + y, solid_color)
				if full_circle:
					img.set_pixel(x0 - i, y0 - y, solid_color)
					img.set_pixel(x0 - y, y0 - i, solid_color)
					img.set_pixel(x0 + y, y0 - i, solid_color)
					img.set_pixel(x0 + i, y0 - y, solid_color)
			else:
				img.set_pixel(x0 + i, y0 + y, Color(0,0,0,0))
				img.set_pixel(x0 + y, y0 + i, Color(0,0,0,0))
				img.set_pixel(x0 - y, y0 + i, Color(0,0,0,0))
				img.set_pixel(x0 - i, y0 + y, Color(0,0,0,0))
				if full_circle:
					img.set_pixel(x0 - i, y0 - y, Color(0,0,0,0))
					img.set_pixel(x0 - y, y0 - i, Color(0,0,0,0))
					img.set_pixel(x0 + y, y0 - i, Color(0,0,0,0))
					img.set_pixel(x0 + i, y0 - y, Color(0,0,0,0))
		
		if err <= 0:
			y += 1
			err += dy
			dy += 2

		if (err > 0):
			x -= 1
			dx += 2
			err += dx - (radius << 1)

func _draw() -> void:
	for i in textures.keys():
		draw_texture(textures[i], i)
	for i in explosions.keys():
		draw_circle(i, explosions[i], Color(1,1,1))
